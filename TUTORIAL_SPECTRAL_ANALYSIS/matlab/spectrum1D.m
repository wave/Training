%NCid1=netcdf.open('Surfaces_for_spectra.nc','NC_NOWRITE');
%vartime = netcdf.inqVarID(NCid1,'time');
%varx = netcdf.inqVarID(NCid1,'X');
%x_scale=netcdf.getAtt(NCid1,varx,'scale_factor');
%x_off=netcdf.getAtt(NCid1,varx,'add_offset');
%vary = netcdf.inqVarID(NCid1,'Y');
%y_scale=netcdf.getAtt(NCid1,vary,'scale_factor');
%y_off=netcdf.getAtt(NCid1,vary,'add_offset');
%varz = netcdf.inqVarID(NCid1,'Z');
%z_scale=netcdf.getAtt(NCid1,varz,'scale_factor');
%x=double(netcdf.getVar(NCid1,varx)).*x_scale+x_off;
%y=double(netcdf.getVar(NCid1,vary)).*y_scale+y_off;
%time=netcdf.getVar(NCid1,vartime);
%zall=netcdf.getVar(NCid1,varz);
%times=(time-time(1))*86400;
%elevation1=double(zall(40,40,:)).*z_scale;
%elevationb=squeeze(mean(mean(double(zall(39:41,39:41,:)).*z_scale,2),1));
%nx=size(x,1);
%ny=size(x,2);
%nt=size(time,1);
%save katsiveli_data_sample times elevation1 nt
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


load ../data/katsiveli_data_sample   % reads previous extracted time series
elevation=squeeze(elevation1);

nmes=length(elevation); % Takes full time series 
%nmes=1000;              % only take the first 1000 points


figure(1) % first plot : time series 
clf
set(gca,'FontSize',16,'LineWidth',1);
plot(times(1:nmes),elevation(1:nmes),'k-','LineWidth',2);
xlabel('time (s)');
ylabel('surface elevation (m)');
%axis([0 times(nmes) -0.5 0.6])

elevation1=elevation;
% Commented line below is 3-point boxcar average
% elevation(2:nt-1)=1./3.*((elevation1(3:nt)+elevation1(1:nt-2))+elevation1(2:nt-1));
% Below is 3-point filter with weights [ 0.25 0.5 0.25 ]
elevation(2:nmes-1)=0.25.*((elevation1(3:nmes)+elevation1(1:nmes-2))+2.*elevation1(2:nmes-1));


Fs=12.;                        % sampling frequency in Herz ... should be equal to 1/(times(2)-times(1))
nfft=1000;                     % nfft specifies the FFT length 
df=Fs/nfft;                    % Frequential resolution 
numoverlap = nfft/2 ;          % numoverlap is the number of samples by which the sections overlap.
Nf=nfft/2+1;
Nfo=(Nf-1)/2;

hanning=transpose(0.5 * (1-cos(2*pi*linspace(0,nfft-1,nfft)/(nfft-1))));  %Vector of Hanning window
%hanning=hanning.*0+1; % decomment this line to remove window
wc2=1/mean(hanning.^2);                                                   % window correction factor

freq1=linspace(0,df*(nfft-1),nfft);
freq=linspace(df,df*(Nf-1),Nf-1)';  

NS1=floor(nmes/nfft);
NS=NS1*2-1;
Eh=ones(1,NS);
H=hanning*Eh;               % array with Hanning windows on each column

elevmat=zeros(nfft,NS);     % creates array 
% Now fills array with the contents of the window (Welch method)
% First lines: cutting the time series along the windows
elevmat(1:nfft,1:NS1)=reshape(elevation(1:nfft*NS1),[nfft,NS1]);
% After that: adding the shifted windows
elevmat(1:nfft,NS1+1:NS)=reshape(elevation(1+nfft/2:nfft/2+nfft*(NS1-1)),[nfft,NS1-1]);

elevmat2=detrend(elevmat,'linear');          % We remove the linear trend from each column
Hs1=4.*sqrt(mean(var(elevmat2)))             % We compute the significant wave height
 
elevmat2=H.*detrend(elevmat2,'constant');    % remove mean and apply window

%figure(1) % first plot : time series 
%hold on
%plot(times(1:nfft),elevmat2,'r-','LineWidth',2);

Hs2=4.*sqrt(mean(var(elevmat2)))             % Check the impact on the wave height ... must be corrected
   
% Computes FFT
zspec=fft(elevmat2,nfft,1)/nfft;
% computes Power Spectral Density (PSD) 
Szz=(abs(zspec).^2)*wc2/df;
% Gathers the variance on both sides of the Nyquist 
% -> single-sided spectrum
Ef=2.*mean(Szz(2:Nf,:),2);


figure(7) % first plot : amplitudes
clf
mpdc10 = hsv(NS);
ha = axes; hold(ha,'on')
set(ha,'ColorOrder',mpdc10) 
plot(abs(zspec),'-','LineWidth',1);
set(gca,'FontSize',16,'LineWidth',1);
xlabel('frequency index');
ylabel('|Z| (m)');

figure(8) % second : phases
clf
ha = axes; hold(ha,'on')
set(ha,'ColorOrder',mpdc10) 
plot(angle(zspec)./pi,'.','LineWidth',1);
set(gca,'FontSize',16,'LineWidth',1);
xlabel('frequency index');
ylabel('phase / \pi');



% Double check that variance is conserved 
Hs3=4.*sqrt(sum(Ef.*df))

%%%% Now a few plots %%%% 
figure(10)
clf
loglog(freq,Ef,'k-','LineWidth',2);
set(gca,'FontSize',16,'LineWidth',1);
xlabel('frequency (Hz)');
ylabel('E(f)  ({m^2/Hz})');
axis([0.01 8 1E-7 0.1])
grid on

%%%% plots spectra before averaging %%%% 
figure(30)
clf
loglog(freq,2.*Szz(2:Nf,:),'LineWidth',2);
set(gca,'FontSize',16,'LineWidth',1);
xlabel('frequency (Hz)');
ylabel('E(f)  ({m^2/Hz})');
hold on
plot(freq,Ef,'k-','LineWidth',3)
axis([0.01 8 1E-7 0.1])
grid on

%%%% plots non-dimensional spectrum %%%% 
figure(20)
clf
fac=(2*pi)^4/(9.81^2);
plot(freq,Ef.*freq.^5.*fac,'LineWidth',2);

set(gca,'FontSize',14);
xlabel('frequency (Hz)');
ylabel('E(f) {\times \sigma^5/(2 \pi g^2)}  (no dimensions)');



