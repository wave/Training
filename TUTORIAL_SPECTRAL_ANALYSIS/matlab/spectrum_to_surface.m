%--------------------------------------------------------------------------
% Module "Vagues" UBO - Master SML 2012
% TD analyse spectrale - partie 1: Superposition d'ondes linéaires 
% Auteurs: Fabrice Ardhuin 
%          
% But:  Reading directional moments, computing 2D spectrum & visualization
%--------------------------------------------------------------------------

close all;
clear all;
clc;

ndates=521;
[lat,lon,freq,df,time,efi,th1mi,sth1mi,th2mi,sth2mi]=readWWNC_FREQ('../data/CDIP_51201_2012_avg3h_freq.nc');
%
% 1. Reads in spectra from a file (*.spec is ASCII, *spec.nc is NetCDF) 
%

nf=size(efi,1);
%dth=360/nth;
efnow=efi(:,1,ndates);
th1now=th1mi(:,1,ndates);
sth1now=sth1mi(:,1,ndates);
th2now=th2mi(:,1,ndates);
sth2now=sth2mi(:,1,ndates);
d2r=pi/180;
m1=abs(1.-0.5*(sth1now(:)*d2r).^2);
m2=abs(1.-2*(sth2now(:)*d2r).^2);
a1=cos(th1now*d2r).*m1;
b1=sin(th1now*d2r).*m1;
a2=cos(2.*th2now*d2r).*m2;
b2=sin(2.*th2now*d2r).*m2;
nth=72;
  [SD,Efth] = MEM_calc(a1,a2,b1,b2,efnow,nth); % input to MEM are line vectors
 Efth=SD.*repmat(efnow',nth,1)';
 x1=0:(360/nth):360-360/nth;
 %    x1=x1-270;  % shift reference 
 %    I=find(x1<0);
 %    x1(I)=x1(I)+360.;
 %    [ang,I2]=sort(x1);
 %    Efth(:,:)=Efth(:,I2);

  
  nth=size(Efth,2);

dir=x1'.*d2r;
%
% Reads the color palette for pretty pictures ... 
%
col=load('../data/doppler_modified_land_2.ct');
col(:,254)=col(:,253);
col(:,255)=col(:,253);
col(:,256)=col(:,253);
%
% Changes dir from nautical (0 is North, clockwise) to trigonometric convention
%
dirmemo=dir;
dir=pi/2.-dir;
%
datevec(time(ndates))
%

%-------------------------------------------------------------
% 2. Plots the spectrum in polar coordinates
%-------------------------------------------------------------
% 2.1. Repeads the 1st direction at the end to 
%      wrap around nicely 
dirr=[dir' dir(1)]';
dir2=repmat(dirr',nf,1);
freq2=repmat(freq,1,nth+1);
Efth2=zeros(nf,nth+1);
Efth2(:,1:nth)=Efth;
Efth2(:,nth+1)=Efth(:,1);
x2=-cos(dir2).*freq2;  % the - transforms "direction to" in "direction from"
y2=-sin(dir2).*freq2;  % the - transforms "direction to" in "direction from"
%
% 2.2. plots 2D spectrum
%
figure(1);
set(gca,'FontSize',15);
colormap(col'./255);
pcolor(x2,y2,Efth2);axis equal;shading flat;
colorbar;
hold on;
for i=1:7 
    plot(0.1*i*cos(linspace(0,2*pi,25)),0.1*i*sin(linspace(0,2*pi,25)))
end
title('Spectrum estimated from MEM method');
%pause 

%
% 2.3. plots 1D spectrum in frequency only: E(f) 
%
figure(2);
clf
dth=2.*pi./real(nth);

Ef=sum(Efth,2)*dth;
plot(freq,Ef,'k-+','LineWidth',2);
set(gca,'FontSize',15);
xlabel('Frequency (Hz)');
ylabel('E(f) (m^2/Hz)');
%pause

dir=dirmemo;
% Calcul des directions moyennes ... 
dir2=repmat(dir',nf,1);
a1=sum(Efth.*cos(dir2),2).*dth./Ef;
b1=sum(Efth.*sin(dir2),2).*dth./Ef;

m1=sqrt(a1.^2+b1.^2);
sth1m=sqrt(2*(1-m1))*180/pi;

figure(5);
clf
th1m=mod(180-atan2(b1,a1)*180/pi,360);
plot(freq,th1m,'k-',freq,th1mi(:,1,ndates),'ro','LineWidth',2);
set(gca,'FontSize',15);
xlabel('Frequency (Hz)');
ylabel('Mean direction th1m (deg)');
legend('After MEM','before MEM')

figure(6);
clf
plot(freq,sth1m,'k-',freq,sth1mi(:,1,ndates),'ro','LineWidth',2);
set(gca,'FontSize',15);
xlabel('Frequency (Hz)');
ylabel('Spread sth1m (deg)');
legend('After MEM','before MEM')




%-------------------------------------------------------------
% 3. Computes maps of surface elevation
%-------------------------------------------------------------
% a . random draw of phases ... 
phases=rand(nf,nth)*2*pi;
nk=nf;
% b.   Calcul des amplitudes
% b.1  espacement des frequences
df=freq.*(1.1-1/1.1)./2;
% b.2  pas angulaire
dth=2.*pi./real(nth);
as=zeros(nk,nth);
for i=1:nf
    as(i,:)=sqrt(2.*Efth(i,:)*df(i)*dth);
end

% c. definition du domaine de realisation de la surface (carre de 1 km par 1km)
nx=201;
x=linspace(0,1000,nx);
y2=repmat(x',1,nx);
x2=repmat(x,nx,1);
y=x;
g=9.81;
%%%  Calcule les nombre d'onde
sig=2*pi.*freq;
ks=sig.^2./g;   %     utiliser la relation de dispersion 

% d. temps d'evaluation de la surace pour movie
nt=20;
t=linspace(0,(nt-1)*0.5,nt);
% Change les couleurs pour faire joli ... 
col=load('../data/doppler_modified_land_2.ct');
col(:,254)=col(:,253);
col(:,255)=col(:,253);
col(:,256)=col(:,253);
colormap(col'./255);

nfig=3;
figure(nfig);
clf
set(nfig,'Position',[1 1 3*nx+40 3*nx+40])
colormap(col'./255);
M=struct([]);
mov = VideoWriter('example_51001.avi');
open(mov)

for ii=1:nt
%
% initialise la surface a zero
%
zeta=zeros(nx,nx);
hs=0.;
E=0;
% indices 1->8, houle 1
% indices 9->14 houle 2
for i=15:nk
    for j=1:nth
        zeta(:,:)= zeta(:,:)+as(i,j)*cos(ks(i)*cos(pi/2-dir(j)).*x2 ...
            +ks(i)*sin(pi/2-dir(j)).*y2 + phases(i,j)-sig(i)*t(ii));
    E=E+as(i,j).^2./2.;
    end
end
pcolor(x2,y2,zeta);
caxis([-2.5,2.5]);
axis equal;shading interp;colorbar;
xlabel('x (m)');
ylabel('y (m)');
N(ii)=getframe;
F=getframe;
writeVideo(mov,F);
end
% Verifie la valeur de la hauteur significative Hs
hs=4*sqrt(E)

close(mov);
movie(N,1,1)

