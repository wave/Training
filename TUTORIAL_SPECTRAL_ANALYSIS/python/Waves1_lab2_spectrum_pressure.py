
# coding: utf-8

# # Waves 1, Lab #2, part 2: Computing wave spectra from bottom pressure
# ### The goals of this lab are to:
# ### [1] use transfer functions
# ### [2] get familiar with the different frequency bands of waves: IG waves, swell, wind sea 
# 
# #### We will use bottom pressure data obtained below a waverider buoy located off Waimea, in 166 m  depth on the north shore of Oahu. 
# 
# ##### gmarecha@ifremer.fr and ardhuin@ifremer.fr (November 2021). Plouzané, France

# In[ ]:


from IPython.display import Image
from IPython.core.display import HTML 
Image(url= "https://www.pacioos.hawaii.edu/wp-content/uploads/2018/05/news-kalaeloa-barbers-point-wave-buoy.jpg")


# In[ ]:


###########
#Usual libraries
###########
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import dates
import math
import netCDF4 as NC4
import os
import datetime


###########
#Signal processing functions
###########
from scipy.signal import detrend,welch

###########
#Additionals functions
###########
from scipy.io import loadmat
from scipy.stats.distributions import chi2
import numpy.matlib
from IPython import display


# * ***Loading*** pressure sensor and surface CDIP buoy data

# In[ ]:


# This part gets the buoy data. 
#This is a moored buoy: it has an anchor at the bottom and a mooring line that 
# lets it move within a 50 m radius around 


#edit the path where are data here
path_pressure_data='/forums/public/pub/formation-vagues/TRAINING/DATA/SPECTRAL_ANALYSIS/data/'

os.chdir(path_pressure_data)
data = loadmat('deep_waimea.mat')

pres=data['hh']*0.01    # this is pressure in meters of water


path_buoy='/forums/public/pub/formation-vagues/TRAINING/DATA/SPECTRAL_ANALYSIS/data/'
file_buoy='CDIP_51201_2012_avg3h_freq.nc'

nc_buoy=NC4.Dataset(os.path.join(path_buoy+file_buoy))
lat=nc_buoy.variables['latitude'][:,0]
lon=nc_buoy.variables['longitude'][:,0]
freqb=nc_buoy.variables['frequency'][:]
f2b=nc_buoy.variables['frequency2'][:]
f1b=nc_buoy.variables['frequency1'][:]
dfb=f2b-f1b
timeb=nc_buoy.variables['time'][:]
Efb=nc_buoy.variables['ef'][:,0,:]
th1mi=nc_buoy.variables['th1m'][:,0,:]
sth1mi=nc_buoy.variables['sth1m'][:,0,:]
th2mi=nc_buoy.variables['th2m'][:,0,:]
sth2mi=nc_buoy.variables['sth2m'][:,0,:]

dfb=f2b-f1b


# * ***Preamble [1]***:   specral analysis parameters

# In[ ]:


offbot=0.1 # height of pressure sensor over bottom [m]
zone=0
fs=1.
nfft=400 # nfft specifies the FFT length that csd uses.
df=fs/nfft                  # Frequential resolution 
numoverlap = nfft/2        # numoverlap is the number of samples by which the sections overlap.
Nf=int(nfft/2+1)
Nfo=Nf-1  # up to Nyquist.
#freq=np.linspace(df,df*(Nf-1),Nf-1)

print('Frequency resolution is',df,'Hz.')


# * ***Preamble [2]***: informations of pressure data

# In[ ]:


g=9.81


from scipy.io import loadmat
data = loadmat('deep_waimea.mat')
hh=data['hh']
time=data['time']

[nt,nr]=np.shape(hh)
print('Pressure data contains', nr ,'records of', nt ,'points each')
recfac=4        # number of spectra per pressure record 
nrec=209*recfac # number of spectra that we will compute
ntr=3600*3       # seconds in 3 hours


# ***FUNCTION[1]***: Inversion of the surface gravity waves dispersion relationship by Newton Method

# In[ ]:


def dispNewtonTH(f,dep):
# This function inverts the linear dispersion relation (2*pi*f)^2=g*k*tanh(k*dep) to get 
# k from f and dep. 2 Arguments: f and dep are frequency in Hertz and depth in meters
# % Uses Newton method. Original code from T.H.C. Herbers
    eps=0.000001
    g=9.81
    sig=2*np.pi*f
    Y=dep*sig**2./g 

    X=np.sqrt(Y)
    I=1
    F=1
    while abs(np.amax(F)) > eps:
        H=np.tanh(X)
        F=Y-X*H
        FD=-H-X/(np.cosh(X)**2)
        X=X-F/FD

    dispNewtonTH=X/dep

    return dispNewtonTH
    #return 2*np.pi*f/np.sqrt(9.81*dep)


# * ***Spectral analysis*** : PSD of bottom pressure

# In[ ]:


Sppmat=[]
HS_TOT=[]
HS_TOT_band=[]
HS_TOT_IG=[]
HS_4_comparison=[]
Ef_all=np.zeros((int(Nf),int(nrec)))

path_save_figs='/tmp/'
path_save_data='/tmp/'


irec=52   # put 0 here to process full time series
recmax=53 # put nrec here to process full time series
plot_yes=1

# Main processing loop over time 
while irec < recmax: 
    jrec=int(np.floor(irec/recfac)) # index for pressure record
    krec=int((irec-jrec*recfac))
    i1=krec*ntr
    i2=min([nt,ntr*(krec+1)])
    print ('processing record',jrec,'part',krec+1,'out of',recfac,'using indices',i1,i2)
    sub_time=time[0][jrec]
    elevation = pres[i1:i2,jrec] # hh : variable in deep_waimea.mat matrix ; corresponds to pressure divided by rho*g [m]. The 0.01 comes from the conversion from cm to m.
    
    
    t=np.linspace(0,len(elevation)-1,len(elevation))/fs
    
    #####################
    #----Spectral analysis
    #####################

    now=time[0][int(jrec)+1]+krec/8
    
    freq, E_p=welch(elevation, fs=fs, window='hann', nperseg=nfft, detrend='linear', return_onesided=True, scaling='density')
    print(np.size(t),np.size(elevation))
    
    if plot_yes==1:
        fig,ax=plt.subplots(figsize=(12,4))
        plt.plot(t[0:1000],elevation[0:1000],'m-')
        plt.xlabel('time (s)')
        plt.ylabel('pressure (m of water)')
        plt.title('First 1000 samples of raw pressure data');
        #plt.savefig(path_save_figs+'/%f_PRESSURE.pdf'%irec,dpi=100)
        plt.show()
        plt.close()
        
        
        fig,ax=plt.subplots(figsize=(12,2))
        plt.plot(t,elevation,'k-')
        plt.plot(t[0:1000],elevation[0:1000],'m-')

        plt.xlabel('time (s)')
        plt.ylabel('pressure (m of water)')
        plt.title('Full time series analized');
        #plt.savefig(path_save_figs+'full_time_pressure_data.pdf',dpi=100)        
        
        fig,ax=plt.subplots(figsize=(12,4))
        plt.loglog(freq,E_p,'m-')
        ax.set_xticks([5*10**-3,7*10**-3,1*10**-2,3*10**-2,5*10**-2,7*10**-2,10**-1,5*10**-1])
        ax.set_xticklabels([5*10**-3,7*10**-3,1*10**-2,3*10**-2,5*10**-2,7*10**-2,10**-1,5*10**-1])
        plt.gcf().autofmt_xdate(rotation=25)

        plt.xlabel('Frequency (Hz)')
        plt.ylabel('PSD of bottom pressure (m$^{2}$/Hz)')
        plt.show()
        #plt.savefig(path_save_figs+'Ep.pdf',dpi=100)        



    irec=irec+1


# * ***Switch*** from PSD of bottom pressure to PSD of surface elevation

# In[ ]:


# Based on the above plots we define a maximum frequency where noise can be neglected, 

fmax=0.08 # choose a value, edit here ...

ifmax = int(fmax/df)  


print('Converting to elevation for f <',freq[ifmax], 'i.e. indices up to',ifmax,Nf)
dep=np.mean(elevation)
print('Mean depth is',dep,' m')
k=dispNewtonTH(freq,dep)
k[0]=0
print('kmax is',k[ifmax],'rad/m, kmax*D=', k[ifmax]*dep)
offbot=0.1 # height of pressure sensor over bottom 
# cosur is square of near-bottom pressure to elevation transfer function 
M=np.cosh(k*dep)/np.cosh(k*offbot)
cosur=M**2
cosur[0]=1.
cosur[ifmax+1:Nf]=1.
Ef=E_p*cosur

fig,ax=plt.subplots(figsize=(12,4))
plt.loglog(freq,M,'k-')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Unitless')
plt.title('Transfert function')
#plt.savefig(path_save_figs+'/TF.pdf',dpi=100)

plt.show()

fig,ax=plt.subplots(figsize=(12,4))
plt.loglog(freq,Ef,'k-')
ax.set_xticks([5*10**-3,7*10**-3,1*10**-2,3*10**-2,5*10**-2,7*10**-2,10**-1,5*10**-1])
ax.set_xticklabels([5*10**-3,7*10**-3,1*10**-2,3*10**-2,5*10**-2,7*10**-2,10**-1,5*10**-1])
plt.gcf().autofmt_xdate(rotation=25)
plt.xlabel('Frequency (Hz)')
plt.ylabel('PSD of surface elevation (m$^{2}$/Hz)')
#plt.savefig(path_save_figs+'/Ef.pdf',dpi=100)

plt.show()


# * ***Repeat*** the process along the entire time serie

# In[ ]:


# Now runs the whole thing
Hsp=np.zeros(nrec)
Epf_all=np.zeros((Nf,nrec))
Ef_all =np.zeros((Nf,nrec))
time_p=np.zeros(nrec)
irec=0
recmax=nrec

# Main processing loop over time 
while irec < recmax: 
    jrec=int(np.floor(irec/recfac)) # index for pressure record
    krec=int((irec-jrec*recfac))
    i1=krec*ntr
    i2=min([nt,ntr*(krec+1)])
    print ('processing record',jrec,'part',krec+1,'out of',recfac,'using indices',i1,i2)
    elevation = hh[i1:i2,jrec]*0.01 # hh : variable in deep_waimea.mat matrix ; corresponds to pressure divided by rho*g [m]. The 0.01 comes from the conversion from cm to m.
    
    #t=np.linspace(0,len(elevation)-1,len(elevation))/fs
    #now=time[0][int(jrec)+1]+krec/8
    
    freq, E_p=welch(elevation, fs=fs, window='hann', nperseg=nfft, detrend='linear', return_onesided=True, scaling='density')
    dep=np.mean(elevation)
    k=dispNewtonTH(freq,dep)
    k[0]=0
    offbot=0.1 # height of pressure sensor over bottom 
    # cosur is square of near-bottom pressure to elevation transfer function 
    M=np.cosh(k*dep)/np.cosh(k*offbot)
    cosur=M**2
    cosur[0]=1.
    cosur[ifmax+1:Nf]=1.
    Ef=E_p*cosur
    Epf_all[:,irec]=E_p
    Ef_all[:,irec]=Ef
    time_p[irec]=time[0][jrec]+krec/(recfac*2)
    irec = irec+1
    


# * Plot the ***periodogram*** of the pressure sensor

# In[ ]:


# Let us look at the result ... 
fig,ax=plt.subplots(figsize=(15,8))
dt = (time[0][1]-time[0][0])/2.
ifp = int(0.15/df)   # you can try 0.08 or a little less 

#extent = [time_p[0]-dt, time_p[-1]+dt, freq[0]-df, freq[-1]+df]
extent = [-dt, time_p[-1]-time_p[0]+dt, freq[0]-df, freq[-1]+df] #freq[-1]+df]
im=ax.imshow(10*np.log10(Ef_all[:,:]),origin='lower',extent=extent,aspect='auto')
plt.title('Surface elevation PSD from bottom pressure (dB)');    
plt.xlabel('time (days)')
plt.ylabel('Frequency (Hz)')
plt.colorbar(im)
#plt.savefig(path_save_figs+'/periodogram_pressure.pdf',dpi=100)

plt.show()


# * Significant wave height in the ***infragravity wave band***

# In[ ]:


iG1= int(0.003/df) 
iG2= int(0.04/df)
HIG=4*np.sqrt(np.sum(Ef_all[iG1:iG2,:],axis=0)*df)
fig,ax=plt.subplots(figsize=(12,2))
plt.plot(time_p-time_p[0],HIG,'k-')
plt.xlabel('time (days)')
plt.ylabel('HIG (m)')
plt.title('Significant infragravity wave height (m)')
#plt.savefig(path_save_figs+'/Hs_time_IG.pdf',dpi=100)


# * ***Compare*** the significant wave height from the pressure sensor and the wave buoy above

# In[ ]:


if1= int(0.05/df) 
if2= ifmax
Hs=4*np.sqrt(np.sum(Ef_all[if1:if2,:],axis=0)*df)
fig,ax=plt.subplots(figsize=(12,2))
plt.plot(time_p-time_p[0],Hs,'k-',label='Hs from pressure')
plt.xlabel('time (days)')
plt.ylabel('Hs (m)')
plt.title('Significant wave height (m) for waves with frequency up to fmax')
#print(dfb) # WARNING: df is not constant for buoy ... 
print(np.shape(freqb),f1b[2])
ifmaxb=int((fmax-freqb[0])/dfb[0])
print('Cutting buoy data at frequency',fmax,freqb[ifmaxb])
Hsb=4*np.sqrt(np.sum(Efb[:,0:ifmaxb],axis=1)*dfb[0])
print(np.shape(timeb),np.shape(Efb))
plt.plot(timeb-timeb[0]-1,Hsb,'r--',label='Hs from buoy')
plt.legend(fontsize=15)

#plt.savefig(path_save_figs+'/compare_HS.pdf',dpi=100)


# * Plot the ***periodogram*** of the wave buoy

# In[ ]:


fig,ax=plt.subplots(figsize=(15,8))
dtb = (timeb[1]-timeb[0])/2.

#extent = [time_p[0]-dt, time_p[-1]+dt, freq[0]-df, freq[-1]+df]
extent = [-dtb, timeb[-1]-timeb[0]+dtb, freqb[0]-dfb[0], freqb[-1]+dfb[-1]] #freq[-1]+df]
im=ax.imshow(10*np.log10(np.transpose(Efb[:,:])),origin='lower',extent=extent,aspect='auto')
plt.title('Surface elevation PSD from buoy (dB)');    
plt.xlabel('time (days)')
plt.colorbar(im)
#plt.savefig(path_save_figs+'/periodogram_buoy.pdf',dpi=100)

plt.show()


# * ***Instantaneous*** buoy data

# In[ ]:


id_t=103 #edit here, to change the time

nctime=nc_buoy.variables['time'][id_t]
efnow=Efb[id_t,:]
th1now=th1mi[id_t,:]
th2now=th1mi[id_t,:]
sth1now=sth1mi[id_t,:]
sth2now=sth2mi[id_t,:]



nctime = nc_buoy.variables['time'][id_t] # get values
t_unit = nc_buoy.variables['time'].units # get unit  "days since 1950-01-01T00:00:00Z"
tvalue = NC4.num2date(nctime,units = t_unit)
str_time = tvalue.strftime("%Y-%m-%d %H:%M") 


print ('Estimation of the wave spectrum for:',str_time,'--UTC')


# * Compute the ***First 5’*** spectral wave parameters

# In[ ]:


# cc Kuik et al. 1988 JPO
#We need the First 5’ spectral wave parameters for a specific time
d2r=np.pi/180
m1=abs(1.-0.5*(sth1now[:]*d2r)**2)
m2=abs(1.-2*(sth2now[:]*d2r)**2)

a1=np.cos(th1now*d2r)*m1 # Directional distribution 2*pi periodic component (cosine component)
b1=np.sin(th1now*d2r)*m1 # Directional distribution 2*pi periodic component (sine component)
a2=np.cos(2*th2now*d2r)*m2 # Directional distribution pi periodic component (cosine component)
b2=np.sin(2*th2now*d2r)*m2 # Directional distribution pi periodic component (sine component)


# * ***FUNCTION [2]***: MEM method

# In[ ]:


#the MEM method
def buoy_spectrum2d(sf,a1,a2,b1,b2, dirs = np.arange(0,365,10)):
    """(a1,a2,b1,b2,en,ndirs):
    purpose: compute spectrum(f,theta) from coef ai,bi
    author: Wang He
    # Maximum entropy method to estimate the Directional Distribution
    
    # Maximum Entropy Method - Lygre & Krogstad (1986 - JPO)
    # Eqn. 13:
    # phi1 = (c1 - c2c1*)/(1 - abs(c1)^2)
    # phi2 = c2 - c1phi1
    # 2piD = (1 - phi1c1* - phi2c2*)/abs(1 - phi1exp(-itheta) -phi2exp(2itheta))^2
    # c1 and c2 are the complex fourier coefficients
    
    # inumpyuts : dirs in degrees
    """
    nfreq = np.size(sf)
    nbin = np.size(dirs)
    
    c1 = a1+1j*b1
    c2 = a2+1j*b2
    p1 = (c1-c2*np.conj(c1))/(1.-abs(c1)**2)
    p2 = c2-c1*p1
    
    # numerator(2D) : x
    x = 1.-p1*np.conj(c1)-p2*np.conj(c2)
    x = numpy.tile(numpy.real(x),(nbin,1)).T
    
    # denominator(2D): y
    a = dirs*numpy.pi/180.
    e1 = numpy.tile(numpy.cos(a)-1j*numpy.sin(a),(nfreq,1))
    e2 = numpy.tile(numpy.cos(2*a)-1j*numpy.sin(2*a),(nfreq,1))
    
    p1e1 = numpy.tile(p1,(nbin,1)).T*e1
    p2e2 = numpy.tile(p2,(nbin,1)).T*e2
    
    y = abs(1-p1e1-p2e2)**2
    
    D = x/(y*2*numpy.pi)
    
    # normalizes the spreading function,
    # so that int D(theta,f) dtheta = 1 for each f  
    
    dth = dirs[1]-dirs[0]

    tot = numpy.tile(numpy.sum(D, axis=1)*dth/180.*numpy.pi,(nbin,1)).T
    D = D/tot
    
    sp2d = numpy.tile(sf,(nbin,1)).T*D
    
    return sp2d,D


# * Apply the **MEM method*** on CDIP buoy data

# In[ ]:


#Apply the method for a specific time
nth=72 #number of direction in the 2D wave spectrum

x1=np.arange(0,365,360./nth)-360/nth
dir00=x1*np.pi/180
dir0=np.pi/2-dir00

dirs = np.arange(0,365,5)
F,DIRS=np.meshgrid(freqb,x1*np.pi/180)
[Efth,_] = buoy_spectrum2d(efnow,a1,a2,b1,b2, x1 ) #Here the MEM method that estimate the 2D spectrum from the first 5 Fourier Parameters


# * Plot the wave spectrum

# In[ ]:


#Plot the 2D spectrum
DIR0,FREQ0=np.meshgrid(dir0,freqb)
x2=np.cos(DIR0)*FREQ0#the - transforms "direction to" in "direction from"
y2=np.sin(DIR0)*FREQ0 #the - transforms "direction to" in "direction from"

#If we integrate the 2D spectrum in Azimuths:
dth=2*np.pi/np.real(nth)
Ef=np.sum(Efth,axis=1)*dth

plt.figure(figsize=(13,5))
plt.subplot(121)
plt.pcolor(x2,y2,Efth,vmin=0,vmax=5,cmap='Spectral_r')
plt.xlim([-.5,.5])
plt.ylim([-.5,.5])
plt.xlabel('K$_{x}$ [m$^{-1}$]',fontsize=14)
plt.ylabel('K$_{y}$ [m$^{-1}$]',fontsize=14)
plt.title('Two dimensional wave spectrum')

plt.colorbar()
plt.subplot(122)
plt.plot(freqb,Ef,'k-+',linewidth=2)
plt.xlabel('Frequency [Hz]',fontsize=14)
plt.ylabel('E(f) [m$^2$/Hz]',fontsize=14)
plt.title('One dimensional wave spectrum')
#plt.savefig(path_save_figs+'/2D_1D_spectrum.pdf',dpi=100)


# * Inverse the wave spectrum to estimate ***surface elevation*** field with a random phase

# In[ ]:


from IPython.display import Image
import time as time_new


path_save_frames_4_movie='/tmp/'


#random draw of phases 
phases=np.random.rand(np.size(Efth,axis=0),np.size(Efth,axis=1))*2*np.pi 
nk=np.size(Efth,axis=0)

df=freqb*(1.1-1/1.1)/2 #frequency step axis

dth=2*np.pi/nth #angular axis
AS=np.zeros((nk,nth+1))

for i in range(nk):
    AS[i,:]=np.sqrt(2*Efth[i,:]*df[i]*dth)
    
#create a 2D domain
nx=201
x=np.linspace(0,1000,nx)
y20=np.matlib.repmat(x,nx,1)
x2=np.matlib.repmat(x,nx,1)
y2=np.transpose(y20)
y=x

#compute the wavenumber
sig=2*np.pi*freqb
ks=sig**2/g


hs=0.
E=0
nt=40 #edit here to define the number of frame

t=np.linspace(0,(nt-1)*0.5,nt)

dir0=x1.T*d2r

ID_LF,ID_HF=1,nk #edit here for the frequency boundaries (wave systems)
# ID_LF,ID_HF=1,8 #edit here for the frequency boundaries (wave systems)
#  indices 1->8, swell 1
#  indices 9->14 swell 2
#  indices 9->14 wind sea 

for ii in range(nt):
    zeta=np.zeros((nx,nx))

    fig,ax=plt.subplots()
    for i  in range(ID_LF,ID_HF):
        for j in range(nth):
            zeta= zeta+AS[i,j]*np.cos(ks[i]*np.cos(np.pi/2-dir0[j])*x2                 +ks[i]*np.sin(np.pi/2-dir0[j])*y2 + phases[i,j]+sig[i]*t[ii])
        E=E+AS[i,j]**2/2.
        
    #-----------Plot the elevation field
    p1=plt.contourf(x2,y2,zeta,np.linspace(-2,2,40),extend='both',cmap='RdBu_r')
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    cb=plt.colorbar(ticks=np.arange(-2,2.2,.5))
    cb.set_label('$\zeta$[m]',fontsize=12)
    plt.title('Time=%.1f sec'%t[ii],fontsize=12)
    
    #----------Save the figures
    fig.savefig(path_save_frames_4_movie+'%i.png'%int(ii),dpi=100)
    
    display.display(Image(filename=path_save_frames_4_movie+str(ii)+'.png'))
    display.clear_output(wait=True) 
    time_new.sleep(.2)
    
    
    plt.close()
print('Movie: The significant wave height is equal to',4*np.sqrt(E),'m')


# ### Significant wave height from wave spectrum and surface elevation variance

# In[ ]:


print ('The significant wave height from the estimated surface elevation',       4*np.sqrt(np.var(zeta)),'m')
print ('The significant wave height from the wave spectrum measured by the buoy',       4*np.sqrt(np.nansum(np.nansum(np.transpose(Efth)*df,axis=1)*dth,axis=0)),'m')


# * ***READ*** the movie

# In[ ]:


for i in range(30):    
    display.display(Image(filename=path_save_frames_4_movie+str(i)+'.png'))
    display.clear_output(wait=True) 
    time_new.sleep(.5)

