
# coding: utf-8

# # Waves 1, Lab #2: Computing wave spectra
# ### Most wave measurements around the world come from floating buoys such as this one ... but we will actually use  stereo-video data from a system like this https://www.dais.unive.it/wass/videos/wass_out_small_web.mp4  
# ##### gmarecha@ifremer.fr and ardhuin@ifremer.fr (November 2021). Plouzané, France

# In[ ]:



from IPython.display import Image
from IPython.core.display import HTML 
Image(url= "https://media.springernature.com/lw685/springer-static/image/art%3A10.1038%2Fs41597-020-0492-9/MediaObjects/41597_2020_492_Fig4_HTML.png")


# In[ ]:


###########
#Usual libraries
###########
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import dates
import math
import netCDF4 as NC4
import os
import datetime


###########
#Signal processing functions
###########
from scipy.signal import detrend,welch

###########
#Additionals functions
###########
from scipy.io import loadmat
from scipy.stats.distributions import chi2
import numpy.matlib


# * ***Loading*** one dimensional sea surface elevation record 

# In[ ]:


from scipy.io import loadmat
#edit here 
path_data='/forums/public/pub/formation-vagues/TRAINING/DATA/SPECTRAL_ANALYSIS/data/'

data = loadmat(path_data+'katsiveli_data_sample.mat')

nt=data['nt']
times=data['times']
elevation=data['elevation1']
dt=np.diff(times)

                        
zeta=np.squeeze(elevation)

nmes=np.size(zeta) #length of full time serie

print('We have',nmes,'data points in this time series.')


# * ***Plot*** the surface elevation time serie (quick look and the full time serie) 

# In[ ]:


#Let's take a quick look at the data
fig,ax=plt.subplots(figsize=(12,2))

plt.plot(times[0:nmes-1],zeta[0:nmes-1],color='k')
plt.xlabel('time (s)',fontsize=16)
plt.ylabel('surface \n elevation (m)',fontsize=16)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim([0,100])
plt.ylim([-.55,.55])
plt.grid()


# In[ ]:


# And here is a plot of the full time series... 
fig,ax=plt.subplots(figsize=(12,2))

plt.plot(times[0:nmes-1],zeta[0:nmes-1],color='k')
plt.xlabel('time (s)',fontsize=16)
plt.ylabel('surface \n elevation (m)',fontsize=16)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim([0,nmes/12])
plt.ylim([-.55,.55])
plt.grid()


# * Start the ***Spectral analysis***

# ##### [1] Define the length of the signal, the sampling frequency and the frequency axis

# In[ ]:



nfft=len(zeta) #number of point for the spectral analysis: you will have to change this value
               # you can try 600, 1200, 2400  ... 
nfft=1200
    
    
Nf=nfft/2+1
#Number of windows without overlap  
nmes=np.size(zeta) # length of full time series
NS1=nmes//nfft     # number of samples of length nfft that fit in the time series 
print('There will be',NS1,'independant samples')

Fs=12.             # sampling frequency in Herz ... should be equal to 1/(times(2)-times(1))

df=Fs/nfft         # frequency resolution 

print('the frequency resolution df is equal to',df,'Hz')

#Defines frequency array
df=Fs/nfft                    # Frequency resolution 
Nf=nfft//2+1                   # number of non-ambiguoys frequencies (up to Nyquist)
Nfo=(Nf-1)//2                  # number of frequencies for output (the higher frequencies can be just noise)
freq=  np.linspace(0,df*(Nf-1),Nf)


# ##### [2]Create window function to avoid the spectral leakage

# In[ ]:


# We define a window that is used to avoid Gibbs effect 
hanning=(0.5 * (1-np.cos(2*np.pi*np.linspace(0,nfft-1,nfft)/(nfft-1)))) # define a window function (here hanning)


# ##### plot the windowed data

# In[ ]:


fig,ax=plt.subplots(figsize=(12,2))
plt.plot(times[0:nfft-1],zeta[0:nfft-1]*hanning[0:nfft-1],color='r')
plt.grid()


# ##### [3] Initialized the overlap between the sub-signal and detrend it

# In[ ]:


NS=NS1*2-1  #edit for overlap
nf2=nfft//2
Eh=np.ones((NS,1))
H=(Eh*hanning).T
elevmat=H*0   # creates 2D array to put surface elevations 
elevmat[0:nfft,0:NS1]=zeta[0:(NS1*nfft)].reshape((nfft,NS1), order='F')
# and now shifts the samples by nf2 
elevmat[0:nfft,NS1:NS]=zeta[nf2:nf2+((NS1-1)*nfft)].reshape((nfft,NS1-1), order='F')
# 
elevmat2=H*detrend(elevmat,type='linear',axis=0)#remove mean and apply window
fig,ax=plt.subplots(figsize=(12,2))
# just to be sure everything is right, let's plot the first windowed sample
plt.plot(elevmat2[0:nfft-1,0])
plt.grid()


# ##### [4] Perform the Fourier analysis and return the power spectral density (PSD)

# In[ ]:


wc2=1/(np.nanmean(hanning**2))
#Computes FFT over nfft point (don't forget to normalized your result)
zspec=np.fft.fft(elevmat2,axis=0)/nfft 
#Computes PSD (m^2/Hz) from complex amplitude zspec
Szz=(np.abs(zspec)**2)*wc2/df
#Folding spectrum onto positive frequencies only
Ef=2*Szz[0:Nf,0:NS].mean(axis=1)  #= single-sided spectrum


# ##### Plot the PSD of the wave elevation

# In[ ]:


fig,ax=plt.subplots()
ax.loglog(freq[0:Nf],Ef[0:Nf],'k')
ax.set_xlabel('Frequency (Hz)',fontsize=15)
ax.set_ylabel('Wave elevation spectrum (m$^{2}$/Hz)',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid()


# * Compute the ***significant wave height*** from the initial record and from your spectrum

# In[ ]:


# Let us now check a few things on the normalization: 
# First the wave height from the original record
Hs1=4*np.sqrt(np.var(zeta))
# Second the "area under the curve"
Hs2=4*np.sqrt(np.sum(Ef)*df)
print('These two wave heights (in meters) should be similar:',Hs1,Hs2, 'm')
# If not, then there is a normalization problem or window correction error ... 


# * Perform the same spectral analysis with ***Scipy module*** 

# In[ ]:


from scipy.signal import welch
f, Pxx_spec=welch(zeta, fs=12, window='hann', nperseg=nfft, detrend='linear', return_onesided=True, scaling='density')


plt.loglog(freq,Ef,'k',label='my spectrum')
plt.loglog(f,Pxx_spec,'r--',label='Welch method from scipy')
ax.set_xlabel('Frequency [Hz]',fontsize=15)
ax.set_ylabel('wave elevation spectrum [$m^{2}$/Hz]',fontsize=15)
plt.grid()
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.legend(fontsize=15)


# In[ ]:


# let us now look at the uncertainties around the mean spectrum
# If we plot all the different spectra we find a "thick" distribution that covers about a factor 100 
# over the y axis. 

fig,ax=plt.subplots()
ax.loglog(freq[0:Nf],2*Szz[0:Nf,0:NS])
ax.set_xlabel('Frequency (Hz)',fontsize=15)
ax.set_ylabel('Wave elevation spectrum (m$^{2}$/Hz)',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid()



# * The distribution of the wave elevation and the ***$\chi^{2}$*** distribution

# In[ ]:


# Theory tells us that any spectral density follows a chi-squared distribution 
# For example let's check at 0.33 Hz 
I= np.where((freq > 0.33))
idx = np.argmin(abs(freq -0.33))
print("We are looking at index ",idx,"which corresponds to ",freq[idx],"Hz")
print("Here is a histogram of the normalized spectrum")

# we now make a histogram of the values of the spectrum 
Szz1=np.sort(2*Szz[idx,0:NS])/Ef[idx]
plt.hist(Szz1, bins = 10)
plt.xlabel('$E_i(f)/E(f)$',fontsize=16)
plt.ylabel('count',fontsize=16)
plt.show()

#plt.plot(Szz1,color='r',marker='o',linestyle="None")
##plt.grid()
#plt.xlabel('sample number',fontsize=16)
#plt.ylabel('value of PSD (m2/Hz)',fontsize=16)


# In[ ]:


# now we compare to the theoretical chi2 distribution for NS*2 degrees of freedom.
from scipy.stats import chi2
bins=np.linspace(0, 10, 101)
Ev=np.ones((NS,1))
Ef2=(Ef*Ev).T
# Normalizes spectra for each frequency by the mean spectrum 
Szzall=2*Szz[1:Nf,0:NS]/Ef2[1:Nf,0:NS]
Szz1=Szzall.reshape(-1,1)
fig, ax = plt.subplots(1, 1)
ax.hist(Szz1, bins = bins,density=True, histtype='stepfilled',label='pdf of spectrum')
ax.plot(bins, chi2.pdf(bins, 2,scale=0.5), 'r-', lw=5, alpha=0.6, label='chi2 pdf')
ax.legend(loc='best', frameon=False)
#r1=0.0506/2
#r2=7.38/2
r1=chi2.ppf(0.025, 2,scale=0.5)
r2=chi2.ppf(0.975, 2,scale=0.5)
p1 = chi2.cdf(r1, 2,scale=0.5)
p2 = chi2.cdf(r2, 2,scale=0.5)
print('we verify that the probability that E_i/E < ',r1,'  is ',p1)
print('we verify that the probability  E_i/E < ',r2, 'is ',p2)
plt.show()


# * ***Confidence interval*** for the mean spectrum (random error)

# In[ ]:


# Now, what is the confidence interval for the mean spectrum? 
# Well, you can check that averaging N spectrum gives a scaled chi2 distribuition with 2*N 
# degrees of freedom. 
# The confidence interval should be given by 
r1=chi2.ppf(0.025, 2*NS1,scale=1/(2*NS1))
r2=chi2.ppf(0.975, 2*NS1,scale=1/(2*NS1))
print('With ',NS1,'independant spectra, we have',NS1*2,'degrees of freedom.')
print('We thus expect that 95% of the spectrum will be in the range E(f)*',r1,' to E(f)*',r2)


# * Plot the mean spectrum and the associated ***errorbars***

# In[ ]:


# So here is how you should plot a spectrum
# WARNING: the error here is just the random error
# there is also a bias due to noise
err=[Ef-r1*Ef,r2*Ef-Ef]
fig,ax=plt.subplots()
ax.loglog(freq[0:Nf],Ef[0:Nf],'k')
ax.errorbar(freq[0:Nf],Ef[0:Nf],yerr=err,errorevery=2,color='r',zorder=1)
ax.set_xlabel('Frequency (Hz)',fontsize=15)
ax.set_ylabel('Wave elevation spectrum (m$^{2}$/Hz)',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid()


# In[ ]:




