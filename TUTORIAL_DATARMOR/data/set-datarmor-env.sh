#!/bin/bash -e

# datalogin and datapwd are automatically defined in local bashrc 
# depending on the local user name


# create public rsa key for auto ssh on datarmor
cd $HOME
source /opt/scripts/module-lpo.sh
. /forums/public/pub/WAVE_DATA/ww3.env
rm -f ~/.ssh/id_rsa && ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
export DISPLAY=:0
export SSH_ASKPASS="$WAVE_COURSE/TUTORIALS/TUTORIAL_DATARMOR/data/echo_pwd.sh"
SSH_OPTIONS="-oLogLevel=error -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null"
echo "do ssh and add pub key"
cat .ssh/id_rsa.pub | setsid ssh ${SSH_OPTIONS} $datalogin@datarmor 'cat >> .ssh/authorized_keys'
ssh-add


# add cshrc.datarmor on datarmor accounts
echo "set datarmor cshrc"
ssh $datalogin@datarmor "cat >> ~/.cshrc << EOF

## Initialization wave course

if ( -f /home/datawork-WW3/COURS/WAVES_SHORT_COURSE/TUTORIALS/TUTORIAL_DATARMOR/data/cshrc.datarmor ) then
      source /home/datawork-WW3/COURS/WAVES_SHORT_COURSE/TUTORIALS/TUTORIAL_DATARMOR/data/cshrc.datarmor
endif

EOF"


# mount datarmor home and work
echo "mount datarmor home and work for $datalogin"
mkdir -p $HOME/datahome && echo $datapwd | sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 $datalogin@datarmor:/home2/datahome/$datalogin $HOME/datahome -o password_stdin
mkdir -p $HOME/datawork && echo $datapwd | sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 $datalogin@datarmor:/home2/datawork/$datalogin $HOME/datawork -o password_stdin

