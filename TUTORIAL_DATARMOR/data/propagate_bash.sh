#!/bin/bash

host="$(hostname)"

# loop on iuem users matlab1 to matlab20
for num in {1..20..1}
do
  echo "matlab$num"

  # add bashrc.iuem on local accounts
  echo "set iuem bashrc"
  ssh matlab${num}@$host "cat >> ~/.bashrc << EOF

# Initialization wave course

if [ -f  /forums/public/pub/WAVES_SHORT_COURSE/TUTORIALS/TUTORIAL_DATARMOR/data/bashrc.iuem ] ; then
      . /forums/public/pub/WAVES_SHORT_COURSE/TUTORIALS/TUTORIAL_DATARMOR/data/bashrc.iuem
fi

EOF"

done

