import sys,getopt,gc
import os,glob
import importlib
import json
import time
from datetime import datetime


def MkDirectory(path):
    try:
        if not path or os.path.exists(path):
            return
        parent = os.path.abspath(os.path.join(path, '..'))
        if not os.path.exists(parent):
            MkDirectory(parent)
        os.mkdir(path)
        if ( not os.path.isdir(path) ):
            print("could not create directory '"+path+"'")
    except Exception as e:
        print(str(e))

def OpenJSONFile(filename):
    global outputFile
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args,file= outputFile)
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
    else:
        return data
    return None

def ProcessJSONFile(filename):
    global outputFile,baseDirectory,outputFileName
    try:
        
        data = OpenJSONFile(filename)

        if 'baseDirectory' in data:
            baseDirectory = data['baseDirectory']
        else:
            print("base directory not provided. exiting...")
            sys.exit(-1)
        
        if 'outputLogFile' in data:
            outputFileName = data["outputLogFile"]
        else:
            print("output log file path not provided. exiting...")
            sys.exit(-1)   

    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e) )
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:"+ str(e))

def ProcessCmdLineOptions():
    global outputFile
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:",['json='])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)
    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: "+ e.msg,file=outputFile)
    except Exception as e:
        print("an error occured while reading json file : "+ str(e),file= outputFile)
######
#
#   script starts HERE
#
######
start = datetime.now()
timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")

outputFile = None
baseDirectory = None
outputFileName = None

ProcessCmdLineOptions()
pathTosearch = os.path.join(baseDirectory,"**/"+"stats")
dirsIterator = glob.iglob(pathTosearch,recursive=True)

filename, file_extension = os.path.splitext(outputFileName)
outputPath = os.path.dirname( outputFileName)
outputFileName = tmpFileName = os.path.basename(outputFileName)+"_"+timeStr+file_extension

MkDirectory(outputPath)
outputFileName = os.path.join(outputPath,outputFileName)



startTime = time.time()

print("[INFO] script Checking 'stats' data started on "+str(start))

print("[INFO] output directory log set to '"+outputPath+"'",file = outputFile)
print("[INFO] output file log set to      '"+outputFileName+"'",file = outputFile)

outputFile = open(outputFileName,'w')

for dir in dirsIterator:
    print("[INFO] detected directory '"+dir+"'",file=outputFile)
    filesIterator = glob.iglob(os.path.join(dir,"*.json"),recursive=True)
    for path in filesIterator:
        file = open(path,"r")
        jsonObj = json.load(file)
        file.close()
        print("[INFO] scanning json file '"+path+"'",file=outputFile)
        for v in jsonObj:

            min = 0.
            max = 0.
            mean = 0.
            stats = jsonObj[v]['stats']
            if 'min' in stats:
                min = stats["min"]
            else:
                print("[ERROR] 'min' tag not found. skipping")
                continue

            if "max" in stats:
                max = stats["max"]
            else:
                print("[ERROR] 'max' tag not found. skipping")
                continue
            if 'mean' in stats:
                mean = stats["mean"]
            else:
                print("[ERROR] 'mean' tag not found. skipping")
                continue
            if (mean < min) or (mean>max):
                print("[ERROR] mean = "+str(mean)+" is out of range ["+str(min)+";"+str(max)+"]",file=outputFile)                                

end = datetime.now()
endTime = time.time()
elapsedTime = endTime-startTime
print("[INFO] script Checking 'stats' data ended on "+str(end),file=outputFile)
print("Total elapsed time :",elapsedTime,file=outputFile)
print("[INFO] exiting. ",file=outputFile)
outputFile.close()

