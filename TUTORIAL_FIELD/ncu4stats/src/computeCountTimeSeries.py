#!/usr/bin/python
import sys,getopt
import os,glob
import importlib
import time
import json
import numpy as np

from datetime import datetime
from string import Formatter
from datetime import timedelta
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
from dateutil import parser as dateUtilsParser

import math

#
# utility functions


def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = int(tdelta.total_seconds())
    elif inputtype in ['s', 'seconds']:
        remainder = int(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = int(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = int(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = int(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('W', 'D', 'H', 'M', 'S')
    constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)

def printUsage( exitNow = False ):
    print("usage : python3 computeCountTimeSeries.py --json=path_to_json_file")
    print(" json file sample is provided as ")
    print("{")
    print("  \"baseDirectory#\": \"a_path\",")
    print("  \"baseFilename\": \"ESACCI-SEASTATE-L2P-SWH-Jason-2*\",")
    print("  \"baseExtension\":\"*nc\",")
    print("  \"netCDF4Variables\":[\"swh_quality\"],")
    print("  \"netCDF4StartDate\":\"time_coverage_start\",")
    print("  \"netCDF4EndDate\":\"time_coverage_end\",")
    print("  \"outputBaseFileName\":\"Jason2_Year_2008_Day_\",")
    print("  \"colorPlots\":[\"red\", \"black\", \"grey\", \"yellow\",\"blue\"],")
    print("}")
    if exitNow:
        print("exiting...")
        sys.exit(-1)

#
#
# open a JSON file and return the object read from this file
#
def OpenJSONFile(filename):
    """ process a json file to retrieve content

    Parameters
    ----------   
    filename : str
             a path to the json file

    Returns
    -------
    none
    """
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args)
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
    else:
        return data
    return None


#function to indicate test passed
def PrintSuccess():
    global outputFile
    print('[PASSED]',file=outputFile)

def PrintFailed():
    global outputFile
    print('[FAILED]',file=outputFile)

#function to indicate failure
def PrintError(errStr,outFile):
    print( "[ERROR] "+errStr ,file=outFile)


#function to indicate failure and exit
def PrintErrorAndExits(errStr, outFile):
    
    PrintError(errorStr,outFile)
    PrintFileFooter(outFile)
    sys.exit(-1)
    print("Exiting with errors...")


def PrintFileFooter(outFile):
    global startTime
    endTime = time.time()
    end = datetime.now()
    elapsedTime = endTime-startTime
    print("Total elapsed time (s) :",elapsedTime,file=outFile)
    msgSessionEnded ="session ended on "+end.strftime("%B %d, %Y")+" "+end.strftime("%H:%M:%S")
    print(msgSessionEnded,file=outFile)
    print("Exiting...",file=outFile)
    outFile.close()


def ExitsWithErrorMessage(errStr,outFile):
    print( "[ERROR] : "+errStr,file=outFile )
    PrintFileFooter(outFile)
    sys.exit(-1)
    print("Exiting with errors...")

def PrintWARNING(warningStr,outFile):
        print("[WARNING] : "+warningStr,file=outFile)



#function to print current task message
def PrintCurrentTask(message,outFile):
    print("[INFO]"+message, end='',file=outFile )

#
# check for a particular option
#
def GetOptionArgFromCmdLine(optStr):
    """  check command line arguments for a specific option

    Parameters
    ----------
    optStr : str
             the whole command line from unix system view point
    
    Returns
    -------
    the associated value to the required option if any
    """
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:",['json=','outputFile=',"single_directory="])
        for (o,a) in opts:
            if (o == optStr):
                return a
        
        if(optStr in args):
            return optStr

        return None
    except getopt.GetoptError as e:
        print("command line argument or option '"+optStr+" was not found. ", e.msg)    

#
# process command line options if any
#
def ProcessCmdLineOptions():
    """  process command line arguments especially the json parameters file

    Parameters
    ----------
    none
    
    Returns
    -------
    none
    """
    global currentDirectory
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:",['json=',"single_directory="])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)

            if o in ("--single_directory"):
                currentDirectory = a


    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: ", e.msg)

#
# process a json file to retrieve user parameters
#
def ProcessJSONFile(filename):
    """ process a json file to retrieve user parameters

    Parameters
    ----------   
    filename : str
             a path to the json file

    Returns
    -------
    none
    """
    global outputFileName,outputFile,netCDF4Variables
    global baseFilename,baseExtension,currentDirectory
    global outputBaseFileName,netCDF4StartDate,netCDF4EndDate,colorPlots
    try:
            data = OpenJSONFile(filename)

            if ( data["netCDF4Variables"] != None):
                netCDF4Variables = data["netCDF4Variables"]
            else:
                printUsage()
                raise Exception("\"netCDF4Variables\" not found")

            if ( data["colorPlots"] != None ):
                colorPlots = data["colorPlots"] 
            else:
                printUsage()
                raise Exception("\"colorPlots\" not found")

            if ( data["netCDF4EndDate"] != None ):
                netCDF4EndDate = data["netCDF4EndDate"]
            else:
                printUsage()
                raise Exception("\"netCDF4EndDate\" not found")


            if ( data["netCDF4StartDate"] != None ):
                netCDF4StartDate = data["netCDF4StartDate"]
            else:
                printUsage()
                raise Exception("\"netCDF4StartDate\" not found")


            if ( data["outputBaseFileName"] != None ):
                outputBaseFileName = data["outputBaseFileName"]
            else:
                printUsage()
                raise Exception("\"outputBaseFileName\" not found")

            if data["baseFilename"] != None:
                baseFilename = data["baseFilename"]
            else:
                printUsage()
                raise Exception("\"baseFilename\" not found")   

            if data["baseExtension"] != None:
                baseExtension = data["baseExtension"] 
            else:
                printUsage()
                raise Exception("\"baseExtension\" not found")   

            if "baseDirectory" in data:
                currentDirectory = data["baseDirectory"]
            else:
                printUsage()
                raise Exception("\"baseDirectory\" not found")   
            
            if "outputLogFile" in data:
                outputFileName = data["outputLogFile"]
            else:
                printUsage()
                raise Exception("\"outputLogFile\" not found")  

    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e) )
        printUsage()
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:", str(e))
        printUsage()

def WriteFileHeader():
    global outputFile,start,outputFileName
    msgSessionStarted ="session started on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print("===",file=outputFile)
    print(msgSessionStarted,file=outputFile)
    print("script name : "+sys.argv[0],file=outputFile)
    print("command line: "+''.join(sys.argv[1:]),file=outputFile )
    print("output File : "+outputFileName,file=outputFile)
    print("===",file=outputFile)

def OpenNetCDF4(path):
    """ given a path opens and returns a nectdf4 file object or none if fails

    Parameters
    ----------
    path : str
         a path to a netcdf4 file
    
    Returns
    -------

    a netcdf4 object or None
    
    """
    global nc,warningStr
    try:
        netCDF4File = nc.Dataset(path, "r", format="NETCDF4")
    except IOError as e:
        warningStr += "file '"+path+"' could not be open. "+str(e)+"\n"
    except Exception as e:
        warningStr+= "file '"+path+"' could not be open. "+str(e)+"\n"
    else:
        return netCDF4File

def MkDirectory(path):
    """ utility function to recusrsively create a tree file directory

    Parameters
    ----------

    path : str
         the absolute path name of the directory to create
    
    Returns
    -------

    none

    """
    global outputFile
    try:
        if ( not os.path.isdir(path) ):
            os.mkdir(path)
        if ( not os.path.isdir(path) ):
            PrintErrorAndExits("could not create directory '"+path+"'",outputFile)
    except Exception as e:
        PrintErrorAndExits(str(e),outputFile)
 

def ExtractCountTimeSeriesForVariable(year,day,timePeriod,var,name,data,netCDF4File):
    """ 
    extracts from a json object containig all informations to compute count time series from a netcdf4 variable in a netcdf4 file 
    stores in a png file the data for this variable as an histogram 

    Parameters
    ----------

    year : str
        the current year

    day : str
         the current day 
    timePeriod : str
        a string representation of a given time period for this variable
    var : str
        name of a netcdf4 variable
    data : dict
        a dictionary storing informations about time count series of variable var
    netCDF4File : dataset netcdf4 object
        the netcdf4 file object
    
    Returns
    -------

    none

    """
    global netCDFVariablesStats,netCDF4HistogramsVariables,currentPath,outputBaseFileName
    global outputFile

    path = os.path.join(currentPath,outputBaseFileName+day+"_histograms")
    MkDirectory(path)

    try:
        netCDF4File.set_auto_mask(False)
        varData = netCDF4File[var][:]
        # get counts for each unique values of var data
        unique, counts = np.unique(varData, return_counts=True)
        # now we get all bin values
        unique.sort()
        
        for i in range (unique[0],unique[len(unique) - 1]+1):
            data[var][year][day][timePeriod][str(i)] = 0
        j=0
        for i in unique:
            data[var][year][day][timePeriod][str(i)] = counts[j]
            j+=1
        data[var][year][day][timePeriod]['TotalCount'] = np.sum(counts)
        
        filePath = os.path.join(path,outputBaseFileName+year+"_"+day+"_"+timePeriod+"_histogram_"+var+".png")
        pl.hist(varData,bins='auto')
        pl.savefig(filePath)
        pl.clf()
    except Exception as e:
        print(str(e))
        PrintWARNING("ExtractCountTimeSeriesForVariable : a problem occured for variable '"+var+"' :"+str(e), outputFile)
    



def processSingleDirectory(var,directory):
    """ 
    process netcdf4 files in a single directory 
    get a list of all files matching a given pattern
    extracts informations from path to build a dictionary
    dictionary contains for each variable
    the time series is specific to a netcdf4 variable 'var', a year and a day
    * the year as a number
    * the day as a number (0;364)
    * the time period time for example [20081123T052956Z-20081123T062608Z]
    * the bins to compute time series for. each bin corresponding to a unique velue in the data array of var 'var'
    * mean computed for a given time period a givne day and a given year and across all years


    Parameters
    ----------

    directory : str
              name of current cirectory to be processed
    
    Returns
    -------

    none 

    """
    global warningStr,outputFile,currentPath,outputBaseFileName,netCDF4StartDate,netCDF4EndDate
    if (directory == None ):
        PrintError("no baseDirectory provided. Aborting...",outputFile)
        printUsage()
        return
    if not os.path.isdir(directory):
        PrintError("directory '"+directory+"' not a proper directory.",outputFile)
        return
    matchingPattern = os.path.join(directory,baseFilename+"."+baseExtension)
    
    listSubDirs = iter([x for x in os.listdir(directory) if os.path.isdir( os.path.join(directory,x) )])
    data = dict()
    data[var] = dict()
    strt = time.time()
    day = None
    year = None
    total = dict()
    for subDir in listSubDirs:
        start = time.time()
        currentPath = os.path.join(directory,subDir)
        # extract day
        ll = currentPath.split(os.path.sep)
        day = ll[ len(ll) - 1]
        year = ll[ len(ll) - 2]
        if not (year in data[var]):
            data[var][year] = dict()
        if not (day in data[var][year]):
            data[var][year][day] = dict()

        matchingPattern = os.path.join(currentPath,baseFilename+"."+baseExtension)
        listFiles = glob.iglob(matchingPattern) 
        startDate = None
        endDate = None
        for f in listFiles:

            name = os.path.splitext(os.path.basename(f))[0]
            warningStr =""
            netCDF4File = OpenNetCDF4(f)
            if ( netCDF4File == None or warningStr ):
                PrintWARNING("file '"+f+"' could not be open. skipping.",outputFile)
                continue
            netCDF4File.set_auto_mask(False)
            # check start date format is as expected
            if netCDF4File.__dict__[netCDF4StartDate] !=None:
                startDate = netCDF4File.__dict__[netCDF4StartDate]
            else:
                PrintError(" tag '"+netCDF4StartDate+"' not found in netCDF4 file meta data.",outputFile)
                return
            if netCDF4File.__dict__[netCDF4EndDate] !=None:
                endDate = netCDF4File.__dict__[netCDF4EndDate]
            else:
                PrintError(" tag '"+netCDF4EndDate+"' not found in netCDF4 file meta data.",outputFile)
                return
            
            timePeriod = "["+startDate+"-"+endDate+"]"
            data[var][year][day][timePeriod] = dict()
            ExtractCountTimeSeriesForVariable(year, day,timePeriod,var,name,data,netCDF4File)
            netCDF4File.close()
        # compute mean for one day
        dayDataTimePeriod = data[var][year][day]
        numValues = 0
        for tp in dayDataTimePeriod:
            for bin in data[var][year][day][tp]:
                try:
                    total[bin] = 0
                except Exception as e:
                    continue
            numValues += 1

        for tp in dayDataTimePeriod:
            for bin in data[var][year][day][tp]:
                if ( bin == 'TotalCount'):
                    bb = data[var][year][day][tp][bin]
                    data[var][year][day][tp][bin] = str(bb)
                    continue
                bb = data[var][year][day][tp][bin]
                data[var][year][day][tp][bin] = str(bb)
                total[bin]+=bb   
        data[var][year][day]["mean"]=dict()
        for bin in total:
            if ( bin == 'TotalCount'):
                continue
            data[var][year][day]["mean"][bin] = total[bin]/numValues
        end = time.time()
        PrintCurrentTask("variable '"+var+"' : day = '"+day+"'  time elapsed in seconds "+str(end-start),outputFile)
        print("variable '"+var+"' : day = '"+day+"'  time elapsed in seconds "+str(end-start))
    for year in data[var]:
        data[var][year]["bins"] = dict()
        for bin in total:
            data[var][year]["bins"][bin] = dict()
            data[var][year]["bins"][bin]["mean"] = 0
        
    for year in data[var]:
        numValues = 0
        
        for day in data[var][year]:
            try:
                for bin in data[var][year][day]["mean"]:
                    data[var][year]["bins"][bin]["mean"] += data[var][year][day]["mean"][bin]
                numValues += 1
            except Exception as e:
                continue
            
        nBins = 0
        data[var][year]["mean"] = 0
        for bin in data[var][year]["bins"]:
            try:
                data[var][year]["bins"][bin]["mean"] /= numValues
                nBins += 1
                data[var][year]["mean"] += data[var][year]["bins"][bin]["mean"]
            except Exception as e:
                continue
        data[var][year]["mean"] /= nBins

    # compute mean across all years
    yearsCount = 0.
    for year in data[var]:
        yearsCount +=1
    data[var]["mean"] = 0.
    for year in data[var]:
        try:
            data[var]["mean"] += data[var][year]["mean"]
        except Exception as e:
            continue

    data[var]["mean"] /= yearsCount
    #
    #now for the yearly temporal graphes for each bin
    graphs = dict()

    numGraphs = 0
    for bin in total:
        graphs[bin] = dict()
        graphs[bin]['x'] = []
        graphs[bin]['y'] = []
        numGraphs +=1



    for day in data[var][year]:
        if ( day == 'mean'):
            continue
        for bin in graphs:
            graphs[bin]['x'].append( day )
            if not('mean' in data[var][year][day]) or not ( bin in data[var][year][day]["mean"] ):
                graphs[bin]['y'].append( 0)
            else:
                graphs[bin]['y'].append( float(data[var][year][day]["mean"][bin]) )

    

    fig,axes = pl.subplots(numGraphs,sharex=True, sharey=True, gridspec_kw={'hspace': 0})
    fig.suptitle("'"+var+"' mean",fontsize=8)
    i = 0
    j = 0
    pl.xlabel("day",fontsize=6)
    pl.ylabel('mean',fontsize=6)
    pl.rc('xtick',labelsize=2)
    pl.rc('ytick',labelsize=2)

    for bin in graphs:
        axes[i].plot(graphs[bin]['x'],graphs[bin]['y'],'go--', linewidth=1, markersize=1,color=colorPlots[j])
        axes[i].set_title(bin)
        i+=1
        j+=1
        if ( j == len(colorPlots)):
            j=0

    pl.savefig(outputBaseFileName+"_graph.png")

    # save dictionary to binary file
    outJSONFile = open(outputBaseFileName+"_data.json","wt")
    allDict = dict()
    allDict["data"] = data
    allDict["graphs"] = graphs
    json.dump(allDict,outJSONFile)
    nd = time.time()
    PrintCurrentTask("variable '"+var+"' :year time elapsed in seconds "+str(nd-strt),outputFile)
    PrintSuccess()
###################################################################################################################################
#
#
#
#  script starts from now
#
#
# global variables
#
# Time offset in WW3 NetCDF files
ww3_toffset = '1990-01-01 00:00:00'
ExpectednetCDF4TimeStamps =[] # time stamps expected from time meta data
start = datetime.today()

startTime = time.time()

timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")

colorPlots=["red", "black", "grey", "yellow","blue"]

outputFile = None
outputFileName = None
tmpFileName = None
#
# check termcolor module is available
#





currentDirectory = None

currentPath=""
baseFilename = None
baseExtension = "nc"
errorStr = ""
warningStr =""

currentCheck =""
subDirectories = []

outputBaseFileName = None
netCDF4StartDate=None
netCDF4EndDate=None
netCDF4Variables = ['swh_quality']

ProcessCmdLineOptions()

if ( outputFileName == None):
    outputFileName = "output.log"
filename, file_extension = os.path.splitext(outputFileName)
outputFileName = tmpFileName = filename+"_"+timeStr+file_extension

outputFile = open(outputFileName,'w')


#
# check netCDF4 module : mandatory
#
nc = None
msg = "Checking for NetCDF4 python module..."
PrintCurrentTask(msg,outputFile)
netCDF4_found = importlib.util.find_spec("netCDF4") != None
if not netCDF4_found:
    PrintErrorAndExits("netCDF4 module not found",outputFile)
import netCDF4  as nc
PrintSuccess()

WriteFileHeader()
for var in netCDF4Variables:
    processSingleDirectory(var,currentDirectory)
PrintFileFooter(outputFile)








