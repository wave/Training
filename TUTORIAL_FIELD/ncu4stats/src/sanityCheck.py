#!/usr/bin/python
import sys,getopt,gc
import os,glob
import importlib
import json
import numpy as np

from datetime import datetime
from string import Formatter
from datetime import timedelta
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
from dateutil import parser as dateUtilsParser

from scipy import stats
import time
import math
import pandas

#
# utility functions
#
#


def OpenJSONFile(filename):
    """  open a JSON file and return the json object read from this file or None

    Parameters
    ----------
    filename : str
             the absolute path of the json file
    
    Returns
    -------
    none
    """
    global outputFile
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args,file= outputFile)
        printUsage()
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
        printUsage()
    else:
        return data
    return None


#function to indicate test passed
def PrintSuccess():
    print('[PASSED]',file=outputFile)

def PrintFailed():
    print('[FAILED]',file=outputFile)

#function to indicate failure
def PrintError(errStr,outFile):
    print( "[ERROR] "+errStr ,file=outFile)


#function to indicate failure and exit
def PrintErrorAndExits(errStr, outFile):
    PrintError(errorStr,outFile)
    PrintFileFooter(outFile)
    sys.exit(-1)


def PrintFileFooter(outFile):
    global startTime
    endTime = time.time()
    elapsedTime = endTime-startTime
    print("Total elapsed time :",elapsedTime,file=outFile)
    end = datetime.today()
    msgSessionEnded ="session ended on "+end.strftime("%B %d, %Y")+" "+end.strftime("%H:%M:%S")
    print(msgSessionEnded,file=outFile)
    print("Exiting...",file=outFile)
    outFile.close()


def ExitsWithErrorMessage(errStr,outFile):
    print( "[ERROR] : "+errStr,file=outFile )
    PrintFileFooter(outFile)
    sys.exit(-1)


def PrintWARNING(warningStr,outFile):
    print("[WARNING] : "+warningStr,file=outFile)



#function to print current task message
def PrintCurrentTask(message,outFile):
    print("[INFO]"+message, end='',file=outFile )


def printUsage( exitNow = False ):
    print("usage : python3 sanityCheck.py --json=path_to_json_file")
    print(" json file sample is provided as ")
    print("{")
    print(" \"baseFilename\": \"RSCD_WW3-RSCD*\",")
    print(" \"baseExtension\": \"*nc\,")
    print(" \"ExpectedSubDirectories\": \"None\,")
    print(" \"netCDF4Dimensions\": [\"time\", \"level\"],")
    print(" \"netCDF4Variables\": \"all\",")
    print(" \"netCDFVariableConsistency\": {\"time\": {\"monotonous\": true, \"doublons\": true, \"correctness\": true}},")
    print(" \"netCDF4SpatialCoordinates\": [\"longitude\", \"latitude\"],")
    print(" \"singleFile\": \"None\",")
    print(" \"timeTags\": [\"start_date\", \"stop_date\"],")
    print(" \"baseDirectory\": \"None\",")
    print(" \"singleDirectory\": \"path_to_a_directory\",")
    print(" \"outputLogFile\": \"path/output.log\"")
    print(" \"storeStatsAsJson\": false")
    print("}")
    print("exiting...")
    if exitNow:
        sys.exit(-1)
def ProcessCmdLineOptions():
    """ Process command line options. currently only the json file containing parameters
    Parameters
    ----------   
    none

    Returns:
    none

    """
    global currentDirectory,outputFile
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        if (len(l) == 0):
            print("no json file provided.")
            printUsage()
            return
        opts,args = getopt.getopt(l,"j:h:",['json=','help'])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)
            if ( o in("-h","--help")):
                printUsage(True)
    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: "+ e.msg,file=outputFile)
        printUsage()
    except Exception as e:
        print("an error occured while reading json file : "+ str(e),file= outputFile)
        printUsage()
#
# 
#
def ProcessJSONFile(filename):
    """ process a json file to retrieve user parameters

    Parameters
    ----------   
    filename : str
             a path to the json file

    Returns
    -------
    none
    """
    global ExpectedSubDirectories, netCDFSubDirectoriesNames, mpiDirectories,currentDirectory
    global outputFileName,outputFile,ExpectednetCDFDimensions,netCDFVariableConsistency
    global ExpectednetCDFVariables,baseFilename,baseExtension,netCDF4HistogramsVariables,netCDF4SpatialCoordinates
    global singleFile,timeTags,outputFile,singleDirectory,outputDirectory,storeStatsAsJson
    try:
        
            data = OpenJSONFile(filename)

            if 'singleDirectory' in data and data['singleDirectory'] != "None":
                singleDirectory = data['singleDirectory']
            
            if 'timeTags' in data :
                timeTags = data["timeTags"]

            if ( 'outputLogFile' and data["outputLogFile"] != None):
                outputFileName = data["outputLogFile"]

            if ( data["netCDF4SpatialCoordinates"] != None):
                netCDF4SpatialCoordinates = data["netCDF4SpatialCoordinates"]

            if ( 'outputDirectory' in data ):
                outputDirectory = data["outputDirectory"]

            if "singleFile" in data and data["singleFile"] != "None":
                singleFile = data["singleFile"]

            if "baseFilename" in data:
                baseFilename = data["baseFilename"]
            else:
                PrintError(" no 'baseFilename' provided in json file",outputFile)
                printUsage()
                

            if "baseExtension" in data:
                baseExtension = data["baseExtension"] 
            else:
                PrintErrorAndExits(" no 'baseExtension' provided in json file",outputFile)

            if "ExpectedSubDirectories" in data and data["ExpectedSubDirectories"] != "None":
                ExpectedSubDirectories = data["ExpectedSubDirectories"]
                if ( isinstance(ExpectedSubDirectories,list)):
                    ExpectedSubDirectories.sort()
            else:
                ExpectedSubDirectories = "None"
        
            if "baseDirectory" in data:
                currentDirectory = data["baseDirectory"]
                
            if ( "netCDF4Dimensions" in data and isinstance(data["netCDF4Dimensions"],list)):
                ExpectednetCDFDimensions = data["netCDF4Dimensions"]

            if ( "netCDF4Variables" in data):
                if isinstance(data["netCDF4Variables"],list):
                    ExpectednetCDFVariables = data["netCDF4Variables"]
                elif data["netCDF4Variables"] != "all":
                    PrintErrorAndExits("'netCDF4Variables' provided not recognized in json file",outputFile)
                else:
                    ExpectednetCDFVariables = "all"
            else:
                PrintError(" no 'netCDF4Variables' provided in json file",outputFile)
                printUsage()

            if ( "netCDFVariableConsistency" in data and isinstance(data["netCDFVariableConsistency"],dict)):
                netCDFVariableConsistency = data["netCDFVariableConsistency"] 

            if ( 'storeStatsAsJson' in data):
                storeStatsAsJson = data['storeStatsAsJson']

    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e),file=outputFile )
        printUsage()
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:"+ str(e),file=outputFile)
        printUsage()


# function to scan a directory

def GetDirectoryContent(dir):
    """ retrieve sub directories content of a given directory

    Parameters
    ----------

    dir : str
        name of the direcotyr we want retrieve the sub directories
    
    Returns
    -------
    none

    """
    global errorStr
    listDir = []
    if not os.path.exists(dir) or not os.path.isdir(dir) :
        errorStr = "directory '"+dir+"' not found. exiting"
        return listDir
    listDir =   os.listdir(dir)
    # filter only directories 
    listDir = FilterSubDirectoriesFrom(dir,listDir)
    return listDir



def GetFileContent(dir):
    """ get files content of a given directory

    Parameters
    ----------

    dir: str
         name of dir we retrieve to content from

    Returns
    -------
    none

    """
    global errorStr,baseFilename,baseExtension
    listFiles = []
    if not os.path.exists(os.path.abspath(dir) ) or not os.path.isdir(os.path.abspath(dir) ) :
        errorStr = "directory '"+dir+"' not found. exiting"
        return listFiles
    matchingPattern = os.path.join(dir,baseFilename+"."+baseExtension)
    listDir = glob.glob(matchingPattern) #os.listdir(dir)
    # filter only files 
    listDir = FilterFilesFrom(dir,listDir)
    listDir.sort()
    return listDir

# function that filters sub directories from a list of elements
def filterSubDirectory(element):
    global warningStr,AllExpectedDirectories
    return os.path.isdir( os.path.abspath(element) )

def filterFileDirectory(element):
    global warningStr,AllExpectedDirectories
    return os.path.isfile( os.path.abspath(element) )

def FilterFilesFrom(baseDirectory, elements):
    os.chdir(baseDirectory)
    l=list(filter(filterFileDirectory,elements))
    l.sort()
    return l

def FilterSubDirectoriesFrom(baseDirectory, elements):
    currentPath = os.curdir
    os.chdir(baseDirectory)
    l=list(filter(filterSubDirectory,elements))
    l.sort()
    os.chdir(currentPath)
    return l

#
# 
# dirs contains a list a subdirectories to scan
# 

def TestDirectoryStructure(parentDirectory,directory,level):
    """ analyzes recursively a tree directory and check current sub directories against an expected list contained in ExpectedSubDirectories

    Parameters
    ----------
    
    parentDirectory : str
                    parent directory of current directory
    directory : str
              current directory
    level : int
          depth of tree file starting from the first call of this recursive function

    Returns
    -------

    none

    """
    global ExpectedSubDirectories,outputFile

    if level == len(ExpectedSubDirectories):
        return

    if  not os.path.exists(os.path.join(parentDirectory,directory)) :
        PrintWARNING( "'"+os.path.join(parentDirectory,directory)+"' does not exist.",outputFile)
        return
    dirs = os.listdir(directory)

    subdirs = [item for item in dirs if (os.path.isdir(os.path.join(directory,item))) ] # and item in ExpectedSubDirectories[level])]
    subdirs.sort()
    difference = [item for item in subdirs if item not in ExpectedSubDirectories[level]]

    if len(difference) != 0 :
        PrintWARNING(" directory '"+os.path.join(parentDirectory,directory)+"' structure partially matches expected directory structure",outputFile)
        warningStr = " directories "
        for d in difference:
            warningStr += "'"+d+"' "
        warningStr += "were expected but not found."
        PrintWARNING(warningStr,outputFile)
        #print(" directory '"+os.path.join(parentDirectory,directory)+"' structure partially matches expected directory structure")
        #print(warningStr)


    level += 1
    for d in subdirs:
        TestDirectoryStructure(directory,d,level)
    

def WriteFileHeader():
    global outputFile,start,outputFileName
    msgSessionStarted ="session started on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print("===",file=outputFile)
    print(msgSessionStarted,file=outputFile)
    print("script name : "+sys.argv[0],file=outputFile)
    print("command line: "+''.join(sys.argv[1:]),file=outputFile )
    print("output File : "+outputFileName,file=outputFile)
    print("===",file=outputFile)

def Process():
    """ main function when a recursive analysis of a base directory is required.

    Parameters
    ----------
    none

    Returns
    -------

    none

    """
    global currentDirectory,currentCheck,subDirectories,outputFile,warningStr,checkDirectoryTree
    global totalNumberOfFilesToProcess

    if ( currentDirectory == None):
        PrintError(" no current directory provided.",outputFile)
        return

    if ( ExpectedSubDirectories != None and ExpectedSubDirectories != "None"):
        currentCheck = "scanning directory '"+currentDirectory+"'..."
        # retrieve directory content
        subDirectories = GetDirectoryContent(currentDirectory)

        if  ExpectedSubDirectories !=  len(subDirectories) == 0 :
            PrintCurrentTask(currentCheck,outputFile)
            PrintError(" no sub directories found.",outputFile)
            return
        else:
            PrintCurrentTask(currentCheck,outputFile)
            PrintSuccess()
#
# testing number of sub directories 
#
# scan each sub directory structure
#
    if ( checkDirectoryTree):
        if not os.path.isdir(currentDirectory):
            print("[ERROR] provided base directory '"+currentDirectory+"' not found.",file=outputFile)
            sys.exit(-1)

        currentCheck = "scanning structure of sub directories in '"+currentDirectory+"'..."
        warningStr = ""
        level = 0
        TestDirectoryStructure(os.path.dirname(currentDirectory),  currentDirectory,level)
        PrintCurrentTask(currentCheck,outputFile)
        PrintSuccess()

        """
        if not TestSubDirectoryStructure(subDirectories):
            PrintCurrentTask(currentCheck,outputFile)
            PrintWARNING(errorStr,outputFile)
        else:
            PrintCurrentTask(currentCheck,outputFile)
            PrintSuccess()   
        if ( warningStr ):
            PrintWARNING(warningStr,outputFile)
        """
    level = 0
    ProcessNetCDF4Files(level, os.path.dirname(currentDirectory),  currentDirectory)


def BuildUtilityDictionaries(netCDF4File):
    """ 
        builds utility dictionaries to map netcdf4 dimensions to variables and map axis index to variable names

        Parameters
        ----------
        netCDF4File : dataset object
                    object representing netcdf4 file

        Returns
        -------

        none

    """
    global ExpectednetCDFVariables, netCDFDimensions,netCDFVariables,outputFile,axisByname
    global netCDF4SpatialCoordinates

    netCDFDimensions.clear()
    vars = netCDF4File.variables

    #get mapping between spatial coordinates and dimension
    for v in vars:
        if ( (not v in netCDF4SpatialCoordinates)):
            continue
        dims = vars[v].dimensions
        for i in range(0, len(dims) ):
                if not dims[i] in netCDFDimensions:
                    netCDFDimensions[dims[i]] = []
                netCDFDimensions[dims[i]].append(v)

    try:

        netCDFVariables.clear()        

        for v in vars :
            if v in netCDF4SpatialCoordinates:
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            i = 0
            axisByname[v] = dict()
            for d in dims:
                if d in netCDFDimensions:
                    axisByname[v][d] = i
                    for vv in netCDFDimensions[d]:
                        axisByname[v][vv] = i
                else:
                    axisByname[v][d] = i
                #axisByname[d] = i
                i+=1

        for v in vars :
            if ( v in netCDF4SpatialCoordinates or v == 'time' ):
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            
            netCDFVariables[v]=[]

            for d in dims:
                l=[]
                if d in netCDFDimensions:
                    
                    for dd in netCDFDimensions[d]:
                        l.append(dd)
                    netCDFVariables[v].append(l)
                else:
                    l.append(d)
                    netCDFVariables[v].append(l)
    except Exception as e:
        print(str(e),file=outputFile)
    else:                   
        PrintCurrentTask("building utility dictionaries...",outputFile)
        PrintSuccess()


def ComputeTimeStamps(date1, date2,strfreq):
    """ 
        given a start date and end date computes a sorted list of time stamps

        Parameters
        ----------
        date1 : datetime  
                start date
            
        date2 : datetime
                end date


        Returns
        -------
        tS : list
            a list of time stamps

    """
    global outputFile
    sfreq = 'H'
    if ( "dayly" in strfreq):
        sfreq='D'
    elif ("hourly" in strfreq):
        sfreq = 'H'
    elif("secondly" in strfreq):
        sfreq = 'S'
    else:
        print("[ERROR] time frequency to compute date range not interpreted successfully. results are undefined",file=outputFile)
    if '-' in strfreq:
        sfreq = strfreq[0]+sfreq
    dates = pandas.date_range(date1, date2, freq=sfreq).to_pydatetime()
    tS = []
    for d in dates:
        tS.append( datetime.timestamp(d) )
    tS.sort()
    return tS



def OpenNetCDF4(path):
    """ given a path opens and returns a nectdf4 file object or none if fails

    Parameters
    ----------
    path : str
         a path to a netcdf4 file
    
    Returns
    -------

    a netcdf4 object or None
    
    """

    global outputFile
    netCDF4File = None
    try:
        netCDF4File = nc.Dataset(path, "r", format="NETCDF4")

        BuildUtilityDictionaries(netCDF4File)

    except IOError as e:

        print("file '"+path+"' "+str(e),file=outputFile)
        print("ERROR file '"+path+"' "+str(e))
    except Exception as e:
        print("file '"+path+"' "+str(e),file=outputFile)
        print("file '"+path+"' "+str(e))
    else:
        return netCDF4File


def MkDirectory(path):
    """ utility function to recusrsively create a tree file directory

    Parameters
    ----------

    path : str
         the absolute path name of the directory to create
    
    Returns
    -------

    none

    """
    try:
        if not path or os.path.exists(path):
            return
        parent = os.path.abspath(os.path.join(path, '..'))
        if not os.path.exists(parent):
            MkDirectory(parent)
        os.mkdir(path)
        if ( not os.path.isdir(path) ):
            print("could not create directory '"+path+"'")
    except Exception as e:
        print(str(e))




def ComputeMaskedArrayMinMaxSequentially(fillValue,netCDF4File,v,var,listobj,objStat):
    """ 
    alternative method to compute stats from a netcdf4 file when a memory error occurs.
    it streams file with respect to time variable, loading one slice of data at a time

    Parameters
    ----------
    fillValue : float
              fill value of masked array associated to variable v
    
    netCDF4File : netcdf4 dataset 
            a nectcdf4 dataset object
    
    v : str
      the current variable name

    listobj : str
            a json object which will contain the stats of variables on which v depends
    objStat : a json object 
            contain the stats of v

    Returns
    -------
    listobj and objStat
    """
    global outputFile,storeStatsAsJson
    try:
        netCDF4File.set_auto_mask(True)
        netCDF4File.set_auto_scale(True)
        print("[INFO] computing MaskedArray MinMax Sequentially",file=outputFile)
        shape = var.shape
        globalMin = sys.float_info.max
        globalMinIndice = -1
        globalMaxIndice = -1
        indexMin = -1
        indexMax = -1
        norm = 1./float(shape[0])
        mean = 0.
        stdDeviation = 0.
        globalMax = sys.float_info.min
        mean = 0
        stdDeviation = 0
        
        for t in range(0,shape[0]):
            x = np.ma.MaskedArray(var[t,:],fill_value=fillValue)
            minV = np.ma.amin(x)
            mean += np.mean(x)
            stdDeviation += np.std(x)
            minI = np.unravel_index(np.argmin(x, axis=None), x.shape)
            if minV < globalMin:
                globalMin = minV
                globalMinIndice = minI
                indexMin = t
            
            maxV = np.amax(x)
            maxI = np.unravel_index(np.argmax(x, axis=None), x.shape)
            if maxV > globalMax:
                globalMax = maxV
                globalMaxIndice = maxI
                indexMax = t
            del x
        mean *= norm
        stdDeviation *= norm
        CheckValidMean(minV,maxV,mean)
        if storeStatsAsJson:
            objStat = {"min":str(globalMin),"max":str(globalMax),"mean":str(mean),"std deviation":str(stdDeviation)}

        strDump= "[STATS] variable:"+v+" min:"+str(globalMin)+" max:"+str(globalMax)+" mean:"+str(mean)+" std deviation:"+str(stdDeviation)
        strMin="[STATS] corresponding minimum "+v+" for "
        strMax="[STATS] corresponding maximum "+v+" for "
        i=-1
        l = None
        for dimension in var.dimensions:
            if i == -1:
                strMin +="dimension '"+dimension+"' min at "+str(indexMin)+" "
                strMax +="dimension '"+dimension+"' max at "+str(indexMax)+" "

                if storeStatsAsJson:
                    listobj[dimension]= { "min":str(indexMin),"max":str(indexMax)}

                l = netCDFVariables[v][0]
            else:   

                if storeStatsAsJson:
                    listobj[dimension]= { "min":str(globalMinIndice[i]),"max":str(globalMaxIndice[i])}

                strMin +="dimension '"+dimension+"' min at "+str(globalMinIndice[i])+" "
                strMax +="dimension '"+dimension+"' max at "+str(globalMaxIndice[i])+" "
                l = netCDFVariables[v][i+1]
            
            data = None

            for vv in l:
                if ( vv in netCDF4File.variables):
                    #print("[INFO] allocating data for "+vv,file=outputFile)
                    data = netCDF4File[vv][:].flatten()
                else:
                    #PrintWARNING("'"+vv+"' not found as variable. Aborting...",outputFile)
                    #print("'"+vv+"' not found. Aborting...")
                    #print(msg+"[FAILED]")
                    #aborted = True
                    i+=1
                    continue
                if i == -1:
                    valuem = data[indexMin]
                    valueM = data[indexMax]
                    strMin+=str(vv)+"["+str(indexMin)+"]:"+str(valuem)+"; "
                    strMax+=str(vv)+"["+str(indexMax)+"]:"+str(valueM)+"; "
                else:
                    valuem = data[globalMinIndice[i]]
                    valueM = data[globalMaxIndice[i]]
                    strMin+=str(vv)+"["+str(globalMinIndice[i])+"]:"+str(valuem)+"; "
                    strMax+=str(vv)+"["+str(globalMaxIndice[i])+"]:"+str(valueM)+"; "
                    
                    if storeStatsAsJson:
                        listobj[vv]= { "min":str(valuem),"max":str(valueM)}

                del data
            i+=1
        print(strDump,file=outputFile)
        print(strMin,file=outputFile)
        print(strMax,file=outputFile)
    except Exception as e:
        print("[WARNING] error occured while computing sequentially min/max for masked array : "+str(e),file= outputFile)
    


def CheckMinMaxnetCDFVariables(netCDF4File,path,filename):
    """ 
    given a netcdf4 dataset object, a path and the filename of netcdf4 file computes min/max and mean/stdDev
    of variables expected from ExpectednetCDFVariables
    
    Parameters
    ----------

    netCDF4File : netcdf4 dataset object
                netcdf4 object
    path : str
         a name of the path where to store stats json files
    filename : str
        the name of absolute path of the netcdf4 file
    
    Returns
    -------

    none

    """
    
    global netCDFVariables,np,currentDirectory,netCDFDimensions,outputFile,netCDFVariablesStats
    global netCDF4SpatialCoordinates,outputDirectory,storeStatsAsJson
    strDump=""
    
    print("[DEBUG] starting min/max for file '"+filename+"'",file = outputFile)

    if storeStatsAsJson :
        path = os.path.join(path,"stats")

        if ( outputDirectory ):
            print("out put directory "+outputDirectory,file=outputFile)
            path = os.path.join(outputDirectory,"stats")
        MkDirectory(path)

        if storeStatsAsJson:
            netCDFVariablesStats = dict()

    if ExpectednetCDFVariables == "all":
        Variables = netCDF4File.variables
    else:
        Variables = ExpectednetCDFVariables

    listobj = None
    objStat = None
    for v in Variables:
        if not(v in netCDF4File.variables ):
            continue

        if v in netCDF4SpatialCoordinates or v == 'time' or v == 'TIME':
            continue
        var = netCDF4File[v]

        if not ('float' in var.datatype.name or  'int' in var.datatype.name) :
                continue
        msg = "computing min/max and std deviation for variable '"+v+"'..."

        print("[DEBUG] "+msg,file = outputFile)

        print(msg,file=outputFile)
        a = None
        minV = None
        minI = None
        maxV = None
        maxI = None
        mean = 0
        stdDeviation = 0
        if storeStatsAsJson:
            listobj =dict()
            netCDFVariablesStats[v] = dict()

        try:
            fillValue = None
            scaleFactor = 1
            try:
                scaleFactor = var.scale_factor
            except Exception:
                scaleFactor = 1
            try:
                fillValue =float(var._FillValue)
                fillValue *= scaleFactor
            except Exception:
                fillValue = None
            if fillValue:
                print("[INFO] masked array with fill value detected for '"+v+"' fill value = "+str(fillValue),file=outputFile)
                netCDF4File.set_auto_mask(False)
                netCDF4File.set_auto_scale(True)
                try:
                    a = var[:]
                except Exception as e:
                    print("[ERROR] fill value detected  : '"+v+"' data not retrieved "+str(e),file=outputFile)

                print("[INFO] variable '"+v+"' data shape : "+str(a.shape),file=outputFile)

                if ( a.size == 1 or str(str(a.shape)) == "(1,)"):
                    print("[INFO] variable '"+v+"' just contains one value. skipping...",file=outputFile)
                    continue

                CheckValidValues(var,a,fillValue)

                try:
                    print("[INFO] computing min/max from linearized data",file=outputFile)
                    aMin = np.where( abs(a - fillValue) > 0.00001,a,sys.float_info.max)
                    aLinear = aMin.flatten()
                    index = np.argmin(aLinear)
                    del aLinear
                    minLinearized = GetElementFormLinearizedIndice(aMin,index)
                    print("[CHECK] ----> min computed from linearized data "+str(f'{minLinearized:.2f}'),file=outputFile)
                    minV = aMin.min() #np.amin(aMin)
                    print("[CHECK] ----> min computed from original data   "+str(f'{minV:.2f}'),file=outputFile)
                    minI = list(np.unravel_index(np.argmin(aMin, axis=None), aMin.shape))
                    del aMin

                    aMax = np.where( abs(a - fillValue) > 0.00001,a,sys.float_info.min)
                    aLinear = aMax.flatten()
                    index = np.argmax(aLinear)
                    del aLinear
                    maxLinearized = GetElementFormLinearizedIndice(aMax,index)
                    print("[CHECK] ----> max computed from linearized data "+str(f'{maxLinearized:.2f}'),file=outputFile)
                    maxV = aMax.max() #np.amax(aMax)
                    print("[CHECK] ----> max computed from original data   "+str(f'{maxV:.2f}'),file=outputFile)
                    maxI = list(np.unravel_index(np.argmax(aMax, axis=None), aMax.shape))
                    del aMax
                    del a
                    netCDF4File.set_auto_mask(True)
                    netCDF4File.set_auto_scale(True)
                    x = np.ma.MaskedArray(var[:],fill_value = fillValue)
                    mean = x.mean()
                    stdDeviation = x.std()
                    print("[CHECK] ----> mean "+str(f'{mean:.3f}')+" std deviation "+str(f'{stdDeviation:.3f}'),file=outputFile)
                    del x
                    CheckValidMean(minV,maxV,mean)
                    if storeStatsAsJson:
                        objStat = {"min":str(minV),"max":str(maxV),"mean":str(mean),"std deviation":str(stdDeviation)}
                    try:
                        a = var[:]
                    except Exception as e:
                        print("[ERROR] fill value detected  : '"+v+"' data not retrieved "+str(e),file=outputFile)

                except Exception as e:
                    print("[WARNING] bypassing exception raised :"+str(e),file=outputFile)
                    ComputeMaskedArrayMinMaxSequentially(fillValue,netCDF4File, v,var,listobj,objStat)
                    PrintCurrentTask(msg,outputFile)
                    PrintSuccess()
                    continue
            else: 
                try:
                    a = var[:]
                except Exception as e:
                    print("[ERROR] no fill value detected  : '"+v+"' data not retrieved "+str(e),file=outputFile)

                print("[INFO] variable '"+v+"' data shape : "+str(a.shape),file=outputFile)
                if ( a.size == 1 or str(str(a.shape)) == "(1,)"):
                    print("[INFO] variable '"+v+"' just contains one value. skipping...",file=outputFile)
                    continue

                CheckValidValues(var,a,None)
                minV = np.amin(a)
                minI = list(np.unravel_index(np.argmin(a, axis=None), a.shape))
                maxV = np.amax(a)
                maxI = list(np.unravel_index(np.argmax(a, axis=None), a.shape))
                mean = np.mean(a)
                stdDeviation = np.std(a)
                CheckValidMean(minV,maxV,mean)
                if storeStatsAsJson:
                    objStat = {"min":str(minV),"max":str(maxV),"mean":str(mean),"std deviation":str(stdDeviation)}

            print("[CHECK] ----> min obtained from aMin "+str(minV)+" from aMax "+str(maxV),file=outputFile)
        except Exception as e:
            a = var[:]
            shape = var.shape
            globalMin = sys.float_info.max
            globalMinIndice = -1
            globalMaxIndice = -1
            indexMin = -1
            indexMax = -1
            norm = 1./float(shape[0])
            mean = 0.
            stdDeviation = 0.
            globalMax = sys.float_info.min
            for t in range(0,shape[0]):

                a=var[t,:]
                minV = np.amin(a)
                mean += np.mean(a)
                stdDeviation += np.std(a)
                minI = np.unravel_index(np.argmin(a, axis=None), a.shape)
                if minV < globalMin:
                    globalMin = minV
                    globalMinIndice = minI
                    indexMin = t
                
                maxV = np.amax(a)
                maxI = np.unravel_index(np.argmax(a, axis=None), a.shape)
                if maxV > globalMax:
                    globalMax = maxV
                    globalMaxIndice = maxI
                    indexMax = t
            mean *= norm
            stdDeviation *= norm
            CheckValidMean(minV,maxV,mean)
            if storeStatsAsJson:
                objStat = {"min":str(globalMin),"max":str(globalMax),"mean":str(mean),"std deviation":str(stdDeviation)}
            
            strDump= "[STATS] variable:"+v+" min:"+str(globalMin)+" max:"+str(globalMax)+" mean:"+str(mean)+" std deviation:"+str(stdDeviation)
            strMin="[STATS] corresponding minimum "+v+" for "
            strMax="[STATS] corresponding maximum "+v+" for "
            i=-1
            l = None
            for dimension in var.dimensions:
                if i == -1:
                    strMin +="dimension '"+dimension+"' min at "+str(indexMin)+" "
                    strMax +="dimension '"+dimension+"' max at "+str(indexMax)+" "

                    if storeStatsAsJson:
                        listobj[dimension]= { "min":str(indexMin),"max":str(indexMax)}

                    l = netCDFVariables[v][0]
                else:  

                    if storeStatsAsJson: 
                        listobj[dimension]= { "min":str(globalMinIndice[i]),"max":str(globalMaxIndice[i])}

                    strMin +="dimension '"+dimension+"' min at "+str(globalMinIndice[i])+" "
                    strMax +="dimension '"+dimension+"' max at "+str(globalMaxIndice[i])+" "
                    l = netCDFVariables[v][i+1]
                
                data = None

                for vv in l:
                    if ( vv in netCDF4File.variables):
                        data = netCDF4File[vv][:].flatten()
                    else:
                        break
                    if i == -1:
                        valuem = data[indexMin]
                        valueM = data[indexMax]
                        strMin+=str(vv)+"["+str(indexMin)+"]:"+str(valuem)+"; "
                        strMax+=str(vv)+"["+str(indexMax)+"]:"+str(valueM)+"; "
                    else:
                        valuem = data[globalMinIndice[i]]
                        valueM = data[globalMaxIndice[i]]
                        strMin+=str(vv)+"["+str(globalMinIndice[i])+"]:"+str(valuem)+"; "
                        strMax+=str(vv)+"["+str(globalMaxIndice[i])+"]:"+str(valueM)+"; "
                    
                    if storeStatsAsJson:
                        listobj[vv]= { "min":str(valuem),"max":str(valueM)}
                i+=1
            
            print(strDump,file=outputFile)
            print(strMin,file=outputFile)
            print(strMax,file=outputFile)
            PrintCurrentTask(msg,outputFile)
            PrintSuccess()
            continue

        strDump= "[STATS] variable:"+v+" min:"+str(minV)+" max:"+str(maxV)+" mean:"+str(mean)+" std deviation:"+str(stdDeviation)

        strMin="[STATS] corresponding minimum "+v+" value at "
        strMax="[STATS] corresponding maximum "+v+" value at "


        aborted = False
        try:
            # retrieve all variables associated with dimensions
            for i in range(0,a.ndim):
                dimension = var.dimensions[i]

                if storeStatsAsJson:
                    listobj[dimension]= { "min":str(minI[i]),"max":str(maxI[i])}

                strMin +="dimension : "+dimension+"["+str(minI[i])+"] : " 
                strMax +="dimension : "+dimension+"["+str(maxI[i])+"] : " 
                l = netCDFVariables[v][i]
                data = None
                for vv in l:
                    if ( vv in netCDF4File.variables):
                        data = netCDF4File[vv][:].flatten()
                    else:
                        continue
                    valuem = data[minI[i]]
                    valueM = data[maxI[i]]
                    strMin+="matching to variables "+str(vv)+"["+str(minI[i])+"]:"+str(valuem)+"; "
                    strMax+="matching to variables "+str(vv)+"["+str(maxI[i])+"]:"+str(valueM)+"; "

                    if storeStatsAsJson:
                        listobj[vv]= { "min":str(valuem),"max":str(valueM)}
            if  not aborted:
                
                if storeStatsAsJson:
                    netCDFVariablesStats[v]['stats']=objStat
                    netCDFVariablesStats[v]['vars']=listobj

                print(strDump,file=outputFile)
                print(strMin,file=outputFile)
                print(strMax,file=outputFile)
                PrintCurrentTask(msg,outputFile)
                PrintSuccess()
            else:
                PrintCurrentTask(msg,outputFile)
                PrintFailed()

        except Exception as e:
            print("error detected : "+str(e) ,file=outputFile)
        
        print("--",file=outputFile)
    
    if storeStatsAsJson:
        outJSON = open(os.path.join(path,filename+"_stats.json"),"w")
        json.dump(netCDFVariablesStats,outJSON)
        outJSON.close()

def CheckValidMean(min,max,mean):
    """ 
    check mean value is in range min/max

    Parameters
    ----------
    min : float
        min value of interval
    max : float
        max value of range
    mean : float
        mean value to check for
    
    Returns
    -------

    """
    global outputFile
    if (mean < min) or (mean>max):
        print("[ERROR] mean value out of range min/max ="+str(min)+"/"+str(max),file=outputFile)

def CheckValidValues(var,a,fillValue):
    """ given a netcdf4 variable and its associated data and fill value if any, check if 
        all data values are within valid_min/valid_max range

    Parameters
    ----------

    var : netcdf4 variable object
        current variable object under investigation

    a : numpy array
        numpy data array associated to var

    fillValue: float
             fill value of varaible var if any

    Returns
    -------

    none

    """
    global outputFile
    vmin = None
    vmax = None
    try:
        if var.valid_min != None:
            vmin = var.valid_min.T
        if var.valid_max != None:
            vmax = var.valid_max.T
        if vmin == None or vmax == None:
            print("[VALID] valid_min or valid_max not defined. skipping values out of range check.",file= outputFile)
            return
        print("[VALID] valid_min and valid_max values detected valid_min/valid_max : "+str(vmin)+"/"+str(vmax),file=outputFile)
        print("[VALID]  checking for values out of range ["+str(f'{vmin:.2f}')+";"+str(f'{vmax:.2f}')+"]",file=outputFile )
        if fillValue:
            indices = np.argwhere(((a < vmin) | (a > vmax)) & ( abs(a-fillValue)> 0.0001 ) )
        else:
            indices = np.argwhere(((a < vmin) | (a > vmax))  )
        if indices.size != 0:
            sTr = "[VALID]  found  "+str(indices.size)+" values out of range : here are first values : "
            n = min(10,indices.shape[0])
            for i in range(0,n):
                data = tuple(indices[i])
                sTr+="indices "+str(data)+" value "+str(f'{a.item(data):.4f}')
                if i != indices.shape[0]:
                    sTr += ";  "
            print(sTr+"....[FAILED]",file=outputFile)
        else:
            print("[VALID] no values out of range ["+str(f'{vmin:.2f}')+";"+str(f'{vmax:.2f}')+"] found....[PASSED]",file=outputFile)
    except Exception as e:
        print("[WARNING] error occured for valid values with exception "+str(e),file=outputFile)

def CheckTimeVariableCorrectness(netCDF4File):
    """ check time stamps from netcdf4 data with time stamps computed from start and end dates from metadata

    Parameters
    ----------

    netCDF4File : netcdf4 dataset object
                netcdf4 object file

    Returns
    -------

    none            

    """
    global ExpectednetCDF4TimeStamps,dateUtilsParser,ww3_toffset,outputFile
    global timeTags

    msg = "checking 'time' variable correctness from metadata..."

    try:
        dd = netCDF4File.__dict__ # dictionary of meta data
        startDateTime = None
        endDateTime = None
        frequency = 'hourly'
        if ( timeTags[0] in dd ):
            startDateTime =    dateUtilsParser.parse(dd[timeTags[0]])
        if ( timeTags[1] in dd ):
            endDateTime = dateUtilsParser.parse(dd[timeTags[1]])
        if ( 'field_type' in dd):
            frequency = dd['field_type']
        else:
            PrintWARNING("[FAILED]  could not retrieve header time frequency meta data. WARNING taking hourly.",outputFile)

    except Exception as e:
        ExitsWithErrorMessage("could not retrieve header time meta data. "+str(e),outputFile)
        return
    else:
        expectedTimeStamps = ComputeTimeStamps(startDateTime,endDateTime,frequency)
        #
        # date from wich compute days offset fo each time data
        #
        baseDate = dateUtilsParser.parse(ww3_toffset)
        #
        # get time data
        #
        try:
            if ( "time" in netCDF4File.variables):
                times = netCDF4File['time'][:]
            else:
                times = netCDF4File['TIME'][:]
            
        except Exception as e:
            PrintWARNING("time variable not found : '"+str(e)+"'",outputFile)
        else:
        #
        # compute time stamps from time data
        #
            netCDF4TimeStamps = []
            for t in times:
                netCDF4TimeStamps.append( datetime.timestamp( baseDate+ timedelta(days=t))  )
            netCDF4TimeStamps.sort()
            PrintCurrentTask(msg,outputFile)
            difference =  [item for item in netCDF4TimeStamps if item not in expectedTimeStamps]
            if ( len(difference) == 0 ):
                PrintSuccess()
            else:
                PrintFailed()
                PrintError("expected time data range from metadata and extracted time data range do not coincide",outputFile)
            
    print("[DEBUG] passed",file = outputFile)


def CheckVariableConsistency(netCDF4File):
    """ check the existence of doublons, the monotony and variable step size of given netcdf4 variables data

    Parameters
    ----------

    netCDF4File : netcdf4 dataset object
                netcdf4 object file
    Returns
    -------

    none

    """
    global outputFile,netCDFVariableConsistency

    if netCDFVariableConsistency == None:
        return
    for v in netCDFVariableConsistency:
        l = netCDFVariableConsistency[v]
        try:
            data = netCDF4File[v][:]
        except Exception as e:
            PrintWARNING("'"+v+"' not found. skipping consitency check."+str(e),outputFile )
            continue
        else:
            unique,counts = np.unique(data,return_counts = True)
            
            if ( l["doublons"] == True):
                msg = "checking '"+v+"' coordinate does not contain doublons..."
                PrintCurrentTask(msg,outputFile)
                doublons_indices =np.ravel( np.nonzero(counts > 1) )
                has_doublons = len(doublons_indices) != 0
                if has_doublons:
                    indices = []
                    values = []
                    for i in doublons_indices:
                        val = unique[i]
                        delta = list(np.ravel(np.nonzero( data == val )))
                        for j in delta:
                            values.append(data[j])
                        indices += delta
                    print("variable '"+v+"' doublons at indices: "+str(indices),file=outputFile)
                    print("variable '"+v+"' doublons values: ",str(values),file=outputFile)
                    PrintFailed()
                else:
                    PrintSuccess()
            if ( l["monotonous"] == True):   
                msg = "checking '"+v+"' coordinate is monotonous..."
                diff = np.diff(data)
                is_monotonous_increasing = np.all(diff>= 0) 
                is_monotonous_decreasing = np.all(diff<=0)
                is_monotonous = is_monotonous_decreasing or is_monotonous_increasing
                PrintCurrentTask(msg,outputFile)
                if is_monotonous:
                    PrintSuccess()
                else:
                    if not( is_monotonous_increasing ):
                        indices = np.nonzero(diff <= 0)
                        values = np.take(data,indices)
                        print("variable '"+v+"' non increasing at indices: "+str(list(np.ravel(indices))),file=outputFile)
                        print("variable '"+v+"' non increasing     values: ",str(list(values)),file=outputFile)
                                               
                    elif not(is_monotonous_decreasing):
                        indices = np.nonzero(diff >= 0)
                        values = np.take(data,indices)
                        print("variable '"+v+"' non decreasing at indices: "+str(list(np.ravel(indices))),file=outputFile)
                        print("variable '"+v+"' non decreasing values: ",str(list(values)),file=outputFile)
                  
                    PrintFailed()
                indices = np.ravel(np.nonzero( (diff - diff[0]) > 0.00000001 ))
                if ( len(indices) != 0):
                    values = np.take(diff,indices)
                    print("variable '"+v+"' variables steps detected at indices: "+str(list(np.ravel(indices))),file=outputFile)
                    print("variable '"+v+"' variables steps detected     values: ",str(np.ravel(values)),file=outputFile)

                else:
                    print("variable '"+v+"'  steps are uniforms and equal to :"+str(diff[0]),file=outputFile)
            if ( l["correctness"] == True and v== 'time' or v == 'TIME' ):
                CheckTimeVariableCorrectness(netCDF4File)
                print("[DEBUG] step 2 CheckTimeVariableCorrectness passed",file = outputFile)


def CheckAttributes(netCDF4File):
    """ check data type consistency of metadata variables valid_min and valid_max

    Parameters
    ----------

    netCDF4File : netcdf4 dataset object
                nectcdf4 object file

    Returns
    -------

    none

    """
    global outputFile,netCDF4SpatialCoordinates

    if netCDF4File == None:
        return
    msg = "   checking variables valid_min and valid_max data type consistency. "
    check = True
    for name, variable in netCDF4File.variables.items():
        if name in netCDF4SpatialCoordinates or name == 'time':
            continue
        try:
            if not np.issubdtype(variable.dtype, np.number):
                continue
            if ( "float" in  variable.dtype.name  or variable.dtype == int ):
                ok = (variable.dtype == variable.valid_max.dtype) &  (variable.dtype == variable.valid_min.dtype)  
            else:
                continue
        except AttributeError:
            continue
        else:
            check &= ok
            if ( not ok ):
                msg = "variable '"+name+"' : data type '"+variable.dtype.name+"'  valid_min : '"+variable.valid_min.dtype.name+"' valid_max : '"+variable.valid_max.dtype.name+"'"
                PrintWARNING(msg,outputFile)

    PrintCurrentTask(msg,outputFile)
    if ( check ):
        PrintSuccess()
    else:
        PrintFailed()


#
# scan netCDF files in each sub directory
#  
def ProcessFile(path):
    """ process a single file checking for attributes valid data type
        and computing basic stats

        Parameters
        ----------

        path : str
            the name of the absolute path of netcdf4 to process

        Returns
        -------

        none


    """
    filename = os.path.splitext(os.path.basename(path))[0]
    dir = os.path.dirname(path)
    msg = "checking netCDF4 file '"+path+"' can be opened"
    netCDF4File = OpenNetCDF4( path )
    if ( netCDF4File != None ):
        PrintCurrentTask(msg,outputFile)
        PrintSuccess()
        CheckAttributes(netCDF4File)
        #
        # check basic consistency
        #
        CheckVariableConsistency(netCDF4File)

        try:
            CheckMinMaxnetCDFVariables(netCDF4File,dir,filename)
        except Exception as e:
            print("exception was raised : "+str(e),file= outputFile)
            print("exception was raised : "+str(e))
            sys.exit(-1)
        print("closing file : "+path,file= outputFile)   
        netCDF4File.close()
    else:
        PrintCurrentTask(msg,outputFile)
        PrintFailed()

def ProcessNetCDF4Files(level, parentDirectory, directory):
    """ recursive traversal of netcdf4 files to process in a given directory

    Parameters
    ----------

    level : int
           recursive index to check against ExpectedSubDirectories intricated list of expected directories

    Returns
    -------

    none

    """
    global ExpectedSubDirectories,outputFile

    if ( ExpectedSubDirectories == None or ExpectedSubDirectories == "None"):
        subdirs = [filename for filename in os.listdir(directory) if os.path.isdir(os.path.join(directory,filename))]
        subdirs.sort()
        for subDirectory in subdirs:
            path = os.path.join(os.path.join(parentDirectory,directory),subDirectory)
            processSingleDirectory(path)
        return
    if level == len(ExpectedSubDirectories):
        return
    if  not os.path.exists(os.path.join(parentDirectory,directory)) :
        PrintWARNING( "'"+os.path.join(parentDirectory,directory)+"' does not exist.",outputFile)
        return
    subdirs = [filename for filename in os.listdir(directory) if (os.path.isdir(os.path.join(directory,filename)) and filename in ExpectedSubDirectories[level])]
    subdirs.sort()
    for subDirectory in subdirs:
        path = os.path.join(os.path.join(parentDirectory,directory),subDirectory)
        processSingleDirectory(path)

    level += 1
    for subDirectory in subdirs:
        ProcessNetCDF4Files(level,directory,subDirectory)

def processSingleDirectory(directory):
    """ 
    process netcdf4 files  in a single directory 
    get a list of all files matching a given pattern
    execute ProcessFile function on each file found which matches the pattern
    compute total size in Gb of this direcotry as well as the number of files found matching the pattern

    Parameters
    ----------

    directory : str
              name of current cirectory to be processed
    
    Returns
    -------

    none 

    """
    global warningStr,outputFile,currentPath
    if not os.path.isdir(directory):
        PrintErrorAndExits("directory '"+directory+"' not a proper directory.",outputFile)
    matchingPattern = os.path.join(directory,baseFilename+"."+baseExtension)

    print("[INFO] processing directory '"+directory+"'",file=outputFile)
    total_size = 0
    filesIterator = glob.iglob(matchingPattern)
    total = sum(1 for _ in filesIterator)
    filesIterator = glob.iglob(matchingPattern)
    i = 0
    for f in filesIterator:
        try:
            i+=1
            print("processing "+str(i)+"/"+str(total),file=outputFile)
            ProcessFile(f)
        except Exception as e:
            print("exception occured "+str(e),file=outputFile)
        total_size += os.path.getsize(f)
    if ( i == 0 ):
        return
    
    sinGb = 1./(1024*1024*1024)
    print("[INFO] directory '"+directory+"' size in Gbytes is "+"{:.2f}".format(total_size*sinGb)+" and contains "+str(i)+" files.",file=outputFile)
    PrintCurrentTask(" processing directory '"+directory+"'",outputFile)
    PrintSuccess()

def GetIndicesFromLinerarizedIndex(a,globalIndex):
    """ utility function to compute multi dimensional indices from an equivalent linearized index array

    Parameters
    ----------

    a : a numpy nd-array of shape(i,j,k,...)
    globalIndex : int
                 linearized global index to be decomposed into a tuple of multi indices

    Returns
    -------

    a tuple of indices equivalent to the global linear index


    """
    if a.ndim == 1 or a.size == 1:
        return globalIndex

    index = [None] * a.ndim

    M = [None] * (a.ndim-1) # multipliers for each dimension
    shape = list(reversed(a.shape))

    M[0]=shape[0]
    for i in range(1,a.ndim-1):
        p = shape[i]
        for j in range(0,i):
            p *= shape[j]
        M[i] = p

    index[a.ndim-1] = int(float(globalIndex)/float(M[-1]))
    for i in range(a.ndim-2,-1,-1):
        sum = 0
        for j in range(i,len(M)):
            sum += M[j]*index[j+1]
        index[i] = int(globalIndex - sum)
        if ( i != 0):
            index[i] /= M[i-1]
            index[i] = int(index[i])
    return tuple(reversed(index))

def GetElementFormLinearizedIndice(a,globalIndex):
    """ retrieve the entry at global linear index of a multi dimensional array

    Parameters
    ----------

    a : a numpy nd-array of shape (i,j,k,...)
       nd-array to get element from
    globalIndex : int

    Returns
    -------

    the entry from array a at global linearized index globalIndex



    """
    indices = GetIndicesFromLinerarizedIndex(a,globalIndex)
    if not (isinstance(indices,list)):
        return a[indices]

    index = tuple(indices)
    return a.item(index)
###################################################################################################################################
#
#
#
#  script starts from now
#
#
# global variables
#
# Time offset in WW3 NetCDF files
###################################################################################################################################
"""
    # test linearized index
    a = array = np.arange(720).reshape(3,6,2,5,4)
    b = a.flatten()
    print ("original  array "+str(a))
    print ("flattened array "+str(b))
    for i in range(0,len(a.flatten())):
        index = GetIndicesFromLinerarizedIndex(a,i)
        print(str(i)+"  "+str(index)+" "+str(a.item(index))+" " +str(b[i]))"""

ww3_toffset = '1990-01-01 00:00:00'
ExpectednetCDF4TimeStamps =[] # time stamps expected from time meta data
start = datetime.today()

startTime = time.time()

timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")

timeTags = ["start_date","stop_date"]
storeStatsAsJson = False
outputFile = None
outputFileName = None
tmpFileName = None
outputDirectory = None
#
# check mpi parallel processing is requested
#
mpi_comm = None
mpi_rank = None
mpi_size = None
mpi_found = False

ExpectednetCDFDimensions = ["time","level"]
netCDFVariableConsistency = None
ExpectednetCDFVariables = None
netCDF4SpatialCoordinates = ["longitude","latitude"]
ExpectedSubDirectories = None

netCDFSubDirectoriesNames = None


currentDirectory = None
singleDirectory = None
singleFile = None
outputFileName = None
currentPath=""
baseFilename = None
baseExtension = "nc"
netCDF4HistogramsVariables = None
netCDF4VariablesFilters = None


try:
    ProcessCmdLineOptions()
except Exception as e:
    print("exception occured while processing json parameters file :"+str(e))

print("[INFO] json file processed. Starting processing...",file = outputFile)

checkDirectoryTree = ExpectedSubDirectories != None and isinstance(ExpectedSubDirectories,list)

if ( outputFileName == None):
    cwd=os.getcwd()
    outputFileName = os.path.join(cwd,"output.log")

filename, file_extension = os.path.splitext(outputFileName)
outputPath = os.path.dirname( outputFileName)
outputFileName = tmpFileName = os.path.basename(outputFileName)+"_"+timeStr+file_extension

MkDirectory(outputPath)
outputFileName = tmpFileName = os.path.join(outputPath,outputFileName)

print("[INFO] output directory log set to '"+outputPath+"'",file = outputFile)
print("[INFO] output file log set to      '"+outputFileName+"'",file = outputFile)

outputFile = open(outputFileName,"w")


totalNumberOfFilesToProcess = 0
numbersOFProcessedFiles = 0

mpiDirectories = []
errorStr = ""
warningStr =""

currentCheck =""
subDirectories = []

netCDFDimensions = dict()
netCDFVariables = dict()

netCDFVariables = dict()

netCDFVariablesStats = None

if storeStatsAsJson:
    netCDFVariablesStats = dict()

axisByname = dict()

AllExpectedDirectories = []

WriteFileHeader()
outputFile.flush()

print("[INFO] expected subdirectories set to "+str(ExpectedSubDirectories),file = outputFile)

if  ExpectedSubDirectories != None and ExpectedSubDirectories != "None" :
    for e in ExpectedSubDirectories:
        AllExpectedDirectories += e


#
# check netCDF4 module : mandatory
#

msg = "Checking for NetCDF4 python module..."
PrintCurrentTask(msg,outputFile)
netCDF4_found = importlib.util.find_spec("netCDF4") != None
if not netCDF4_found:
    PrintErrorAndExits("netCDF4 module not found",outputFile)
import netCDF4  as nc
PrintSuccess()



if ExpectedSubDirectories != None and (ExpectedSubDirectories == "None" and (singleDirectory == None or singleDirectory == "None") ):
    singleDirectory = currentDirectory

if ( singleDirectory != None or singleFile != None):
    if ( singleDirectory and singleDirectory != "None"):
        processSingleDirectory(singleDirectory)

    if ( singleFile != None):
        try:
            ProcessFile(singleFile)
        except Exception as e:
            print("exception occured "+str(e),file=outputFile)

    PrintFileFooter(outputFile)
    sys.exit(0)


Process()

PrintFileFooter(outputFile)







