import sys,getopt
import os,glob
import importlib
import json

satellites = ["cryosat-2",  "envisat", "jason-1",  "jason-2",  "jason-3",  "saral"]
years = dict()
years["cryosat-2"]=dict()
years["cryosat-2"]["range"]=range(2010,2021)
years["cryosat-2"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["envisat"]=dict()
years["envisat"]["range"]=range(2002,2013)
years["envisat"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-1"]=dict()
years["jason-1"]["range"]=range(2002,2010)
years["jason-1"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-2"]=dict()
years["jason-2"]["range"]=range(2008,2017)
years["jason-2"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-3"] = dict()
years["jason-3"]["range"]=range(2016,2020)
years["jason-3"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["saral"]=dict()
years["saral"]["range"]=range(2013,2020)
years["saral"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"




jsonStr = dict()


jsonStr["baseExtension"]="*nc"
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4HistogramsVariables"]={"all":True,"variables":["hs"]}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude","glon.00", "glat.00","lon","lat"]
jsonStr["singleDirectory"]="None"
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False
jsonStr["cumulate"]=True
jsonStr["numBins"]=50
jsonStr["histosFilters"]={"all":{"sea_ice_fraction":["*",0.2],"distance_to_coast":[50,"*"],"swh_quality":[3,3],"bathymetry":["*",0]} }
for sat in satellites:

    for year in years[sat]["range"]:
        jsonStr["baseFilename"]= years[sat]["baseFilename"]
        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=100gb \n"
        strPBS+= "#PBS -l walltime=5:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"
        jsonStr["baseDirectory"]= "/home/datawork-cersat-public/provider/cci_seastate/processing/v2/altimeter/retracking/l2p/"+sat+"/"+str(f'{year}')
        jsonStr["outputDirectory"]= "/home1/datawork/pdescour/datawork-cersat-public/provider/cci_seastate/processing/v2/altimeter/retracking/l2p/"+sat+"/"+str(f'{year}')+"/"+str(jsonStr["numBins"])+"Bins"
        jsonStr["outputLog"]=jsonStr["outputDirectory"]+"/output.log"
        strPBS+= "python3 /home1/datawork/pdescour/computeHistograms1D.py --json=/home1/datawork/pdescour/histos1DCCIv2_"+sat+"_"+str(f'{year}')+".json"

        fjson = open("histos1DCCIv2_"+sat+"_"+str(f'{year}')+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("histos1DCCIv2_"+sat+"_"+str(f'{year}')+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for zone in satellites:
    for year in range(1993,2020):
        STR += "qsub histos1DCCIv2_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchCCIv2histos1DTasks.sh","w")
fl.write(STR)
fl.close()

