import sys,getopt
import os,glob
import importlib
import json

zones = ["POINTS"]

yearsRange=[1993,2019]
subDirectories = ["FREQ_NE20to59","FREQ_NW19toE19","FREQ_SW","SPEC_NE140to180","SPEC_NW180to140","SPEC_SE",         
"FREQ_AT_BUOYS",  "FREQ_NE60to99",   "FREQ_NW20to59", "SPEC_NE20to59", "SPEC_NW19toE19", "SPEC_SW",         
"FREQ_NE100to139", "FREQ_NW139to100", "FREQ_NW60to99",  "SPEC_AT_BUOYS",   "SPEC_NE60to99",   "SPEC_NW20to59",    
"FREQ_NE140to180", "FREQ_NW180to140", "FREQ_SE", "SPEC_NE100to139", "SPEC_NW139to100", "SPEC_NW60to99"]



jsonStr = dict()


jsonStr["baseFilename"]= "LOPS_WW3*"
jsonStr["baseExtension"]="*nc"
jsonStr["ExpectedSubDirectories"]=[["FIELD_NC"]]
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4Dimensions"]=["time","level"]
jsonStr["netCDF4Variables"]="all"
jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude"]
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False

baseDirectory = "/home1/datawork/pdescour"

for zone in zones:
    


    for d in subdirs:
        for year in range(1993,2020):

            strPBS="#PBS -q sequentiel \n"
            strPBS+= "#PBS -l ncpus=1 \n"
            strPBS+= "#PBS -l mem=100gb \n"

            strPBS+= "#PBS -l walltime=20:00:00 \n"
            strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
            strPBS+= "conda activate sanitycheck \n"

            jsonStr["outputDirectory"] = baseDirectory+"/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+d+"/"+str(f'{year}')+"/"
            jsonStr["baseDirectory"]= "/home/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+str(f'{year}')
            jsonStr["outputLogFile"]=jsonStr["outputDirectory"]+"output.log"
            strPBS+= "python3 "+baseDirectory+"/sanityCheck.py --json="+baseDirectory+"/sanityCheck_GLOBMULTI_ERA5_GLOBCUR_01_"+zone+"_"+str(f'{year}')+".json"

            fjson = open("sanityCheck_GLOBMULTI_ERA5_GLOBCUR_01_"+zone+"_"+str(f'{year}')+".json","w")
            json.dump(jsonStr,fjson)
            fjson.close()
            fpbs = open("sanityCheck_GLOBMULTI_ERA5_GLOBCUR_01_"+zone+"_"+str(f'{year}')+".pbs","w")
            fpbs.write(strPBS)
            fpbs.close()

STR = "#!/bin/bash \n"
for zone in zones:
    for year in range(1993,2020):
        STR += "qsub sanityCheck_GLOBMULTI_ERA5_GLOBCUR_01_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchSanityCheck_GLOBMULTI_ERA5_GLOBCUR_01_Tasks.sh","w")
fl.write(STR)
fl.close()

