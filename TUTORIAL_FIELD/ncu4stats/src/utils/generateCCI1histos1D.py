import sys,getopt
import os,glob
import importlib
import json

satellites = ["cryosat-2",  "envisat",  "ers-1",  "ers-2",  "gfo",  "jason-1",  "jason-2",  "jason-3",  "saral",  "topex" ]
years = dict()
years["cryosat-2"]=range(2010,2020)
years["envisat"]=range(2002,2013)
years["ers-1"]=range(1991,1997)
years["ers-2"]=range(1995,2012)
years["gfo"]=range(2000,2009)
years["jason-1"]=range(2002,2014)
years["jason-2"]=range(2008,2019)
years["jason-3"]=range(2016,2019)
years["saral"]=range(2013,2019)
years["topex"]=range(1992,2006)



jsonStr = dict()

jsonStr["baseFilename"]= "ESACCI-SEASTATE-L2P-SWH*"
jsonStr["baseExtension"]="*nc"
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4HistogramsVariables"]={"all":True,"variables":["hs"]}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude","lat", "lon"]
jsonStr["singleDirectory"]="None"
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False
jsonStr["cumulate"]=True
jsonStr["numBins"]=50
jsonStr["histosFilters"]={"all":{"sea_ice_fraction":["*",0.2],"distance_to_coast":[50,"*"],"swh_quality":[3,3],"bathymetry":["*",0]} }
for sat in satellites:

    for year in years[sat]:

        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=100gb \n"
        strPBS+= "#PBS -l walltime=6:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"
        jsonStr["baseDirectory"]= "/home/datawork-cersat-public/provider/cci_seastate/products/v1.1/data/l2p/"+sat+"/"+str(f'{year}')
        jsonStr["outputDirectory"]= "/home1/datawork/pdescour/datawork-cersat-public/provider/cci_seastate/products/v1.1/data/l2p/"+sat+"/"+str(f'{year}')+"/"+str(jsonStr["numBins"])+"Bins"
        jsonStr["outputLog"]=jsonStr["outputDirectory"]+"/output.log"
        strPBS+= "python3 /home1/datawork/pdescour/computeHistograms1D.py --json=/home1/datawork/pdescour/histos1DCCIv1_"+sat+"_"+str(f'{year}')+".json"

        fjson = open("histos1DCCIv1_"+sat+"_"+str(f'{year}')+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("histos1DCCIv1_"+sat+"_"+str(f'{year}')+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for zone in satellites:
    for year in range(1993,2020):
        STR += "qsub histos1DCCIv1_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchCCIv1histos1DTasks.sh","w")
fl.write(STR)
fl.close()

