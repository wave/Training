import sys,getopt
import os,glob
import importlib
import json

satellites = ["cryosat-2",  "envisat", "jason-1",  "jason-2",  "jason-3",  "saral"]
years = dict()
years["cryosat-2"]=dict()
years["cryosat-2"]["range"]=range(2010,2021)
years["cryosat-2"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["envisat"]=dict()
years["envisat"]["range"]=range(2002,2013)
years["envisat"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-1"]=dict()
years["jason-1"]["range"]=range(2002,2010)
years["jason-1"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-2"]=dict()
years["jason-2"]["range"]=range(2008,2017)
years["jason-2"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["jason-3"] = dict()
years["jason-3"]["range"]=range(2016,2020)
years["jason-3"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"
years["saral"]=dict()
years["saral"]["range"]=range(2013,2020)
years["saral"]["baseFilename"] = "ESACCI-SEASTATE-L2P*"

jsonStr = dict()

jsonStr["baseExtension"]="*nc"
jsonStr["ExpectedSubDirectories"]="None"
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4Dimensions"]=["time","level"]
jsonStr["netCDF4Variables"]="all"
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude","lon","lat"]
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["time_coverage_start","time_coverage_start"]
jsonStr["USE_MPI"]=False

for sat in satellites:

    for year in years[sat]["range"]:
        jsonStr["baseFilename"]= years[sat]["baseFilename"]
        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=100gb \n"
        strPBS+= "#PBS -l walltime=5:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"
        jsonStr["baseDirectory"]= "/home/datawork-cersat-public/provider/cci_seastate/processing/v2/altimeter/retracking/l2p/"+sat+"/"+str(f'{year}')
        jsonStr["outputDirectory"]= "/home1/datawork/pdescour/datawork-cersat-public/provider/cci_seastate/processing/v2/altimeter/retracking/l2p/"+sat+"/"+str(f'{year}')+"/"
        jsonStr["outputLogFile"]=jsonStr["outputDirectory"]+"/output.log"
        strPBS+= "python3 /home1/datawork/pdescour/sanityCheck.py --json=/home1/datawork/pdescour/sanityCheck_CCIv2_"+sat+"_"+str(f'{year}')+".json"

        fjson = open("sanityCheck_CCIv2_"+sat+"_"+str(f'{year}')+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("sanityCheck_CCIv2_"+sat+"_"+str(f'{year}')+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for zone in satellites:
    for year in range(1993,2020):
        STR += "qsub sanityCheck_CCIv2_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchCCIv2_sanityCheck_Tasks.sh","w")
fl.write(STR)
fl.close()

