import sys,getopt
import os,glob
import importlib
import json

baseDirectory = "/home/datawork-resourcecode/EFTP/DATA/HINDCAST/"
dirs = ["01",  "02",  "03",  "04",  "05",  "06",  "07",  "08",  "09",  "10",  "11",  "12"]

subDirectories = ["FIELD_NC","FREQ_NC","SPEC_NC"]


jsonStr = dict()


jsonStr["baseFilename"]= "RSCD_WW3-RSCD*"
jsonStr["baseExtension"]="*nc"
jsonStr["ExpectedSubDirectories"]="None"
jsonStr["netCDF4Dimensions"]=["time","level"]
jsonStr["netCDF4Variables"]="all"
jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude"]
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["storeStatsAsJson"]=True
for year in range(1994,2020):


    for dir in dirs:

        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=115gb \n"
        strPBS+= "#PBS -l walltime=24:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"

        jsonStr["baseDirectory"]= baseDirectory+str(f'{year}')+"/"+dir+"/FIELD_NC"
        jsonStr["outputLogFile"]="/home1/datawork/pdescour/datawork-resourcecode/EFTP/DATA/HINDCAST/"+str(f'{year}')+"/"+dir+"/FIELD_NC/output.log"
        strPBS+= "python3 /home1/datawork/pdescour/sanityCheck.py --json=/home1/datawork/pdescour/sanityCheck_EFTP_RSCD_"+str(f'{year}')+"_"+dir+".json"

        fjson = open("sanityCheck_EFTP_RSCD_"+str(f'{year}')+"_"+dir+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("sanityCheck_EFTP_RSCD_"+str(f'{year}')+"_"+dir+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for year in range(1994,2020):
    for dir in dirs:

        STR += "qsub sanityCheck_EFTP_RSCD_"+str(f'{year}')+"_"+dir+".pbs \n"

fl = open("launchSanityCheckRESOURCECODETasks.sh","w")
fl.write(STR)
fl.close()

