
#!/usr/bin/python
import sys,getopt
import os,glob
import importlib
import json





jsonStr = dict()


jsonStr["baseFilename"]= "RSCD_WW3-RSCD-UG*"
jsonStr["baseExtension"]="*nc"
jsonStr["ExpectedSubDirectories"]=[["BUOY_STAT",  "FIELD_NC",  "FREQ_NC",  "SPEC_NC"]]
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4Dimensions"]=["time","level"]
jsonStr["netCDF4Variables"]="all"
jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude"]
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False

for jobNum in range(1,12):

    strPBS="#PBS -q sequentiel \n"
    strPBS+= "#PBS -l ncpus=1 \n"
    strPBS+= "#PBS -l mem=115gb \n"
    strPBS+= "#PBS -l walltime=72:00:00 \n"
    strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
    strPBS+= "conda activate sanitycheck \n"

    jsonStr["baseDirectory"]= "/home/datawork-resourcecode/EFTP/DATA/2017_v3/"+str(f'{jobNum:02}')
    jsonStr["outputLogFile"]="output_"+str(f'{jobNum:02}')+".log"
    strPBS+= "python3 /home1/datahome/pdescour/sanityCheck.py --json=/home1/datahome/pdescour/sanityCheck_"+str(f'{jobNum:02}')+".json"

    fjson = open("sanityCheck_"+str(f'{jobNum:02}')+".json","w")
    json.dump(jsonStr,fjson)
    fjson.close()
    fpbs = open("sanityCheck_"+str(f'{jobNum:02}')+".pbs","w")
    fpbs.write(strPBS)
    fpbs.close()

STR = "#!/bin/bash \n"
for jobNum in range(1,13):
    STR += "qsub sanityCheck_"+str(f'{jobNum:02}')+".pbs \n"

fl = open("launchSanityCheckTasks.sh","w")
fl.write(STR)
fl.close()