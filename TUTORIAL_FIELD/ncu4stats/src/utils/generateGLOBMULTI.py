import sys,getopt
import os,glob
import importlib
import json

zones = ["AFRICA-10M",  "ARC-12K", "ATNE-10M",  "ATNW-10M",  "CRB-3M",  "GLOB-30M",  "NC-3M" ,"PACE-10M"]
yearsRange=[1993,2019]
subDirectories = ["FIELD_NC"]


jsonStr = dict()


jsonStr["baseFilename"]= "LOPS_WW3*"
jsonStr["baseExtension"]="*nc"
jsonStr["ExpectedSubDirectories"]=[["FIELD_NC"]]
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4Dimensions"]=["time","level"]
jsonStr["netCDF4Variables"]="all"
jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude"]
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False

for zone in zones:

    for d in subDirectories:
        for year in range(1993,2020):

            strPBS="#PBS -q sequentiel \n"
            strPBS+= "#PBS -l ncpus=1 \n"
            strPBS+= "#PBS -l mem=100gb \n"
            strPBS+= "#PBS -l walltime=3:00:00 \n"
            strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
            strPBS+= "conda activate sanitycheck \n"

            jsonStr["baseDirectory"]= "/home/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+str(f'{year}')
            jsonStr["outputLogFile"]="/home1/datahome/pdescour/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+d+"/"+str(f'{year}')+"/"+"output.log"
            strPBS+= "python3 /home1/datahome/pdescour/sanityCheck.py --json=/home1/datahome/pdescour/sanityCheck_"+zone+"_"+str(f'{year}')+".json"

            fjson = open("sanityCheck_"+zone+"_"+str(f'{year}')+".json","w")
            json.dump(jsonStr,fjson)
            fjson.close()
            fpbs = open("sanityCheck_"+zone+"_"+str(f'{year}')+".pbs","w")
            fpbs.write(strPBS)
            fpbs.close()

STR = "#!/bin/bash \n"
for zone in zones:
    for year in range(1993,2020):
        STR += "qsub sanityCheck_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchSanityCheckTasks.sh","w")
fl.write(STR)
fl.close()

