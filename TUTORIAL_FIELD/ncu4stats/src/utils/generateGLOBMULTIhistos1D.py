import sys,getopt
import os,glob
import importlib
import json

zones = ["AFRICA-10M",  "ARC-12K", "ATNE-10M",  "ATNW-10M",  "CRB-3M",  "GLOB-30M",  "NC-3M" ,"PACE-10M","POINTS"]
yearsRange=[1993,2019]
subDirectories = ["FIELD_NC"]


jsonStr = dict()

jsonStr["baseFilename"]= "LOPS_WW3*"
jsonStr["baseExtension"]="*nc"
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4HistogramsVariables"]={"all":True,"variables":["fp"]}

jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude"]
jsonStr["singleDirectory"]="None"
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["start_date","stop_date"]
jsonStr["USE_MPI"]=False
jsonStr["cumulate"]=True
jsonStr["numBins"]=50
jsonStr["histosFilters"]={"all":{"dpt":[0.5,"*"],"wlv":["*",1.5]}}

for zone in zones:

    for year in range(1993,2020):

        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=100gb \n"
        strPBS+= "#PBS -l walltime=3:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"
        jsonStr["baseDirectory"]= "/home/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+str(f'{year}')+"/FIELD_NC/"
        jsonStr["outputDirectory"]= "/home1/datahome/pdescour/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+str(f'{year}')+"/"+str(jsonStr["numBins"])+"Bins"
        jsonStr["outputLog"]="/home1/datahome/pdescour/datawork-WW3/HINDCAST/GLOBMULTI_ERA5_GLOBCUR_01/"+zone+"/"+str(f'{year}')+"/"+str(jsonStr["numBins"])+"Bins/"+"output.log"
        strPBS+= "python3 /home1/datahome/pdescour/computeHistograms1D.py --json=/home1/datahome/pdescour/histos1DGLOBALMULTI_"+zone+"_"+str(f'{year}')+".json"

        fjson = open("histos1DGLOBALMULTI_"+zone+"_"+str(f'{year}')+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("histos1DGLOBALMULTI_"+zone+"_"+str(f'{year}')+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for zone in zones:
    for year in range(1993,2020):
        STR += "qsub histos1DGLOBALMULTI_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchGLOBALMULTIhistos1DTasks.sh","w")
fl.write(STR)
fl.close()

