import sys,getopt
import os,glob
import importlib
import json

satellites = ["cryosat-2",  "envisat",  "ers-1",  "ers-2",  "gfo",  "jason-1",  "jason-2",  "jason-3",  "saral",  "topex" ]
years = dict()
years["cryosat-2"]=range(2010,2020)
years["envisat"]=range(2002,2013)
years["ers-1"]=range(1991,1997)
years["ers-2"]=range(1995,2012)
years["gfo"]=range(2000,2009)
years["jason-1"]=range(2002,2014)
years["jason-2"]=range(2008,2019)
years["jason-3"]=range(2016,2019)
years["saral"]=range(2013,2019)
years["topex"]=range(1992,2006)



jsonStr = dict()

jsonStr["baseFilename"]= "ESACCI-SEASTATE-L2P-SWH*"
jsonStr["baseExtension"]="*nc"
jsonStr["mpi"]=[ "/home/pdescour/TESTPYTHON"]
jsonStr["netCDF4Variables"]="all"
jsonStr["ExpectedSubDirectories"]="None"
jsonStr["netCDFVariableConsistency"]={"time":{"monotonous":True,"doublons":True,"correctness":True}}
jsonStr["netCDF4SpatialCoordinates"]=["longitude","latitude","lat", "lon"]
jsonStr["singleDirectory"]="None"
jsonStr["singleFile"]="None"
jsonStr["timeTags"]=["time_coverage_start","time_coverage_end"]
jsonStr["USE_MPI"]=False

for sat in satellites:

    for year in years[sat]:

        strPBS="#PBS -q sequentiel \n"
        strPBS+= "#PBS -l ncpus=1 \n"
        strPBS+= "#PBS -l mem=100gb \n"
        strPBS+= "#PBS -l walltime=24:00:00 \n"
        strPBS+= "source /appli/anaconda/latest/etc/profile.d/conda.csh \n"
        strPBS+= "conda activate sanitycheck \n"
        jsonStr["baseDirectory"]= "/home/datawork-cersat-public/provider/cci_seastate/products/v1.1/data/l2p/"+sat+"/"+str(f'{year}')
        jsonStr["outputDirectory"]= "/home1/datawork/pdescour/datawork-cersat-public/provider/cci_seastate/products/v1.1/data/l2p/"+sat+"/"+str(f'{year}')+"/"
        jsonStr["outputLogFile"]=jsonStr["outputDirectory"]+"/output.log"
        strPBS+= "python3 /home1/datawork/pdescour/sanityCheck.py --json=/home1/datawork/pdescour/sanityCheck_CCIv1_"+sat+"_"+str(f'{year}')+".json"

        fjson = open("sanityCheck_CCIv1_"+sat+"_"+str(f'{year}')+".json","w")
        json.dump(jsonStr,fjson)
        fjson.close()
        fpbs = open("sanityCheck_CCIv1_"+sat+"_"+str(f'{year}')+".pbs","w")
        fpbs.write(strPBS)
        fpbs.close()

STR = "#!/bin/bash \n"
for zone in satellites:
    for year in range(1993,2020):
        STR += "qsub sanityCheck_CCIv1_"+zone+"_"+str(f'{year}')+".pbs \n"

fl = open("launchCCIv1_sanityCheck_1DCCIv1_Tasks.sh","w")
fl.write(STR)
fl.close()

