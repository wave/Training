#!/usr/bin/python
import sys,getopt
import os,glob,fnmatch
import importlib
import json
import numpy as np
# high level functions for working with NetCDF files
from datetime import datetime
from string import Formatter
from datetime import timedelta
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.basemap import Basemap, cm
from dateutil import parser as dateUtilsParser
from scipy import stats
import time
import math
import pandas
import seaborn as sns

#
# utility functions

def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = int(tdelta.total_seconds())
    elif inputtype in ['s', 'seconds']:
        remainder = int(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = int(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = int(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = int(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('W', 'D', 'H', 'M', 'S')
    constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)

#
#
# open a JSON file and return the object read from this file
#
def OpenJSONFile(filename):
    """  open a JSON file and return the json object read from this file or None

    Parameters
    ----------
    filename : str
             the absolute path of the json file
    
    Returns
    -------
    none
    """
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args)
        printUsage()
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
        printUsage()
    else:
        return data
    return None


#function to indicate test passed
def PrintSuccess():
    global outputFile
    print('[PASSED]',file=outputFile)

def PrintFailed():
    global outputFile
    print('[FAILED]',file=outputFile)

#function to indicate failure
def PrintError(errStr,outFile):
    print( "[ERROR] "+errStr ,file=outFile)


#function to indicate failure and exit
def PrintErrorAndExits(errStr, outFile):
    PrintError(errStr,outFile)
    PrintFileFooter(outFile)
    print("Exiting with errors...")
    sys.exit(-1)


def PrintFileFooter(outFile):
    global startTime
    endTime = datetime.now()
    elapsedTime = endTime-startTime
    print("===",file=outFile)
    print("Total elapsed time :",strfdelta(elapsedTime),file=outFile)
    msgSessionEnded ="session ended on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print(msgSessionEnded,file=outFile)
    print("Exiting...",file=outFile)
    outFile.close()


def ExitsWithErrorMessage(errStr,outFile):
    print( "[ERROR] : "+errStr,file=outFile )
    PrintFileFooter(outFile)
    print("Exiting with errors...")
    sys.exit(-1)

def PrintWARNING(warningStr,outFile):
    print("[WARNING] : "+warningStr,file=outFile)

def PrintINFO(infoStr,outFile):
    print("[INFO] : "+infoStr,file=outFile)



#function to print current task message
def PrintCurrentTask(message,outFile):
    print("[TASK] "+message, end='',file=outFile )

#
# check for a particular option
#
def GetOptionArgFromCmdLine(optStr):
    """  check command line arguments for a specific option

    Parameters
    ----------
    optStr : str
             the whole command line from unix system view point
    
    Returns
    -------
    the associated value to the required option if any
    """
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:h:",['help','json=','USE_MPI','outputFile=',"single_directory="])
        for (o,a) in opts:
            if (o == optStr):
                return a
        if(optStr in args):
            return optStr

        return None
    except getopt.GetoptError as e:
        print("command line argument or option '"+optStr+" was not found. ", e.msg)    
        printUsage()

#
# process command line options if any
#
def ProcessCmdLineOptions():
    """  process command line arguments especially the json parameters file

    Parameters
    ----------
    none

    Returns
    -------
    none
    """
    global currentDirectory,singleDirectory
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        if ( len(l) == 0):
            printUsage()
        opts,args = getopt.getopt(l,"j:d:o:h:",['json=',"help","USE_MPI","mpi=","single_directory="])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)
            if o in ("-j"):
                currentDirectory = a
            if o in ("--single_directory"):
                singleDirectory = a
            if o in ("--help","-h" ):
                printUsage(True)


    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: ", e.msg)
        printUsage()

def printUsage( exitNow = False ):
    """ print usage

    Parameters
    ----------   

    none

    Returns
    -------
    none
    """
    print("usage : python3 computeHistograms2D.py --json=path_to_json_file")
    print(" json file sample is provided as ")
    print("{")
    print(" \"outputLog\":\"/home/user/work/histos2D.log\", ")
    print(" \"outputDirectory\":\"/home/user/output\", ")
    print(" \"baseDirectory\": \"/home/user/work\", ")
    print(" \"baseFilename\": \"RSCD_WW3-RSCD-UG*\", ")
    print(" \"baseExtension\":\"*nc\",")
    print(" \"netCDF4FiltersFile\":\"filters.json\",")
    print(" \"netCDF4SpatialCoordinates\":[\"longitude\",\"latitude\"],")
    print(" \"singleDirectory\":\"None\",")
    print(" \"singleFile\":\"None\"")
    print("}")
    print("where filters.json is a json file containing constraints like these :")
    print("{")
    print(" \"dpt\":")
    print("  {")
    print("	 \"hexbin\":")
    print("     	{")		
    print("		    \"ranges\":")
    print("		            {")
    print("			    \"latitude\":[45,55],")
    print("			    \"dpt\":[2,4],")
    print("			    \"time\":[\"2017-01-01 00:00:00\",\"2017-01-01 07:00:00\"],")
    print("			    \"longitude\":[-5,5]")
    print("		            },")
    print("	        \"plot\":")
    print("	                {") 
    print("			\"colormap\":\"OrRd\",")
    print("			\"axes\":[\"longitude\",\"latitude\"],")
    print("			\"operators\":[[\"min\",\"time\"],[\"max\",\"time\"],[\"mean\",\"time\"],[\"rms\",\"time\"],[\"percentile95\",\"time\"],[\"count\",\"time\"]]")
    print("         		}")
    print("	        }")
    print("  }")
    print("}")
    if exitNow:
        print("exiting...")
        sys.exit(-1)


#
# process a json file to retrieve user parameters
#
def ProcessJSONFile(filename):
    """ process a json file to retrieve user parameters

    Parameters
    ----------
    filename : str
            the absolute path file name of json file to process
    
    Returns
    -------
    none
    """
    global currentDirectory,outputDirectory
    global outputFileName,outputFile,singleDirectory,singleFile
    global baseFilename,baseExtension,netCDF4SpatialCoordinates
    try:
        
            data = OpenJSONFile(filename)

            if ( 'outputDirectory' in data ):
                outputDirectory = data["outputDirectory"]
                outputDirectory = os.path.abspath(outputDirectory)

            if "outputLog" in data:
                outputFileName = data["outputLog"]
                outputFileName = os.path.abspath(outputFileName)

            if "netCDF4SpatialCoordinates" in data:
                netCDF4SpatialCoordinates = data["netCDF4SpatialCoordinates"]

            if "baseFilename" in data :
                baseFilename = data["baseFilename"]
            if data["baseExtension"] != None:
                baseExtension = data["baseExtension"] 

            
            if not ("baseDirectory" in data):
                msg = "base directory tag not found in json. Aborting"
                ExitsWithErrorMessage(msg,outputFile)
            else:
                currentDirectory = data["baseDirectory"]
                currentDirectory = os.path.abspath(currentDirectory)


            if "netCDF4FiltersFile" in data:
                if( os.path.isfile(data["netCDF4FiltersFile"])):
                    ProcessFiltersFile( data["netCDF4FiltersFile"] )
                else:
                    PrintError("filters file provided could not be opened",outputFile)
                

            if "singleDirectory" in data and data["singleDirectory"] != "None":
                singleDirectory = data["singleDirectory"]

            if "singleFile" in data and data["singleFile"] != "None":
                singleFile = data["singleFile"]

    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e) )
        printUsage()
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:", str(e))
        printUsage()


#
# helper function to retrieve a netCDF4 variable and apply filters
#   

def ProcessFiltersFile(filename):
    """  open and build netCDF4VariablesFilters dictionary from json file containing constraints required on netcdf4 variables if any

    Parameters
    ----------
    filename : str
            the absolute path file name of json file to process
    
    Returns
    -------
    none
    """
    global netCDF4VariablesFilters
    read_file = open(filename, "r")
    try:
        msg="Processing filters file..."
        PrintCurrentTask(msg,outputFile)
        netCDF4VariablesFilters = json.load(read_file)
        PrintSuccess()
    except Exception as e:
        PrintFailed()
        print(str(e))

#
# check a given filter is a valid coordinate for variable var
#
def CheckFilter(filter,var):
    """  check a given filter is a valid coordinate for variable var

    Parameters
    ----------
    filter : str
            name of the filter
    var : str
            name of the netcdf4 variable
    Returns
    -------
    True or False depending if filter is found in netCDFVariables dictionary
    """
    global netCDFVariables
    for l in netCDFVariables[var]:
        if filter in l:
            return True
    return False

def ProcessVariableFilters(path,var,histo, netCDF4File):
    """  apply filters on variable var

    Parameters
    ----------
    path : str
            an absolute file path where to serialize 2D histograms
    histo : str
            name of the type of histogram required presently only "hexbin" is used and defined
    netCDF4File : a netcdf4 dataset object
                netcdf4 file object
    Returns
    -------
    none
    """
    global netCDF4VariablesFilters,axisByname,outputFile,netCDFVariables,netCDF4SpatialCoordinates
    
    indices = dict() # contains for each axis indices to filter data
    filters = None
    
    if ( 'all' in  netCDF4VariablesFilters and  netCDF4VariablesFilters['all']['check'] == True ):
        filters =netCDF4VariablesFilters['all'][histo]["ranges"]
    else:
        filters =netCDF4VariablesFilters[var][histo]["ranges"]
    vv = netCDF4File[var]
    data = vv[:]
#
# first multidimensional variables
# filter data with other multidimensional variables
    ndims = netCDF4File.variables[var].ndim
    for filter in filters:
        if ( filter != 'time' and filter not in netCDF4SpatialCoordinates and filter in netCDF4File.variables) and (netCDF4File.variables[filter].ndim == ndims):
            if netCDF4File.variables[filter].shape != netCDF4File.variables[var].shape:
                PrintWARNING("variables '"+filter+"' and '"+var+"' dont share same shape.skipping.",outputFile)
                continue
            tmp = netCDF4File[filter][:]
            range = filters[filter]
            try:
                data = np.where( (tmp > range[0] ) & (tmp < range[1]),data,0.)
            except Exception as e:
                print(str(e))
    #
    #process 1d variable as longitude latitude
    #
        elif ( filter in netCDF4File.variables) and (netCDF4File.variables[filter].ndim == 1):
            #
#   time variable
#   special processing needed
            if filter == "time":
                if not ( 'time' in netCDF4File.variables[var].dimensions ):
                    continue
                base = dateUtilsParser.parse(ww3_toffset).timestamp()
                startdate = (dateUtilsParser.parse(filters[filter][0]).timestamp() - base)/86400. # in days not seconds
                enddate    = (dateUtilsParser.parse(filters[filter][1]).timestamp() - base)/86400. # in days not seconds
                # select indices nearest to startdate and enddate
                try:
                    i = str(axisByname[var][filter])
                    dataT = netCDF4File['time'][:]
                    if ( not (i  in indices) ): # axis not in dictionary
                        indices[i] = np.argwhere( (dataT > startdate) & (dataT < enddate)).flatten()
                    else:
                        # compute intersection of indices
                        tmp = np.argwhere( (dataT > startdate) & (dataT < enddate)).flatten()
                        indices[i] = np.intersect1d(tmp,indices[i])
                    del dataT
                except Exception as e:
                    print(str(e))
                    
            elif CheckFilter(filter,var):
                i = str(axisByname[var][filter]) # get corresponding axis
                tmp = netCDF4File[filter][:]
                conditions = None
                if ( 'all' in netCDF4VariablesFilters and netCDF4VariablesFilters['all']['check'] == True ):
                    conditions = netCDF4VariablesFilters['all'][histo]["ranges"][filter]
                else:
                    conditions = netCDF4VariablesFilters[var][histo]["ranges"][filter]

                if ( not (i  in indices) ):
                    indices[i] = np.argwhere( (tmp > conditions[0]) & (tmp < conditions[1])).flatten()
                else:
                    ii = np.argwhere( (tmp > conditions[0]) & (tmp < conditions[1])).flatten()
                    indices[i] = np.intersect1d(ii,indices[i])

    for i in indices:
        ax = int(i)
        data = np.take(data,indices[i],axis = ax)

    axes = netCDF4VariablesFilters[var][histo]["plot"]["axes"]
    x = netCDF4File[axes[0]][:]
    y = netCDF4File[axes[1]][:]
    ndimx = x.ndim
    if ndimx == 1:
        x = np.take(x,indices[str(axisByname[var][axes[0]])])
        y = np.take(y,indices[str(axisByname[var][axes[1]]) ])
    colormap = "inferno"
    if ( "colormap" in netCDF4VariablesFilters[var][histo]["plot"]):
        colormap = netCDF4VariablesFilters[var][histo]["plot"]["colormap"]

    if ( netCDF4VariablesFilters[var][histo]["plot"]["operators"] != None):
        for  op in netCDF4VariablesFilters[var][histo]["plot"]["operators"]:
            cpath = os.path.join(path,var+"_"+op[0]+".png")
            if ( op[0] == "min"):
                tmp = np.min(data,axis = axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif ( op[0] == "max"):
                tmp = np.max(data,axis = axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif ( op[0] == "mean"):
                tmp = np.mean(data,axis = axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif ( op[0] == "percentile95"):
                tmp = np.percentile(data,95,axis = axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif ( op[0] == "rms"): # compute root mean square along axis
                tmp = ComputeRMS(data,axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif (op[0] == "count"):
                tmp = np.sum(data,axisByname[var][op[1]])
                SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap,op[2])
            elif (op[0] == "pcolormesh"):
                tmp = data[op[1],]  # warning : time must be the first dimension ! otherwise convert into pandas dataframes with named columns&lines
                SavePColorMeshToFile(cpath,x,y,tmp,var,colormap)
            elif (op[0] == "scatter"):
                tmp = data[op[1],]  # warning : time must be the first dimension ! otherwise convert into pandas dataframes with named columns&lines
                SaveScatterToFile(cpath,x,y,tmp,var,colormap)
            elif (op[0] == "npstere"):
                tmp = data[op[1],]  # warning : time must be the first dimension ! otherwise convert into pandas dataframes with named columns&lines    
                SaveNPStereToFile(cpath,x,y,tmp,var,colormap,op[2])


def ComputeRMS(a,axe):
    """  compute root mean square of a given numpy array along a given axe

    Parameters
    ----------
    a : numpy array
            a multi dimensional numpy data array
    axe : integer
            the axis along which compute the RMS
    Returns
    -------
    none
    """
    try:
        mean = np.mean(np.square(a), axis=axe)
        return np.sqrt(mean)
    except Exception as e:
        print(str(e))

def SavePColorMeshToFile(path, x,y ,data,labelColorMap="counts",colormap ="inferno"):
    """  stores the pcolormesh plot to disk

    Parameters
    ----------
    path : str
         the absolute path name where to store the plot pcolormesh
    x,y : list of floats
         the data positions defining the X and Y axis
    data : numpy multi dimensional array
         the projected data on x,y
    labelColorMap : str
         a string to label the plot
    colormap : str
         The matplotlib Colormap instance or registered colormap name used to map the bin values to colors.
         see for instance https://matplotlib.org/stable/tutorials/colors/colormaps.html
    Returns
    -------
    none
    """
    fig = pl.figure()
    ax2D = fig.add_subplot(111)
    try:
        im = pl.pcolormesh(x,y,data)
        cb = fig.colorbar(im, ax=ax2D)
        cb.set_label(labelColorMap)
        pl.savefig(path)
        pl.show()
    except Exception as e:
        print(str(e))   


def SaveScatterToFile(path, x,y ,data,labelColorMap="counts",colormap ="inferno"):
    """  stores the scatter plot to disk

    Parameters
    ----------
    path : str
         the absolute path name where to store the plot scatter
    x,y : list of floats
         the data positions defining the X and Y axis
    data : numpy multi dimensional array
         the projected data on x,y
    labelColorMap : str
         a string to label the plot
    colormap : str
         The matplotlib Colormap instance or registered colormap name used to map the bin values to colors.
         see for instance https://matplotlib.org/stable/tutorials/colors/colormaps.html
    Returns
    -------
    none
    """
    fig = pl.figure()
    ax2D = fig.add_subplot(111)
    try:
        im = pl.scatter(x,y,data,c=data)
        cb = fig.colorbar(im, ax=ax2D)
        cb.set_label(labelColorMap)
        pl.savefig(path)
        pl.show()
    except Exception as e:
        print(str(e))   


def SaveNPStereToFile(path, x,y ,data,labelColorMap="counts",colormap ="inferno",bound=66):
    """  stores the npstere plot to disk

    Parameters
    ----------
    path : str
         the absolute path name where to store the plot npstere
    x,y : list of floats
         the data positions defining the X and Y axis
    data : numpy multi dimensional array
         the projected data on x,y
    labelColorMap : str
         a string to label the plot
    colormap : str
         The matplotlib Colormap instance or registered colormap name used to map the bin values to colors.
         see for instance https://matplotlib.org/stable/tutorials/colors/colormaps.html
    bound : float
         the boundary in latitudes where to cut the map projection
    Returns
    -------
    none
    """
    fig = pl.figure()
    ax2D = fig.add_subplot(111)
    try:
        map = Basemap(projection='npstere',boundinglat=bound,lon_0=0,resolution='l',round=True)
        map.drawcoastlines(linewidth=0.25)
        map.drawcountries(linewidth=0.25)
        map.fillcontinents(color='coral',lake_color='aqua')
        map.drawmapboundary(fill_color='aqua')
        map.drawmeridians(np.arange(0,360,10))
        map.drawparallels(np.arange(-90,90,10))
        map.drawmeridians(np.arange(0,360,20),labels=[0,0,1,0]) # labels = [left,right,top,bottom]
        map.drawparallels(np.arange(-90,90,10),labels=[1,1,0,0])
        X, Y = map(x, y)
        cs = map.pcolormesh(X,Y,data,cmap=cm.GMT_haxby) #,vmin=0,vmax=20)
        map.fillcontinents(color='coral',lake_color='aqua')
        map.drawcoastlines(linewidth=0.25)
        #cbar = map.colorbar(cs,location='bottom')
        pl.colorbar(label=labelColorMap)
        pl.savefig(path)
        pl.show()
    except Exception as e:
        print(str(e))   

def SaveHexBinHistoGramToFile(path, x,y ,data,labelColorMap="counts",colormap ="inferno",gridsize=200):
    """  stores the 2D histogram to disk

    Parameters
    ----------
    path : str
         the absolute path name where to store the plot histogram 2D
    x,y : list of floats
         the data positions defining the 2D binning
    data : numpy multi dimensional array
         the projected data on x,y
    labelColorMap : str
         a string to label the plot
    colormap : str
         The matplotlib Colormap instance or registered colormap name used to map the bin values to colors.
         see for instance https://matplotlib.org/stable/tutorials/colors/colormaps.html
    Returns
    -------
    none
    """
    fig = pl.figure()
    ax2D = fig.add_subplot(111)
    try:
        h = pl.hexbin(x,y,C = data,cmap=colormap,gridsize=gridsize)
        cb = fig.colorbar(h, ax=ax2D)
        cb.set_label(labelColorMap)
        pl.savefig(path)
        pl.show()
    except Exception as e:
        print(str(e))   

def WriteFileHeader():
    global outputFile,start,USE_MPI,outputFileName
    msgSessionStarted ="session started on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print("===",file=outputFile)
    print(msgSessionStarted,file=outputFile)
    print("script name : "+sys.argv[0],file=outputFile)
    print("command line: "+''.join(sys.argv[1:]),file=outputFile )
    print("output File : "+outputFileName,file=outputFile)
    print("===",file=outputFile)


def Process():
    """ main function when a recursive processing of a base directory is required.

    Parameters
    ----------
    none

    Returns
    -------

    none

    """
    global currentDirectory,currentCheck,subDirectories,outputFile,warningStr
# move to current directory
    os.chdir(currentDirectory)
    # retrieve directory content
    if ( baseFilename == None or baseExtension == None):
        PrintError(" baseFilename or baseExtension not defined. Aborting...",outputFile)
        return


    if currentDirectory == None or currentDirectory == "None" or not os.path.isdir(currentDirectory):
        PrintErrorAndExits("directory '"+currentDirectory+"' not a proper directory.",outputFile)

    # ** is used for recursive search
    matchingPattern = os.path.join(currentDirectory,"**/"+baseFilename+"."+baseExtension)
    listFiles = glob.iglob(matchingPattern,recursive=True) 
    fileCount=0
    for f in listFiles:
        fileCount+=1
        ProcessFile(f)
    if ( fileCount == 0 ):
        PrintErrorAndExits("no files found corresponding to matching pattern. Aborting...",outputFile)



def BuildUtilityDictionaries(netCDF4File):
    """ builds utility dictionaries to map netcdf4 dimensions to variables and map axis index to variable names

        Parameters
        ----------
        netCDF4File : dataset object
                    object representing netcdf4 file

        Returns
        -------

        none

    """
    global ExpectednetCDFVariables, netCDFDimensions,netCDFVariables,outputFile,axisByname
    global netCDF4SpatialCoordinates

    netCDFDimensions.clear()
    vars = netCDF4File.variables

    #get mapping between spatial coordinates and dimension
    for v in vars:
        if ( (not v in netCDF4SpatialCoordinates)):
            continue
        dims = vars[v].dimensions
        for i in range(0, len(dims) ):
                if not dims[i] in netCDFDimensions:
                    netCDFDimensions[dims[i]] = []
                netCDFDimensions[dims[i]].append(v)
    PrintCurrentTask("Building utility dictionaries...",outputFile)
    try:

        netCDFVariables.clear()        

        for v in vars :
            if v in netCDF4SpatialCoordinates:
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            i = 0
            axisByname[v] = dict()
            for d in dims:
                if d in netCDFDimensions:
                    axisByname[v][d] = i
                    for vv in netCDFDimensions[d]:
                        axisByname[v][vv] = i
                else:
                    axisByname[v][d] = i
                #axisByname[d] = i
                i+=1

        for v in vars :
            if ( v in netCDF4SpatialCoordinates or v == 'time' ):
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            
            netCDFVariables[v]=[]

            for d in dims:
                if d in netCDFDimensions:
                    l=[]
                    for dd in netCDFDimensions[d]:
                        l.append(dd)
                    netCDFVariables[v].append(l)
                else:
                    
                    netCDFVariables[v].append(d)
    except Exception as e:
        PrintFailed()
        print(str(e))
    else:                   
        PrintSuccess()

def OpenNetCDF4(path):
    """ given a path checks if netcdf4 file opens and returns a nectdf4 file object or none if fails

    Parameters
    ----------
    path : str
         a path to a netcdf4 file
    
    Returns
    -------
    a netcdf4 object or None
    
    """
    netCDF4File = None
    try:
        netCDF4File = nc.Dataset(path, "r", format="NETCDF4")

        BuildUtilityDictionaries(netCDF4File)

    except IOError as e:
        print("file '"+path+"' "+str(e))
    except Exception as e:
        print("file '"+path+"' "+str(e))
    else:
        return netCDF4File

def MkDirectory(path):
    """ utility function to recursively create a tree file directory

    Parameters
    ----------

    path : str
         the absolute path name of the directory to create
    
    Returns
    -------
    none

    """
    try:
        if not path or os.path.exists(path):
            return
        parent = os.path.abspath(os.path.join(path, '..'))
        if not os.path.exists(parent):
            MkDirectory(parent)
        os.mkdir(path)
        if ( not os.path.isdir(path) ):
            print("could not create directory '"+path+"'")
        else:
            print("directory '"+path+"' successfully created")
    except Exception as e:
        print(str(e))



def ProcessVariablesFilters(netCDF4File):
    """ process specified constraints on the content of a givne netCDF4 dataset object

    Parameters
    ----------

    nectCDF4File : dataset netcdf4 object
         the netcdf4 file object
    
    Returns
    -------

    none

    """
    global netCDF4VariablesFilters,currentPath,outputDirectory

    path = os.path.join(currentPath,"histograms2D")
    if ( outputDirectory != None):
        path = os.path.join(outputDirectory,"histograms2D")
    MkDirectory(path)

    if ( 'all' in netCDF4VariablesFilters and netCDF4VariablesFilters['all']['check'] == True):
        for histo in netCDF4VariablesFilters['all'] :
            if ( histo == 'check'):
                continue
            for v in netCDF4File.variables:
                if ( v in netCDF4SpatialCoordinates or v == 'time'):
                    continue
                ProcessVariableFilters(path,v,histo,netCDF4File)
        return

    for v in netCDF4VariablesFilters:
        for histo in netCDF4VariablesFilters[v]:
            ProcessVariableFilters(path,v,histo,netCDF4File)

    outputFile.flush()
#
# scan netCDF files in each sub directory
#  
def ProcessFile(f):
    """ process the given absolute path file name 

    Parameters
    ----------

    f : str
        absolute path file name of a netcdf4 file
    
    Returns
    -------
    none

    """
    global outputFile,currentPath
    if not ( os.path.isfile(f)):
        PrintWARNING("file '"+f+"' not found. skipping",outputFile)
        return
    print("processing file '"+f+"'")
    startT = time.time()
    netCDF4File = OpenNetCDF4( f )
    if ( netCDF4File == None):
        PrintWARNING( "file '"+f+"' could not be open. skipping...",outputFile)
        return
    netCDF4File.set_auto_mask(False)
    currentPath = os.path.dirname(f)
    PrintCurrentTask("Processing file '"+f+"'.",outputFile)
    PrintSuccess()

    ProcessVariablesFilters(netCDF4File)
    netCDF4File.close()
    endTime = time.time()
    print("elapsed time (s) "+str(endTime-startT),file=outputFile)

def ProcessSingleDirectory(directory):
    """ helper function called when processing a single directory

    Parameters
    ----------

    directory : str
         current directory where to start looking for netcdf4 files to compute 2D histograms from and store them as json and png files
    
    Returns
    -------
    none
    """
    global warningStr,outputFile,currentPath
    if not os.path.isdir(directory):
        PrintErrorAndExits("directory '"+directory+"' not a valid directory.",outputFile)

    matchingPattern = os.path.join(directory,"**/"+baseFilename+"."+baseExtension)
    listFiles = glob.iglob(matchingPattern,recursive=True) 
    for f in listFiles:
        warningStr =""
        netCDF4File = OpenNetCDF4(f)
        if ( warningStr ):
            PrintWARNING(warningStr,outputFile)
            continue
        ProcessFile(f)



###################################################################################################################################
#
#
#
#  script starts from now
#
#
# global variables
#
# Time offset in WW3 NetCDF files
ww3_toffset = '1990-01-01 00:00:00'
ExpectednetCDF4TimeStamps =[] # time stamps expected from time meta data
start = datetime.today()

startTime = start.now()

timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")

outputFile = None
outputFileName = None
tmpFileName = None
outputDirectory = None

#
# check mpi parallel processing is requested
#
mpi_comm = None
mpi_rank = None
mpi_size = None
mpi_found = False

USE_MPI = GetOptionArgFromCmdLine("--USE_MPI") != None
outputFileName =  GetOptionArgFromCmdLine("-o")
if ( outputFileName == None):
    outputFileName = "output.log"
outputBaseName = os.path.basename( outputFileName)
filename, file_extension = os.path.splitext(outputBaseName)
outputPath = os.path.dirname( outputFileName)
outputLogName = tmpFileName = filename+"_"+timeStr+file_extension

MkDirectory(outputPath)
outputFileName = tmpFileName = os.path.join(outputPath,outputLogName)

if not USE_MPI:
    outputFile = open(outputFileName,'w')
else:
    CheckForMPIModule()

# if mpi not found but was requested
if not mpi_found and USE_MPI:
    USE_MPI =False
    outputFileName = tmpFileName
    filename, file_extension = os.path.splitext(outputFileName)
    outputFileName = filename+"_"+timeStr+file_extension


netCDF4SpatialCoordinates = ["longitude","latitude"]

currentDirectory = "/home/user/work"
singleDirectory = None
singleFile = None
currentPath=""
baseFilename = None
baseExtension = "nc"

netCDF4VariablesFilters = None

mpiDirectories = []
errorStr = ""
warningStr =""

currentCheck =""
subDirectories = []
ProcessCmdLineOptions()




netCDFDimensions = dict()
netCDFVariables = dict()

netCDFVariables = dict()
netCDFVariablesStats = dict()
axisByname = dict()

#
# check netCDF4 module : mandatory
#
nc = None
msg = "Checking for NetCDF4 python module..."
PrintCurrentTask(msg,outputFile)
netCDF4_found = importlib.util.find_spec("netCDF4") != None
if not netCDF4_found:
    PrintErrorAndExits("netCDF4 module not found",outputFile)
import netCDF4  as nc
PrintSuccess()

WriteFileHeader()
outputFile.flush()

if ( singleDirectory != None or singleFile != None):
    if ( singleDirectory):
        msg='Process single directory'
        PrintINFO(msg,outputFile)
        ProcessSingleDirectory(singleDirectory)
    if ( singleFile ):
        msg='Process single file'
        PrintINFO(msg,outputFile)
        ProcessFile(singleFile)
    sys.exit(1)

if USE_MPI:
    index = mpi_rank
    while (index < len(mpiDirectories)):
        currentDirectory = mpiDirectories[index]
        Process()
        index += mpi_size
else:
    msg="Process recursively"
    PrintINFO(msg,outputFile)
    Process()

PrintFileFooter(outputFile)






