#!/usr/bin/python
import sys,getopt
import os,glob
import importlib
import json
import numpy as np
# high level functions for working with NetCDF files
import xarray as xr
from datetime import datetime
from string import Formatter
from datetime import timedelta
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
from dateutil import parser as dateUtilsParser

from astropy import visualization as astropyvisu

from scipy import linspace
from scipy import stats

import math
import pandas
import seaborn as sns

#
# utility functions

def date_to_jd(year,month,day,hour,minute,second):
    """
    Convert a date to Julian Day.
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet', 
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : float
        Day, may contain fractional part.
    Returns
    -------
    jd : float
        Julian Day
    Examples
    --------
    Convert 6 a.m., February 17, 1985 to Julian Day
    
    >>> date_to_jd(1985,2,17.25)
    2446113.75
    
    """
    
    day += hour/24.+minute/(24.*60.)+second/(24.*60.*60.)
    
    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
        (year == 1582 and month < 10) or
        (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)
        
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
        
    D = math.trunc(30.6001 * (monthp + 1))
    
    jd = B + C + D + day + 1720994.5
    
    return jd

def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = int(tdelta.total_seconds())
    elif inputtype in ['s', 'seconds']:
        remainder = int(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = int(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = int(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = int(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('W', 'D', 'H', 'M', 'S')
    constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)
#
#
# open a JSON file and return the object read from this file
#
def OpenJSONFile(filename):
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args)
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
    else:
        return data
    return None


#function to indicate test passed
def PrintSuccess():
    global termcolor_found,outputFile
    if termcolor_found:
        print(colored('[PASSED]','green') ,file=outputFile)
    else:
        print('[PASSED]',file=outputFile)

def PrintFailed():
    global termcolor_found,outputFile
    if termcolor_found:
        print(colored('[FAILED]','red') ,file=outputFile)
    else:
        print('[FAILED]',file=outputFile)

#function to indicate failure
def PrintError(errStr,outFile):
    global termcolor_found
    if termcolor_found :
        print( colored("[ERROR] : "+errStr,'red'),file=outFile )
    else:
        print( "[ERROR] "+errStr ,file=outFile)


#function to indicate failure and exit
def PrintErrorAndExits(errStr, outFile):
    
    PrintError(errorStr,outFile)
    PrintFileFooter(outFile)
    sys.exit(-1)
    print("Exiting with errors...")


def PrintFileFooter(outFile):
    global startTime
    endTime = datetime.now()
    elapsedTime = endTime-startTime
    print("Total elapsed time :",strfdelta(elapsedTime),file=outFile)
    msgSessionEnded ="session ended on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print(msgSessionEnded,file=outFile)
    print("Exiting...",file=outFile)
    outFile.close()


def ExitsWithErrorMessage(errStr,outFile):
    print( "[ERROR] : "+errStr,file=outFile )
    PrintFileFooter(outFile)
    sys.exit(-1)
    print("Exiting with errors...")

def PrintWARNING(warningStr,outFile):
    global termcolor_found
    if termcolor_found:
        print(colored("[WARNING] : "+warningStr,'red'),file=outFile )
    else:
        print("[WARNING] : "+warningStr,file=outFile)



#function to print current task message
def PrintCurrentTask(message,outFile):
    print("[INFO]"+message, end='',file=outFile )


#
# check if mpi module found if mpi requested
#
def CheckForMPIModule():
    global USE_MPI,outputFile,outputFileName
    global mpi_found,mpi_comm,mpi_rank,mpi_size,timeStr
    if not USE_MPI:
        return
    mpi_found = importlib.util.find_spec("mpi4py") != None
    if mpi_found:
        from mpi4py import MPI
        mpi_comm = MPI.COMM_WORLD
        mpi_rank = mpi_comm.Get_rank()
        mpi_size = mpi_comm.Get_size()
        filename, file_extension = os.path.splitext(outputFileName)
        outputFileName = filename+"_#mpi#"+str(mpi_rank)+"_"+timeStr+file_extension
        outputFile = open(outputFileName,"w")
        msg = "Checking for MPI python module..."
        PrintCurrentTask(msg,outputFile)
    
    if mpi_found :
        PrintSuccess()
    else:
        outputFile = open(outputFileName,"w")
        PrintWARNING("MPI module requested but not found in available python modules. going back to sequential execution...",outputFile)

#
# check for a particular option
#
def GetOptionArgFromCmdLine(optStr):
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:",['json=','USE_MPI','outputFile=',"single_directory="])
        for (o,a) in opts:
            if (o == optStr):
                return a
        
        if(optStr in args):
            return optStr

        return None
    except getopt.GetoptError as e:
        print("command line argument or option '"+optStr+" was not found. ", e.msg)    

#
# process command line options if any
#
def ProcessCmdLineOptions():
    global currentDirectory,singleDirectory
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:",['json=',"USE_MPI","mpi=","single_directory="])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)
            if o in ("-j"):
                currentDirectory = a
            if o in ("--single_directory"):
                singleDirectory = a


    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: ", e.msg)

#
# process a json file to retrieve user parameters
#
def ProcessJSONFile(filename):
    global ExpectedSubDirectories, netCDFSubDirectoriesNames,USE_MPI, mpiDirectories,currentDirectory
    global outputFileName,outputFile,USE_MPI,ExpectednetCDFDimensions,netCDFVariablesConsistencyCheckList
    global ExpectednetCDFVariables,baseFilename,baseExtension,netCDF4HistogramsVariables,netCDF4SpatialCoordinates
    try:
        
            data = OpenJSONFile(filename)

            if ( data["netCDF4SpatialCoordinates"] != None):
                netCDF4SpatialCoordinates = data["netCDF4SpatialCoordinates"]

            if ( data["netCDF4HistogramsVariables"] != None):
                netCDF4HistogramsVariables = data["netCDF4HistogramsVariables"]

            if data["baseFilename"] != None:
                baseFilename = data["baseFilename"]
            if data["baseExtension"] != None:
                baseExtension = data["baseExtension"] 

            if not data["ExpectedSubDirectories"] == None and isinstance(data["ExpectedSubDirectories"],list):
                ExpectedSubDirectories = data["ExpectedSubDirectories"]
                ExpectedSubDirectories.sort()
            else:
                PrintWARNING("tag 'ExpectedSubDirectories' not found in json file '"+filename+"'",outputFile)
            if not data["netCDFSubDirectoriesNames"] == None and isinstance(data["netCDFSubDirectoriesNames"],list):
                netCDFSubDirectoriesNames = data["netCDFSubDirectoriesNames"]
                netCDFSubDirectoriesNames.sort()
            else:
                PrintWARNING("tag 'netCDFSubDirectoriesNames' not found in json file '"+filename+"'",outputFile)
            if not data["mpi"] == None and isinstance(data["mpi"],list):
                USE_MPI = True
                mpiDirectories = data["mpi"]
            if data["baseDirectory"] == None:
                msg = "base directory tag not found in json. Aborting"
                ExitsWithErrorMessage(msg,outputFile)
            else:
                currentDirectory = data["baseDirectory"]

            if ( not data["netCDF4Dimensions"] == None and isinstance(data["netCDF4Dimensions"],list)):
                ExpectednetCDFDimensions = data["netCDF4Dimensions"]

            if ( not data["netCDF4Variables"] == None and isinstance(data["netCDF4Variables"],list)):
                ExpectednetCDFVariables = data["netCDF4Variables"]

            if ( not data["netCDFVariableConsistency"] == None and isinstance(data["netCDFVariableConsistency"],dict)):
                netCDFVariablesConsistencyCheckList = data["netCDFVariableConsistency"] 
            if ( not data["netCDF4FiltersFile"] == None ):
                if( os.path.isfile(data["netCDF4FiltersFile"])):
                    ProcessFiltersFile( data["netCDF4FiltersFile"] )
                else:
                    PrintError("filters file provided could not be opened",outputFile)
                


    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e) )
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:", str(e))


# function to scan a directory

def GetDirectoryContent(baseDirectory,dir):
    global errorStr
    os.chdir(baseDirectory)
    listDir = []
    if not os.path.exists(dir) or not os.path.isdir(dir) :
        errorStr = "directory '"+dir+"' not found. exiting"
        return listDir
    listDir =   os.listdir(dir)
    # filter only directories 
    listDir = FilterSubDirectoriesFrom(dir,listDir)
    return listDir

def GetFileContent(dir):
    global errorStr
    listFiles = []
    if not os.path.exists(os.path.abspath(dir) ) or not os.path.isdir(os.path.abspath(dir) ) :
        errorStr = "directory '"+dir+"' not found. exiting"
        return listFiles
    matchingPattern = os.path.join(dir,baseFilename+"."+baseExtension)
    listDir = glob.glob(matchingPattern) #os.listdir(dir)
    # filter only files 
    listDir = FilterFilesFrom(dir,listDir)
    return listDir

# function that filters sub directories from a list of elements
def filterSubDirectory(element):
    global warningStr,AllExpectedDirectories
    ok = os.path.isdir( os.path.abspath(element) )
    if ok and not (element in AllExpectedDirectories):
        warningStr += "'"+os.path.abspath(element)+"' does not match expected value\n"
    return ok

def filterFileDirectory(element):
    global warningStr,AllExpectedDirectories
    ok = os.path.isfile( os.path.abspath(element) )
    
    return ok

def FilterFilesFrom(baseDirectory, elements):
    os.chdir(baseDirectory)
    l=list(filter(filterFileDirectory,elements))
    l.sort()
    return l

def FilterSubDirectoriesFrom(baseDirectory, elements):
    currentPath = os.curdir
    os.chdir(baseDirectory)
    l=list(filter(filterSubDirectory,elements))
    l.sort()
    os.chdir(currentPath)
    return l

def TestSubDirectories(dirs):
    global errorStr
    ok = len(dirs) == ExpectedSubDirectoriesCount
    if  not ok :
        errorStr = "sub directories in '"+currentDirectory+" does not match"
    return ok

#
# helper function to retrieve a netCDF4 variable and apply filters
#   

def ProcessFiltersFile(filename):
    global netCDF4VariablesFilters
    read_file = open(filename, "r")
    try:  
        netCDF4VariablesFilters = json.load(read_file)
    except Exception as e:
        print(str(e))

#
# check a given filter is a valid coordinate for variable var
#
def CheckFilter(filter,var):
    global netCDFVariables
    for l in netCDFVariables[var]:
        if filter in l:
            return True
    return False

def ProcessVariableFilters(path,var,histo, netCDF4File):
    global netCDF4VariablesFilters,axisByname,outputFile,netCDFVariables,netCDF4SpatialCoordinates
    
    indices = dict() # contains for each axis indices to filter data
    filters = None
    
    if ( 'all' in  netCDF4VariablesFilters and  netCDF4VariablesFilters['all']['check'] == True ):
        filters =netCDF4VariablesFilters['all'][histo]["ranges"]
    else:
        filters =netCDF4VariablesFilters[var][histo]["ranges"]
    vv = netCDF4File[var]
    data = vv[:]
#
# first multidimensional variables
# filter data with other multidimensional variables
    ndims = netCDF4File.variables[var].ndim
    for filter in filters:
        if ( filter != 'time' and filter not in netCDF4SpatialCoordinates and filter in netCDF4File.variables) and (netCDF4File.variables[filter].ndim == ndims):
            if netCDF4File.variables[filter].shape != netCDF4File.variables[var].shape:
                PrintWARNING("variables '"+filter+"' and '"+var+"' dont share same shape.skipping.",outputFile)
                continue
            tmp = netCDF4File[filter][:]
            range = filters[filter]
            try:
                data = np.where( (tmp > range[0] ) & (tmp < range[1]),data,0.)
            except Exception as e:
                print(str(e))
    #
    #process 1d variable as longitude latitude
    #
        elif ( filter in netCDF4File.variables) and (netCDF4File.variables[filter].ndim == 1):
            #
#   time variable
#   special processing needed
            if filter == "time":
                if not ( 'time' in netCDF4File.variables[var].dimensions ):
                    continue
                base = dateUtilsParser.parse(ww3_toffset).timestamp()
                startdate = (dateUtilsParser.parse(filters[filter][0]).timestamp() - base)/86400. # in days not seconds
                enddate    = (dateUtilsParser.parse(filters[filter][1]).timestamp() - base)/86400. # in days not seconds
                # select indices nearest to startdate and enddate
                try:
                    i = str(axisByname[var][filter])
                    dataT = netCDF4File['time'][:]
                    if ( not (i  in indices) ): # axis not in dictionary
                        indices[i] = np.argwhere( (dataT > startdate) & (dataT < enddate)).flatten()
                    else:
                        # compute intersection of indices
                        tmp = np.argwhere( (dataT > startdate) & (dataT < enddate)).flatten()
                        indices[i] = np.intersect1d(tmp,indices[i])
                    del dataT
                except Exception as e:
                    print(str(e))
                    
            elif CheckFilter(filter,var):
                i = str(axisByname[var][filter]) # get corresponding axis
                tmp = netCDF4File[filter][:]
                conditions = None
                if ( 'all' in netCDF4VariablesFilters and netCDF4VariablesFilters['all']['check'] == True ):
                    conditions = netCDF4VariablesFilters['all'][histo]["ranges"][filter]
                else:
                    conditions = netCDF4VariablesFilters[var][histo]["ranges"][filter]

                if ( not (i  in indices) ):
                    indices[i] = np.argwhere( (tmp > conditions[0]) & (tmp < conditions[1])).flatten()
                else:
                    ii = np.argwhere( (tmp > conditions[0]) & (tmp < conditions[1])).flatten()
                    indices[i] = np.intersect1d(ii,indices[i])

    for i in indices:
        ax = int(i)
        data = np.take(data,indices[i],axis = ax)

    axes = netCDF4VariablesFilters[var][histo]["plot"]["axes"]
    s = axes[0]
    x = netCDF4File[s][:]
    x = np.take(x,indices[str(axisByname[var][axes[0]])])
    y = netCDF4File[axes[1]][:]
    y = np.take(y,indices[str(axisByname[var][axes[1]]) ])
    colormap = "inferno"
    if ( "colormap" in netCDF4VariablesFilters[var][histo]["plot"]):
        colormap = netCDF4VariablesFilters[var][histo]["plot"]["colormap"]

    if ( netCDF4VariablesFilters[var][histo]["plot"]["operators"] != None):
        for  op in netCDF4VariablesFilters[var][histo]["plot"]["operators"]:
            cpath = os.path.join(path,"hexbin2D_"+var+"_"+op[0]+".png")
            if ( op[0] == "min"):
                tmp = np.min(data,axis = axisByname[var][op[1]])
            elif ( op[0] == "max"):
                tmp = np.max(data,axis = axisByname[var][op[1]])
            elif ( op[0] == "mean"):
                tmp = np.mean(data,axis = axisByname[var][op[1]])
            elif ( op[0] == "percentile95"):
                tmp = np.percentile(data,95,axis = axisByname[var][op[1]])
            elif ( op[0] == "rms"): # compute root mean square along axis
                tmp = ComputeRMS(data,axisByname[var][op[1]])
            elif (op[0] == "count"):
                tmp = np.sum(data,axisByname[var][op[1]])
            SaveHexBinHistoGramToFile(cpath,x,y,tmp,op[0],colormap)

    return data

def ComputeRMS(a,axe):
    try:
        mean = np.mean(np.square(a), axis=axe)
        return np.sqrt(mean)
    except Exception as e:
        print(str(e))

def SaveHexBinHistoGramToFile(path, x,y ,data,labelColorMap="counts",colormap ="inferno"):
    fig = pl.figure()
    ax2D = fig.add_subplot(111)
    try:
        
        h = pl.hexbin(x,y,C = data,cmap=colormap,gridsize=200)
        cb = fig.colorbar(h, ax=ax2D)
        cb.set_label(labelColorMap)
        pl.savefig(path)
        #pl.show()
    except Exception as e:
        print(str(e))   



#
# analyze sub directory structure
# dirs contains a list a subdirectories to scan
# 
def TestSubDirectoryStructure(dirs):
    global errorStr,outputFile,currentDirectory,warningStr
    matches = 0
    for dir in dirs:
        absPath = os.path.join(currentDirectory,dir)
        listDir = GetDirectoryContent(currentDirectory,dir)
        listDir = FilterSubDirectoriesFrom(absPath,listDir )
        listDir.sort()
        if listDir == netCDFSubDirectoriesNames:
            matches += 1
        else:
            warningStr += " sub directory '"+os.path.abspath(dir)+"' does not match expected directory structure.\n"
            

    if matches == len(dirs):
        return True
    else:
        errorStr = "some sub directories do not match expected srtucture"
    return False

def WriteFileHeader():
    global outputFile,start,USE_MPI,outputFileName
    msgSessionStarted ="session started on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print("===",file=outputFile)
    print(msgSessionStarted,file=outputFile)
    print("script name : "+sys.argv[0],file=outputFile)
    print("command line: "+''.join(sys.argv[1:]),file=outputFile )
    print("MPI         : "+str(USE_MPI),file=outputFile)
    print("output File : "+outputFileName,file=outputFile)
    print("===",file=outputFile)

def Process():
    global currentDirectory,currentCheck,subDirectories,outputFile,warningStr
# move to current directory
    os.chdir(currentDirectory)
#
# testing directory content
#
    
    currentCheck = "scanning directory '"+currentDirectory+"'..."
    
    # retrieve directory content
    subDirectories = GetDirectoryContent('.',currentDirectory)

    if  len(subDirectories) == 0 :
        PrintCurrentTask(currentCheck,outputFile)
        PrintErrorAndExits(errorStr,outputFile)
    else:
        PrintCurrentTask(currentCheck,outputFile)
        PrintSuccess()
#
# testing number of sub directories
#

#
    currentCheck = "scanning sub directories in '"+currentDirectory+"'..."
    warningStr = ""
    if not TestSubDirectories(subDirectories) :
        PrintCurrentTask(currentCheck,outputFile)
        PrintErrorAndExits(errorStr,outputFile)
    else:
        PrintCurrentTask(currentCheck,outputFile)
        PrintSuccess()
    if warningStr :
        PrintWARNING(warningStr,outputFile)
#
# scan each sub directory structure
#
    currentCheck = "scanning structure of sub directories in '"+currentDirectory+"'..."
    warningStr = ""
    if not TestSubDirectoryStructure(subDirectories):
        PrintCurrentTask(currentCheck,outputFile)
        PrintErrorAndExits(errorStr,outputFile)
    else:
        PrintCurrentTask(currentCheck,outputFile)
        PrintSuccess()   
    if ( warningStr ):
        PrintWARNING(warningStr,outputFile)
    ProcessNetCDF4(subDirectories)
#
# process netCDF4 files checking
#
    currentCheck = "scanning structure of sub directories in '"+currentDirectory+"'..."
#
# open a netCDF4 file
#

def BuildUtilityDictionaries(netCDF4File):
    global ExpectednetCDFVariables, netCDFDimensions,netCDFVariables,outputFile,axisByname
    global netCDF4SpatialCoordinates

    netCDFDimensions.clear()
    vars = netCDF4File.variables

    #get mapping between spatial coordinates and dimension
    for v in vars:
        if ( (not v in netCDF4SpatialCoordinates)):
            continue
        dims = vars[v].dimensions
        for i in range(0, len(dims) ):
                if not dims[i] in netCDFDimensions:
                    netCDFDimensions[dims[i]] = []
                netCDFDimensions[dims[i]].append(v)

    try:

        netCDFVariables.clear()        

        for v in vars :
            if v in netCDF4SpatialCoordinates:
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            i = 0
            axisByname[v] = dict()
            for d in dims:
                if d in netCDFDimensions:
                    axisByname[v][d] = i
                    for vv in netCDFDimensions[d]:
                        axisByname[v][vv] = i
                else:
                    axisByname[v][d] = i
                #axisByname[d] = i
                i+=1

        for v in vars :
            if ( v in netCDF4SpatialCoordinates or v == 'time' ):
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            
            netCDFVariables[v]=[]

            for d in dims:
                if d in netCDFDimensions:
                    l=[]
                    for dd in netCDFDimensions[d]:
                        l.append(dd)
                    netCDFVariables[v].append(l)
                else:
                    
                    netCDFVariables[v].append(d)
    except Exception as e:
        print(str(e))
    else:                   
        PrintCurrentTask("building utility dictionaries...",outputFile)
        PrintSuccess()


# time offset correction for ww3 netcdf data
def datenum(d):
    return 366 + d.toordinal() + (d - datetime.fromordinal(d.toordinal())).total_seconds()/(24*60*60)

def ComputeTimeStamps(date1, date2,strfreq):
    sfreq = 'H'
    if ( strfreq == "dayly"):
        freq='D'
    elif (strfreq == "hourly"):
        freq = 'H'
    elif(strfreq == "secondly"):
        freq = 'S'
    dates = pandas.date_range(date1, date2, freq=sfreq).to_pydatetime()
    tS = []
    for d in dates:
        tS.append( datetime.timestamp(d))
    tS.sort()
    return tS



def OpenNetCDF4(path):
    global nc,warningStr,xr
    try:
        netCDF4File = nc.Dataset(path, "r", format="NETCDF4")

        BuildUtilityDictionaries(netCDF4File)

    except IOError as e:
        warningStr += "file '"+path+"' could not be open. "+str(e)+"\n"
    except Exception as e:
        warningStr+= "file '"+path+"' could not be open. "+str(e)+"\n"
    else:
        return netCDF4File


def OpenNetCDF4FromXarray(path):
    global nc,warningStr,xr,outputFile
    try:
        xArrayDataSet = xr.open_dataset(path)
        print(xArrayDataSet,file=outputFile)
        outputFile.flush()

    except IOError as e:
        warningStr += "file '"+path+"' could not be open. "+str(e)+"\n"
    except Exception as e:
        warningStr+= "file '"+path+"' could not be open. "+str(e)+"\n"
    else:
        return xArrayDataSet


def ComputeOnDataArray(var,xArrayDataSet):
    #
    # get dimensions
    #
    dims = []
    coords={}
    for d in netCDFVariables[var]:
        dims.append(d[0])
    try:
        fig = pl.subplot()
        print (xArrayDataSet)
        keys = xArrayDataSet.variables

        for k in keys:
            print(k)

        var = xArrayDataSet['hs']
        #var.coords['lon'] = ('node',)
        g = var.groupby_bins(group='node',bins=20) 
        pl.show()
    except Exception as e:
            print(str(e))
    toto = True
#    
#
def CheckNetCDF4FileHeader(netCDF4File):
    global ExpectednetCDFDimensions,ExpectednetCDFVariables,errorStr,outputFile
    if netCDF4File == None:
        return False
    ok = True

    for var in netCDF4File.variables.values():
        print(var,file = outputFile)
    outputFile.flush()

    for d in ExpectednetCDFDimensions:
        if not d in   netCDF4File.dimensions:
            ok = False
            errorStr += "[ERROR]: dimension '"+d+"' not found in netCDF4 Header."
    for v in ExpectednetCDFVariables:
        if not v in   netCDF4File.variables:
            ok = False
            errorStr += "[ERROR]: variable '"+v+"' not found in netCDF4 Header."
    return ok 

def SaveHistogramToFile(path,title, xlabel,ylabel,x,y):
    global pl
    fig = pl.hist2d(x,y,normed=0,bins='auto')
    pl.title(title)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    pl.savefig(path)

#
# https://github.com/oldmonkABA/optimal_histogram_bin_width
#
def ComputeOptimalBinWidth_ShimazakiShinomoto(data,min,max):
    n_min = 2   #Minimum number of bins Ideal value = 2
    n_max = 200  #Maximum number of bins  Ideal value =200
    n_shift = 30     #number of shifts Ideal value = 30
    N = np.array(range(n_min,n_max))
    D = float(max-min)/N    #Bin width vector
    Cs = np.zeros((len(D),n_shift)) #Cost function vector
    #Computation of the cost function
    for i in range(np.size(N)):
        shift = linspace(0,D[i],n_shift)
        for j in range(n_shift):
            edges = linspace(min+shift[j]-D[i]/2,max+shift[j]-D[i]/2,N[i]+1) # shift the Bin edges
            binindex = np.digitize(data,edges) #Find binindex of each data point
            ki=np.bincount(binindex)[1:N[i]+1] #Find number of points in each bin
            k = np.mean(ki) #Mean of event count
            v = sum((ki-k)**2)/N[i] #Variance of event count
            Cs[i,j]+= (2*k-v)/((D[i])**2) #The cost Function
    C=Cs.mean(1)

    #Optimal Bin Size Selection
    loc = np.argwhere(Cs==Cs.min())[0]
    cmin = C.min()
    idx  = np.where(C==cmin)
    idx = idx[0][0]
    optD = D[idx]
    #print ('Optimal Bin Number :',N[idx])
    #print ('Optimal Bin Width :',optD)
    return (N[idx],optD)


def Compute3DHistograms(netCDF4File):
    for v in netCDF4HistogramsVariables["elements"]:
        msg = "computing histograms for variable '"+v+"'..."
        var = netCDF4File[v][:]
        p = netCDFVariables[v][1]
        l = p[1]
        latlon = []
        for i in l:
            latlon.append( netCDF4File[i][:] )
        latitudes = np.linspace(latlon[0].min(),latlon[0].max())
        longitudes = np.linspace(latlon[1].min(),latlon[1].max())
        for lat in latitudes:
            for lon in longitudes:
                np.argwhere

def ComputeHistogramArea(histogram,dx):
    Area = 0.
    for h in histogram:
        Area += h*dx
    return Area

def Compute3DHistogram(v, netCDF4File):
    var = netCDF4File[v][:]
    data = []
    try:
        data = np.mean( var , axis = 0)
    except Exception as e:
        print(str(e))

    p = netCDFVariables[v][1]
    l = p[1]
    latlon = []
    for i in l:
        latlon.append( netCDF4File[i][:] )
    fig = pl.figure()
    h2 = pl.hexbin(latlon[0],latlon[1],C = data,cmap="inferno",gridsize=200)
    height = h2.get_array()
    coord = h2.get_offsets()
    x = []
    y = []
    for c in coord:
        x.append(c[0])
        y.append(c[1])
    ax3D = fig.add_subplot(111,projection='3d')
    ax3D.bar3d( x,y,0, 1, 1, height )    
    pl.savefig("/home/pdescour/TESTPYTHON/bar3d.png")
    pl.show()

"/home/datawork-resourcecode/EFTP/DATA/TESTYEAR2017/01/FREQ_NC"

def ComputeCountInRange(a,b,data):
    try:
        count =  ((a <= data) & (data < b)).sum()
    except Exception as e:
        print(str(e))
    return count

def MkDirectory(path):
    global outputFile
    try:
        if ( not os.path.isdir(path) ):
            os.mkdir(path)
        if ( not os.path.isdir(path) ):
            PrintErrorAndExits("could not create directory '"+path+"'",outputFile)
    except Exception as e:
        PrintErrorAndExits(str(e),outputFile)


def ProcessHistogramForVariable(name,path,netCDF4File,v):
    global outputFile,netCDF4SpatialCoordinates
    if  v in netCDF4SpatialCoordinates or v == 'time':
        return
    data = []
    try:
        netCDF4File.set_auto_mask(False)
        var = netCDF4File[v][:]
        data = var.ravel()
        del var
        pl.hist(data,bins='auto')
        del data
        filePath = os.path.join(path,"histogram_"+v+".png")
        pl.savefig(filePath)
    except Exception as e:
        print(str(e))
        PrintWARNING("ProcessHistogramForVariable : a problem occured for variable '"+v+"' :"+str(e), outputFile)

def ComputeInverseDataHistogram(name,netCDF4File):
    global netCDFVariablesStats,netCDF4HistogramsVariables,currentPath

    if ( netCDF4HistogramsVariables == None):
        return
    path = os.path.join(currentPath,"histograms")
    MkDirectory(path)
    elements = []
    if ( netCDF4HistogramsVariables["all"] == True):
        elements = netCDF4File.variables
    else:
        elements= netCDF4HistogramsVariables["elements"]

    for v in elements:
        ProcessHistogramForVariable(path,netCDF4File,v)
    
def ComputeCountTimeSeries(directory):
    global netCDFVariablesStats,netCDF4HistogramsVariables,currentPath
    vars = netCDF4File.variables
    data =dict()
    for v in vars:
        ComputeVariableCountTimeSeries(v,netCDF4File,data)


def ComputeVariableCountTimeSeries(var,nectCDF4File,data):

    data[var]=dict()

def CheckMinMaxnetCDFVariables(netCDF4File):
    
    global netCDFVariables,np,currentDirectory,netCDFDimensions,outputFile,netCDFVariablesStats
    strDump=""
    
    for v in ExpectednetCDFVariables:
        msg = "computing min/max and std deviation for variable '"+v+"'..."
        PrintCurrentTask(msg,outputFile)
        var = netCDF4File[v]
        a = var[:] 
        strDump=""
        
        minV = np.amin(a)
        minI = np.unravel_index(np.argmin(a, axis=None), a.shape)
        maxV = np.amax(a)
        maxI = np.unravel_index(np.argmax(a, axis=None), a.shape)
        mean = np.mean(a)
        stdDeviation = np.std(a)
        strDump= " variable:"+v+" min:"+str(minV)+" max:"+str(maxV)+" mean:"+str(mean)+" std deviation:"+str(stdDeviation)
        objStat = {"min":str(minV),"max":str(maxV),"mean":str(mean),"std deviation":str(stdDeviation)}
        netCDFVariablesStats[v]=objStat
        PrintSuccess()
        
        print(strDump,file=outputFile)
        if a.ndim == 1:
            continue
        else:
            strMin=""
            strMax=""
            listobj =dict()
            try:
                # retrieve all variables associated with dimensions
                for i in range(0,a.ndim):
                    p = netCDFVariables[v][i]
                    l = p[1]
                    for vv in l:
                        data = netCDF4File[vv]
                        valuem = data[minI[i]]
                        valueM = data[maxI[i]]
                        strMin+="  variable:"+str(vv)+" min:"+str(valuem)
                        strMax+="  variable:"+str(vv)+" max:"+str(valueM)
                        listobj[vv]= { "min":str(valuem),"max":str(valueM)}
                netCDFVariablesStats[v]=(objStat,listobj)
            except Exception as e:
                print("error detected : "+str(e) ,file=outputFile)
            strMin +="\n"
            strMax +="\n"
            print(json.dumps(netCDFVariablesStats))
            print(strMin,file=outputFile)
            print(strMax,file=outputFile)
            outputFile.flush()


def CheckTimeVariableCorrectness(netCDF4File):
    global ExpectednetCDF4TimeStamps,dateUtilsParser,ww3_toffset,outputFile
    msg = "checking 'time' variable correctness from metadata..."
    data = None
    try:
        dd = netCDF4File.__dict__ # dictionary of meta data
        startDateTime = None
        endDateTime = None
        frequency = 'hourly'
        if ( dd['start_date'] ):
            startDateTime =    dateUtilsParser.parse(dd['start_date'])
        if ( dd['stop_date'] ):
            endDateTime = dateUtilsParser.parse(dd['stop_date'])
        if ( dd['field_type']):
            frequency = dd['field_type']
    except Exception as e:
        ExitsWithErrorMessage("could not retrieve header time meta data. "+str(e),outputFile)
    else:
        expectedTimeStamps = ComputeTimeStamps(startDateTime,endDateTime,frequency)
        #
        # date from wich compute days offset fo each time data
        #
        baseDate = dateUtilsParser.parse(ww3_toffset)
        #
        # get time data
        #
        try:
            times = netCDF4File['time']
        except Exception as e:
            PrintWARNING("time variable not found : '"+str(e)+"'",outputFile)
        else:
        #
        # compute time stamps from time data
        #
            netCDF4TimeStamps = []
            for t in times:
                netCDF4TimeStamps.append( datetime.timestamp( baseDate+ timedelta(days=t))  )
            netCDF4TimeStamps.sort()
            PrintCurrentTask(msg,outputFile)
            if ( netCDF4TimeStamps == expectedTimeStamps):
                PrintSuccess()
            else:
                ExitsWithErrorMessage("expected time data range from metadata and extracted time data range do not coincide",outputFile)
            



def CheckVariableConsistency(netCDF4File):
    global outputFile,netCDFVariablesConsistencyCheckList

    for v in netCDFVariablesConsistencyCheckList:
        l = netCDFVariablesConsistencyCheckList[v]
        try:
            data = netCDF4File[v][:]
        except Exception as e:
            PrintWARNING("'"+v+"' not found. skipping consitency check."+str(e),outputFile )
        else:
            unique,counts = np.lib.arraysetops.unique(data,return_counts = True)
            if ( l["doublons"] == True):
                msg = "checking '"+v+"' coordinate does not contain doublons..."
                PrintCurrentTask(msg,outputFile)
                has_doublons = np.argmax(counts > 1) != 0
                if has_doublons:
                    PrintFailed()
                else:
                    PrintSuccess()
            if ( l["monotonous"] == True):   
                msg = "checking '"+v+"' coordinate is monotonous..."
                is_monotonous = np.array_equal(data,unique)
                PrintCurrentTask(msg,outputFile)
                if is_monotonous:
                    PrintSuccess()
                else:
                    PrintFailed()
    outputFile.flush()

def ProcessVariablesFilters(netCDF4File):
    global netCDF4VariablesFilters,currentPath

    path = os.path.join(currentPath,"histograms2D")
    MkDirectory(path)

    if ( 'all' in netCDF4VariablesFilters and netCDF4VariablesFilters['all']['check'] == True):
        for histo in netCDF4VariablesFilters['all'] :
            if ( histo == 'check'):
                continue
            for v in netCDF4File.variables:
                if ( v in netCDF4SpatialCoordinates or v == 'time'):
                    continue
                ProcessVariableFilters(path,v,histo,netCDF4File)
        return

    for v in netCDF4VariablesFilters:
        for histo in netCDF4VariablesFilters[v]:
            ProcessVariableFilters(path,v,histo,netCDF4File)


def GetAttributeValue(netCDF4File, varname,attrname):
    variable = netCDF4File[varname]
    print( variable.dtype )
    return getattr(variable, attrname)

def CheckAttributes(netCDF4File):
    global outputFile

    if netCDF4File == None:
        return
    msg = "checking valid_min and valid_max data type for multi-dimensional variables"
    check = True
    for name, variable in netCDF4File.variables.items():
        if ( variable.ndim == 1 ):
            continue
        try:
            ok = (variable.dtype == variable.valid_max.dtype) &  (variable.dtype == variable.valid_min.dtype)  
        except AttributeError:
            continue
        else:
            check &= ok
            if ( not ok ):
                msg = "variable '"+name+"' : data type '"+variable.dtype.name+"'  valid_min : '"+variable.valid_min.dtype.name+"' valid_max : '"+variable.valid_max.dtype.name+"'"
                PrintWARNING(msg,outputFile)

    PrintCurrentTask(msg,outputFile)
    if ( check ):
        PrintSuccess()
    else:
        PrintFailed()
    outputFile.flush()
#
# scan netCDF files in each sub directory
#  
def ProcessNetCDF4(dirs):
    global currentDirectory,warningStr,outputFile,errorStr,currentPath

    netCDF4File = None
    xArrayDataSet = None
    absPath = ""
    for d in dirs:
        
        subDirs = GetDirectoryContent(currentDirectory,d)
        base = os.path.join(currentDirectory,d)
        os.chdir(base)
        for subd in subDirs:
            warningStr = ""
            errorStr = ""
            netCDF4File = None
            
            currentPath = os.path.abspath(subd) 
            files = GetFileContent(currentPath)
            if ( not files):
                PrintWARNING(currentPath+" is empty",outputFile)
                continue

            for f in files:
                absPath = os.path.join(currentPath,f)
                msg = "checking netCDF4 file '"+absPath+"' can be opened"
                netCDF4File = OpenNetCDF4( absPath )

                #CheckAttributes(netCDF4File)
                #CheckVariableConsistency(netCDF4File)
                #ProcessVariablesFilters(netCDF4File)

                #
                # check date correctness
                #
                #CheckTimeVariableCorrectness(netCDF4File)
                #xArrayDataSet = OpenNetCDF4FromXarray(absPath)
                if ( netCDF4File != None ):

                   

                    PrintCurrentTask(msg,outputFile)
                    PrintSuccess()
                    msg = "checking netCDF4 file '"+absPath+"' header"
                    h = CheckNetCDF4FileHeader(netCDF4File)
                    if ( h ):
                        PrintCurrentTask(msg,outputFile)
                        PrintSuccess()
                    else:
                        PrintWARNING(" header ",outputFile)


                    
                    CheckMinMaxnetCDFVariables(netCDF4File)





                else:
                    PrintCurrentTask(msg,outputFile)
                    PrintFailed()

            msg = "checking files opening in '"+currentPath+"'"
            PrintCurrentTask(msg,outputFile)
            if ( not warningStr ):
                PrintSuccess()
            else:
                PrintFailed()
                PrintWARNING(warningStr,outputFile)
                PrintError("some files could not be read properly",outputFile)
            msg = "checking files headers in '"+currentPath+"'"    
            if ( not errorStr) :
                PrintSuccess()
            else:
                PrintErrorAndExits(errorStr,outputFile)
            outputFile.flush()
def processSingleDirectory(directory):
    global warningStr,outputFile,currentPath
    if not os.path.isdir(directory):
        PrintErrorAndExits("directory '"+directory+"' not a proper directory.",outputFile)
    matchingPattern = os.path.join(directory,baseFilename+"."+baseExtension)
    os.chdir(singleDirectory)
    listSubDirs = os.listdir()
    for subDir in listSubDirs:
        currentPath = os.path.join(singleDirectory,subDir)
        if ( not os.path.isdir(currentPath)):
            continue
        matchingPattern = os.path.join(currentPath,baseFilename+"."+baseExtension)
        listFiles = glob.glob(matchingPattern) 
        listFiles.sort()
        last_dir=""
        for f in listFiles:
            dir = os.path.dirname(f)

            warningStr =""
            netCDF4File = OpenNetCDF4(f)
            if ( warningStr ):
                PrintWARNING(warningStr,outputFile)
                continue
            ComputeInverseDataHistogram(netCDF4File)

            last_dir = dir


###################################################################################################################################
#
#
#
#  script starts from now
#
#
# global variables
#
# Time offset in WW3 NetCDF files
ww3_toffset = '1990-01-01 00:00:00'
ExpectednetCDF4TimeStamps =[] # time stamps expected from time meta data
start = datetime.today()

startTime = start.now()

timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")

outputFile = None
outputFileName = None
tmpFileName = None
#
# check termcolor module is available
#
USE_TERMCOLOR = False

termcolor_found = False
if USE_TERMCOLOR:
    msg = "Checking for termcolor python module..."
    termcolor_found = importlib.util.find_spec("termcolor") != None
#
# check mpi parallel processing is requested
#
mpi_comm = None
mpi_rank = None
mpi_size = None
mpi_found = False

USE_MPI = GetOptionArgFromCmdLine("--USE_MPI") != None
outputFileName =  GetOptionArgFromCmdLine("-o")
if ( outputFileName == None):
    outputFileName = "output.log"
filename, file_extension = os.path.splitext(outputFileName)
outputFileName = tmpFileName = filename+"_"+timeStr+file_extension

if not USE_MPI:
    outputFile = open(outputFileName,'w')
else:
    CheckForMPIModule()

# if mpi not found but was requested
if not mpi_found and USE_MPI:
    USE_MPI =False
    outputFileName = tmpFileName
    filename, file_extension = os.path.splitext(outputFileName)
    outputFileName = filename+"_"+timeStr+file_extension

ExpectednetCDFDimensions = ["time","level"]
netCDFVariablesConsistencyCheckList = ["time"]
ExpectednetCDFVariables = ["longitude","latitude","hs"]
netCDF4SpatialCoordinates = ["longitude","latitude"]
ExpectedSubDirectories = ["01","02","03","04","05","06","07","08","09","10","11","12"]
ExpectedSubDirectories.sort()
netCDFSubDirectoriesNames = ["BUOY_STAT",  "FIELD_NC",  "FREQ_NC",  "SPEC_NC"]
netCDFSubDirectoriesNames.sort()
ExpectedSubDirectoriesCount = len(ExpectedSubDirectories)
currentDirectory = "/home/pdescour/TESTPYTHON"
singleDirectory = None
currentPath=""
baseFilename = None
baseExtension = "nc"
netCDF4HistogramsVariables = None
netCDF4VariablesFilters = None

mpiDirectories = []
errorStr = ""
warningStr =""

currentCheck =""
subDirectories = []
ProcessCmdLineOptions()




netCDFDimensions = dict()
netCDFVariables = dict()

netCDFVariables = dict()
netCDFVariablesStats = dict()
axisByname = dict()

AllExpectedDirectories = ExpectedSubDirectories+netCDFSubDirectoriesNames

if USE_TERMCOLOR:
    if termcolor_found:
        from termcolor import colored
        PrintCurrentTask(msg,outputFile)
        PrintSuccess()
    else:
        msg = "termcolor python module not found but not mandatory."
        PrintWARNING(msg,outputFile)

#
# check netCDF4 module : mandatory
#
nc = None
msg = "Checking for NetCDF4 python module..."
PrintCurrentTask(msg,outputFile)
netCDF4_found = importlib.util.find_spec("netCDF4") != None
if not netCDF4_found:
    PrintErrorAndExits("netCDF4 module not found",outputFile)
import netCDF4  as nc
PrintSuccess()

WriteFileHeader()
outputFile.flush()

if ( singleDirectory != None):
    processSingleDirectory(singleDirectory)
    sys.exit(1)

if USE_MPI:
    index = mpi_rank
    while (index < len(mpiDirectories)):
        currentDirectory = mpiDirectories[index]
        Process()
        index += mpi_size
else:
    Process()







