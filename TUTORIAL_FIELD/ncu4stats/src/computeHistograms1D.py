#!/usr/bin/python
import sys,getopt
import os,glob,fnmatch
import importlib
import json
import numpy as np
# high level functions for working with NetCDF files
from datetime import datetime
from string import Formatter
from datetime import timedelta
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
from dateutil import parser as dateUtilsParser
from scipy import stats
import time
import math

#
# utility functions

def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = int(tdelta.total_seconds())
    elif inputtype in ['s', 'seconds']:
        remainder = int(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = int(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = int(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = int(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('W', 'D', 'H', 'M', 'S')
    constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)

#
#
# open a JSON file and return the object read from this file
#
def OpenJSONFile(filename):
    """  open a JSON file and return the json object read from this file or None

    Parameters
    ----------
    filename : str
             the absolute path of the json file
    
    Returns
    -------
    none
    """
    data = None
    try:
        read_file = open(filename, "r")
        data = json.load(read_file)
    except IOError as e:
        print("could not open json file '"+filename+"' "+e.args)
        printUsage()
    except: #handle other exceptions such as attribute errors
        print ("Unexpected error:", sys.exc_info()[0])
        printUsage()
    else:
        return data
    return None


#function to indicate test passed
def PrintSuccess():
    global outputFile
    print('[PASSED]',file=outputFile)

def PrintFailed():
    global outputFile
    print('[FAILED]',file=outputFile)

#function to indicate failure
def PrintError(errStr,outFile):
    print( "[ERROR] "+errStr ,file=outFile)


#function to indicate failure and exit
def PrintErrorAndExits(errStr, outFile):
    PrintError(errStr,outFile)
    PrintFileFooter(outFile)
    print("Exiting with errors...")
    sys.exit(-1)


def PrintFileFooter(outFile):
    global startTime
    endTime = datetime.now()
    elapsedTime = endTime-startTime
    print("===",file=outFile)
    print("Total elapsed time :",strfdelta(elapsedTime),file=outFile)
    msgSessionEnded ="session ended on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print(msgSessionEnded,file=outFile)
    print("Exiting...",file=outFile)
    outFile.close()


def ExitsWithErrorMessage(errStr,outFile):
    print( "[ERROR] : "+errStr,file=outFile )
    PrintFileFooter(outFile)
    print("Exiting with errors...")
    sys.exit(-1)

def PrintWARNING(warningStr,outFile):
    print("[WARNING] : "+warningStr,file=outFile)

def PrintINFO(infoStr,outFile):
    print("[INFO] : "+infoStr,file=outFile)



#function to print current task message
def PrintCurrentTask(message,outFile):
    print("[TASK] "+message, end='',file=outFile )

#
# check for a particular option
#
def GetOptionArgFromCmdLine(optStr):
    """  check command line arguments for a specific option

    Parameters
    ----------
    optStr : str
             the whole command line from unix system view point
    
    Returns
    -------
    the associated value to the required option if any
    """
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        opts,args = getopt.getopt(l,"j:d:o:h:",['help','json=','outputFile=',"single_directory="])
        for (o,a) in opts:
            if (o == optStr):
                return a
        if(optStr in args):
            return optStr

        return None
    except getopt.GetoptError as e:
        print("command line argument or option '"+optStr+" was not found. ", e.msg)    
        printUsage()

#
# process command line options if any
#
def ProcessCmdLineOptions():
    """  process command line arguments especially the json parameters file

    Parameters
    ----------
    none

    Returns
    -------
    none
    """
    global currentDirectory,singleDirectory
    try:
        s = ''.join(sys.argv[1:])
        l = s.split()
        if ( len(l) == 0):
            printUsage()
        opts,args = getopt.getopt(l,"j:d:o:h:",['json=',"help","mpi=","single_directory="])
        for o,a in opts:
            if o in ( "-j","--json","-json"):
                ProcessJSONFile(a)
            if o in ("-j"):
                currentDirectory = a
            if o in ("--single_directory"):
                singleDirectory = a
            if o in ("--help","-h" ):
                printUsage(True)


    except getopt.GetoptError as e:
        print("At leat one command line parameters was not recognized: ", e.msg)
        printUsage()

def printUsage( exitNow = False ):
    """ print usage

    Parameters
    ----------   

    none

    Returns
    -------
    none
    """
    print("usage : python3 computeHistograms1D.py --json=path_to_json_file")
    print(" json file sample is provided as ")
    print("{")
    print(" \"outputLog\":\"/home/user/work/histos1D.log\", ")
    print(" \"outputDirectory\":\"/home/user/output\", ")
    print(" \"baseDirectory\": \"/home/user/work\", ")
    print(" \"baseFilename\": \"RSCD_WW3-RSCD-UG*\", ")
    print(" \"baseExtension\":\"*nc\", ")
    print(" \"netCDF4HistogramsVariables\":{\"all\":true,\"variables\":[\"hs\"]}, ")
    print(" \"netCDF4SpatialCoordinates\":[\"longitude\",\"latitude\"], ")
    print(" \"singleDirectory\":\"None\", ")
    print(" \"singleFile\":\"/home/user/work/LOPS_WW3-PACE-10M_199810_fp.nc\", ")
    print(" \"cumulate\":true, ")
    print(" \"numBins\":50, ")
    print(" \"histosFilters\":{\"all\":{\"dpt\":[1,2],\"ptp1\":[1,1.5]}}, ")
    print(" \"xTicksMultiplicator\":10 ")
    print("}")
    if exitNow:
        print("exiting...")
        sys.exit(-1)


#
# process a json file to retrieve user parameters
#
def ProcessJSONFile(filename):
    """ process a json file to retrieve user parameters

    Parameters
    ----------
    filename : str
            the absolute path file name of json file to process
    
    Returns
    -------
    none
    """
    global currentDirectory,netCDF4HistogramsVariables
    global outputFileName,outputFile,singleDirectory,singleFile
    global baseFilename,baseExtension,netCDF4SpatialCoordinates
    global cumulate,numBins,manualRange,outputDirectory,histosFilters,plotInfos,xTicksMultiplicator
    try:
            print("[INFO] processing json file ",file=outputFile )
            data = OpenJSONFile(filename)

            if 'histosFilters' in data:
                histosFilters = data['histosFilters']

            if ( 'outputDirectory' in data ):
                outputDirectory = data["outputDirectory"]

            if 'plotInfos' in data:
                plotInfos = data["plotInfos"]

            if 'xTicksMultiplicator' in data:
                xTicksMultiplicator = data['xTicksMultiplicator']

            if ( 'manualRange' in data and data['manualRange'] != 'None'):
                manualRange = data['manualRange']
                for v in manualRange:
                    if not( "range" in manualRange[v]):
                        print("[WARNING] manual Range detected but no range was provided for '"+v+"'. exiting...")
                        printUsage()
                    if not("numBins" in manualRange[v]):
                        print("[WARNING] manual Range detected but no bins number was provided for '"+v+"'")
                        printUsage()

            if "cumulate" in data:
                cumulate = data["cumulate"]

            if "numBins" in data:
                numBins = data["numBins"]

            if "outputLog" in data:
                outputFileName = data["outputLog"]

            if "netCDF4SpatialCoordinates" in data:
                netCDF4SpatialCoordinates = data["netCDF4SpatialCoordinates"]

            if "baseFilename" in data :
                baseFilename = data["baseFilename"]
            if data["baseExtension"] != None:
                baseExtension = data["baseExtension"] 

            
            if not ("baseDirectory" in data):
                msg = "base directory tag not found in json. Aborting"
                ExitsWithErrorMessage(msg,outputFile)
            else:
                currentDirectory = data["baseDirectory"]


            if  not ("netCDF4HistogramsVariables" in data["netCDF4HistogramsVariables"] ):
                netCDF4HistogramsVariables = data["netCDF4HistogramsVariables"]
            else:
                print("[ERROR] parameter netCDF4HistogramsVariables not provided in json file. Aborting...")
                printUsage()

            if "singleDirectory" in data and data["singleDirectory"] != "None":
                singleDirectory = data["singleDirectory"]

            if "singleFile" in data and data["singleFile"] != "None":
                singleFile = data["singleFile"]

    except IOError as e:
        print("could not open json file '"+filename+"' : "+str(e) )
        printUsage()
    except Exception as e: #handle other exceptions such as attribute errors
        print ("Unexpected error:", str(e))
        printUsage()


#

def WriteFileHeader():
    global outputFile,start,outputFileName
    msgSessionStarted ="session started on "+start.strftime("%B %d, %Y")+" "+start.strftime("%H:%M:%S")
    print("===",file=outputFile)
    print(msgSessionStarted,file=outputFile)
    print("script name : "+sys.argv[0],file=outputFile)
    print("command line: "+''.join(sys.argv[1:]),file=outputFile )
    print("output File : "+outputFileName,file=outputFile)
    print("===",file=outputFile)


def Process():
    """ main function when a recursive processing of a base directory is required.

    Parameters
    ----------
    none

    Returns
    -------

    none

    """
    global currentDirectory,currentCheck,subDirectories,outputFile,warningStr, cumulate
    global outputDirectory
# move to current directory
    os.chdir(currentDirectory)
    # retrieve directory content
    if ( baseFilename == None or baseExtension == None):
        PrintError(" baseFilename or baseExtension not defined. Aborting...",outputFile)
        return


    if currentDirectory == None or currentDirectory == "None" or not os.path.isdir(currentDirectory):
        PrintErrorAndExits("directory '"+currentDirectory+"' not a proper directory.",outputFile)

    # ** is used for recursive search
    matchingPattern = os.path.join(currentDirectory,"**/"+baseFilename+"."+baseExtension)
    listFiles = glob.iglob(matchingPattern,recursive=True) 
    listFiles.sort()
    if ( cumulate ):
        print("[INFO] computing global min/max for all data.",file = outputFile)
        fileCount=0
        for f in listFiles:
            fileCount+=1
            print("[INFO] computing global min/max for all data in file '"+f+"'")
            print("[INFO] computing global min/max for all data in file '"+f+"'",file = outputFile)
            ProcessComputeGlobalMinMax(f)
        if ( fileCount == 0 ):
            PrintErrorAndExits("no files found corresponding to matching pattern. Aborting...",outputFile)
            
    ProcessNetCDF4(listFiles)
    SaveConcatenatedHistograms(currentDirectory)
        

def BuildUtilityDictionaries(netCDF4File):
    """ builds utility dictionaries to map netcdf4 dimensions to variables and map axis index to variable names

        Parameters
        ----------
        netCDF4File : dataset object
                    object representing netcdf4 file

        Returns
        -------

        none

    """
    global netCDFDimensions,netCDFVariables,outputFile,axisByname
    global netCDF4SpatialCoordinates

    netCDFDimensions.clear()
    vars = netCDF4File.variables

    #get mapping between spatial coordinates and dimension
    for v in vars:
        if ( (not v in netCDF4SpatialCoordinates)):
            continue
        dims = vars[v].dimensions
        for i in range(0, len(dims) ):
                if not dims[i] in netCDFDimensions:
                    netCDFDimensions[dims[i]] = []
                netCDFDimensions[dims[i]].append(v)
    PrintCurrentTask("Building utility dictionaries...",outputFile)
    try:

        netCDFVariables.clear()        

        for v in vars :
            if v in netCDF4SpatialCoordinates:
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            i = 0
            axisByname[v] = dict()
            for d in dims:
                if d in netCDFDimensions:
                    axisByname[v][d] = i
                    for vv in netCDFDimensions[d]:
                        axisByname[v][vv] = i
                else:
                    axisByname[v][d] = i
                #axisByname[d] = i
                i+=1

        for v in vars :
            if ( v in netCDF4SpatialCoordinates or v == 'time' ):
                continue
            dims = vars[v].dimensions # list of dimensions associated to variable
            
            netCDFVariables[v]=[]

            for d in dims:
                if d in netCDFDimensions:
                    l=[]
                    for dd in netCDFDimensions[d]:
                        l.append(dd)
                    netCDFVariables[v].append(l)
                else:
                    
                    netCDFVariables[v].append(d)
    except Exception as e:
        PrintFailed()
        print(str(e))
    else:                   
        PrintSuccess()

def OpenNetCDF4(path):
    """ given a path checks if netcdf4 file opens and returns a nectdf4 file object or none if fails

    Parameters
    ----------
    path : str
         a path to a netcdf4 file
    
    Returns
    -------
    a netcdf4 object or None
    
    """
    netCDF4File = None
    try:
        netCDF4File = nc.Dataset(path, "r", format="NETCDF4")

        BuildUtilityDictionaries(netCDF4File)

    except IOError as e:
        print("file '"+path+"' "+str(e))
    except Exception as e:
        print("file '"+path+"' "+str(e))
    else:
        return netCDF4File

def MkDirectory(path):
    """ utility function to recursively create a tree file directory

    Parameters
    ----------

    path : str
         the absolute path name of the directory to create
    
    Returns
    -------
    none

    """
    try:
        if not path or os.path.exists(path):
            return
        parent = os.path.abspath(os.path.join(path, '..'))
        if not os.path.exists(parent):
            MkDirectory(parent)
        os.mkdir(path)
        if ( not os.path.isdir(path) ):
            print("could not create directory '"+path+"'")
        else:
            print("directory '"+path+"' successfully created")
    except Exception as e:
        print(str(e))





def ProcessHistogramForVariable(path,netCDF4File,v):
    """ core computation of histogram data for a given variable

    Parameters
    ----------

    path : str
         the absolute path name of the directory to output histogram png files and associated json data
    
    netCDF4File : opencdf4 dataset object
                the current processed netcdf4 file
    v : str
      name of the current netcdf4 variable
    Returns
    -------

    none

    """
    global outputFile,netCDF4SpatialCoordinates
    global cumulate,histosHashMap,numBins,histosFilters,plotInfos,xTicksMultiplicator

    if  v in netCDF4SpatialCoordinates or v == 'time' or not (v in netCDF4File.variables):
        return
    if not (v in netCDF4File.variables):
        return
    if cumulate:
        if v not in histosHashMap:
            return
    print("[INFO] global min/max for variable '"+v+"' "+str(histosHashMap[v]['min'])+"/"+str(histosHashMap[v]['max']),file=outputFile)
    print("[INFO] global min/max for variable '"+v+"' "+str(histosHashMap[v]['min'])+"/"+str(histosHashMap[v]['max']))        
    data = []
    counts = []
    bins = []
    try:
        fillValue = None
        scaleFactor = 1
        try:
            scaleFactor = netCDF4File[v].scale_factor
        except Exception:
            scaleFactor = 1
        try:
            fillValue =float(netCDF4File[v]._FillValue)
            fillValue *= scaleFactor
        except Exception:
            fillValue = 1
        var = netCDF4File[v][:]
        data =  var.flatten()
        del var
        
        if ( data.size == 0 ):
            PrintWARNING("variable '"+v+"' data empty. skipping...",outputFile)
            return
        if histosFilters != None:
            if "all" in histosFilters or v in histosFilters:
                indices = []
                # apply to variable
                var = v
                if "all" in histosFilters:
                    var ="all"
                tmp = []
                print("scanning constraints ",file = outputFile)
                if v in histosFilters[var]:
                    try:
                            print("found variable "+v+" in filters. applying constraint only constraint "+str(histosFilters[var][v]),file = outputFile)
                            vv = netCDF4File[v][:].flatten()
                            if ( isinstance(histosFilters[var][v][0],str) ):
                                indices = np.argwhere( (vv <= histosFilters[var][v][1]) ).flatten()
                            elif ( isinstance(histosFilters[var][v][1],str) ):
                                indices = np.argwhere( (vv >= histosFilters[var][v][0]) ).flatten()
                            elif isinstance(histosFilters[var][v],list):
                                indices = np.argwhere( (vv >= histosFilters[var][v][0]) & (vv <= histosFilters[var][v][1]) ).flatten()
                            elif ( histosFilters[var][v][1] == histosFilters[var][v][0]):
                                indices = np.argwhere( (data == histosFilters[var][v][1])).flatten()
                            else:
                                print("[WARNING] variable '"+vv+"' not able to apply constraints. skipping...")
                                return
                    except Exception as e:  
                        print("[WARNING] exception occured :"+str(e),file=outputFile)
                        return
                else:
                    for key in histosFilters[var]:
                        print("computing constraints from key "+key+"  value "+str(histosFilters[var][key]),file = outputFile)
                        if key not in netCDF4File.variables:
                            if isinstance(histosFilters[var][key],list):
                                if ( isinstance(histosFilters[var][key][0],str) ):
                                    tmp = np.argwhere( (data <= histosFilters[var][key][1])).flatten()
                                elif ( isinstance(histosFilters[var][key][1],str) ):
                                    tmp = np.argwhere( (data >= histosFilters[var][key][0]) ).flatten()
                                elif ( histosFilters[var][key][1] == histosFilters[var][key][0]):
                                    tmp = np.argwhere( (data == histosFilters[var][key][1])).flatten()
                                else:
                                    tmp = np.argwhere( (data >= histosFilters[var][key][0]) & (data <= histosFilters[var][key][1])).flatten()
                                if len(indices) == 0:
                                    indices = tmp
                                else:
                                    indices = np.intersect1d(tmp,indices)
                            else:
                                print("[WARNING] constraints on variable '"+v+"' coud not be interpreted correctly. skipping...",file=outputFile)
                                continue
                        else:
                            if netCDF4File.variables[key].shape != netCDF4File.variables[v].shape:
                                print("[WARNING] variables '"+key+"' and '"+v+"' dont share same shape.skipping constraints.",file=outputFile)
                                continue
                            try:
                                vv = netCDF4File[key][:].flatten()
                                if ( isinstance(histosFilters[var][key][0],str) ):
                                    tmp = np.argwhere( (vv <= histosFilters[var][key][1]) ).flatten()
                                elif ( isinstance(histosFilters[var][key][1],str) ):
                                    tmp = np.argwhere( (vv >= histosFilters[var][key][0]) ).flatten()
                                elif isinstance(histosFilters[var][key],list):
                                    tmp = np.argwhere( (vv >= histosFilters[var][key][0]) & (vv <= histosFilters[var][key][1]) ).flatten()
                                elif ( histosFilters[var][key][1] == histosFilters[var][key][0]):
                                    tmp = np.argwhere( (data == histosFilters[var][key][1])).flatten()
                                else:
                                    print("[WARNING] variable '"+vv+"' not able to apply constraints. skipping...")
                                    return
                                if len(indices) == 0:
                                    indices = tmp
                                else:
                                    indices = np.intersect1d(tmp,indices)
                                
                            except Exception as e:  
                                print("[WARNING] exception occured :"+str(e),file=outputFile)
                                return
                if len(indices) != 0:
                    data = np.take(data,indices).flatten()
                else:
                    print("[WARNING]   constraints produced empty filter on variable '"+v+"'. skipping contraints",file=outputFile)
        if fillValue:
            mask = np.nonzero(data != fillValue)
            data = np.take(data,mask).flatten()
        if ( data.size == 0 ):
            PrintWARNING("variable '"+v+"' data empty. skipping...",outputFile)
            return 
        if not cumulate:
            counts, bins = np.histogram(data,bins='fd')
        else:
            binsdata = histosHashMap[v]['bins']
            counts, bins = np.histogram(data,bins=binsdata)
        del data

        if not cumulate:
            pl.clf()
            pl.hist(bins[:-1], bins, weights=counts)
            histojson = dict()
            histojson["bins"] = list(bins)
            histojson["counts"] = list(counts)
            jsonFile = open(os.path.join(path,"histogram_"+v+".json"),"w")
            json.dump(str(histojson),jsonFile)
            jsonFile.close()
            filePath = os.path.join(path,"histogram_"+v+".png")
            if plotInfos:
                pl.suptitle("'"+v+"'"+plotInfos)
            else:
                pl.suptitle("'"+v+"'")
            N = int(len(list(bins))/xTicksMultiplicator)
            xticks = np.linspace(bins[0],bins[-1],num=N )
            pl.xticks(ticks=xticks,labels=xticks)
            pl.xlabel(netCDF4File[v].units)
            pl.savefig(filePath)
        else:

            #if not (v in histosHashMap):
            #    histosHashMap[v] = dict()
            #    histosHashMap[v]['counts'] = counts
            #    histosHashMap[v]['units'] = netCDF4File[v].units
            #    return

            if not('units' in histosHashMap[v] ):
                try:
                    histosHashMap[v]['units'] = netCDF4File[v].units
                except Exception:
                    histosHashMap[v]['units'] = "none"
            if not('counts' in histosHashMap[v] ):
                histosHashMap[v]['counts']  = counts
                return
            histosHashMap[v]['counts']  =  histosHashMap[v]['counts']+counts
    except Exception as e:
        print(str(e))
        return
        #PrintWARNING("ProcessHistogramForVariable : a problem occured for variable '"+v+"' :"+str(e), outputFile)

def ComputeGlobalMinMax(netCDF4File,vars):
    """ 
    compute global min/max values for a given netcdf4 file. Allows to compute min/max values 
    for histogram in case global cumulate is true. cumulate boolean allows to compute min/max across
    all netcdf4 files

    Parameters
    ----------

    netCDF4File : opencdf4 dataset object
                the current processed netcdf4 file
    vars : list of variables names for which histogram computation is required
           
    Returns
    -------

    result is stored in global hash map histosHashMap

    """
    global netCDFVariablesStats,netCDF4HistogramsVariables,currentPath,numBins,outputFile,histosHashMap

    for v in vars:
        if not( v in netCDF4File.variables):
            continue
        fillValue = None
        scaleFactor = 1
        try:
            scaleFactor = netCDF4File[v].scale_factor
        except Exception:
            scaleFactor = 1
        try:
            fillValue =float(netCDF4File[v]._FillValue)
            fillValue *= scaleFactor
        except Exception:
            fillValue = None

        var = netCDF4File[v][:]
        data =  var.ravel()
        del var
        if fillValue:
            mask = np.nonzero(data != fillValue)
            data = np.take(data,mask)
        if ( data.size == 0):
            PrintWARNING("variable '"+v+"' contains no data. skipping... ",outputFile)
            continue
        min = data.min()
        max = data.max()
        del data
        if ( v not in histosHashMap ):
            histosHashMap[v] = dict()
            histosHashMap[v]['min'] = sys.float_info.max
            histosHashMap[v]['max'] = sys.float_info.min
        print("variable '"+v+"' local file min/max :"+str(min)+"/"+str(max) ,file=outputFile)
        if min < histosHashMap[v]['min']:
            histosHashMap[v]['min'] = min
        if max > histosHashMap[v]['max']:
            histosHashMap[v]['max'] = max
        print("variable '"+v+"' global min:"+str(histosHashMap[v]['min'])+"  max "+str(histosHashMap[v]['max']),file=outputFile )
        histosHashMap[v]['bins']=np.linspace(histosHashMap[v]['min'],histosHashMap[v]['max'],numBins+1)
        histosHashMap[v]['increment']= histosHashMap[v]['bins'][1]-histosHashMap[v]['bins'][0]

def getVariablesForHistos(path):
    """ retrieve variables for which to compute histograms based on the value of netCDF4HistogramsVariables

    Parameters
    ----------

    path : str
                the absolute path to a netcdf4 file
           
    Returns
    -------
    a list of variables names for which to compute histograms

    """
    global netCDF4HistogramsVariables
    if netCDF4HistogramsVariables["all"] == True:
        netCDF4File = OpenNetCDF4(path)
        if netCDF4File == None:
            return
        elements = netCDF4File.variables()
        netCDF4File.close()
        return elements
    
    elements= netCDF4HistogramsVariables["variables"]
    return elements

def getVariablesForHistos(netCDF4File):
    """ retrieve variables for which to compute histograms based on the value of netCDF4HistogramsVariables

    Parameters
    ----------

    netCDF4File : a netcdf4 dataset object
                a nectcdf4 file object
           
    Returns
    -------
    a list of variables names for which to compute histograms

    """
    global netCDF4HistogramsVariables
    if netCDF4HistogramsVariables["all"] == True:
        elements = list(netCDF4File.variables)
        return elements

    elements= netCDF4HistogramsVariables["variables"]
    return elements

def ComputeInverseDataHistogram(netCDF4File):
    """ helper function which calls core computation function on each variable for which to get histogram

    Parameters
    ----------

    netCDF4File : a netcdf4 dataset object
                a nectcdf4 file object
           
    Returns
    -------
    none

    """
    global netCDFVariablesStats,netCDF4HistogramsVariables,currentPath,outputDirectory

    if ( netCDF4HistogramsVariables == None):
        return
    path = os.path.join(currentPath,"histograms")
    if ( outputDirectory != None):
        path = os.path.join(outputDirectory,"histograms")
    MkDirectory(path)
    
    elements = getVariablesForHistos(netCDF4File)
    for v in elements:
        ProcessHistogramForVariable(path,netCDF4File,v)


#
# scan netCDF files in each sub directory
#  
def ProcessFile(f):
    """ helper function to open a given file and process computation of histograms on required variables in current file

    Parameters
    ----------

    f : str
        absolute path file name of a netcdf4 file
    
    Returns
    -------
    none

    """
    global outputFile,currentPath,cumulate
    if not ( os.path.isfile(f)):
        PrintWARNING("file '"+f+"' not found. skipping",outputFile)
        return
    print("processing file '"+f+"'")
    startT = time.time()
    netCDF4File = OpenNetCDF4( f )
    if ( netCDF4File == None):
        PrintWARNING( "file '"+f+"' could not be open. skipping...",outputFile)
        return
    netCDF4File.set_auto_mask(False)
    currentPath = os.path.dirname(f)
    PrintCurrentTask("Processing file '"+f+"'.",outputFile)
    PrintSuccess()

    ComputeInverseDataHistogram(netCDF4File)
    netCDF4File.close()
    endTime = time.time()
    print("elapsed time (s) "+str(endTime-startT),file=outputFile)



def ProcessComputeGlobalMinMax(f):
    """ helper function to scan a file to retrieve local min/max values in order to compute histograms in case cumulate is true

    Parameters
    ----------

    f : str
        name of the absolute path of a netcdf4 file
           
    Returns
    -------
    none
    """
    global outputFile,currentPath,cumulate
    if not ( os.path.isfile(f)):
        PrintWARNING("file '"+f+"' not found. skipping",outputFile)
        return
    print("[INFO] processing file '"+f+"' for min/max values",file=outputFile)
    startT = time.time()
    netCDF4File = OpenNetCDF4( f )
    if ( netCDF4File == None):
        PrintWARNING( "file '"+f+"' could not be open. skipping...",outputFile)
        return
    elements = getVariablesForHistos(netCDF4File)
    netCDF4File.set_auto_mask(False)
    currentPath = os.path.dirname(f)
    PrintCurrentTask("processing file '"+f+"'.",outputFile)
    PrintSuccess()

    ComputeGlobalMinMax(netCDF4File,elements)
    netCDF4File.close()
    endTime = time.time()
    print("elapsed time (s) "+str(endTime-startT),file=outputFile)

def ProcessNetCDF4(files):
    """ helper function to process a list of files

    Parameters
    ----------

    files : list
        a list of netcdf4 files names
           
    Returns
    -------
    none
    """
    for f in files:
        ProcessFile(f)
    outputFile.flush()

def filterDirectory(element):
    return os.path.isdir( os.path.abspath(element) )


def searchMinMax(file):
    """ helper function to search for min/max values to process histograms

    Parameters
    ----------

    files : str
        a netcdf4 file name
           
    Returns
    -------
    none
    """
    global numBins,manualRange,outputFile,histosHashMap

    if ( netCDF4HistogramsVariables == None):
        return
    netCDF4File = OpenNetCDF4(file)
    if (netCDF4File == None):
        return

    elements = getVariablesForHistos(netCDF4File)
    
    if ( manualRange != None):

        for v in elements:
            if not (v in manualRange ):
                PrintWARNING(" variable '"+v+"' not in manual Range",outputFile)
                continue
            if v in histosHashMap:
                continue
            histosHashMap[v]=dict()
            histosHashMap[v]['bins']=np.linspace(manualRange[v]["range"][0],manualRange[v]["range"][1],manualRange[v]["numBins"]+1)
            histosHashMap[v]['min']= manualRange[v]["range"][0]
            histosHashMap[v]['max']= manualRange[v]["range"][1]
            histosHashMap[v]['increment'] = histosHashMap[v]['bins'][1]-histosHashMap[v]['bins'][0]
        netCDF4File.close()
        return

    ComputeGlobalMinMax(netCDF4File,elements)
    netCDF4File.close()

def SaveConcatenatedHistograms(directory):
    """ helper function to serialize on disk json histograms data for each required variable

    Parameters
    ----------

    directory : str
        a directory absolute path where to store png files and json data
           
    Returns
    -------
    none
    """
    global histosHashMap,cumulate,outputDirectory,plotInfos

    if ( cumulate ):
        path = os.path.join(directory,"cumulated_histos")
        if ( outputDirectory != None):
            path = os.path.join(outputDirectory,"cumulated_histos")
        MkDirectory(path)

        for v in histosHashMap:
            bins = list(histosHashMap[v]['bins'])
            counts = []
            try:
                counts = list(histosHashMap[v]['counts'])
            except Exception:
                print("no counts found for variable "+v,file=outputFile)
                continue
            pl.clf()
            pl.hist(bins[:-1], bins, weights=counts)
            histojson = dict()
            histojson["bins"] = bins
            histojson["counts"] = counts
            histojson["increment"] = histosHashMap[v]['increment']
            histojson["units"] = histosHashMap[v]['units']
            histojson["min"] = histosHashMap[v]['min']
            histojson["max"] = histosHashMap[v]['max']
            jsonFile = open(os.path.join(path,"histogram_"+v+".json"),"w")
            json.dump(str(histojson),jsonFile)
            jsonFile.close()
            filePath = os.path.join(path,"cumulated_histogram_"+v+".png")
            if plotInfos:
                pl.suptitle("'"+v+"'"+plotInfos)
            else:
                pl.suptitle("'"+v+"'")
            pl.xlabel(histosHashMap[v]['units'])
            N = int(len(list(bins))/xTicksMultiplicator)
            xticks = np.linspace(bins[0],bins[-1],num=N )
            xticksLabels = [None] * len(xticks)
            for i in range(0,len(xticks)):
                xticksLabels[i] = str(f'{xticks[i]:0.2f}')
            pl.xticks(ticks=xticks,labels=xticksLabels)

            pl.savefig(filePath)


def processGlobalMinMax(parentDirectory):
    """ 
    recursive helper function to process all directories where to search for netcdf4 files to retrieve global min/max
    to compute histograms for.

    Parameters
    ----------

    parentDirectory : str
        parent directory of current directory where to list netcdf4 files according to specified wildcards
           
    Returns
    -------
    none
    """
    global baseFilename,baseExtension
    for root, subdirs, files in os.walk(parentDirectory):
        if (len(files) != 0):
            matchingPattern = os.path.join(os.path.join(parentDirectory,baseFilename+"."+baseExtension) )
            listFiles = glob.glob(matchingPattern) 
            listFiles.sort()
            for f in listFiles:
                searchMinMax(f)
            
        if ( len(subdirs) != 0):
            for d in subdirs:
                processGlobalMinMax( os.path.join(parentDirectory,d) )
            return
        # this directory has no sub directories

def processDirectory(parentDirectory):
    """ 
    recursive helper function to process all directories where to search for netcdf4 files to retrieve global min/max
    to compute histograms for.

    Parameters
    ----------

    parentDirectory : str
        parent directory of current directory where to list netcdf4 files according to specified wildcards
           
    Returns
    -------
    none
    """
    global baseFilename,baseExtension
    for root, subdirs, files in os.walk(parentDirectory):
        if (len(files) != 0):
            matchingPattern = os.path.join(os.path.join(parentDirectory,baseFilename+"."+baseExtension) )
            listFiles = glob.iglob(matchingPattern) 
            for f in listFiles:
                ProcessFile(f)
            
        if ( len(subdirs) != 0):
            for d in subdirs:
                processDirectory( os.path.join(parentDirectory,d) )
            return
        # this directory has no sub directories
        
    
def ProcessSingleDirectory(directory):
    """ helper function called when processing a single directory

    Parameters
    ----------

    directory : str
         current directory where to start looking for netcdf4 files to compute histograms from and store them as json and png files
    
    Returns
    -------
    none
    """
    global warningStr,outputFile,currentPath,histosHashMap,outputDirectory

    if not os.path.isdir(directory):
        PrintErrorAndExits("directory '"+directory+"' not a valid directory.",outputFile)

    processDirectory(directory)
    SaveConcatenatedHistograms(directory)
    return
    
###################################################################################################################################
#
#
#
#  script starts from now
#
#
# global variables
#
# Time offset in WW3 NetCDF files
ww3_toffset = '1990-01-01 00:00:00'
ExpectednetCDF4TimeStamps =[] # time stamps expected from time meta data
start = datetime.today()

startTime = start.now()

timeStr = start.strftime("%m_%d_%Y_%H_%M_%S")
cumulate = False
numBins = None
outputFile = None
outputFileName = None
tmpFileName = None
plotInfos = None
xTicksMultiplicator = 1.


netCDF4SpatialCoordinates = ["longitude","latitude"]

currentDirectory = "/home/user/work"
singleDirectory = None
singleFile = None
currentPath=""
baseFilename = None
baseExtension = "nc"

histosHashMap = None
histosFilters = None


netCDF4VariablesFilters = None

errorStr = ""
warningStr =""

currentCheck =""
subDirectories = []
manualRange = None
outputDirectory = None
ProcessCmdLineOptions()


if cumulate:
    histosHashMap = dict()

if ( outputFileName == None):
    outputFileName = "output.log"

filename, file_extension = os.path.splitext(outputFileName)
outputPath = os.path.dirname( outputFileName)
outputFileName = tmpFileName = os.path.basename(outputFileName)+"_"+timeStr+file_extension

MkDirectory(outputPath)
outputFileName = tmpFileName = os.path.join(outputPath,outputFileName)


outputFile = open(outputFileName,'w')


netCDFDimensions = dict()
netCDFVariables = dict()

netCDFVariables = dict()
netCDFVariablesStats = dict()
axisByname = dict()

#
# check netCDF4 module : mandatory
#
nc = None
msg = "Checking for NetCDF4 python module..."
PrintCurrentTask(msg,outputFile)
netCDF4_found = importlib.util.find_spec("netCDF4") != None
if not netCDF4_found:
    PrintErrorAndExits("netCDF4 module not found",outputFile)
import netCDF4  as nc
PrintSuccess()

WriteFileHeader()
outputFile.flush()

if ( singleDirectory != None or singleFile != None):
    if ( singleDirectory):
        msg='Process single directory'
        PrintINFO(msg,outputFile)
        if ( cumulate ):
            processGlobalMinMax(singleDirectory)
        ProcessSingleDirectory(singleDirectory)
    if ( singleFile ):
        msg='Process single file'
        PrintINFO(msg,outputFile)
        print("[INFO] computing global min/max for all data in file '"+singleFile+"'")
        print("[INFO] computing global min/max for all data in file '"+singleFile+"'",file=outputFile)
        ProcessComputeGlobalMinMax(singleFile)
        ProcessFile(singleFile)
        outpath = os.path.dirname(singleFile)
        SaveConcatenatedHistograms(outpath)
else:
    msg="Process recursively"
    PrintINFO(msg,outputFile)
    Process()

PrintFileFooter(outputFile)






