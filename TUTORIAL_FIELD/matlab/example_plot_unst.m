% example_plot_unstr
%
% example of using read_WWNC_var and plotting some results
% 
it=0;
filename='RSCD_WW3-RSCD-UG_20200101_hs.nc';               % This example uses a file with
                                                % with only hs in it
varname='hs';
[tri,lat,lon,time,mat1,var1,unit1]=read_WWNC_UG_var(filename,varname);

if it==0
% Looks for a specific time: here 2020/1/1 at 6:00:00 UTC 
tt=find(time >= datenum(2020,1,1,6,0,0));
it=tt(1);
end

rundate =  time(it);
figure(2)   
clf
trisurf(tri',lon,lat,mat1(:,it))
view(0,90);shading interp;     
colormap(jet);colorbar;
caxis([0 10]);
latmin=min(lat);
latmax=max(lat);
coslat=cos(0.5*(latmax+latmin)*pi/180);
set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio

title(['H_{m0} (m): ' datestr(rundate,31)]);
xlabel('Longitude (deg)')
ylabel('Latitude (deg)')

%print(gcf,'-dpng',[filename '_it' num2str(it,'%3.3d') '.png'])

