% example_plot_rect
%
% example of using read_WWNC_var and plotting some results
% 

filename='LOPS_WW3-GLOB-30M_20200101_hs.nc';  % This example uses a file with only hs in it
                              
varname='hs';
[lat,lon,time,mat1,var1,unit1]=read_WWNC_var(filename,varname);

% Looks for a specific time: here 2020/1/1 at 6:00:00 UTC 
tt=find(time >= datenum(2020,1,1,6,0,0));
it=tt(1);


rundate =  time(it);
figure(1) 
clf
pcolor(lon,lat,double(squeeze(mat1(:,:,it)))')
shading flat;     
colormap(jet);colorbar;
caxis([0 10]);
latmin=min(lat);
latmax=max(lat);
coslat=cos(0.5*(latmax+latmin)*pi/180);
set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio

title(['H_{m0} (m): ' datestr(rundate,31)]);
xlabel('Longitude (deg)')
ylabel('Latitude (deg)')

%print(gcf,'-dpng',[filename '_it' num2str(it,'%3.3d') '.png'])

