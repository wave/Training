

%% 1. Setup

% mean : sum[trk]/count
% rms : sqrt{ sum[(trk)**2]/count }
% nrmse : sqrt{ sum[(mod-obs)**2]/ sum[obs**2] }
% rmse : sqrt{ sum[(mod-obs)**2]/count }
% si : rmse/mean(obs)*100
% bias : mean(mod)-mean(obs) 
clear all

% 1.1 Specify WW3 run name (use folder name)
run_id = '11bGLOB_tstep_B160_WCOR_08';
run_id2 ='11aGLOB_ECMWF_ref';


% 1.2 Base dir path to satelite track:
%base_dir = ['/home1/datawork/malday/RUNS/ERA5/' run_id '/lastwork/SAT/GLOB-30M'];
base_dir = '../work';

%1.2.1 compare runs?(comp_run = 1 to activate, comp_run = 0 to deactivate)
comp_run = 1;

% to compare
base_dir2 = '../SAT/ref';


%1.2.2 Regional Analysis? (region = 1 to activate, region = 0 to deactivate)
region = 1;
if region
    % Define region list, ex.: (North Atlantic, South Atlantic, North Pacific, South Pacific, Indian Ocean)
    % (for automatic plots, use a maximum of 8 sub regions)
    region_list = {'North_Atlantic','South_Atlantic','Indian_Ocean','Southern_Ocean',...
                   'NE_Pacific','SE_Pacific','NO_SOUTH'};
    
    %just for ploting legends
    region_names = {'North Atlantic','South Atlantic','Indian Ocean','Southern Ocean',...
                    'North East Pacific','South East Pacific','NO SOUTH'};

    % Define Longitude and latitude "blocks" for each region
    lon_block = [-80 -5;-68 20;50 100;-179.98 179.5000;-179 -100;-179 -73;-179.98 179.5000];
    lat_block = [10 60;-54 -2;-30 25;-70 -55;5 60;-54 -2;-55 66];
end
%--------------------------------------------------------------------------

% 1.3 Select alti data (1=cryosat, 2=envisat, 3=ers-2, 4=jason-1, 5=jason-2)
sat_id = 6;

alti_list = {'cryosat-2','envisat','ers-2','jason-1','jason-2',''};

% 1.4 Define variables to load from NetCDF file
var_list = {'time','lat','lon','swh_denoised','distance_to_coast','hs'};

% 1.5 Define Hs "bins"

% dHs increment
dHs = 0.5; %[m]

% number of bins (example n_bins=5 :[0-0.25[ [0.25-0.5[ [0.5-0.75[ [0.75-1.0[ [1.0-1.25[
n_bins = 27;

bin_list = 0:dHs:n_bins*dHs;



%% 2. Load Variables

% 2.1 NetCDF file with satellite track data:
sat_file = dir(fullfile([base_dir '/' char(alti_list(sat_id))], '*_track.nc'));

if comp_run
    sat_file2 = dir(fullfile([base_dir2 '/' char(alti_list(sat_id))], '*_track.nc'));
end


% 2.2 Loading variables into workspace (inside SAT structure)
disp(['Loading file from: ' base_dir])
disp(['Loading variables from: ' sat_file.name])
if comp_run
    disp(['Loading file from: ' base_dir2])
    disp(['Loading variables from: ' sat_file2.name])
end


tic
for i=1:numel(var_list)
    
    SAT.(var_list{i}) = ncread([base_dir '/' char(alti_list(sat_id)) '/' sat_file.name],var_list{i});
    
    %convert to double
    if isa(SAT.(var_list{i}),'single');
        SAT.(var_list{i}) = double(SAT.(var_list{i}));
    end
    
    if comp_run
        
        
        SAT2.(var_list{i}) = ncread([base_dir2 '/' char(alti_list(sat_id)) '/' sat_file2.name],var_list{i});

        %convert to double
        if isa(SAT2.(var_list{i}),'single');
            SAT2.(var_list{i}) = double(SAT2.(var_list{i}));
        end              
    end

end
toc
disp('...DONE!')


%% 3. Calculate parameters

% 3.1 Hs Bin construction

disp('Hs Bin construction and calculating stat parameters')
for i = 2:length(bin_list)
    if any(strcmp(var_list,'hs')) && any(strcmp(var_list,'swh_denoised'))
        
        b_name = {['bin_' num2str(i-1)]};

        % model hs values within bin wave height values
        hs_bin_values = find(SAT.hs>=bin_list(i-1) & SAT.hs<bin_list(i));

        % altimeter swh within bin wave height values
        swh_bin_values = find(SAT.swh_denoised>=bin_list(i-1) & SAT.swh_denoised<bin_list(i));

        for j = 1:numel(var_list)
            BINS.(b_name{1}).(var_list{j}) =  SAT.(var_list{j})(hs_bin_values);
        end
    
    
        %RMSE
        BINS.(b_name{1}).rmse = sqrt(mean((BINS.(b_name{1}).hs - BINS.(b_name{1}).swh_denoised).^2,'omitnan'));
        %NRMSE
        BINS.(b_name{1}).nrmse = BINS.(b_name{1}).rmse/mean(BINS.(b_name{1}).swh_denoised,'omitnan')*100;
        %BIAS
        BINS.(b_name{1}).bias = mean(BINS.(b_name{1}).hs,'omitnan') - mean(BINS.(b_name{1}).swh_denoised,'omitnan');
        %MEAN NORMALIZED BIAS
        BINS.(b_name{1}).norm_bias = mean((BINS.(b_name{1}).hs - BINS.(b_name{1}).swh_denoised)./BINS.(b_name{1}).swh_denoised,'omitnan')*100;
        %NORMALIZED MEAN BIAS
        BINS.(b_name{1}).nm_bias = sum((BINS.(b_name{1}).hs - BINS.(b_name{1}).swh_denoised),'omitnan')./sum(BINS.(b_name{1}).swh_denoised,'omitnan')*100;
        
        % WW3 hs distribution
        BINS.(b_name{1}).Hs_count = length(hs_bin_values);
        % altimeter swh distribution
        BINS.(b_name{1}).swh_count = length(swh_bin_values);
                
    else
        disp('Check variables names in NetCDF file and modify var_list')
        
    end
    
    if comp_run
        if any(strcmp(var_list,'hs')) && any(strcmp(var_list,'swh_denoised'))

            b_name = {['bin_' num2str(i-1)]};

            % model hs values within bin wave height values
            hs_bin_values = find(SAT2.hs>=bin_list(i-1) & SAT2.hs<bin_list(i));

            % altimeter swh within bin wave height values
            swh_bin_values = find(SAT2.swh_denoised>=bin_list(i-1) & SAT2.swh_denoised<bin_list(i));

            for j = 1:numel(var_list)
                BINS2.(b_name{1}).(var_list{j}) =  SAT2.(var_list{j})(hs_bin_values);
            end


            %RMSE
            BINS2.(b_name{1}).rmse = sqrt(mean((BINS2.(b_name{1}).hs - BINS2.(b_name{1}).swh_denoised).^2,'omitnan'));
            %NRMSE
            BINS2.(b_name{1}).nrmse = BINS2.(b_name{1}).rmse/mean(BINS2.(b_name{1}).swh_denoised,'omitnan')*100;
            %BIAS
            BINS2.(b_name{1}).bias = mean(BINS2.(b_name{1}).hs,'omitnan') - mean(BINS2.(b_name{1}).swh_denoised,'omitnan');
            %NORMALIZED BIAS
            BINS2.(b_name{1}).norm_bias = mean((BINS2.(b_name{1}).hs - BINS2.(b_name{1}).swh_denoised)./BINS2.(b_name{1}).swh_denoised,'omitnan')*100;
            %NORMALIZED MEAN BIAS
            BINS2.(b_name{1}).nm_bias = sum((BINS2.(b_name{1}).hs - BINS2.(b_name{1}).swh_denoised),'omitnan')./sum(BINS2.(b_name{1}).swh_denoised,'omitnan')*100;
            
            % WW3 hs distribution
            BINS2.(b_name{1}).Hs_count = length(hs_bin_values);
            % altimeter swh distribution
            BINS2.(b_name{1}).swh_count = length(swh_bin_values);

        else
            disp('Check variables names in NetCDF file and modify var_list')

        end                   
        
    end
    

end


%% 4. Regional Analysis 
if region

% 4.1 Hs Bin construction

    disp('Hs Bin construction and calculating stat parameters for Regional Analysis')
    
    for i_reg =1:numel(region_list)

        for i = 2:length(bin_list)
            if any(strcmp(var_list,'hs')) && any(strcmp(var_list,'swh_denoised'))
                b_name = {['bin_' num2str(i-1)]};

                %find Hs values in BIN within region
                reg_id = find(BINS.(b_name{1}).lon>=lon_block(i_reg,1) & BINS.(b_name{1}).lon<=lon_block(i_reg,2) ...
                                  & BINS.(b_name{1}).lat>=lat_block(i_reg,1) & BINS.(b_name{1}).lat<=lat_block(i_reg,2));
                
                % altimeter swh within BIN wave height and region values
                reg_swh_bin_values = find(SAT.swh_denoised>=bin_list(i-1) & SAT.swh_denoised<bin_list(i) ...
                                & SAT.lon>=lon_block(i_reg,1) & SAT.lon<=lon_block(i_reg,2) ...
                                  & SAT.lat>=lat_block(i_reg,1) & SAT.lat<=lat_block(i_reg,2));              

                for j = 1:numel(var_list)

                    BINS.(b_name{1}).(region_list{i_reg}).(var_list{j}) = BINS.(b_name{1}).(var_list{j})(reg_id);

                end

                %RMSE
                BINS.(b_name{1}).(region_list{i_reg}).rmse = sqrt(mean((BINS.(b_name{1}).(region_list{i_reg}).hs - BINS.(b_name{1}).(region_list{i_reg}).swh_denoised).^2,'omitnan'));
                %NRMSE
                BINS.(b_name{1}).(region_list{i_reg}).nrmse = BINS.(b_name{1}).(region_list{i_reg}).rmse/mean(BINS.(b_name{1}).(region_list{i_reg}).swh_denoised,'omitnan')*100;
                %BIAS
                BINS.(b_name{1}).(region_list{i_reg}).bias = mean(BINS.(b_name{1}).(region_list{i_reg}).hs,'omitnan') - mean(BINS.(b_name{1}).(region_list{i_reg}).swh_denoised,'omitnan');
                %MEAN NORMALIZED BIAS
                BINS.(b_name{1}).(region_list{i_reg}).norm_bias = mean((BINS.(b_name{1}).(region_list{i_reg}).hs - BINS.(b_name{1}).(region_list{i_reg}).swh_denoised)./BINS.(b_name{1}).(region_list{i_reg}).swh_denoised,'omitnan')*100;
                %NORMALIZED MEAN BIAS
                BINS.(b_name{1}).(region_list{i_reg}).nm_bias = sum((BINS.(b_name{1}).(region_list{i_reg}).hs - BINS.(b_name{1}).(region_list{i_reg}).swh_denoised),'omitnan')./sum(BINS.(b_name{1}).(region_list{i_reg}).swh_denoised,'omitnan')*100;

                % WW3 hs distribution
                BINS.(b_name{1}).(region_list{i_reg}).Hs_count = length(BINS.(b_name{1}).(region_list{i_reg}).hs);
                % altimeter swh distribution
                BINS.(b_name{1}).(region_list{i_reg}).swh_count = length(reg_swh_bin_values);
            else
                disp('sorry!!..no Hs values , check the variables on your source files')
            end
        end
    
    end
    
    disp('...DONE!')
 
    
end



%% 5. Plots

mean_bins = 0.5*dHs:dHs:(n_bins*dHs-0.5*dHs);
if 0
    figure(100) ,clf, hold on
    for i=1:n_bins
        b_name = {['bin_' num2str(i)]};
        plot(mean_bins(i),BINS.(b_name{1}).bias,'o r')

        if comp_run
            plot(mean_bins(i),BINS2.(b_name{1}).bias,'x k')

        end

    end
    set(gcf,'color','w')
    xlabel('modeled Hs [m]'), ylabel('Bias(Hs) [m]')
    if comp_run
        title(['Red Circles : ' run_id '-- Black crosses : ' run_id2],'Interpreter', 'none')
        xlabel(['modeled Hs [m] -- File: ' sat_file.name],'Interpreter', 'none')    
    else

        title([run_id ' - File: ' sat_file.name],'Interpreter', 'none')
    end
    %axis equal, 
    grid on, hold off
end

if 0
    figure(101), clf, hold on
    for i=1:n_bins
        b_name = {['bin_' num2str(i)]};
        plot(mean_bins(i),BINS.(b_name{1}).norm_bias,'o b')

        if comp_run
            plot(mean_bins(i),BINS2.(b_name{1}).norm_bias,'x k')

        end
    end
    set(gcf,'color','w')
    xlabel('modeled Hs [m]'), ylabel('Mean Normalized Bias (Hs) [%]')
    if comp_run
        title(['Blue Circles : ' run_id '-- Black crosses : ' run_id2],'Interpreter', 'none')
        xlabel(['modeled Hs [m] -- File: ' sat_file.name],'Interpreter', 'none')    
    else
        title([run_id ' - File: ' sat_file.name],'Interpreter', 'none')
    end
    grid on, hold off
end

figure(111), clf
subplot(2,1,1)
for i=1:n_bins
    b_name = {['bin_' num2str(i)]};
    plot(mean_bins(i),BINS.(b_name{1}).nm_bias,'o b','LineWidth',2), hold on
    
    if comp_run
        plot(mean_bins(i),BINS2.(b_name{1}).nm_bias,'x k','LineWidth',2)
        
    end
end
set(gcf,'color','w')
xlabel('modeled Hs [m]'), ylabel('Normalized Mean Bias (Hs) [%]')
if comp_run
    title({'Normalized Mean Bias (Hs) [%]',['Blue Circles : ' run_id ' -- Black crosses : ' run_id2]},'Interpreter', 'none')
    xlabel({'modeled Hs [m]', ['File: ' sat_file.name]},'Interpreter', 'none')    
else
    title({'Normalized Mean Bias (Hs) [%]',[run_id ' - File: ' sat_file.name]},'Interpreter', 'none')
end
grid on, hold off

subplot(2,1,2)
for i=1:n_bins
    b_name = {['bin_' num2str(i)]};
    plot(mean_bins(i),BINS.(b_name{1}).nrmse,'o b','LineWidth',2), hold on
    
    if comp_run
        plot(mean_bins(i),BINS2.(b_name{1}).nrmse,'x k','LineWidth',2)
    end
end
set(gcf,'color','w')
xlabel('modeled Hs [m]'), ylabel('NRMSE (Hs) [%]')
if comp_run
    title({'NRMSE (Hs) [%]',['blue Circles : ' run_id ' -- Black crosses : ' run_id2]},'Interpreter', 'none')
    xlabel({'modeled Hs [m]', ['File: ' sat_file.name]},'Interpreter', 'none') 
else
    title('NRMSE (Hs) [%]',{[run_id ' - File: ' sat_file.name]},'Interpreter', 'none')
    
end
grid on, hold off

if 0
    figure(102), clf, hold on
    for i=1:n_bins
        b_name = {['bin_' num2str(i)]};
        plot(mean_bins(i),BINS.(b_name{1}).rmse,'o r')

        if comp_run
            plot(mean_bins(i),BINS2.(b_name{1}).rmse,'x k')

        end
    end
    set(gcf,'color','w')
    xlabel('modeled Hs [m]'), ylabel('RMSE (Hs) [m]')
    if comp_run 
        title(['Red Circles : ' run_id '-- Black crosses : ' run_id2],'Interpreter', 'none')
        xlabel(['modeled Hs [m] -- File: ' sat_file.name],'Interpreter', 'none')    
    else
        title([run_id ' - File: ' sat_file.name],'Interpreter', 'none')
    end
    grid on, hold off
end


%--------------------------------------------------------------------------

for i=1:n_bins
    b_name = {['bin_' num2str(i)]};
%     plot(mean_bins(i),BINS.(b_name{1}).Hs_count,'o r','filled')
%     plot(mean_bins(i),BINS.(b_name{1}).swh_count,'o b','filled')   
    hs_count(i) = BINS.(b_name{1}).Hs_count;
    swh_count(i) = BINS.(b_name{1}).swh_count;
    
end
figure(104), clf, hold on
plot(mean_bins,hs_count,'- r')
plot(mean_bins,swh_count,'- b')
scatter(mean_bins,hs_count,'filled','r')
scatter(mean_bins,swh_count,'filled','b')

legend('WW3 Hs distribution','ALTIMETER swh distribution')
hold off
set(gcf,'color','w')

xlabel(['Hs [m]  (Pearson Corr.: ' num2str(corr2(hs_count',swh_count')) ')']), ylabel('absolute occurence freq.')
title([run_id ' - File: ' sat_file.name],'Interpreter', 'none')

grid on, hold off
%%
if region
    for i_reg=1:numel(region_list)

        for i=1:n_bins
            b_name = {['bin_' num2str(i)]};
            NM_BIAS.(region_list{i_reg})(i) = BINS.(b_name{1}).(region_list{i_reg}).nm_bias;
            NRMSE.(region_list{i_reg})(i) = BINS.(b_name{1}).(region_list{i_reg}).nrmse;
            HS_BIN_COUNT.(region_list{i_reg})(i) =  BINS.(b_name{1}).(region_list{i_reg}).Hs_count;
            SWH_BIN_COUNT.(region_list{i_reg})(i)=  BINS.(b_name{1}).(region_list{i_reg}).swh_count;
        end

        
    end
    
    figure(1000), clf, hold on
    plot(SAT.lon,SAT.lat,'. b')
    for i_reg =1:numel(region_list)
        plot([lon_block(i_reg,1) lon_block(i_reg,2) lon_block(i_reg,2) lon_block(i_reg,1) lon_block(i_reg,1)],...
            [lat_block(i_reg,1) lat_block(i_reg,1) lat_block(i_reg,2) lat_block(i_reg,2) lat_block(i_reg,1)],'- k','LineWidth',2);
    end
    
    hold off
    set(gcf,'color','w'), grid on
    xlabel('longitude [deg]'), ylabel('Latitude [deg]')
    title(['Grid wet point and Regions layout File: ' sat_file.name],'Interpreter', 'none')   
    axis equal
    
    figure(1001), clf
    subplot(2,1,1)
    plot(mean_bins,NM_BIAS.(region_list{1}),'- k'), hold on
    plot(mean_bins,NM_BIAS.(region_list{2}),'- b')
    plot(mean_bins,NM_BIAS.(region_list{3}),'- g')
    plot(mean_bins,NM_BIAS.(region_list{4}),'- r')
    plot(mean_bins,NM_BIAS.(region_list{5}),'- c')
    plot(mean_bins,NM_BIAS.(region_list{6}),'- m')
    plot(mean_bins,NM_BIAS.(region_list{7}),'-- k')
    
    scatter(mean_bins,NM_BIAS.(region_list{1}),'filled','k')
    scatter(mean_bins,NM_BIAS.(region_list{2}),'filled','b')
    scatter(mean_bins,NM_BIAS.(region_list{3}),'filled','g')
    scatter(mean_bins,NM_BIAS.(region_list{4}),'filled','r')
    scatter(mean_bins,NM_BIAS.(region_list{5}),'filled','c')
    scatter(mean_bins,NM_BIAS.(region_list{6}),'filled','m')
    scatter(mean_bins,NM_BIAS.(region_list{7}),'filled','k')
    plot(mean_bins,NM_BIAS.(region_list{7}),'o r','LineWidth',1.5);
    
    legend(region_names{1},region_names{2},region_names{3},region_names{4},region_names{5},region_names{6},region_names{7})
    hold off
    set(gcf,'color','w'), grid on
    xlabel('Modeled Hs [m]'), ylabel('Normalized Mean Bias [%]')
    title({'Normalized Mean Bias [%]',[run_id ' - File: ' sat_file.name]},'Interpreter', 'none')
    ylim([-30 6])
    
    figure(1001)
    subplot(2,1,2)
    plot(mean_bins,NRMSE.(region_list{1}),'- k'), hold on
    plot(mean_bins,NRMSE.(region_list{2}),'- b')
    plot(mean_bins,NRMSE.(region_list{3}),'- g')
    plot(mean_bins,NRMSE.(region_list{4}),'- r')
    plot(mean_bins,NRMSE.(region_list{5}),'- c')
    plot(mean_bins,NRMSE.(region_list{6}),'- m')
    plot(mean_bins,NRMSE.(region_list{7}),'-- k')
    
    
    scatter(mean_bins,NRMSE.(region_list{1}),'filled','k')
    scatter(mean_bins,NRMSE.(region_list{2}),'filled','b')
    scatter(mean_bins,NRMSE.(region_list{3}),'filled','g')
    scatter(mean_bins,NRMSE.(region_list{4}),'filled','r')
    scatter(mean_bins,NRMSE.(region_list{5}),'filled','c')
    scatter(mean_bins,NRMSE.(region_list{6}),'filled','m')
    scatter(mean_bins,NRMSE.(region_list{7}),'filled','k')
    plot(mean_bins,NRMSE.(region_list{7}),'o r','LineWidth',1.5);
    
    legend(region_names{1},region_names{2},region_names{3},region_names{4},region_names{5},region_names{6},region_names{7})
    hold off
    set(gcf,'color','w'), grid on
    xlabel('Modeled Hs [m]'), ylabel('NRMSE [%]')
    title({'NRMSE [%]',[run_id ' - File: ' sat_file.name]},'Interpreter', 'none') 
    ylim([0 50])
    
 %%  PDF plots %------------------------------------------------------------ 
 figure(1003), clf
    for i = 1:floor(numel(region_list)/2)
        
        subplot(2,2,i)
        plot(mean_bins,HS_BIN_COUNT.(region_list{i}),'- r'), hold on
        plot(mean_bins,SWH_BIN_COUNT.(region_list{i}),'- b')
        scatter(mean_bins,HS_BIN_COUNT.(region_list{i}),'filled','r')
        scatter(mean_bins,SWH_BIN_COUNT.(region_list{i}),'filled','b')

        legend('WW3 Hs distribution','ALTIMETER swh distribution')
        hold off
        set(gcf,'color','w')

        xlabel(['Hs [m]  (Pearson Corr.: ' num2str(corr2(HS_BIN_COUNT.(region_list{i})',SWH_BIN_COUNT.(region_list{i})')) ')' ]), ylabel('absolute occurence freq.')
        title({[region_list{i} ' ' run_id], ['     File: ' sat_file.name]},'Interpreter', 'none')

        grid on, hold off    
    end
%%    
 figure(1004), clf
    for i = floor(numel(region_list)/2)+1:numel(region_list)

        subplot(2,2,i-floor(numel(region_list)/2))
        plot(mean_bins,HS_BIN_COUNT.(region_list{i}),'- r'), hold on
        plot(mean_bins,SWH_BIN_COUNT.(region_list{i}),'- b')
        scatter(mean_bins,HS_BIN_COUNT.(region_list{i}),'filled','r')
        scatter(mean_bins,SWH_BIN_COUNT.(region_list{i}),'filled','b')

        legend('WW3 Hs distribution','ALTIMETER swh distribution')
        hold off
        set(gcf,'color','w')

        xlabel(['Hs [m]  (Pearson Corr.: ' num2str(corr2(HS_BIN_COUNT.(region_list{i})',SWH_BIN_COUNT.(region_list{i})')) ')' ]), ylabel('absolute occurence freq.')
        title({[region_list{i} ' ' run_id], ['     File: ' sat_file.name]},'Interpreter', 'none')

        grid on, hold off    
    end
    
%--------------------------------------------------------------------------    
%%  just to check zones (sanity check)
    if 0
        for i=1:numel(region_list)-1 %(the -1 is to avoid ploting the NO SOUTH section which covers all )
            region_check_lon = [];
            region_check_lat = [];
            for j=1:n_bins 
                b_name = {['bin_' num2str(j)]};
                region_check_lon = [region_check_lon;BINS.(b_name{1}).(region_list{i}).lon];
                region_check_lat = [region_check_lat;BINS.(b_name{1}).(region_list{i}).lat];

            end
            if i==1
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. k')
            end
            if i==2
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. c')
            end
            if i==3
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. g')
            end 
            if i==4
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. r')
            end
            if i==5
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. c')
            end 
            if i==6
            figure(1000), hold on
                plot(region_check_lon,region_check_lat,'. m')
            end             
        end
        hold off
    end
    
    
end
