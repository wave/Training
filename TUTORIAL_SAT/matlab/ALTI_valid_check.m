%  CHECKING WW3 GLOBAL ALTI VALIDATION          %
clear all
%% 1. Setup (up to 3 runs contrast)

% 1.1 Define number of runs to compare (min 2, max 3... use 2 because I'm still writing for the 3rd one)
ww3_runs = 2;

% 1.2 Select alti data (1= all, 2=cryosat, 3=envisat, 4=ers-2, 5=jason-1, 6=jason-2)
sat_id = 7;

alti_list = {'all','cryosat-2','envisat','ers-2','jason-1','jason-2',''};

% 1.3 Select variable to compare (specify index from var_list)

var_id = 19;

var_list = {'mean_swh_denoised','count_swh_denoised','max_swh_denoised','rms_swh_denoised',...
            'mean_wind_speed_alt','count_wind_speed_alt','max_wind_speed_alt','rms_wind_speed_alt',...
            'mean_hs','count_hs','max_hs','rms_hs'...
            'mean_wnd','count_wnd','max_wnd','rms_wnd'...
            'rmse_swh_denoised','nrmse_swh_denoised','si_swh_denoised','bias_swh_denoised',...
            'rmse_wind_speed_alt','nrmse_wind_speed_alt','si_wind_speed_alt','bias_wind_speed_alt',...
            'latitude','longitude'};
        
% 1.4 complete path to ALTI validation output files (equal to max number of runs to compare)

% run name: 
%(!!! change the name of the runs according to wdir1 and wdir2)
%run1 = '11aGLOB_ECMWF_ref';
run2 = '11bGLOB_tstep_B160_WCOR_08';

% wdir1 : use this one to set the reference run , for example a test that
%         we know is good, or a first setup of the model.
% wdir2 : this one is to contrast with wdir1, and verify the decrease of
%         some error parameters (rmse , nrmse, scatter index)

wdir1 = '../SAT/ref';
wdir2 = '../work';


% run name: 
%(!!! change the name of the runs according to wdir1 and wdir2)
run1 = '11aGLOB_ECMWF';
run2 = '11bGLOB_B160_WCOR_08';

%--------------------------------------------------------------------------
% 1.5 Compare ww3 runs with different ALTI sources (2 alti sources!)
%     (select alti sources from alti_list) ... still developing
alti1_id = 7;
alti2_id = 7;

%     (select ww3 run path from: wdir1, wdir2 or wdir3)
walti = wdir1;

%% 2. Calculate differences between runs for selected variable

% 2.1 netCDF files 

ncdf1 = dir(fullfile([wdir1 '/' char(alti_list(sat_id))], '*stat.nc'));
ncdf2 = dir(fullfile([wdir2 '/' char(alti_list(sat_id))], '*stat.nc'));

if ww3_runs == 3
    ncdf3 = dir(fullfile([wdir3 '/' char(alti_list(sat_id))], '*stat.nc'));
end

% 2.2 Load selected variable into workspace

var1 = ncread([wdir1 '/' char(alti_list(sat_id)) '/' ncdf1.name],char(var_list(var_id)));
var2 = ncread([wdir2 '/' char(alti_list(sat_id)) '/' ncdf2.name],char(var_list(var_id)));

if ww3_runs == 3
    ncdf3 = dir(fullfile([wdir3 '/' char(alti_list(sat_id))], '*stat.nc'));
    var3 = ncread([wdir3 '/' char(alti_list(sat_id)) '/' ncdf3.name],char(var_list(var_id)));
end


%load lon lat 
inv =1; %(use if lon lat vector are inverted from source)
if 0
    lon = ncread([wdir1 '/' char(alti_list(sat_id)) '/' ncdf1.name],char(var_list(25)));
    lat = ncread([wdir1 '/' char(alti_list(sat_id)) '/' ncdf1.name],char(var_list(26)));
else
    lon = ncread([wdir1 '/' char(alti_list(sat_id)) '/' ncdf1.name],char(var_list(26)));
    lat = ncread([wdir1 '/' char(alti_list(sat_id)) '/' ncdf1.name],char(var_list(25)));
end

% 2.3 differences between ww3 runs

% direct difference run2 - run1 
diff_2_1 = var2 - var1;

% percentage difference (run2 - run1)/run1
rel_diff_2_1 = 100.*diff_2_1./abs(var1);


if ww3_runs == 3
    diff_3_1 = var3 - var1;
    rel_diff_3_1 = diff_3_1./abs(var1);
   
    diff_3_2 = var3 - var2;
    rel_diff_3_2 = diff_3_2./abs(var2);
end

% 2.3.1 identify grid points with only reduction of stat parameters (bias, NRMSE, RMSE)
if 1
    reduc_only = diff_2_1; reduc_only(reduc_only>=0)=NaN;
    non_NaN_diff_2_1 = sum(~isnan(diff_2_1(:)));
    non_NaN_reduc_only = sum(~isnan(reduc_only(:)));

    reduction_rate = 100*non_NaN_reduc_only/non_NaN_diff_2_1;
 
end 


%2.4 PLOTS
if 1

minval = min(var1(:)); maxval= 3*mean(var1(:),'omitnan');
    
figure(21),clf, ff = imagesc(lon,lat,var1'); set(ff,'AlphaData',~isnan(var1'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colormap(jet)
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title([run1 ' Run 1 -- File :' ncdf1.name ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)')           
            
figure(22),clf, ff = imagesc(lon,lat,var2'); set(ff,'AlphaData',~isnan(var2'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colormap(jet)
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title([run2 ' Run 2 -- File :' ncdf2.name ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)')
            

minval = 0.1*min(diff_2_1(:)); maxval= 0.1*max(diff_2_1(:));           
figure(23),clf, ff = imagesc(lon,lat,diff_2_1'); set(ff,'AlphaData',~isnan(diff_2_1'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colormap(jet)
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title(['Difference : ' run2 '-' run1 ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)')  
            %
if exist('reduc_only','var')
    if any(~isnan(reduc_only(:)))
        minval = 3*median(reduc_only(:),'omitnan'); maxval=0; 
        figure(231),clf, ff = imagesc(lon,lat,reduc_only'); set(ff,'AlphaData',~isnan(reduc_only'))
        set(gca,'Ydir','Normal'), shading flat
        latmin=min(lat);
        latmax=max(lat);
        coslat=cos(0.5*(latmax+latmin)*pi/180);
        set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
        set(gcf,'color','w')
        colormap(jet)
        colorbar, set(gcf,'color','w')
        caxis([minval maxval]);
        title(['Reduction rate = ' num2str(reduction_rate) '[%] in:'  run2 '-' run1 ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
        xlabel('Longitude (deg)')
        ylabel('Latitude (deg)')
    end
end
            
minval = -abs(3.*mean(rel_diff_2_1(:),'omitnan')); maxval= abs(3.*mean(rel_diff_2_1(:),'omitnan'));            

if 0
figure(24),clf, ff = imagesc(lon,lat,rel_diff_2_1');  set(ff,'AlphaData',~isnan(rel_diff_2_1'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title(['Precentage [%] : ' run2 '-' run1 ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)')
end

    if ww3_runs == 3
        
    minval = min(var1(:)); maxval= max(var1(:));    

    figure(31),clf, ff = imagesc(lon,lat,var3'); set(ff,'AlphaData',~isnan(var3'))
                set(gca,'Ydir','Normal'), shading flat
                latmin=min(lat);
                latmax=max(lat);
                coslat=cos(0.5*(latmax+latmin)*pi/180);
                set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
                set(gcf,'color','w')
                colorbar, set(gcf,'color','w')
                caxis([minval maxval]);
                title(['Run 3 -- File :' ncdf3.name ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
                xlabel('Longitude (deg)')
                ylabel('Latitude (deg)')


    figure(32),clf, ff = imagesc(lon,lat,diff_3_1'); set(ff,'AlphaData',~isnan(diff_3_1'))
                set(gca,'Ydir','Normal'), shading flat
                latmin=min(lat);
                latmax=max(lat);
                coslat=cos(0.5*(latmax+latmin)*pi/180);
                set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
                set(gcf,'color','w')
                colorbar, set(gcf,'color','w')
                %caxis([minval maxval]);
                title(['Difference : (run3 - run1)  -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
                xlabel('Longitude (deg)')
                ylabel('Latitude (deg)') 
                
    minval = -abs(3.*mean(rel_diff_3_1(:),'omitnan')); maxval= abs(3.*mean(rel_diff_3_1(:),'omitnan'));            

    figure(33),clf, ff = imagesc(lon,lat,rel_diff_3_1');  set(ff,'AlphaData',~isnan(rel_diff_3_1'))
                set(gca,'Ydir','Normal'), shading flat
                latmin=min(lat);
                latmax=max(lat);
                coslat=cos(0.5*(latmax+latmin)*pi/180);
                set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
                set(gcf,'color','w')
                colorbar, set(gcf,'color','w')
                caxis([minval maxval]);
                title(['Precentage [%] : (run3 - run1)  -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');
                xlabel('Longitude (deg)')
                ylabel('Latitude (deg)')

    end

end

%% 3. See different altimeters source

% 3.1 netCDF files 

alticdf1 = dir(fullfile([walti '/' char(alti_list(alti1_id))], '*stat.nc'));
alticdf2 = dir(fullfile([walti '/' char(alti_list(alti2_id))], '*stat.nc'));

% 3.2 Load selected variable into workspace

alti_var1 = ncread([walti '/' char(alti_list(alti1_id)) '/' alticdf1.name],char(var_list(var_id)));
alti_var2 = ncread([walti '/' char(alti_list(alti2_id)) '/' alticdf2.name],char(var_list(var_id)));

% 3.3 Differences between alti data
diff_alti = alti_var2 - alti_var1;

rel_diff_alti = diff_alti./alti_var1;


% 3.3 PLOTS
if 0

minval = min(alti_var1(:)); maxval= max(alti_var1(:));    
    
figure(41),clf, ff = imagesc(lon,lat,alti_var1'); set(ff,'AlphaData',~isnan(alti_var1'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title(['File :' alticdf1.name  ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');    
    
figure(42),clf, ff = imagesc(lon,lat,alti_var2'); set(ff,'AlphaData',~isnan(alti_var2'))
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colorbar, set(gcf,'color','w')
            caxis([minval maxval]);
            title(['File :' alticdf2.name ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');     
    
figure(43), clf, ff = imagesc(lon,lat,diff_alti'); set(ff,'AlphaData',~isnan(diff_alti')) 
            set(gca,'Ydir','Normal'), shading flat
            latmin=min(lat);
            latmax=max(lat);
            coslat=cos(0.5*(latmax+latmin)*pi/180);
            set(gca,'DataAspectRatio',[1 coslat 1]);  %#   data aspect ratio
            set(gcf,'color','w')
            colorbar, set(gcf,'color','w')  
        title(['File :' alticdf2.name ' -- alti source: ' char(alti_list(sat_id)) ' -- variable: ' char(var_list(var_id))],'Interpreter', 'none');            
            title(['Difference : (Alti2 - Alti1)  -- alti sources: ' alticdf1.name ' -- ' alticdf2.name],'Interpreter', 'none');
            xlabel('Longitude (deg)')
            ylabel('Latitude (deg)') 
                    
end
