g=9.81;
d2r=pi/180;

tag1='ST4-default';path1='param_ST4_T471';
tag2='Romero (2019)';path2='param_Romero';
tag3='T701';path3='param_ST4_T701';
tag4='T702';path4='param_ST4_T702';
tag5='T707GQM';path5='param_ST4_T707GQM'; 
tag6='T713GQM';path6='param_ST4_T713GQM'; 




it0=181;  % choses time step for spectral plots
nfi=6;nfip=6;  % number of different runs, number of runs for plots

name1='/ww3.2000_src.nc';

syms1=['ko','r+','r>','c+','mx','b^']; %,'m+']; % ];%
syms=reshape(syms1,2,nfip);


% Initialization of figures 
for ifig=1:16
  figure(ifig)
  clf
  hold on
  grid on
  set(gca,'FontSize',16)
end

figure(5)
clf
hold on
set(gca,'FontSize',16,'LineWidth',2);
set(gca,'Yscale','log','Xscale','log');
grid on

figure(16)
clf
hold on
set(gca,'FontSize',16,'LineWidth',2);
grid on


% Loop on different model runs 
for ifi=1:nfi
   eval(['path=[ path' num2str(ifi) '];'])
   file=[ path name1];
   ndates=1000;
   [lat,lon,freq,dir,df,time,Efth,depth,U10,UC,Cdir,unit1]=readWWNC_SPECv2(file,'efth');
   [lat,lon,freq,dir,df,time,Sin,depth,U10,UC,Cdir,unit1]=readWWNC_SPECv2(file,'sin');
   [lat,lon,freq,dir,df,time,sds,depth,U10,UC,Cdir,unit1]=readWWNC_SPECv2(file,'sds');
   [lat,lon,freq,dir,df,time,snl,depth,U10,UC,Cdir,unit1]=readWWNC_SPECv2(file,'snl');
   dates=time;
   k2=((2.*pi.*freq).^2./g).^2;
   taille=size(Efth);
   nk=taille(2);
   nth=taille(1);
   nt=taille(4);

   filename=[path '/ww3.200001.nc'];
   [lat,lon,time,ust,varu,unitu]=read_WWNC_var(filename,'uust');
   ust2=squeeze(ust);
   [lat,lon,~,hs,varu,unitu]=read_WWNC_var(filename,'hs');
   hs2=squeeze(hs);
   [lat,lon,time,taw,varu,unitu]=read_WWNC_var(filename,'utaw');
   tw2=squeeze(taw);
   varmss1='mssu';
   varmss2='mssc';
   varmss3='mssd';
   [lat,lon,time,mssx,varu,unitu]=read_WWNC_var(filename,varmss1);
   [lat,lon,time,mssy,varu,unitu]=read_WWNC_var(filename,varmss2);
   [lat,lon,time,mssd,varu,unitu]=read_WWNC_var(filename,varmss3);
   mss2=squeeze(mssx+mssy);
   [lat,lon,time,wndu,varu,unitu]=read_WWNC_var(filename,'uwnd');
   [lat,lon,time,wndv,varu,unitu]=read_WWNC_var(filename,'vwnd');
   [lat,lon,time,uust,varu,unitu]=read_WWNC_var(filename,'uust');
   [lat,lon,time,vust,varu,unitu]=read_WWNC_var(filename,'vust');
   udir=atan2d(wndv,wndu);
   U10=squeeze(sqrt(wndv.^2+wndu.^2));
   ustar=squeeze(sqrt(uust.^2+vust.^2));
   dird=abs(cosd(udir-mssd));
   mssr=mssx./mssy; 
 % looks for indices at which the mss1 becomes crosswind
   I=find(dird < 0.3);
   mssr(I)=mssy(I)./mssx(I); 
   temp1=mssx;
   temp2=mssy;
   mssx=squeeze(temp2);
   mssy=squeeze(temp1);
   mssx(I)=temp1(I);
   mssy(I)=temp2(I);

   wnd=sqrt(wndu.^2+wndv.^2);
   wnd2=squeeze(wnd);
   ndateso=nt;
   E=zeros(nk,nth);
%
% Extracts one spectrum
%
   Hs=zeros(ndateso,1);
   Tp=zeros(ndateso,1);
   QKK=zeros(ndateso,1);
   mss_wave=zeros(ndateso,1);
   mssr_wave=zeros(ndateso,1);
   mssx_tail=zeros(ndateso,1);
   mssy_tail=zeros(ndateso,1);
   mssr_tail=zeros(ndateso,1);
   mss_tail=zeros(ndateso,1);
   for it=1:ndateso;
      E(:,:)=Efth(:,:,1,it)';
      dth=2*pi./nth;
      if (it == it0) %49) 
         Efth2=E;
         [Hm0,Tp1,imax,km,Ef,sth1,sth2,overlap,df,Hsws,QK]=parameters_from_spec(E,freq,dir);
      end
% Computes some parameters from the spectrum
      [Hm0,Tp1,imax,km,Ef,sth1,sth2,overlap,df,Hsws,QK]=parameters_from_spec(E,freq,dir);
      Hs(it)=Hm0;
      Tp(it)=Tp1; 
      QKK(it)=QK;
   end;

   mssx_tail=mssx*0.;
   mssy_tail=mssy*0.;
   mssr_tail=(mssy+mssy_tail)./(mssx+mssx_tail);
   mss_tail=mssy+mssy_tail+mssx+mssx_tail;
   times=(dates-dates(1))*24;
   timenc=(time-time(1))*24;

   figure(1); plot(timenc,sqrt(hs2.^2-hs2(1)^2),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(2); plot(timenc,(mssx+mssy),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(3); plot(timenc,ust2.^2./(wnd2.^2),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(4); plot(timenc,squeeze(mssr_tail),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(5); plot((freq),(Ef),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(6); plot((freq),(Ef).*(freq.^5),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(7); plot((freq),(Ef).*(freq.^4),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(8); plot(times,wnd2(1)./(9.81.*Tp./(2*pi)),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(16); plot(timenc,QKK,syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   figure(15); plot(freq,overlap,syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);

% Picks some frequencies and gets indices
   f9=0.2;
   f10=0.5;
   f11=0.8;
   f12=0.95;

   I9=find(freq > f9);
   I10=find(freq > f10);
   I11=find(freq > f11);
   I12=find(freq > f12);
   ntick=13;
   figure(9);  plot(dir,Efth2(I9(1),:), syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8); axis([0 360 0 1]);      set(gca,'XTick',linspace(0,360,ntick));
   figure(10); plot(dir,Efth2(I10(1),:),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8); axis([0 360 0 0.009]);  set(gca,'XTick',linspace(0,360,ntick));
   figure(11); plot(dir,Efth2(I11(1),:),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);  axis([0 360 0 1.3E-3]); set(gca,'XTick',linspace(0,360,ntick));
   figure(12); plot(dir,Efth2(I12(1),:),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);   axis([0 360 0 8E-4]);   set(gca,'XTick',linspace(0,360,ntick));
   
   figure(13);   plot(circshift(dir,-10),circshift(-sds(:,I10(1),1,it)./Efth(:,I10(1),1,it),-10),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   axis([0 360 0. 0.005]); set(gca,'XTick',linspace(0,360,ntick))

   figure(14); plot(circshift(dir,-10),circshift(-sds(:,I12(1),1,it)./Efth(:,I12(1),1,it),-10),syms(ifi*2-1:ifi*2),'LineWidth',2,'MarkerSize',8);
   axis([0 360 0.0 0.02]); set(gca,'XTick',linspace(0,360,ntick))
end

figure(1); xlabel('Duration (hours)'); ylabel('Hs (m)'); legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)
axis([0 48 0 2.8])
set(gca,'XTick',linspace(0,48,9));


figure(2); xlabel('Duration (hours)'); ylabel('mss_{f< 1 Hz}')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)
axis([0 48 0 0.05])
set(gca,'XTick',linspace(0,48,9));

figure(3)
xlabel('Duration (hours)')
ylabel('C_D')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)

figure(4)
xlabel('Duration (hours)')
ylabel('mssc/mssd')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)
axis([0 48 0.4 1.1])
set(gca,'XTick',linspace(0,48,9));

figure(5)
xlabel('frequency (Hz)')
ylabel('E(f) (m^2/Hz)')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)
loglog((freq),(freq.^-4)./1000,'k-','LineWidth',1)
loglog((freq),(freq.^-5)./1000,'k-','LineWidth',1)
axis([0.04 2 0.001 10])

figure(6)
xlabel('frequency (Hz)')
ylabel('E(f) x f^5 (m^2 Hz^4)')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)

figure(7)
xlabel('frequency (Hz)')
ylabel('E(f) x f^4 (m^2 Hz^3)')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)

figure(15)
xlabel('frequency (Hz)')
ylabel('I(f)')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)

figure(16)
xlabel('Duration (hours)')
ylabel('Qkk bandwidth')
legend(tag1,tag2,tag3,tag4,tag5,tag6); %,tag7)


