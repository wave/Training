function  [Hm0,Tp,imax,km,Ef,sth1,sth2,overlap,df,Hsws,QK]=HsTpkm_from_spectrum_windsea(E,freq,dir);
g=9.81;
d2r=pi/180;
nk=size(freq,1);
nth=size(dir,1);
dth=2*pi/double(nth);
df=zeros(nk,1);
df(2:nk-1)=0.5*(freq(3:nk)-freq(1:nk-2));
df(1)=freq(2)-freq(1);
df(nk)=freq(nk)-freq(nk-1);
dir2=repmat(dir,1,nk)';
%%% Assuming deep water
Cg=g./(4*pi*freq(:));
k=(2*pi*freq(:)).^2/g;
fac2=Cg./(4*pi^2.*freq).^2;


Ef=sum(E,2)*dth;
A2f=sum(E.^2,2)./fac2;

a1=sum(E.*cosd(dir2),2)*dth;
b1=sum(E.*sind(dir2),2)*dth;
a2=sum(E.*cosd(2.*dir2),2)*dth;
b2=sum(E.*sind(2.*dir2),2)*dth;
m1=sqrt(a1.^2+b1.^2)./Ef;
m2=sqrt(a2.^2+b2.^2)./Ef;
sth1=sqrt(abs(2.0.*(1-m1)) )/d2r;
sth2=sqrt(abs(0.5.*(1-m2)) )/d2r;

overlap=dth.*sum(E(:,1:nth/2).*E(:,1+nth/2:nth),2)./(Ef.^2);

% NB:     BK1 (JSEA) = BK1(JSEA) + A(ITH,IK,JSEA)**2
%          BK2 (JSEA) = BK2 (JSEA) + BK1(JSEA)  * FACTOR* SIG(IK) /WN(IK,ISEA) 
%WRITE(994,*) IK, BK1(JSEA), FACTOR* SIG(IK) /WN(IK,ISEA) , BK1(JSEA), FACTOR* SIG(IK) /WN(IK,ISEA) , BK2 (JSEA)

%E2f=sum(E.^2,2);
E2f=sum((E(:,1:nth/2)+E(:,1+nth/2:nth)).^2,2);

BB1=E2f(:).*df(:).*Cg(:)./k(:)*dth;
QK2=0.5*sum(BB1)./(2*pi);


[Emax,imax]=max(Ef);
i1=max(imax-1,1);
i2=min(imax+1,nk);
fp=(sum(Ef(i1:i2).*freq(i1:i2))+0.00001) ...
    ./(sum(Ef(i1:i2))+0.00001);
Etot=sum(Ef(:).*df(:));

QK=sqrt(QK2)/Etot;

km=(Ef'*df(:)*freq(:))./Etot.*(2*pi/sqrt(g));
Hm0=4.*sqrt(Etot);
Tp=1./fp;
I=find(freq > 0.068);
Etot=(Ef(I)'*df(I));
Hsws=4.*sqrt(Etot);
