!==============================================================================

MODULE W3GDATMD

PUBLIC

   INTEGER,DIMENSION(:,:),ALLOCATABLE            :: TRIGP
   DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE   :: XYB
   INTEGER                                       :: NTRI, NX

END MODULE W3GDATMD

!==============================================================================

MODULE c_caldat

CONTAINS

!==============================================================================

INTEGER FUNCTION julday(id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!

   IMPLICIT NONE
   INTEGER(KIND=4),    INTENT(in)  :: id,mm,iyyy
   INTEGER(KIND=4), PARAMETER :: IGREG=15+31*(10+12*1582)
   INTEGER(KIND=4) ja,jm,jy
   jy=iyyy
   IF (jy.EQ.0) WRITE(6,*) 'There is no zero year !!'
   IF (jy.LT.0) jy=jy+1
   IF (mm.GT.2) THEN
     jm=mm+1
   ELSE
     jy=jy-1
     jm=mm+13
     ENDIF
   julday=INT(365.25*jy)+int(30.6001*jm)+id+1720995
   IF (id+31*(mm+12*iyyy).GE.IGREG) THEN
     ja=INT(0.01*jy)
     julday=julday+2-ja+INT(0.25*ja)
     ENDIF
   RETURN
END FUNCTION JULDAY

!==============================================================================

SUBROUTINE caldat(julian,id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!

IMPLICIT NONE

   INTEGER(KIND=4), INTENT(in)  :: julian
   INTEGER(KIND=4), INTENT(out) :: id,mm,iyyy
   INTEGER(KIND=4), PARAMETER   :: IGREG=2299161
   INTEGER(KIND=4)              :: ja,jalpha,jb,jc,jd,je

   if (julian.GE.IGREG) THEN
      jalpha=INT(((julian-1867216)-0.25)/36524.25)
      ja=julian+1+jalpha-INT(0.25*jalpha)
   ELSE
      ja=julian
   ENDIF
   jb=ja+1524
   jc=INT(6680.+((jb-2439870)-122.1)/365.25)
   jd=365*jc+INT(0.25*jc)
   je=INT((jb-jd)/30.6001)
   id=jb-jd-INT(30.6001*je)
   mm=je-1
   IF (mm.GT.12) mm=mm-12
   iyyy=jc-4715
   IF (mm.GT.2) iyyy=iyyy-1
   IF (iyyy.LE.0) iyyy=iyyy-1
   RETURN
END SUBROUTINE CALDAT

END MODULE c_caldat

!==============================================================================
!==============================================================================

MODULE W3SERVMD
  PUBLIC
!
  INTEGER, PRIVATE        :: NDSTRC = 6, NTRACE = 0, PRFTB
  LOGICAL, PRIVATE        :: FLPROF = .FALSE.
!
CONTAINS

!==============================================================================

SUBROUTINE STRSPLIT(STRING,TAB,cnt)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |          M. Accensi               |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         29-Apr-2013 !
!/                  +-----------------------------------+
!/
!/    29-Mar-2013 : Origination.                        ( version 4.10 )
!/
!  1. Purpose :
!
!     Splits string into words
!
!  2. Method :
!
!     finds spaces and loops
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       STRING   Str   O   String to be splitted
!       TAB      Str   O   Array of strings
!     ----------------------------------------------------------------
!

      IMPLICIT NONE



      CHARACTER, intent(IN)                :: STRING*1024
      CHARACTER, intent(INOUT)             :: TAB(*)*1024
      INTEGER, intent(OUT), optional       :: cnt
      INTEGER                              :: I
      CHARACTER                            :: tmp_str*1024, ori_str*1024

! initializes arrays
      ori_str=ADJUSTL(TRIM(STRING))
      tmp_str=ori_str
      cnt=0

! counts the number of substrings
      DO WHILE ((INDEX(tmp_str,' ').NE.0) .AND. (len_trim(tmp_str).NE.0))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        cnt=cnt+1
        ENDDO
!
! reinitializes arrays
!
      tmp_str=ori_str

! loops on each substring
      DO I=1,cnt
        TAB(I)=tmp_str(:INDEX(tmp_str,' '))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        END DO

      RETURN

END SUBROUTINE STRSPLIT

!==============================================================================

SUBROUTINE NEXTLN ( CHCKC , NDSI , NDSE )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         18-Nov-1999 |
!/                  +-----------------------------------+
!/
!/    15-Jan-1999 : Final FORTRAN 77                    ( version 1.18 )
!/    18-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Sets file pointer to next active line of input file, by skipping
!     lines starting with the character CHCKC.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       CHCKC   C*1   I  Check character for defining comment line.
!       NDSI    Int.  I  Input dataset number.
!       NDSE    Int.  I  Error output dataset number.
!                        (No output if NDSE < 0).
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!       STRACE ( !/S switch )
!
!  5. Called by :
!
!       Any routine.
!
!  6. Error messages :
!
!     - On EOF or error in input file.
!
!  9. Switches :
!
!     !/S  Enable subroutine tracing.
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: NDSI, NDSE
      CHARACTER, INTENT(IN)   :: CHCKC*1
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
!/S      INTEGER, SAVE           :: IENT = 0
      INTEGER                 :: IERR
      CHARACTER               :: TEST*1
!/
!/ ------------------------------------------------------------------- /
!/
!/S      CALL STRACE (IENT, 'NEXTLN')
!
  100 CONTINUE
      READ (NDSI,900,END=800,ERR=801,IOSTAT=IERR) TEST
      IF (TEST.EQ.CHCKC) THEN
          GOTO 100
        ELSE
          BACKSPACE (NDSI,ERR=802,IOSTAT=IERR)
        END IF
      RETURN
!
  800 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,910)
      CALL EXTCDE ( 1 )
!
  801 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 2 )
!
  802 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 3 )
!
! Formats
!
  900 FORMAT (A)
  910 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     PREMATURE END OF INPUT FILE'/)
  911 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR IN READING FROM FILE'/               &
               '     IOSTAT =',I5/)
  912 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR ON BACKSPACE'/                       &
               '     IOSTAT =',I5/)

END SUBROUTINE NEXTLN

!==============================================================================

SUBROUTINE EXTCDE ( IEXIT )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         06-Jan-1999 |
!/                  +-----------------------------------+
!/
!/    06-Jan-1998 : Final FORTRAN 77                    ( version 1.18 )
!/    23-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Perform a program stop with an exit code.
!
!  2. Method :
!
!     Machine dependent.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IEXIT   Int.   I   Exit code to be used.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!  5. Called by :
!
!     Any.
!
!  9. Switches :
!
!     !/MPI  MPI finalize interface if active
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
IMPLICIT NONE
!
!/MPI      INCLUDE "mpif.h"
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: IEXIT
!/
!/ ------------------------------------------------------------------- /
!/
!/MPI      INTEGER                 :: IERR_MPI
!/MPI      LOGICAL                 :: RUN
!/
!/ Test if MPI needs to be closed
!/
!/MPI      CALL MPI_INITIALIZED ( RUN, IERR_MPI )
!/MPI      IF ( RUN ) THEN
!/MPI          CALL MPI_BARRIER ( MPI_COMM_WORLD, IERR_MPI )
!/MPI          CALL MPI_FINALIZE (IERR_MPI )
!/MPI        END IF
!
!/F90      CALL EXIT ( IEXIT )
!/DUM      STOP
!/
!/ End of EXTCDE ----------------------------------------------------- /
!/
END SUBROUTINE EXTCDE

END MODULE W3SERVMD


!==============================================================================
!==============================================================================

      PROGRAM buoy_change_freq_nc

!/                  +-----------------------------------+
!/                  | WAVEWATCH III           IFREMER   |
!/                  |           F. Ardhuin              |
!/                  |           M. Accensi              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         10-Jun-2021 |
!/                  +-----------------------------------+
! 
!  Creation  : 11-Apr-2016
!  Added ndof: 06-Jan-2019
!  Freq per time : 17-Dec-2020
!
!  1. Purpose :
!
!     Changes the frequency grid of a time series file
!
!
!  2. Method :
!
! 1.   Initialization:
! 1.a  IO set-up, set constants
!
! 1.b  Read input parameters from file change_freq_nc.inp
!
! 1.c  Get first info from BUOY file (input_file):
!       number & size of dimensions
!       reference time
!       number & names of the variables to copy (nvarmod)
!
!
! 2.   Create NetCDF output file & define its dimensions
!
!
! 3.   Get variable values from BUOY file:
!
!      Loop on nvarmod
!
! 3.a    IF variable independent of frequency AND time:
!          If variable is depth, get attribute "positive" in addition to other atts
!          copy variable & attributes in output file
!
! 3.b    ELSE IF variable is time:
!          get variable & attributes
!          write converted time variable & attributes (change units) in output file
!                                  
!
! 3.c    ELSE IF variable is central_frequency : store it in freqi(:)
!        ELSE IF variable is band_width_frequency : store it in dfi(:)
!
! 3.e    ELSE IF variable depends on time ONLY:
!          copy variable in outvar_t & write it in output file with its attributes
!
! 3.f    ELSE IF variable depends on frequency & time AND matches freqvarstr
!          store its attributes in funits(ivar), ffillval(ivar), etc.
!          store variable in predefined array (Efi, th1i, sth1i, th2i, sth2i)
!          (freq-time variables not listed in freqvarstr are not processed)
!
!      END loop on nvarmod
!
! 3.g  Close buoy file
!
!
! 4.   Take care of frequency-dependent variables
!
! 4.a  Define freq1i & freq2i like in WW3, from freqi
!
! 4.b  Loop on output frequency band to get if1(J), if2(J), wf1(J) & wf2(J):
!      DO J=1,NFBAND
!        get imin
!        get imax
!        deduce if1(J), if2(J), wf1(J), wf2(J)
!      END DO
!
! 4.c  Loop on times and on stations to compute Ef, th1, sth1, etc.:
!      DO ITIME=1,ntime
!        DO IP=1,np
!          IF (npar.EQ.5)
!            compute a1i, b1i, m1i, a2i, b2i, m2i
!            DO J=1,nfband
!              compute Ef, a1, b1, a2, b2, th1, th2, m1, m2, sth1, sth2
!            END DO
!          ELSE (npar.NE.5)
!            IF (npar.EQ.3) compute a1i,b1i, m1i
!            DO J=1,nfband
!              compute Ef
!              IF (npar.EQ.3) compute a1,b1, th1, m1, sth1
!            END DO
!          END IF (npar.EQ.5)
!        END DO
!      END DO
!
! 5.   Complete NetCDF output file with frequency-dependent parameters
!
! 5.a  Define & write variable 'frequency' (from freq(:)), create attributes
!
! 5.b  Define & write variable 'frequency1' (from freq1(:)), create attributes
!
! 5.c  Define & write variable 'frequency2' (from freq2(:)), create attributes
!
! 5.d  Other frequency-dependent variables:
!      DO iva=1,npar
!        Define freqvarstr(ivar) with dimensions time & frequency
!        Put attributes ffillval(ivar), funits(ivar), etc.
!        Put variable
!      END DO
!
! 5.e  Close output NetCDF file
!
!

!
!  3. Parameters :
!

  USE C_CALDAT
  USE W3SERVMD
  USE NETCDF
  IMPLICIT NONE



  INTEGER                          :: iret, ncid, ncido, nctype, ndims, nvars, natts, ndimtmp
  INTEGER                          :: NDSI, NDSE, NDSO, ivar, itime
  INTEGER                          :: i,j,modyref, modmref, moddref
  INTEGER                          :: modhhref, modmmref, modssref
  INTEGER                          :: IP
  INTEGER                          :: imin, imax, ifreq, ntime, np, nfband, nfbandi, npar
!
  INTEGER                          :: dimln(4), dimid(4), outdimid(4), outvarid(20)
!
  INTEGER, ALLOCATABLE             :: station(:), string16(:), vartype(:)
  INTEGER, ALLOCATABLE             :: varid(:)
  INTEGER, DIMENSION(:),   ALLOCATABLE :: if1,if2
  !   
  REAL                             :: timestride, scalefac
  REAL                             :: d2r,pi
  REAL                             :: FMIN   ! first frequency of output grid
  REAL                             :: XFR
!
  REAL, ALLOCATABLE                :: lons(:,:), lats(:,:), lon(:), lat(:),dpt(:),depth(:,:) 
  REAL, ALLOCATABLE                :: pfillval(:)
  REAL   , DIMENSION(:),   ALLOCATABLE :: freq1,freq2,freq,df
  REAL   , DIMENSION(:),   ALLOCATABLE :: wf1,wf2
  REAL   , DIMENSION(:,:),   ALLOCATABLE :: freqi,freq1i,freq2i,dfi
  REAL   , DIMENSION(:,:,:), ALLOCATABLE :: Ef, Efi,th1,sth1, ndof, ndofi, check_ratio,check_ratioi
  REAL   , DIMENSION(:,:,:), ALLOCATABLE :: m1i,m2i,th1i,sth1i,th2i,sth2i
  REAL   , DIMENSION(:,:,:), ALLOCATABLE :: a1i,b1i,a2i,b2i,a1,b1,a2,b2,th2,sth2,m1,m2
!
  DOUBLE PRECISION                 :: moddateref, outdateref, fillval
!
  DOUBLE PRECISION, ALLOCATABLE    :: modtime(:), moddate(:)
!
  CHARACTER*1                      :: COMSTR
  CHARACTER*1024                   :: input_file, output_file
  CHARACTER*1024                   :: varlist
  CHARACTER*128                    :: freqname
  CHARACTER*128                    :: modtimeunits
!
  CHARACTER*128, ALLOCATABLE       :: freqvarstr(:)
  CHARACTER*25, ALLOCATABLE        :: station_id(:)
!
  LOGICAL                          :: flagdpt


!
! 1.a  IO set-up, set constants
!
  NDSI=10
  NDSE=6
  NDSO=6
  pi = ATAN(1.)*4
  d2r= pi/180.
  flagdpt=.FALSE.

!
! 1.b  Read input parameters from file
!

  OPEN(NDSI,file='buoy_freq_time_nc.inp')
  READ (NDSI,'(A)') COMSTR
  IF (COMSTR.EQ.' ') COMSTR = '$'
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*    ) nctype
  Write(6,*) 'nctype :', nctype
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) input_file
  WRITE(6,*) 'input file : ',trim(input_file)
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) output_file
  WRITE(6,*) 'output file : ',trim(output_file)
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  read(NDSI,'(A)') ! name of final file after time average not needed
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) np
  WRITE(6,*) 'number of stations : ',np
  allocate(station_id(np))
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  DO i=1,np
    READ(NDSI,*) station_id(i)
    WRITE(6,*) 'station id : ',station_id(i)
    CALL NEXTLN(COMSTR, NDSI, NDSE)
  END DO
  READ(NDSI,*) freqname ! name of the frequency variable in buoy file
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) npar ! number of frequency-dependent parameters to convert
  allocate(freqvarstr(npar))
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  DO i=1,npar
    READ(NDSI,*) freqvarstr(i)
    CALL NEXTLN(COMSTR, NDSI, NDSE)
  END DO

  READ(NDSI,'(A)') varlist
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  ! Output frequencies
  READ(NDSI,*) XFR, FMIN, nfband
  WRITE(NDSO,*) 'XFR, FMIN, nfband : ', XFR, FMIN, nfband

  CLOSE(NDSI)

  ! Create the corresponding vectors
  ALLOCATE(FREQ(nfband))       !center frequencies of the bands
  ALLOCATE(FREQ1(nfband))      !center frequencies of the bands
  ALLOCATE(FREQ2(nfband))      !center frequencies of the bands
  ALLOCATE(DF(nfband))         !width of f bands 
  FREQ(1)=FMIN
  FREQ1(1)=FMIN                !definition of boundaries like WW3
  FREQ2(1)=0.5*FMIN*(1+XFR)
  DO I=2,nfband
     FREQ(I)=FREQ(I-1)*XFR
     FREQ1(I)=0.5*(FREQ(I)+FREQ(I-1))
     FREQ2(I-1)=FREQ1(I)
  ENDDO
  FREQ2(nfband)=FREQ(nfband)    !definition like WW3   
  DF(:)=FREQ2(:)-FREQ1(:)


!------------------------------------------------------------------------------------!

!
! 1.c  Get first info from BUOY file (input_file):
!      number & size of dimensions, number of variables,
!      which variables to process
! 
  ! open file if exists, otherwise exit
  WRITE(NDSO,'(/A)') 'Open input file : '
  WRITE(NDSO,'(A)') trim(input_file)
  iret = nf90_open(input_file, NF90_NOWRITE, ncid)
  IF(iret.NE.0) THEN
    WRITE(NDSO,'(2A)') 'ERROR : no file : ', trim(input_file)
    STOP
  END IF

  iret=nf90_inquire(ncid, ndims, nvars, natts)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! Get dimensions
  iret = nf90_inq_dimid(ncid, 'time', dimid(1))
  if (iret.ne.0) iret = nf90_inq_dimid(ncid, 'TIME', dimid(1))
  iret = nf90_Inquire_Dimension(ncid, dimid(1), len = dimln(1))
  ntime=dimln(1)
  iret = nf90_inq_dimid(ncid, 'station', dimid(2))
  if (iret.ne.0) then
    dimln(2)=1
  else
    iret = nf90_Inquire_Dimension(ncid, dimid(2), len = dimln(2))
  end if
  np=dimln(2)
  iret = nf90_inq_dimid(ncid, 'frequency', dimid(3))
  if (iret.ne.0) iret = nf90_inq_dimid(ncid, 'FREQUENCY', dimid(3))
  iret = nf90_Inquire_Dimension(ncid, dimid(3), len = dimln(3))
  nfbandi=dimln(3)

  CALL CHECK_ERROR(IRET,__LINE__)
  write(6,*) 'Number of input time :', ntime
  write(6,*) 'Number of input station :', np
  write(6,*) 'Number of input frequencies :', nfbandi

  ALLOCATE(Efi(nfbandi,np,ntime))
  ALLOCATE(check_ratioi(nfbandi,np,ntime))
  ALLOCATE(ndofi(nfbandi,np,ntime))
  ALLOCATE(a1i(nfbandi,np,ntime))
  ALLOCATE(b1i(nfbandi,np,ntime))
  ALLOCATE(a2i(nfbandi,np,ntime))
  ALLOCATE(b2i(nfbandi,np,ntime))
  ALLOCATE(m1i(nfbandi,np,ntime))
  ALLOCATE(m2i(nfbandi,np,ntime))
  ALLOCATE(th1i(nfbandi,np,ntime))
  ALLOCATE(sth1i(nfbandi,np,ntime))
  ALLOCATE(th2i(nfbandi,np,ntime))
  ALLOCATE(sth2i(nfbandi,np,ntime))

  ALLOCATE(varid(20))
!------------------------------------------------------------------------------------!
  ! Get reference time (from VARIABLE, not dim)
  iret=nf90_inq_varid(ncid,'time', varid(1))
  if (iret.ne.0) iret=nf90_inq_varid(ncid,'TIME', varid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_att(ncid,varid(1),'units',modtimeunits)
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (INDEX(modtimeunits, "seconds").NE.0) THEN
    read(modtimeunits(15:18),'(I4.4)') modyref
    read(modtimeunits(20:21),'(I2.2)') modmref
    read(modtimeunits(23:24),'(I2.2)') moddref
    read(modtimeunits(26:27),'(I2.2)') modhhref
    read(modtimeunits(29:30),'(I2.2)') modmmref
    read(modtimeunits(32:33),'(I2.2)') modssref
  ELSE IF (INDEX(modtimeunits, "days").NE.0) THEN
    read(modtimeunits(12:15),'(I4.4)') modyref
    read(modtimeunits(17:18),'(I2.2)') modmref
    read(modtimeunits(20:21),'(I2.2)') moddref
    read(modtimeunits(23:24),'(I2.2)') modhhref
    read(modtimeunits(26:27),'(I2.2)') modmmref
    read(modtimeunits(29:30),'(I2.2)') modssref
  ELSE IF (index(modtimeunits, "hours").NE.0) THEN
    read(modtimeunits(13:16),'(I4.4)') modyref
    read(modtimeunits(18:19),'(I2.2)') modmref
    read(modtimeunits(21:22),'(I2.2)') moddref
    read(modtimeunits(24:25),'(I2.2)') modhhref
    read(modtimeunits(27:28),'(I2.2)') modmmref
    read(modtimeunits(30:31),'(I2.2)') modssref
  END IF

  moddateref=julday(moddref,modmref,modyref)

  ! Define outdateref for an output in "days since 1990-01-01T00:00:00Z"
  outdateref=julday(1,1,1990)

  ! get model variable time
  IF(ALLOCATED(modtime)) deallocate(modtime)
  allocate(modtime(ntime))
  IF(ALLOCATED(moddate)) deallocate(moddate)
  allocate(moddate(ntime))
  iret=nf90_inq_varid(ncid,'time', varid(1))
  if(iret.ne.0) iret=nf90_inq_varid(ncid,'TIME', varid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_var(ncid, varid(1), modtime)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! convert the julian date in calendar date
  IF (index(modtimeunits, "seconds").NE.0) THEN
  moddate(:)=modtime(:)/86400.+ moddateref - outdateref
  ELSE IF (index(modtimeunits, "hours").NE.0) THEN
  moddate(:)=modtime(:)/24.+ moddateref - outdateref
  ELSE IF (index(modtimeunits, "days").NE.0) THEN
  moddate(:)=modtime(:)+ moddateref - outdateref
  END IF
  timestride=(moddate(ntime)-moddate(1))/(ntime-1)

  allocate(pfillval(npar))

!------------------------------------------------------------------------------------!
  ! Get longitude, latitude & depth
  allocate(lon(np))
  allocate(lat(np))
  allocate(dpt(np))
  iret=nf90_inq_varid(ncid,"longitude",varid(2))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"lon",varid(2))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Longitude",varid(2))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"LONGITUDE",varid(2))
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[WARNING] no longitude variable in input file'
  ELSE
    iret=nf90_get_var(ncid,varid(2),lon)
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
  iret=nf90_inq_varid(ncid,"latitude",varid(3))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"lat",varid(3))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Latitude",varid(3))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"LATITUDE",varid(3))
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[WARNING] no latitude variable in input file'
  ELSE
    iret=nf90_get_var(ncid,varid(3),lat)
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
  iret=nf90_inq_varid(ncid,"depth",varid(4))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"dpt",varid(4))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Depth",varid(4))
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"DEPTH",varid(4))
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[INFO] no depth variable in input file'
    flagdpt=.FALSE.
  ELSE
    iret=nf90_get_var(ncid,varid(4),dpt)
    CALL CHECK_ERROR(IRET,__LINE__)
    flagdpt=.TRUE.
  END IF

!------------------------------------------------------------------------------------!
  ! Get frequency values
  iret=nf90_inq_varid(ncid,freqname,varid(5))
  IF ( iret/=nf90_noerr ) WRITE(6,'(3A)') '[ERROR] no ',trim(freqname),' variable in input file'
  CALL CHECK_ERROR(IRET,__LINE__)
  ALLOCATE(freqi(nfbandi,ntime))
  ! get type of variable
  iret=nf90_inquire_variable(ncid, varid(5),ndims=ndimtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (ndimtmp.EQ.1) THEN
    iret=nf90_get_var(ncid, varid(5), freqi(:,1), start=(/1/), count=(/nfbandi/) )
    DO i=2,ntime
      freqi(:,i)=freqi(:,1)
    END DO
  ELSE IF (ndimtmp.EQ.2) THEN
    iret=nf90_get_var(ncid, varid(5), freqi(:,:), start=(/1,1/), count=(/nfbandi,ntime/) )
  END IF
  CALL CHECK_ERROR(IRET,__LINE__)
  !write(6,*) 'freqi :', freqi(:)
  IF (SUM(freqi).EQ.0) THEN
    WRITE(6,*) '[ERROR] frequency values are empty'
    STOP
  END IF 

!------------------------------------------------------------------------------------!
  ! Get npar variable values
  ALLOCATE(vartype(npar))
  
  iret=nf90_inq_varid(ncid,freqvarstr(1),varid(7))
  IF ( iret/=nf90_noerr ) THEN
    write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(1)),' variable in input file'
    STOP
  ELSE
    ! get type of variable
    iret=nf90_inquire_variable(ncid, varid(7),xtype=vartype(1),ndims=ndimtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    IF (ndimtmp.EQ.2) THEN
      iret=nf90_get_var(ncid,varid(7),efi(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
    ELSE IF (ndimtmp.EQ.3) THEN
      iret=nf90_get_var(ncid,varid(7),efi(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
    END IF
    CALL CHECK_ERROR(IRET,__LINE__)
    ! get scale factor
    iret=nf90_get_att(ncid,varid(7),'_FillValue',fillval)
    IF (iret.ne.0) THEN
      SELECT CASE (vartype(ivar))
        CASE (1);fillval=NF90_FILL_BYTE
        CASE (3);fillval=NF90_FILL_SHORT
        CASE (4);fillval=NF90_FILL_INT
        CASE (5);fillval=NF90_FILL_FLOAT
        CASE (6);fillval=NF90_FILL_DOUBLE
      END SELECT
    END IF
    iret=nf90_get_att(ncid,varid(7),'scale_factor',scalefac)
    if (iret.eq.0) WHERE(efi.NE.fillval) efi=efi*scalefac
  END IF

  ! get th1, sth1
  IF (npar.GE.3) THEN
    ! th1
    iret=nf90_inq_varid(ncid,freqvarstr(2),varid(8))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(2)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(8),xtype=vartype(2),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(8),th1i(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(8),th1i(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(8),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(8),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(th1i.NE.fillval) th1i=th1i*scalefac
    END IF

    ! sth1
    iret=nf90_inq_varid(ncid,freqvarstr(3),varid(9))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(3)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(9),xtype=vartype(3),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(9),sth1i(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(9),sth1i(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(9),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(9),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(sth1i.NE.fillval) sth1i=sth1i*scalefac
    END IF
  END IF

  IF (npar.GE.5) THEN
    ! th2
    iret=nf90_inq_varid(ncid,freqvarstr(4),varid(10))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(4)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(10),xtype=vartype(4),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(10),th2i(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(10),th2i(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(10),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(10),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(th2i.NE.fillval) th2i=th2i*scalefac
    END IF

    ! sth2
    iret=nf90_inq_varid(ncid,freqvarstr(5),varid(11))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(5)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(11),xtype=vartype(5),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(11),sth2i(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(11),sth2i(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(11),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(11),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(sth2i.NE.fillval) sth2i=sth2i*scalefac
    END IF
  END IF

  IF (npar.EQ.7) THEN
    ! check_ratio
    iret=nf90_inq_varid(ncid,freqvarstr(6),varid(12))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(6)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(12),xtype=vartype(6),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(12),check_ratioi(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(12),check_ratioi(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(12),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(12),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(check_ratio.NE.fillval) check_ratio=check_ratio*scalefac
    END IF

    ! ndof
    iret=nf90_inq_varid(ncid,freqvarstr(7),varid(13))
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(freqvarstr(7)),' variable in input file'
      STOP
    ELSE
      ! get type of variable
      iret=nf90_inquire_variable(ncid, varid(13),xtype=vartype(7),ndims=ndimtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF (ndimtmp.EQ.2) THEN
        iret=nf90_get_var(ncid,varid(13),ndofi(:,1,:), start=(/1,1/), count=(/nfbandi,ntime/))
      ELSE IF (ndimtmp.EQ.3) THEN
        iret=nf90_get_var(ncid,varid(13),ndofi(:,:,:), start=(/1,1,1/), count=(/nfbandi,np,ntime/))
      END IF
      CALL CHECK_ERROR(IRET,__LINE__)
      ! get scale factor
      iret=nf90_get_att(ncid,varid(13),'_FillValue',fillval)
      IF (iret.ne.0) THEN
        SELECT CASE (vartype(ivar))
          CASE (1);fillval=NF90_FILL_BYTE
          CASE (3);fillval=NF90_FILL_SHORT
          CASE (4);fillval=NF90_FILL_INT
          CASE (5);fillval=NF90_FILL_FLOAT
          CASE (6);fillval=NF90_FILL_DOUBLE
        END SELECT
      END IF
      iret=nf90_get_att(ncid,varid(13),'scale_factor',scalefac)
      if (iret.eq.0) WHERE(ndof.NE.fillval) ndof=ndof*scalefac
    END IF

  END IF
  ! get attributes
  DO ivar=1,npar
    SELECT CASE (vartype(ivar))
      CASE (1)
        pfillval(ivar)=NF90_FILL_BYTE
!      CASE (2)
!        pfillval(ivar)=NF90_FILL_CHAR
      CASE (3)
        pfillval(ivar)=NF90_FILL_SHORT
      CASE (4)
        pfillval(ivar)=NF90_FILL_INT
      CASE (5)
        pfillval(ivar)=NF90_FILL_FLOAT
      CASE (6)
        pfillval(ivar)=NF90_FILL_DOUBLE
    END SELECT
  END DO

  ! close buoy file
  iret=nf90_close(ncid)

  ! mask FillValues

!------------------------------------------------------------------------------------!
!
! 3. Take care of frequency-dependent variables
!

!  WRITE(6,*) 'input frequencies :', freqi(:)
!  WRITE(6,*) 'output frequencies :', freq(:)

!  !Input frequencies
  ALLOCATE(freq1i(nfbandi,ntime))
  ALLOCATE(freq2i(nfbandi,ntime))
  ALLOCATE(dfi(nfbandi,ntime))

  DO itime=1,ntime
    FREQ1i(1,itime)=freqi(1,itime)
    DO I=2,nfbandi
      FREQ1i(I,itime)=0.5*(FREQi(I,itime)+FREQi(I-1,itime))
      FREQ2i(I-1,itime)=FREQ1i(I,itime)
    END DO
    FREQ2i(nfbandi,itime)=freqi(nfbandi,itime)

!  ! Definition of freq1i & freq2i like in WW3
!  freq1i(1)=freqi(1)                !definition of boundaries like WW3
!  freq2i(1)=freqi(1)+0.5*dfi(1)
!  DO I=2,nfbandi
!     freq1i(I)=FREQi(I)-0.5*dfi(I)
!     freq2i(I)=FREQi(I)+0.5*dfi(I)
!  END DO
!  freq2i(nfbandi)=freqi(nfbandi)    !definition like WW3   

    !WRITE(6,*) 'freq1i :'
    !WRITE(6,*) freq1i(:,itime)
    !WRITE(6,*) 'freq2i :'
    !WRITE(6,*) freq2i(:,itime)


    dfi(:,itime)= freq2i(:,itime)-freq1i(:,itime)
  END DO

  ! The corresponding output frequencies have already been calculated in 1.a

  ! Process Ef, th1m, sth1m, th2m, sth2m
  ! input variables 

  ALLOCATE(Ef(nfband,np,ntime))
  ALLOCATE(ndof(nfband,np,ntime))
  ALLOCATE(check_ratio(nfband,np,ntime))
  ALLOCATE(a1(nfband,np,ntime))
  ALLOCATE(b1(nfband,np,ntime))
  ALLOCATE(m1(nfband,np,ntime))
  ALLOCATE(m2(nfband,np,ntime))
  ALLOCATE(a2(nfband,np,ntime))
  ALLOCATE(b2(nfband,np,ntime))
  ALLOCATE(th1(nfband,np,ntime))
  ALLOCATE(sth1(nfband,np,ntime))
  ALLOCATE(th2(nfband,np,ntime))
  ALLOCATE(sth2(nfband,np,ntime))


  !*********************************************!
  !  FREQUENCY INTERPOLATION TO THE MODEL GRID  !
  !*********************************************!

  ALLOCATE(if1(NFBAND))   !index of the first fband (input) which
                          !has a common part with the output fband
  ALLOCATE(if2(NFBAND))   !index of the last fband (input)
  ALLOCATE(wf1(NFBAND))   !fraction of the first fband in common
  ALLOCATE(wf2(NFBAND))   !fraction of the last fband in common


!
! Loop on time
!
  DO itime=1,ntime

    WRITE(6,*) 'processing time step ', itime, ' over ', ntime, ' times'

    DO ip=1,np ! number of stations

  imin=1
  imax=1
  if1=0
  if2=0
  wf1=0.
  wf2=0.

  ! Loop on output frequency band to get if1(J), if2(J), wf1(J) & wf2(J)
  DO J=1,NFBAND

    ! Look for imin: the last index for which FREQ1i(imin) < FREQ1(J)
    DO WHILE ( (imin.LT.nfbandi) .AND. (FREQ1i(imin,itime).LT.FREQ1(J)) )
!      WRITE(6,*) 'IMIN',J,Imin,FREQ1i(imin,itime),FREQ1(J)
      imin=imin+1 
    END DO
    IF (FREQ1i(imin,itime).GT.FREQ1(J)) imin=imin-1
    imax=imin
!    WRITE(6,*) 'IMAX:',imax,J

    IF (imin == 0) THEN ! means freq1i(1) >= freq2(J) so if1(J)=1 in any case

      ! if freq1i(1) < freq2(J) : means at least freq2i(1) should be OK (?)
      IF (FREQ1i(1,itime).LT.FREQ2(J)) THEN
        imax=1
        DO WHILE ( (imax.LT.nfbandi) .AND. (FREQ2i(imax,itime).LT.FREQ2(J)) )
           !WRITE(6,*) 'imax',J,Imax,FREQ1i(imax,itime),FREQ2(J)
           imax=imax+1             
        ENDDO 
        if1(J)=1
        if2(J)=imax
        IF (imax == 1) THEN
           wf1(J)=DF(J)/ABS(FREQ2i(imax,itime)-FREQ1i(imax,itime))
           wf2(J)=0.
        ELSE
           wf1(J)=(FREQ2i(imin,itime)-FREQ1(J))/ABS(FREQ2(j)-FREQ1i(imax,itime))
           wf2(J)=(FREQ2(J)-FREQ1i(imax,itime))/ABS(FREQ1i(imax,itime)-FREQ2i(imax,itime))
        ENDIF
      ! if even freq1i(1) >= freq2(J), means there is no matching freq2i
      ELSE 
        if1(J)=1
        if2(J)=1
        wf1(J)=0.
        wf2(J)=0.
      ENDIF

    ELSE ! imin .NE. 0

      DO WHILE ( (imax < nfbandi).AND. &
                 ((FREQ2i(imax,itime).LT.FREQ2(J)) .AND. (FREQ1i(imax,itime).LT.FREQ2(J))) )
        imax=imax+1 
!        WRITE(6,*) 'imax',J,Imax,FREQ2i(imax,itime),FREQ2(J),FREQ1i(imax,itime)
      END DO
 
      IF (FREQ1i(imax,itime).GT.FREQ2(J)) imax=imax-1
      if1(J)=imin
      if2(J)=imax
      !WRITE(6,*) 'if1,if2:',J,if1(J),if2(J),FREQ1i(if1(J),itime),FREQ2i(if2(J),itime),dfi(imin,itime)

      IF (imax == 0) THEN 
        if1(J)=1
        if2(J)=1
        wf1(J)=0.
        wf2(J)=0.
      ELSE
        IF (imax.EQ.imin) THEN
           IF (FREQ2i(imax,itime)>FREQ1(J)) THEN 
              wf1(J)=DF(j)/ABS(FREQ1i(imax,itime)-FREQ2i(imax,itime))
           ELSE 
              wf1(J)=0.
           ENDIF
           wf2(J)=0.
        ELSE
           wf1(J)=ABS(FREQ2i(imin,itime)-FREQ1(J))/ABS(dfi(imin,itime))
           wf2(J)=(FREQ2(J)-FREQ1i(imax,itime))/ABS(dfi(imax,itime))
           IF (wf2(J).LT.0) wf2(J)=0.
           IF (wf1(J).LT.0) wf1(J)=0.
        END IF ! imax.EQ.imin
      END IF ! imax.EQ.0

    END IF ! imin.EQ.0

!    WRITE(6,'(I4,2f7.4,2I4,4f8.5)') J,FREQ1(J),FREQ2(J),if1(J),if2(J), &
!                           FREQ2i(if1(J),itime),FREQ1i(if2(J),itime),wf1(J),wf2(J)
    IF (imax == 0) imax=1
    imin=imax

  END DO


      IF (npar.GE.5) THEN
        DO ifreq=1,nfbandi
          IF (th1i(ifreq,ip,itime).NE.pfillval(2)) THEN
            a1i(ifreq,ip,itime)=COS(th1i(ifreq,ip,itime)*d2r)
            b1i(ifreq,ip,itime)=SIN(th1i(ifreq,ip,itime)*d2r)
          ELSE
            a1i(ifreq,ip,itime)=0 ! ??
            b1i(ifreq,ip,itime)=0 ! ??
          END IF
          IF (sth1i(ifreq,ip,itime).NE.pfillval(3)) THEN
            m1i(ifreq,ip,itime)=ABS((1.-0.5*(sth1i(ifreq,ip,itime)*d2r)**2))
          ELSE
            m1i(ifreq,ip,itime)=0 ! ??
          END IF
        END DO
        DO I=1,nfbandi
          IF (m1i(I,ip,itime).NE.0) THEN 
            a1i(I,ip,itime)=a1i(I,ip,itime)*m1i(I,ip,itime)
            b1i(I,ip,itime)=b1i(I,ip,itime)*m1i(I,ip,itime)
          END IF
        END DO
        DO ifreq=1,nfbandi
          IF (th2i(ifreq,ip,itime).NE.pfillval(4)) THEN
            a2i(ifreq,ip,itime)=COS(2.*th2i(ifreq,ip,itime)*d2r)
            b2i(ifreq,ip,itime)=SIN(2.*th2i(ifreq,ip,itime)*d2r)
          ELSE
            a2i(ifreq,ip,itime)=0 ! ??
            b2i(ifreq,ip,itime)=0 ! ??
          END IF
          IF (sth2i(ifreq,ip,itime).NE.pfillval(5)) THEN
            m2i(ifreq,ip,itime)=ABS((1.-2.0*(sth2i(ifreq,ip,itime)*d2r)**2))
          ELSE
            m2i(ifreq,ip,itime)=0 !??
          END IF
        END DO
        DO I=1,nfbandi
          IF (m2i(I,ip,itime).NE.0) THEN 
            a2i(I,ip,itime)=a2i(I,ip,itime)*m2i(I,ip,itime)
            b2i(I,ip,itime)=b2i(I,ip,itime)*m2i(I,ip,itime)
          END IF
        END DO
        ! Finalize computation of E(f), a1, a2, b1, b2, m1, m2,...
        DO J=1,nfband
          Ef(J,ip,itime)=Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                      +Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  
          ndof(J,ip,itime)=ndofi(if1(J),ip,itime)*wf1(J) &
                      +ndofi(if2(J),ip,itime)*wf2(J)  
          check_ratio(J,ip,itime)=check_ratioi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J)*Efi(if1(J),ip,itime) &
                      +check_ratioi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)*Efi(if2(J),ip,itime)*dfi(if2(J),itime) 
          a1(J,ip,itime)=a1i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                      +a1i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  
          b1(J,ip,itime)=b1i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                      +b1i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  
          a2(J,ip,itime)=a2i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                      +a2i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  
          b2(J,ip,itime)=b2i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                      +b2i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  

          DO I=if1(J)+1,if2(J)-1
            Ef(J,ip,itime)=Ef(J,ip,itime)+Efi(I,ip,itime)*dfi(I,itime)
            ndof(J,ip,itime)=ndofi(J,ip,itime)+ndofi(I,ip,itime)
            check_ratio(J,ip,itime)=check_ratioi(J,ip,itime)+check_ratioi(I,ip,itime)*dfi(I,itime)
            a1(J,ip,itime)=a1(J,ip,itime)+a1i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
            a2(J,ip,itime)=a2(J,ip,itime)+a2i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
            b1(J,ip,itime)=b1(J,ip,itime)+b1i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
            b2(J,ip,itime)=b2(J,ip,itime)+b2i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
          END DO
          Ef(J,ip,itime)=Ef(J,ip,itime)/df(J)
          ndof(J,ip,itime)=ndof(J,ip,itime)
          a1(J,ip,itime)=a1(J,ip,itime)/df(J)
          a2(J,ip,itime)=a2(J,ip,itime)/df(J)
          b1(J,ip,itime)=b1(J,ip,itime)/df(J)
          b2(J,ip,itime)=b2(J,ip,itime)/df(J)
          th1(J,ip,itime)=ATAN2(b1(J,ip,itime),a1(J,ip,itime))           
          th2(J,ip,itime)=ATAN2(b2(J,ip,itime),a2(J,ip,itime))/2. 
         
          IF (th1(J,ip,itime).LT.0) th1(J,ip,itime)=th1(J,ip,itime)+pi*2
          IF (th1(J,ip,itime).GT.(2*pi)) WRITE(994,*) 'ERROR:',ITIME,J, &
                                       th1(J,ip,itime),a1(J,ip,itime),b1(J,ip,itime)
          IF (th2(J,ip,itime).LT.0) th2(J,ip,itime)=th2(J,ip,itime)+pi

          IF (Ef(J,ip,itime) /= 0) THEN
            check_ratio(J,ip,itime)=check_ratio(J,ip,itime)/Ef(J,ip,itime)
            m1(J,ip,itime)=SQRT(a1(J,ip,itime)**2+b1(J,ip,itime)**2)/Ef(J,ip,itime)
            m2(J,ip,itime)=SQRT(A2(J,ip,itime)**2+B2(J,ip,itime)**2)/Ef(J,ip,itime)
            sth1(J,ip,itime)  =SQRT(ABS(2.0*(1-m1(J,ip,itime))))/d2r
            sth2(J,ip,itime)  =SQRT(ABS(0.5*(1-m2(J,ip,itime))))/d2r
          ELSE
            sth1(J,ip,itime)=0.
            sth2(J,ip,itime)=0.
          END IF
        END DO  ! J=1,nfband

        !!!!!!! th1/d2r !!!!!!!
!        th1(:,:)=th1(:,:)/d2r
!        th2(:,:)=th2(:,:)/d2r
        !!!!!!!!!!!!!!!!!!!!!!!
      ELSE ! (npar.NE.5) 

        IF (npar.GE.3) THEN 
          a1i(:,ip,itime)=COS(th1i(:,ip,itime)*d2r)
          b1i(:,ip,itime)=SIN(th1i(:,ip,itime)*d2r)
          m1i(:,ip,itime)=ABS((1.-0.5*(sth1i(:,ip,itime)*d2r)**2))
          DO I=1,nfbandi
            IF (m1i(i,ip,itime).NE.0) THEN 
              a1i(I,ip,itime)=a1i(I,ip,itime)*m1i(I,ip,itime)
              b1i(I,ip,itime)=b1i(I,ip,itime)*m1i(I,ip,itime)
            END IF
          END DO
        END IF ! npar.EQ.3
  
        ! Finalizes the calculation of E(f) ... 
        DO J=1,nfband
          Ef(J,ip,itime)=Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                       +Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)

          IF (npar.GE.3) THEN
            a1(J,ip,itime)=a1i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                        +a1i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J)  
            b1(J,ip,itime)=b1i(if1(J),ip,itime)*Efi(if1(J),ip,itime)*dfi(if1(J),itime)*wf1(J) &
                        +b1i(if2(J),ip,itime)*Efi(if2(J),ip,itime)*dfi(if2(J),itime)*wf2(J) 
          END IF 
          DO I=if1(J)+1,if2(J)-1
            Ef(J,ip,itime)=Ef(J,ip,itime)+Efi(I,ip,itime)*dfi(I,itime)
            IF (npar.GE.3) THEN
              a1(J,ip,itime)=a1(J,ip,itime)+a1i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
              b1(J,ip,itime)=b1(J,ip,itime)+b1i(I,ip,itime)*Efi(I,ip,itime)*dfi(I,itime)
            END IF 
          END DO
          Ef(J,ip,itime)=Ef(J,ip,itime)/df(J)
!
          IF (npar.GE.3) THEN
            a1(J,ip,itime)=a1(J,ip,itime)/df(J) 
            b1(J,ip,itime)=b1(J,ip,itime)/df(J)
            th1(J,ip,itime)=ATAN2(b1(J,ip,itime),a1(J,ip,itime))           

            IF (th1(J,ip,itime).LT.0) th1(J,ip,itime)=th1(J,ip,itime)+pi*2
            IF (th1(J,ip,itime).GT.(2*pi)) WRITE(994,*) 'ERROR:',ITIME,J, &
                                        th1(J,ip,itime),a1(J,ip,itime),b1(J,ip,itime)
            IF (Ef(J,ip,itime) /= 0) THEN
              m1(J,ip,itime)=SQRT(a1(J,ip,itime)**2+b1(J,ip,itime)**2)/Ef(J,ip,itime)
              sth1(J,ip,itime)  =SQRT(ABS(2.0*(1-m1(J,ip,itime))))/d2r
            ELSE
              sth1(J,ip,itime)=0.
            END IF
          END IF  ! (npar.EQ.3)
        END DO ! J=1,nfband

        !!!!!!! th1/d2r !!!!!!!
!        IF (npar.EQ.3)  th1(:,:)=th1(:,:)/d2r
        !!!!!!!!!!!!!!!!!!!!!!!
      END IF ! (npar.EQ.5)

    END DO ! loop on stations
  END DO ! loop on ntime

  ! At the end of this loop: we should have Ef (and th1,sth1,th2,sth2 if needed)
  ! almost ready to be written in the output_file (only th1 & th2 to divide by d2r)

!------------------------------------------------------------------------------------!
!
! 4. Create NetCDF output file
!

  ! Create output file 
  IF(NCTYPE.EQ.3)  iret = nf90_create(output_file, NF90_CLOBBER, ncido)
  IF(NCTYPE.EQ.4)  iret = nf90_create(output_file, NF90_NETCDF4, ncido)
  CALL CHECK_ERROR(IRET,__LINE__)  

  ! Define dimensions
  iret = nf90_def_dim(ncido, 'time', NF90_UNLIMITED, outdimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'station', np, outdimid(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'string16', 16, outdimid(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'frequency', nfband, outdimid(4)) 
  CALL CHECK_ERROR(IRET,__LINE__)  

  ! Define variables

!  time
  iret=nf90_def_var(ncido, 'time', NF90_DOUBLE, outdimid(1), outvarid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(1), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(1),'long_name','julian day (UT)')
  iret=nf90_put_att(ncido,outvarid(1),'standard_name','time')
  iret=nf90_put_att(ncido,outvarid(1),'units','days since 1990-01-01 00:00:00')
  iret=nf90_put_att(ncido,outvarid(1),'conventions','Relative julian days with decimal part (as parts of the day)')
  iret=nf90_put_att(ncido,outvarid(1),'axis','T') 

!  station
  iret=nf90_def_var(ncido, 'station', NF90_INT, (/outdimid(2)/), outvarid(2))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(2), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(2),'long_name','station id')
  iret=nf90_put_att(ncido,outvarid(2),'_FillValue',NF90_FILL_INT)
  iret=nf90_put_att(ncido,outvarid(2),'axis','X') 

!  string16
  iret=nf90_def_var(ncido, 'string16', NF90_INT, (/outdimid(3)/), outvarid(3))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(3), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(3),'long_name','station_name number of characters')
  iret=nf90_put_att(ncido,outvarid(3),'_FillValue',NF90_FILL_INT)
  iret=nf90_put_att(ncido,outvarid(3),'axis','W') 

!  station_name
  iret=nf90_def_var(ncido, 'station_name', NF90_CHAR, (/outdimid(3),outdimid(2)/), outvarid(4))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(4), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(4),'long_name','station name')
  iret=nf90_put_att(ncido,outvarid(4),'content','XW')
  iret=nf90_put_att(ncido,outvarid(4),'associates','station string16')

!  longitude
  iret=nf90_def_var(ncido, 'longitude', NF90_FLOAT, (/outdimid(2),outdimid(1)/), outvarid(5))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(5), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(5),'long_name','longitude')
  iret=nf90_put_att(ncido,outvarid(5),'standard_name','longitude')
!  iret=nf90_put_att(ncido,outvarid(5),'globwave_name','longitude')
  iret=nf90_put_att(ncido,outvarid(5),'units','degree_east')
  iret=nf90_put_att(ncido,outvarid(5),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(5),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(5),'valid_min',-180.0)
  iret=nf90_put_att(ncido,outvarid(5),'valid_max',360.)
  iret=nf90_put_att(ncido,outvarid(5),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(5),'content','TX')
  iret=nf90_put_att(ncido,outvarid(5),'associates','time station')

!  latitude
  iret=nf90_def_var(ncido, 'latitude', NF90_FLOAT, (/outdimid(2),outdimid(1)/), outvarid(6))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(6), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(6),'long_name','latitude')
  iret=nf90_put_att(ncido,outvarid(6),'standard_name','latitude')
!  iret=nf90_put_att(ncido,outvarid(6),'globwave_name','latitude')
  iret=nf90_put_att(ncido,outvarid(6),'units','degree_north')
  iret=nf90_put_att(ncido,outvarid(6),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(6),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(6),'valid_min',-90.0)
  iret=nf90_put_att(ncido,outvarid(6),'valid_max',180.)
  iret=nf90_put_att(ncido,outvarid(6),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(6),'content','TX')
  iret=nf90_put_att(ncido,outvarid(6),'associates','time station')

!frequency
  iret=nf90_def_var(ncido, 'frequency', NF90_FLOAT, (/outdimid(4)/), outvarid(7))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(7), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(7),'long_name','frequency of center band')
  iret=nf90_put_att(ncido,outvarid(7),'standard_name','sea_surface_wave_frequency')
!  iret=nf90_put_att(ncido,outvarid(7),'globwave_name','frequency')
  iret=nf90_put_att(ncido,outvarid(7),'units','s-1')
  iret=nf90_put_att(ncido,outvarid(7),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(7),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(7),'valid_min',0.)
  iret=nf90_put_att(ncido,outvarid(7),'valid_max',10.)
  iret=nf90_put_att(ncido,outvarid(7),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(7),'axis','Y') 

!frequency1
  iret=nf90_def_var(ncido, 'frequency1', NF90_FLOAT, (/outdimid(4)/), outvarid(8))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(8), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(8),'long_name','frequency of lower band')
  iret=nf90_put_att(ncido,outvarid(8),'standard_name','frequency_of_lower_band')
!  iret=nf90_put_att(ncido,outvarid(8),'globwave_name','frequency_lower_band')
  iret=nf90_put_att(ncido,outvarid(8),'units','s-1')
  iret=nf90_put_att(ncido,outvarid(8),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(8),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(8),'valid_min',0.)
  iret=nf90_put_att(ncido,outvarid(8),'valid_max',10.)
  iret=nf90_put_att(ncido,outvarid(8),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(8),'content','Y')
  iret=nf90_put_att(ncido,outvarid(8),'associates','frequency')

!frequency2
  iret=nf90_def_var(ncido, 'frequency2', NF90_FLOAT, (/outdimid(4)/), outvarid(9))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(9), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(9),'long_name','frequency of upper band')
  iret=nf90_put_att(ncido,outvarid(9),'standard_name','frequency_of_upper_band')
!  iret=nf90_put_att(ncido,outvarid(9),'globwave_name','frequency_upper_band')
  iret=nf90_put_att(ncido,outvarid(9),'units','s-1')
  iret=nf90_put_att(ncido,outvarid(9),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(9),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(9),'valid_min',0.)
  iret=nf90_put_att(ncido,outvarid(9),'valid_max',10.)
  iret=nf90_put_att(ncido,outvarid(9),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(9),'content','Y')
  iret=nf90_put_att(ncido,outvarid(9),'associates','frequency')

!Ef
  iret=nf90_def_var(ncido, 'ef', NF90_FLOAT,(/outdimid(4),outdimid(2),outdimid(1)/), outvarid(10))
!/NC4       IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(10), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(10),'long_name','surface elevation variance spectrum')
  iret=nf90_put_att(ncido,outvarid(10),'standard_name','sea_surface_wave_variance_spectral_density')
!  iret=nf90_put_att(ncido,outvarid(10),'globwave_name','variance_spectral_density')
  iret=nf90_put_att(ncido,outvarid(10),'units','m2 s')
  iret=nf90_put_att(ncido,outvarid(10),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(10),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(10),'valid_min',0.)
  iret=nf90_put_att(ncido,outvarid(10),'valid_max',10.)
  iret=nf90_put_att(ncido,outvarid(10),'_FillValue', NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(10),'content','TXY')

  IF (npar.GE.3) THEN
!th1m
    iret=nf90_def_var(ncido, 'th1m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(11))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(11), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(11),'long_name','mean wave direction from spectral moments')
    iret=nf90_put_att(ncido,outvarid(11),'standard_name','mean_wave_direction')
!    iret=nf90_put_att(ncido,outvarid(11),'globwave_name','mean_wave_direction')
    iret=nf90_put_att(ncido,outvarid(11),'units','degree')
    iret=nf90_put_att(ncido,outvarid(11),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(11),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(11),'valid_min',0.)
    iret=nf90_put_att(ncido,outvarid(11),'valid_max',360.)
    iret=nf90_put_att(ncido,outvarid(11),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(11),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(11),'associates','time station frequency')

!sth1m
    iret=nf90_def_var(ncido, 'sth1m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(12))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(12), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(12),'long_name','directional spread from spectral moments')
    iret=nf90_put_att(ncido,outvarid(12),'standard_name','mean_wave_spreading')
!    iret=nf90_put_att(ncido,outvarid(12),'globwave_name','mean_wave_spreading')
    iret=nf90_put_att(ncido,outvarid(12),'units','degree')
    iret=nf90_put_att(ncido,outvarid(12),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(12),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(12),'valid_min',0.)
    iret=nf90_put_att(ncido,outvarid(12),'valid_max',360.)
    iret=nf90_put_att(ncido,outvarid(12),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(12),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(12),'associates','time station frequency')
  END IF

  IF (npar.EQ.5) THEN
!th2m
    iret=nf90_def_var(ncido, 'th2m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(13))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(13), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(13),'units','degree')
    iret=nf90_put_att(ncido,outvarid(13),'long_name','mean wave direction for second frequency')
    iret=nf90_put_att(ncido,outvarid(13), 'standard_name',  &
'sea_surface_wave_mean_direction_from_variance_spectral_density_second_frequency_moment')
    iret=nf90_put_att(ncido,outvarid(13),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(13),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(13),'valid_min',0.)
    iret=nf90_put_att(ncido,outvarid(13),'valid_max',360.)
    iret=nf90_put_att(ncido,outvarid(13),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(13),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(13),'associates','time station frequency')

!sth2m
    iret=nf90_def_var(ncido, 'sth2m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(14))
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(14), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(14),'long_name','mean wave spreading direction for second frequency')
    iret=nf90_put_att(ncido,outvarid(14), 'standard_name',  &
    'sea_surface_wave_mean_spreading_from_variance_spectral_density_second_frequency_moment')
    iret=nf90_put_att(ncido,outvarid(14),'units','degree')
    iret=nf90_put_att(ncido,outvarid(14),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(14),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(14),'valid_min',0.)
    iret=nf90_put_att(ncido,outvarid(14),'valid_max',360.)
    iret=nf90_put_att(ncido,outvarid(14),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(14),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(14),'associates','time station frequency')
  END IF

  IF (npar.EQ.7) THEN
!check_ratio
    iret=nf90_def_var(ncido, 'check_ratio', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(16))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(16), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(16),'units','-')
    iret=nf90_put_att(ncido,outvarid(16),'long_name', &
'sea_surface_wave_ratio_of_horizontal_to_vertical_displacement_variance')
    iret=nf90_put_att(ncido,outvarid(16), 'standard_name',  &
'sea_surface_wave_ratio_of_horizontal_to_vertical_displacement_variance')
    iret=nf90_put_att(ncido,outvarid(16),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(16),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(16),'valid_min',0.1)
    iret=nf90_put_att(ncido,outvarid(16),'valid_max',1000.)
    iret=nf90_put_att(ncido,outvarid(16),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(16),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(16),'associates','time station frequency')

!ndof
    iret=nf90_def_var(ncido, 'ndof', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(17))
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(17), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(17),'long_name','number of degrees of freedom')
    iret=nf90_put_att(ncido,outvarid(17), 'standard_name',  &
    'sea_surface_wave_ratio_of_horizontal_to_vertical_displacement_variance')
    iret=nf90_put_att(ncido,outvarid(17),'units','-')
    iret=nf90_put_att(ncido,outvarid(17),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(17),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(17),'valid_min',0.)
    iret=nf90_put_att(ncido,outvarid(17),'valid_max',10000.)
    iret=nf90_put_att(ncido,outvarid(17),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(17),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(17),'associates','time station frequency')
  END IF

!d
  IF (flagdpt) THEN
    iret=nf90_def_var(ncido, 'dpt', NF90_FLOAT, (/ outdimid(2),outdimid(1) /), outvarid(15))
  !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(15), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(15),'long_name','depth')
    iret=nf90_put_att(ncido,outvarid(15),'standard_name','depth')
  !  iret=nf90_put_att(ncido,outvarid(15),'globwave_name','depth')
    iret=nf90_put_att(ncido,outvarid(15),'units','m')
    iret=nf90_put_att(ncido,outvarid(15),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(15),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(15),'valid_min',-100.)
    iret=nf90_put_att(ncido,outvarid(15),'valid_max',10000.)
    iret=nf90_put_att(ncido,outvarid(15),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(15),'content','TX')
    iret=nf90_put_att(ncido,outvarid(15),'associates','time station')
  END IF

  ! nf90_enddef function
  iret=nf90_enddef(ncido)
  CALL CHECK_ERROR(IRET,__LINE__)


!-------------------------------------------------------------------------------------------------!
!
! 5. Put values in NetCDF output file
!
  allocate(station(np))
  DO i=1,np
    station(i)=i
  END DO
  allocate(string16(16))
  string16(:)=0
!time
  iret=nf90_put_var(ncido,outvarid(1),moddate(:),start=(/1/),count=(/ntime/))
  CALL CHECK_ERROR(IRET,__LINE__)
!station
  iret=nf90_put_var(ncido,outvarid(2),station(:),start=(/1/),count=(/np/))
  CALL CHECK_ERROR(IRET,__LINE__)
!string16
  iret=nf90_put_var(ncido,outvarid(3),string16,start=(/1/),count=(/16/))
  CALL CHECK_ERROR(IRET,__LINE__)
!station_name
  iret=nf90_put_var(ncido,outvarid(4),station_id(:),start=(/1,1/),count=(/LEN_TRIM(station_id(1)),np/))
!  iret=nf90_put_var(ncido,outvarid(4),PTNME(J),start=(/1,J1/),count=(/LEN_TRIM(PTNME(J)),1/))
  CALL CHECK_ERROR(IRET,__LINE__)
!longitude
  allocate(lons(np,ntime))
  DO i=1,np
    lons(i,:)=lon(i)
  END DO
  iret=nf90_put_var(ncido,outvarid(5),lons(:,:),start=(/1,1/),count=(/np,ntime/))
  CALL CHECK_ERROR(IRET,__LINE__)
!latitude
  allocate(lats(np,ntime))
  DO i=1,np
    lats(i,:)=lat(i)
  END DO
  iret=nf90_put_var(ncido,outvarid(6),lats(:,:),start=(/1,1/),count=(/np,ntime/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency
  iret=nf90_put_var(ncido,outvarid(7),freq(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency1
  iret=nf90_put_var(ncido,outvarid(8),freq1(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency2
  iret=nf90_put_var(ncido,outvarid(9),freq2(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!Ef
  iret=nf90_put_var(ncido,outvarid(10),Ef(:,:,:),start=(/1,1,1/),count=(/nfband,np,ntime/))
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (npar.GE.3) THEN
!th1m
    iret=nf90_put_var(ncido,outvarid(11),th1(:,:,:)/d2r,start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
!sth1m
    iret=nf90_put_var(ncido,outvarid(12),sth1(:,:,:),start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
  IF (npar.EQ.5) THEN
!th2m
    iret=nf90_put_var(ncido,outvarid(13),th2(:,:,:)/d2r,start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
!sth2m
    iret=nf90_put_var(ncido,outvarid(14),sth2(:,:,:),start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
!depth
  IF (flagdpt) THEN
    allocate(depth(np,ntime))
    DO i=1,np
      depth(i,:)=dpt(i)
    END DO
    iret=nf90_put_var(ncido,outvarid(15),depth(:,:),start=(/1,1/),count=(/np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
  IF (npar.EQ.7) THEN
!check_ratio
    iret=nf90_put_var(ncido,outvarid(16),check_ratio,start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
!ndof
    iret=nf90_put_var(ncido,outvarid(17),ndof,start=(/1,1,1/),count=(/nfband,np,ntime/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF

  iret=nf90_close(ncido) 
  CALL CHECK_ERROR(IRET,__LINE__)

  deallocate(Ef)
  if (allocated(th1))  deallocate(th1)
  if (allocated(sth1)) deallocate(sth1)
  if (allocated(th2))  deallocate(th2)
  if (allocated(sth2)) deallocate(sth2)

  WRITE(6,*) '*** END OF PROGRAM ***'

END PROGRAM buoy_change_freq_nc


!==============================================================================

      SUBROUTINE CHECK_ERROR(IRET, ILINE)

      USE NETCDF
      USE W3SERVMD, ONLY: EXTCDE

      IMPLICIT NONE

      INTEGER IRET, ILINE, NDSE


      NDSE=6

      IF (IRET .NE. NF90_NOERR) THEN
        WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN INTERP :'
        WRITE(NDSE,*) ' LINE NUMBER ', ILINE
        WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
        WRITE(NDSE,*) NF90_STRERROR(IRET)
        CALL EXTCDE ( 59 )
      END IF
      RETURN

      END SUBROUTINE CHECK_ERROR

!==============================================================================
