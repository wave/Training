!*********************************************	
! Program mem_deep_raw: Adaptation of Bill O'Reilly MEM estimator for 
! reading time-series of frequency spectra and directional moments and 
! generating f-dir spectra with a given frequency grid (in input file crest.inp) 
! and direction resolution. These spectra are also back-refracted to deep 
! water using Snel's law (all this is hard-wired in the main routine at the end 
! of the present file). 
!*********************************************	
	subroutine mem_est(nf,nabin,e,ec,es,ec2,es2,ds)
!    mem_est.f
!  ** THIS version really reads in normailzed four. coeff.,s
!   so no division by e
!    subroutine to make MEM estimate of the directional spectrum
!    in 33 .01hz frequency bands
!    Input:   energy and 4 directional moments from a slope array
! (e,ec,es..) in trig coord frame
!    Output: Directional spectrum in 3 degree directional bins (in
!     (ds)   the "compass heading" coordinate frame) for
!            the 33 fbands between .04 and .25hz
!            
!  If nabin=120 directional bands,  1=3 degress....120=360 degrees
!            where the direction is the compass heading from which
!            the wave energy is arriving. e.g. 270=from west, 180=
!            from south  etc.
!*********************************************	
   IMPLICIT NONE
	INTEGER             , intent(in)  :: nf,nabin
   REAL, dimension(:)  , intent(in)  :: e,ec,es,ec2,es2
   REAL, dimension(:,:), intent(inout) :: ds
   
   REAL, dimension(360) :: d 
   REAL a1,b1,a2,b2,chk
   INTEGER I,J,Kbin
! loop thru freq bands
	Kbin=360/nabin
   do i=1,nf
! moments are first four fourier coeff. of the directional
!    distribution, MEM uses normalized values
!		a1=ec(i)
!		b1=es(i)
!		a2=ec2(i)
!		b2=es2(i)
		IF (e(i).EQ.0.) THEN 
 			ds(:,i)=0.
		ELSE
		   a1=ec(i)/e(i)
		   b1=es(i)/e(i)
		   a2=ec2(i)/e(i)
		   b2=es2(i)/e(i)
         CALL mem(a1,a2,b1,b2,d,chk)
! merge into Kbin degrees directional bins
         DO j=Kbin,360-Kbin,Kbin
            ds(j/Kbin,i)=e(i)*(SUM(d(j-kbin/2:j+kbin/2)))
         ENDDO
         ds(nabin,i)=e(i)*(SUM(d(360-kbin/2:360))+SUM(d(1:kbin/2)))
      ENDIF
   ENDDO
	return
	end

!*********************************************	
	subroutine  mem(a1,a2,b1,b2,s,chk)
!	Maximum entropy method (MEM) for the estimation of a directional
!     distribution from pitch-and-roll data.
!     (Lygre and Krogstad, JPO v16 1986: NOTE - there is a typo in the
!     paper...BOTH exponetials in eq. 13 should have negative signs.
!     This is fixed in Krogstad, 1991- THH has the book)
!	variables:
!            a1,b1,b1,b2  fourier coef. of dir. dist.
!            (normalized ala Long [1980]) - TRIG COORDINATES
!  
!            s= MEM estimate od directional spectrum, 1 deg.
!               resolution - COMPASS COORDINATES
!     chk = check factor: MEM likes to make narrow spikes for
!           directional spectra if it can (it fits the a1,a2,
!           b1 abd b2's exactly).  If it does this, then the
!           .1 deg stepsize for making the 1 deg resolution
!           estimate is too coarse.  The check factor should
!           be close to 1.
!*********************************************	
	dimension s(360)
	complex c1,c2,p1,p2,e1,e2,x,y  
  integer i
	data dr/0.0174533/

	do i=1,360
	 s(i)=0.
	end do
! switch to Lygre & Krogstad notation
	d1=a1
	d2=b1
	d3=a2
	d4=b2
	c1=(1.,0)*d1+(0,1.)*d2
	c2=(1.,0)*d3+(0,1.)*d4
	p1=(c1-c2*conjg(c1))/(1.-cabs(c1)**2)
	p2=c2-c1*p1
	x=1.-p1*conjg(c1)-p2*conjg(c2) 
! sum in .1 deg steps, get distribution with 1 degree resolution
	tot=0
	nn=0
	do 100 n=5,3604
		a=real(n)*.1*dr
		e1=(1.,0)*cos(a)-(0,1.)*sin(a)
		e2=(1.,0)*cos(2*a)-(0,1.)*sin(2*a)
		y=cabs((1.,0)-p1*e1-p2*e2)**2
! put in proper 1 deg directional band
	       ndir=1+nn/10
! switch from trig to compass corrdinates
		ndir=270-ndir
! switch from "direction from" to "direction to" (this is WW3 output convention in spc files...)
!		ndir=180-ndir
		if(ndir.gt.360) ndir=ndir-360
		if(ndir.lt.1) ndir=ndir+360
! normalize by 360/(step size)
		s(ndir)=s(ndir)+cabs(x/y)/3600.
		tot=tot+cabs(x/y)
	       nn=nn+1
100	continue
! tot should = 3600.  If directional peak is extremely narrow then
!    1 deg resolution may be insufficient and tot .ne. 360
       chk=tot/3600.
	return
	end

!==============================================================================
!==============================================================================
MODULE c_caldat

CONTAINS

!==============================================================================

INTEGER FUNCTION julday(id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!

   IMPLICIT NONE
   INTEGER(KIND=4),    INTENT(in)  :: id,mm,iyyy
   INTEGER(KIND=4), PARAMETER :: IGREG=15+31*(10+12*1582)
   INTEGER(KIND=4) ja,jm,jy
   jy=iyyy
   IF (jy.EQ.0) WRITE(6,*) 'There is no zero year !!'
   IF (jy.LT.0) jy=jy+1
   IF (mm.GT.2) THEN
     jm=mm+1
   ELSE
     jy=jy-1
     jm=mm+13
     ENDIF
   julday=INT(365.25*jy)+int(30.6001*jm)+id+1720995
   IF (id+31*(mm+12*iyyy).GE.IGREG) THEN
     ja=INT(0.01*jy)
     julday=julday+2-ja+INT(0.25*ja)
     ENDIF
   RETURN
END FUNCTION JULDAY

!==============================================================================

SUBROUTINE caldat(julian,id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!

IMPLICIT NONE

   INTEGER(KIND=4), INTENT(in)  :: julian
   INTEGER(KIND=4), INTENT(out) :: id,mm,iyyy
   INTEGER(KIND=4), PARAMETER   :: IGREG=2299161
   INTEGER(KIND=4)              :: ja,jalpha,jb,jc,jd,je

   if (julian.GE.IGREG) THEN
      jalpha=INT(((julian-1867216)-0.25)/36524.25)
      ja=julian+1+jalpha-INT(0.25*jalpha)
   ELSE
      ja=julian
   ENDIF
   jb=ja+1524
   jc=INT(6680.+((jb-2439870)-122.1)/365.25)
   jd=365*jc+INT(0.25*jc)
   je=INT((jb-jd)/30.6001)
   id=jb-jd-INT(30.6001*je)
   mm=je-1
   IF (mm.GT.12) mm=mm-12
   iyyy=jc-4715
   IF (mm.GT.2) iyyy=iyyy-1
   IF (iyyy.LE.0) iyyy=iyyy-1
   RETURN
END SUBROUTINE CALDAT

END MODULE c_caldat

!==============================================================================
!==============================================================================

MODULE W3SERVMD
  PUBLIC
!
  INTEGER, PRIVATE        :: NDSTRC = 6, NTRACE = 0, PRFTB
  LOGICAL, PRIVATE        :: FLPROF = .FALSE.
!
CONTAINS

!==============================================================================

SUBROUTINE STRSPLIT(STRING,TAB,cnt)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |          M. Accensi               |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         29-Apr-2013 !
!/                  +-----------------------------------+
!/
!/    29-Mar-2013 : Origination.                        ( version 4.10 )
!/
!  1. Purpose :
!
!     Splits string into words
!
!  2. Method :
!
!     finds spaces and loops
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       STRING   Str   O   String to be splitted
!       TAB      Str   O   Array of strings
!     ----------------------------------------------------------------
!

      IMPLICIT NONE



      CHARACTER, intent(IN)                :: STRING*1024
      CHARACTER, intent(INOUT)             :: TAB(*)*1024
      INTEGER, intent(OUT), optional       :: cnt
      INTEGER                              :: I
      CHARACTER                            :: tmp_str*1024, ori_str*1024

! initializes arrays
      ori_str=ADJUSTL(TRIM(STRING))
      tmp_str=ori_str
      cnt=0

! counts the number of substrings
      DO WHILE ((INDEX(tmp_str,' ').NE.0) .AND. (len_trim(tmp_str).NE.0))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        cnt=cnt+1
        ENDDO
!
! reinitializes arrays
!
      tmp_str=ori_str

! loops on each substring
      DO I=1,cnt
        TAB(I)=tmp_str(:INDEX(tmp_str,' '))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        END DO

      RETURN

END SUBROUTINE STRSPLIT

!==============================================================================

SUBROUTINE NEXTLN ( CHCKC , NDSI , NDSE )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         18-Nov-1999 |
!/                  +-----------------------------------+
!/
!/    15-Jan-1999 : Final FORTRAN 77                    ( version 1.18 )
!/    18-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Sets file pointer to next active line of input file, by skipping
!     lines starting with the character CHCKC.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       CHCKC   C*1   I  Check character for defining comment line.
!       NDSI    Int.  I  Input dataset number.
!       NDSE    Int.  I  Error output dataset number.
!                        (No output if NDSE < 0).
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!       STRACE ( !/S switch )
!
!  5. Called by :
!
!       Any routine.
!
!  6. Error messages :
!
!     - On EOF or error in input file.
!
!  9. Switches :
!
!     !/S  Enable subroutine tracing.
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: NDSI, NDSE
      CHARACTER, INTENT(IN)   :: CHCKC*1
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
!/S      INTEGER, SAVE           :: IENT = 0
      INTEGER                 :: IERR
      CHARACTER               :: TEST*1
!/
!/ ------------------------------------------------------------------- /
!/
!/S      CALL STRACE (IENT, 'NEXTLN')
!
  100 CONTINUE
      READ (NDSI,900,END=800,ERR=801,IOSTAT=IERR) TEST
      IF (TEST.EQ.CHCKC) THEN
          GOTO 100
        ELSE
          BACKSPACE (NDSI,ERR=802,IOSTAT=IERR)
        END IF
      RETURN
!
  800 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,910)
      CALL EXTCDE ( 1 )
!
  801 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 2 )
!
  802 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 3 )
!
! Formats
!
  900 FORMAT (A)
  910 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     PREMATURE END OF INPUT FILE'/)
  911 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR IN READING FROM FILE'/               &
               '     IOSTAT =',I5/)
  912 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR ON BACKSPACE'/                       &
               '     IOSTAT =',I5/)

END SUBROUTINE NEXTLN

!==============================================================================

SUBROUTINE EXTCDE ( IEXIT )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         06-Jan-1999 |
!/                  +-----------------------------------+
!/
!/    06-Jan-1998 : Final FORTRAN 77                    ( version 1.18 )
!/    23-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Perform a program stop with an exit code.
!
!  2. Method :
!
!     Machine dependent.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IEXIT   Int.   I   Exit code to be used.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!  5. Called by :
!
!     Any.
!
!  9. Switches :
!
!     !/MPI  MPI finalize interface if active
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
IMPLICIT NONE
!
!/MPI      INCLUDE "mpif.h"
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: IEXIT
!/
!/ ------------------------------------------------------------------- /
!/
!/MPI      INTEGER                 :: IERR_MPI
!/MPI      LOGICAL                 :: RUN
!/
!/ Test if MPI needs to be closed
!/
!/MPI      CALL MPI_INITIALIZED ( RUN, IERR_MPI )
!/MPI      IF ( RUN ) THEN
!/MPI          CALL MPI_BARRIER ( MPI_COMM_WORLD, IERR_MPI )
!/MPI          CALL MPI_FINALIZE (IERR_MPI )
!/MPI        END IF
!
!/F90      CALL EXIT ( IEXIT )
!/DUM      STOP
!/
!/ End of EXTCDE ----------------------------------------------------- /
!/
END SUBROUTINE EXTCDE

END MODULE W3SERVMD

!==============================================================================
!==============================================================================


      PROGRAM buoy_average_time_nc

!/                  +-----------------------------------+
!/                  | WAVEWATCH III           IFREMER   |
!/                  |           F. Ardhuin              |
!/                  |           M. Accensi              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         10-Jun-2021 |
!/                  +-----------------------------------+
! 
!  Creation  : 11-Apr-2016
!
!  1. Purpose :
!
!     Performs a time average of frequency spectra and directional moments
!     at the same time estimates the directional spectrum using MEM
!
!     NB : no use of bulk file for now
!
!
!  2. Method :
!
!
!
!
! 3. Parameters :
!

  USE C_CALDAT
  USE W3SERVMD
  USE NETCDF
  IMPLICIT NONE

  CHARACTER*1024                   :: input_file, output_file
  CHARACTER*1024                   :: varlist
  INTEGER                          :: ncid, ncido, nctype, ndims, nvars, natts
  INTEGER                          :: NDSI, NDSE, NDSO, varidtmp
  CHARACTER*1                      :: COMSTR
  INTEGER, ALLOCATABLE             :: dimid(:), dimln(:), outdimid(:)
  CHARACTER*128, ALLOCATABLE       :: paranamefreq(:), outvarstr(:)

   INTEGER     I,J,is,irec,Icut, ntime, itime, ntimeout,itimeout
 !  INTEGER     nparbo
   INTEGER     np,nfband,npar,nabin
   INTEGER     iyc,imc,idc,ihc,iminc,iminlast,ihlast
   INTEGER(KIND=4) Jdayc
  REAL,    PARAMETER :: pi = ATAN(1.)*4 ,tpi=2*pi,d2r=pi/180
!   REAL   , DIMENSION(:,:),     ALLOCATABLE :: ds,ds2
   REAL   , DIMENSION(:),     ALLOCATABLE :: freq,theta,freq1,freq2,df
   REAL, DIMENSION(:,:,:), ALLOCATABLE :: Ef,a1,a2,b1,b2,m1,m2,th1,sth1,th2,sth2
   REAL, DIMENSION(:,:,:), ALLOCATABLE :: Efa,a1a,a2a,b1a,b2a,m1a,th1a,th2a,sth1a,sth2a
   INTEGER  iyo,imo,ido,iho,imino
   INTEGER(KIND=4)   Jdaylast
   
!   REAL     DENOM, EH, EL, XH, XH2, XL, XL2, FAC1
!   INTEGER  ICEN, IHGH, ILOW
   REAL fcut,fcutw,missingE,missingU,g
!   REAL fp,emax,m1atot               !peak frequency
   REAL dt,tavg,m2a
   REAL Timezone_data,w,w2,wE
!   REAL sth1p,th1p,th2p,sth2p,Curr,Currdir,th1tot,sth1tot,Etot,a1tot,b1tot

   CHARACTER space

  CHARACTER*128                    :: funits(3), flongname(3), fstdname(3)          
  CHARACTER*128, ALLOCATABLE       :: punits(:), plongname(:), pstdname(:)
  REAL                             :: ffillval(3), fscalefac(3),foffset(3)
  REAL                             :: fvalidmin(3), fvalidmax(3)
  REAL, ALLOCATABLE                :: pfillval(:), pscalefac(:), poffset(:)
  REAL, ALLOCATABLE                :: pvalidmin(:), pvalidmax(:)
  INTEGER                          :: modyref, modmref, moddref
  INTEGER                          :: modhhref, modmmref, modssref
  CHARACTER*128                    :: modtimeunits
  DOUBLE PRECISION, ALLOCATABLE    :: modtime(:), moddate(:), timeout(:)
  DOUBLE PRECISION                 :: moddateref, outdateref
  REAL                             :: fracday, nb_days, nb_mins
  REAL, ALLOCATABLE                :: lons(:,:), lats(:,:), depth(:,:)
  INTEGER                          :: iret, deflate=1
  REAL                             :: hcur,mncur, quotient
  INTEGER, ALLOCATABLE             :: station(:), string16(:)
  CHARACTER*25, ALLOCATABLE        :: station_id(:)
  INTEGER                          :: outvarid(20)
  LOGICAL                          :: flagdpt


   interface
   	subroutine mem_est(nf,nabin,e,ec,es,ec2,es2,ds)
      INTEGER             , intent(in)  :: nf,nabin
      REAL, dimension(:)  , intent(in)  :: e,ec,es,ec2,es2
      REAL, dimension(:,:), intent(inout) :: ds
      end subroutine mem_est
   end interface

   Timezone_data=0.


!
! 1.a  IO set-up, set constants
!
  NDSI=10
  NDSE=6
  NDSO=6
  flagdpt=.FALSE.

!
! 1.b  First guess
!
   !this is a guess, the true values are read from the mem file
   dt=10.
   g=9.81
   tavg=180.
   missingE=999.
   missingU=99.
   fcut=0.1
!   Curr=0.
!   Currdir=0.
   nabin=24
   space=' '
   ALLOCATE(THETA(0:nabin-1))
   THETA= (/ (pi*(0.5-2.*FLOAT(I)/FLOAT(nabin)), i=0,nabin-1) /) 

   !!!!!!!!!! WARNING THIS PROGRAM WORKS ONLY WHEN THE AVERAGING TIME IS 1 hour
   !!!!!!!!!! AND THE DATA IS SUPPOSED TO BE SAMPLED AT LEAST ONCE AN HOUR
   !!!!!!!!!! THIS PROGRAM WILL FILL IN WITH ZEROS THE HOURS WITHOUT ANY DATA
   !!!!!!!!!! THIS NEED TO BE FIXED IN ORDER TO PROCESS THE FRF 8M F-theta SPECTRA   
!
! 1.c  Read input parameters from file
!

  OPEN(NDSI,file='buoy_freq_time_nc.inp')
  READ (NDSI,'(A)') COMSTR
  IF (COMSTR.EQ.' ') COMSTR = '$'
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*    ) nctype
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) ! name of input file for 1st program not needed
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) input_file
  WRITE(6,*) '#',input_file
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) output_file
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) np
  WRITE(6,*) '# number of stations: ',np
  allocate(station_id(np))
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  DO i=1,np
    READ(NDSI,*) station_id(i)
    WRITE(6,*) '#',station_id(i)
    CALL NEXTLN(COMSTR, NDSI, NDSE)
  END DO
  READ(NDSI,*) ! name of the frequency variable in buoy file
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) npar ! number of parameters to process
  allocate(paranamefreq(npar))
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  DO i=1,npar
    READ(NDSI,*) !paranamefreq(i)
    CALL NEXTLN(COMSTR, NDSI, NDSE)
  END DO
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,'(A)') varlist
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) ! output frequencies for 1st program not needed
  CALL NEXTLN(COMSTR, NDSI, NDSE)
  READ(NDSI,*) tavg ! average time in minutes

  CLOSE(NDSI)

  paranamefreq(1)='ef'
  IF (npar.GE.3) THEN
    paranamefreq(2)='th1m'
    paranamefreq(3)='sth1m'
  END IF
  IF (npar.EQ.5) THEN
    paranamefreq(4)='th2m'
    paranamefreq(5)='sth2m'
  END IF


!
! 2.  Read data from NetCDF input file (buoy file)
!

  ! open file if exists, otherwise exit
  WRITE(NDSO,'(/A)') 'Open input file : '
  WRITE(NDSO,'(A)') input_file
  iret = nf90_open(input_file, NF90_NOWRITE, ncid)
  IF(iret.NE.0) THEN
    WRITE(NDSO,'(2A)') 'ERROR : no file : ', trim(input_file)
    STOP
  END IF

  iret=nf90_inquire(ncid, ndims, nvars, natts)
  CALL CHECK_ERROR(IRET,__LINE__)

  allocate(dimid(ndims))
  allocate(dimln(ndims))

!
! Get dimensions
!
  WRITE(6,*) 'Get dimensions'
  iret = nf90_inq_dimid(ncid, 'time', dimid(1))
  iret = nf90_Inquire_Dimension(ncid, dimid(1), len = dimln(1))
  ntime=dimln(1)
  iret = nf90_inq_dimid(ncid, 'station', dimid(2))
  iret = nf90_Inquire_Dimension(ncid, dimid(2), len = dimln(2))
  np=dimln(2)
  iret = nf90_inq_dimid(ncid, 'string16', dimid(3))
  iret = nf90_inq_dimid(ncid, 'frequency', dimid(4))
  iret = nf90_Inquire_Dimension(ncid, dimid(4), len = dimln(3))
  nfband=dimln(3)

!
! Get variables
!
  WRITE(6,*) 'Get variables:'

!time
  WRITE(6,*) 'time'
  iret=nf90_inq_varid(ncid,"time",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(6,'(A)') '[ERROR] no time variable in output file'
    STOP
  END IF

  IF(ALLOCATED(modtime)) deallocate(modtime)
  allocate(modtime(ntime))
  IF(ALLOCATED(moddate)) deallocate(moddate)
  allocate(moddate(ntime))

  ! get input variable time
  iret=nf90_get_var(ncid,varidtmp,modtime)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! get attributes
  iret=nf90_get_att(ncid,varidtmp,'units',modtimeunits)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! get input reference time
  IF (INDEX(modtimeunits, "seconds").NE.0) THEN
    read(modtimeunits(15:18),'(I4.4)') modyref
    read(modtimeunits(20:21),'(I2.2)') modmref
    read(modtimeunits(23:24),'(I2.2)') moddref
    read(modtimeunits(26:27),'(I2.2)') modhhref
    read(modtimeunits(29:30),'(I2.2)') modmmref
    read(modtimeunits(32:33),'(I2.2)') modssref
  ELSE IF (INDEX(modtimeunits, "days").NE.0) THEN
    read(modtimeunits(12:15),'(I4.4)') modyref
    read(modtimeunits(17:18),'(I2.2)') modmref
    read(modtimeunits(20:21),'(I2.2)') moddref
    read(modtimeunits(23:24),'(I2.2)') modhhref
    read(modtimeunits(26:27),'(I2.2)') modmmref
    read(modtimeunits(29:30),'(I2.2)') modssref
  ELSE IF (index(modtimeunits, "hours").NE.0) THEN
    read(modtimeunits(13:16),'(I4.4)') modyref
    read(modtimeunits(18:19),'(I2.2)') modmref
    read(modtimeunits(21:22),'(I2.2)') moddref
    read(modtimeunits(24:25),'(I2.2)') modhhref
    read(modtimeunits(27:28),'(I2.2)') modmmref
    read(modtimeunits(30:31),'(I2.2)') modssref
  END IF
  WRITE(6,*) "modtimeunits : ", modyref, modmref, moddref, modhhref, modmmref, modssref

  ! moddateref in julian date (nb of days between 01-01-(-4712) and dateref)
  moddateref=julday(moddref,modmref,modyref)

  ! moddate in days since moddateref
  IF (index(modtimeunits, "seconds").NE.0) THEN
    moddate(:)=modtime(:)/86400.+ moddateref
  ELSE IF (index(modtimeunits, "hours").NE.0) THEN
    moddate(:)=modtime(:)/24.+ moddateref
  ELSE IF (index(modtimeunits, "days").NE.0) THEN
    moddate(:)=modtime(:)+ moddateref
  END IF

!------------------------------------------------------------------------------------!

!station
  WRITE(6,*) 'station'
  allocate(station(np))
  iret=nf90_inq_varid(ncid,'station',varidtmp)
  iret=nf90_get_var(ncid,varidtmp,station)

! string16
  WRITE(6,*) 'string16'
  allocate(string16(16))
  iret=nf90_inq_varid(ncid,'string16',varidtmp)
  iret=nf90_get_var(ncid,varidtmp,string16)

! station_name: already stored in station_id

!------------------------------------------------------------------------------------!
  ! Get longitude, latitude & depth
  allocate(lons(np,ntime))
  allocate(lats(np,ntime))
  allocate(depth(np,ntime))
  WRITE(6,*) 'longitude'
  iret=nf90_inq_varid(ncid,"longitude",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"lon",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Longitude",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[WARNING] no longitude variable found in input file'
  ELSE
    iret=nf90_get_var(ncid,varidtmp,lons)
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF

  WRITE(6,*) 'latitude'
  iret=nf90_inq_varid(ncid,"latitude",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"lat",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Latitude",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[WARNING] no latitude variable found in input file'
  ELSE
    iret=nf90_get_var(ncid,varidtmp,lats)
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF

  WRITE(6,*) 'dpt'
  iret=nf90_inq_varid(ncid,"dpt",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"depth",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"Depth",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    write(*,*) '[INFO] no dpt variable found in input file'
    flagdpt=.FALSE.
  ELSE
    iret=nf90_get_var(ncid,varidtmp,depth)
    CALL CHECK_ERROR(IRET,__LINE__)
    flagdpt=.TRUE.
  END IF

!------------------------------------------------------------------------------------!
  ! Get frequency, frequency1, frequency2
  WRITE(6,*) 'frequency'
  iret=nf90_inq_varid(ncid,"frequency",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"freq",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(6,*) '[WARNING] no variable frequency in output file'
  ELSE
    ALLOCATE(freq(nfband))
    iret=nf90_get_var(ncid,varidtmp,freq)
    CALL CHECK_ERROR(IRET,__LINE__)
    ! get attributes 
    iret=nf90_get_att(ncid,varidtmp,'long_name',flongname(1))
    iret=nf90_get_att(ncid,varidtmp,'standard_name',fstdname(1))
    iret=nf90_get_att(ncid,varidtmp,'units',funits(1))
    if (iret.NE.0) funits(1)='unknown'
    iret=nf90_get_att(ncid,varidtmp,'scale_factor',fscalefac(1))
    iret=nf90_get_att(ncid,varidtmp,'add_offset',foffset(1))
    iret=nf90_get_att(ncid,varidtmp,'valid_min',fvalidmin(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'valid_max',fvalidmax(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'_FillValue',ffillval(1))
  END IF

  WRITE(6,*) 'frequency1'
  iret=nf90_inq_varid(ncid,"frequency1",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"freq1",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(6,*) '[WARNING] no variable frequency1 in output file'
  ELSE
    ALLOCATE(freq1(nfband))
    iret=nf90_get_var(ncid,varidtmp,freq1)
    CALL CHECK_ERROR(IRET,__LINE__)
    ! get attributes 
    iret=nf90_get_att(ncid,varidtmp,'long_name',flongname(2))
    iret=nf90_get_att(ncid,varidtmp,'standard_name',fstdname(2))
    iret=nf90_get_att(ncid,varidtmp,'units',funits(2))
    if (iret.NE.0) funits(2)='unknown'
    iret=nf90_get_att(ncid,varidtmp,'scale_factor',fscalefac(2))
    iret=nf90_get_att(ncid,varidtmp,'add_offset',foffset(2))
    iret=nf90_get_att(ncid,varidtmp,'valid_min',fvalidmin(2))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'valid_max',fvalidmax(2))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'_FillValue',ffillval(2))
  END IF

  WRITE(6,*) 'frequency2'
  iret=nf90_inq_varid(ncid,"frequency2",varidtmp)
  IF ( iret/=nf90_noerr ) iret=nf90_inq_varid(ncid,"freq2",varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(6,*) '[WARNING] no variable frequency2 in output file'
  ELSE
    ALLOCATE(freq2(nfband))
    iret=nf90_get_var(ncid,varidtmp,freq2)
    CALL CHECK_ERROR(IRET,__LINE__)
    ! get attributes 
    iret=nf90_get_att(ncid,varidtmp,'long_name',flongname(3))
    iret=nf90_get_att(ncid,varidtmp,'standard_name',fstdname(3))
    iret=nf90_get_att(ncid,varidtmp,'units',funits(3))
    if (iret.NE.0) funits(3)='unknown'
    iret=nf90_get_att(ncid,varidtmp,'scale_factor',fscalefac(3))
    iret=nf90_get_att(ncid,varidtmp,'add_offset',foffset(3))
    iret=nf90_get_att(ncid,varidtmp,'valid_min',fvalidmin(3))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'valid_max',fvalidmax(3))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_att(ncid,varidtmp,'_FillValue',ffillval(3))
  END IF

  ALLOCATE(df(nfband))
  df(:)=freq2(:)-freq1(:)

  ! calculate fcutw (used for Hs10)
  Icut=nfband
  DO J=nfband,1,-1
   IF (freq2(J).GT.fcut) THEN 
      Icut=J
      fcutw=fcut-freq1(J)
   END IF
  END DO

!------------------------------------------------------------------------------------!

  ! Get npar variable values

  allocate(plongname(npar))
  allocate(pstdname(npar))
  allocate(punits(npar))
  allocate(pscalefac(npar))
  allocate(poffset(npar))
  allocate(pvalidmin(npar))
  allocate(pvalidmax(npar))
  allocate(pfillval(npar))
  allocate(outvarstr(npar))

  WRITE(6,*) trim(paranamefreq(1))
  iret=nf90_inq_varid(ncid,paranamefreq(1),varidtmp)
  IF ( iret/=nf90_noerr ) THEN
    write(6,'(3A)') '[ERROR] no ',trim(paranamefreq(1)),' variable in input file'
    STOP
  ELSE
    outvarstr(1)="ef"
    ALLOCATE(Ef(nfband,np,ntime))  
    iret=nf90_get_var(ncid,varidtmp,Ef)
    CALL CHECK_ERROR(IRET,__LINE__)
    ! store attributes 
    iret=nf90_get_att(ncid,varidtmp,'long_name',plongname(1))
    iret=nf90_get_att(ncid,varidtmp,'standard_name',pstdname(1))
    iret=nf90_get_att(ncid,varidtmp,'units',punits(1))
    if (iret.NE.0) punits='unknown' 
    iret=nf90_get_att(ncid,varidtmp,'scale_factor',pscalefac(1))
    iret=nf90_get_att(ncid,varidtmp,'add_offset',poffset(1))
    iret=nf90_get_att(ncid,varidtmp,'valid_min',pvalidmin(1))
    iret=nf90_get_att(ncid,varidtmp,'valid_max',pvalidmax(1))
    iret=nf90_get_att(ncid,varidtmp,'_FillValue',pfillval(1))
    CALL CHECK_ERROR(IRET,__LINE__)

  END IF
  ! get th1, sth1
  IF (npar.GE.3) THEN
    ! th1
    WRITE(6,*) trim(paranamefreq(2))
    outvarstr(2)="th1m"
    iret=nf90_inq_varid(ncid,paranamefreq(2),varidtmp)
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(paranamefreq(2)),' variable in input file'
      STOP
    ELSE
      ALLOCATE(th1(nfband,np,ntime)) 
      iret=nf90_get_var(ncid,varidtmp,th1)
      CALL CHECK_ERROR(IRET,__LINE__)
      ! store attributes 
      iret=nf90_get_att(ncid,varidtmp,'long_name',plongname(2))
      iret=nf90_get_att(ncid,varidtmp,'standard_name',pstdname(2))
      iret=nf90_get_att(ncid,varidtmp,'units',punits(2))
      if (iret.NE.0) punits='unknown' 
      iret=nf90_get_att(ncid,varidtmp,'scale_factor',pscalefac(2))
      iret=nf90_get_att(ncid,varidtmp,'add_offset',poffset(2))
      iret=nf90_get_att(ncid,varidtmp,'valid_min',pvalidmin(2))
      iret=nf90_get_att(ncid,varidtmp,'valid_max',pvalidmax(2))
      iret=nf90_get_att(ncid,varidtmp,'_FillValue',pfillval(2))
      CALL CHECK_ERROR(IRET,__LINE__)

    END IF
    ! sth1
    WRITE(6,*) trim(paranamefreq(3))
    outvarstr(3)="sth1m"
    iret=nf90_inq_varid(ncid,paranamefreq(3),varidtmp)
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(paranamefreq(3)),' variable in input file'
      STOP
    ELSE
      ALLOCATE(sth1(nfband,np,ntime)) 
      iret=nf90_get_var(ncid,varidtmp,sth1)
      CALL CHECK_ERROR(IRET,__LINE__)
      ! store attributes 
      iret=nf90_get_att(ncid,varidtmp,'long_name',plongname(3))
      iret=nf90_get_att(ncid,varidtmp,'standard_name',pstdname(3))
      iret=nf90_get_att(ncid,varidtmp,'units',punits(3))
      if (iret.NE.0) punits='unknown' 
      iret=nf90_get_att(ncid,varidtmp,'scale_factor',pscalefac(3))
      iret=nf90_get_att(ncid,varidtmp,'add_offset',poffset(3))
      iret=nf90_get_att(ncid,varidtmp,'valid_min',pvalidmin(3))
      iret=nf90_get_att(ncid,varidtmp,'valid_max',pvalidmax(3))
      iret=nf90_get_att(ncid,varidtmp,'_FillValue',pfillval(3))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF
  END IF

  IF (npar.EQ.5) THEN
    ! th2
    WRITE(6,*) trim(paranamefreq(4))
    outvarstr(4)="th2m"
    iret=nf90_inq_varid(ncid,paranamefreq(4),varidtmp)
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(paranamefreq(4)),' variable in input file'
      STOP
    ELSE
      ALLOCATE(th2(nfband,np,ntime)) 
      iret=nf90_get_var(ncid,varidtmp,th2)
      CALL CHECK_ERROR(IRET,__LINE__)
      ! store attributes 
      iret=nf90_get_att(ncid,varidtmp,'long_name',plongname(4))
      iret=nf90_get_att(ncid,varidtmp,'standard_name',pstdname(4))
      iret=nf90_get_att(ncid,varidtmp,'units',punits(4))
      if (iret.NE.0) punits='unknown' 
      iret=nf90_get_att(ncid,varidtmp,'scale_factor',pscalefac(4))
      iret=nf90_get_att(ncid,varidtmp,'add_offset',poffset(4))
      iret=nf90_get_att(ncid,varidtmp,'valid_min',pvalidmin(4))
      iret=nf90_get_att(ncid,varidtmp,'valid_max',pvalidmax(4))
      iret=nf90_get_att(ncid,varidtmp,'_FillValue',pfillval(4))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF
    ! sth2
    WRITE(6,*) trim(paranamefreq(5))
    outvarstr(5)="sth2m"
    iret=nf90_inq_varid(ncid,paranamefreq(5),varidtmp)
    IF ( iret/=nf90_noerr ) THEN
      write(6,'(3A)') '[ERROR] no ',trim(paranamefreq(5)),' variable in input file'
      STOP
    ELSE
      ALLOCATE(sth2(nfband,np,ntime)) 
      iret=nf90_get_var(ncid,varidtmp,sth2)
      CALL CHECK_ERROR(IRET,__LINE__)
      ! store attributes 
      iret=nf90_get_att(ncid,varidtmp,'long_name',plongname(5))
      iret=nf90_get_att(ncid,varidtmp,'standard_name',pstdname(5))
      iret=nf90_get_att(ncid,varidtmp,'units',punits(5))
      if (iret.NE.0) punits='unknown' 
      iret=nf90_get_att(ncid,varidtmp,'scale_factor',pscalefac(5))
      iret=nf90_get_att(ncid,varidtmp,'add_offset',poffset(5))
      iret=nf90_get_att(ncid,varidtmp,'valid_min',pvalidmin(5))
      iret=nf90_get_att(ncid,varidtmp,'valid_max',pvalidmax(5))
      iret=nf90_get_att(ncid,varidtmp,'_FillValue',pfillval(5))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF

  END IF

  iret=nf90_close(ncid)

!!!!!!!

!   WRITE(8,'(A24,3I6,A33)') "'OBSERVATIONS         '",nfband,nabin,np, &
!                           "'NDBC                          '"
!   WRITE(8,'(8G10.3)') freq(1:nfband)
!   WRITE(8,'(7G11.3)') theta(0:nabin-1)  !read directions in radians

!!!!!!


!
! 3. Compute coefficients a1, b1, m1...

!  ALLOCATE(ds(nabin,nfband))
!  ALLOCATE(ds2(nabin,nfband))
  ALLOCATE(a1(nfband,np,ntime))
  ALLOCATE(b1(nfband,np,ntime))
  ALLOCATE(m1(nfband,np,ntime))
  ALLOCATE(a2(nfband,np,ntime))
  ALLOCATE(b2(nfband,np,ntime))
  ALLOCATE(m2(nfband,np,ntime))



  IF (npar.GE.3) THEN
    ! compute a1, b1
    a1(:,:,:)=COS(th1(:,:,:)*d2r)
    b1(:,:,:)=SIN(th1(:,:,:)*d2r)

    ! compute m1 based on Kuik et al. (JPO 1988, eq 32):
    ! there used to be a bug !!
    m1(:,:,:)=ABS(1.-0.5*(sth1(:,:,:)*d2r)**2)
    DO itime=1,ntime
      DO is=1,np
        DO I=1,nfband
          IF (m1(I,is,itime).NE.0) THEN 
           a1(I,is,itime)=a1(I,is,itime)*m1(I,is,itime)
           b1(I,is,itime)=b1(I,is,itime)*m1(I,is,itime)
          END IF
        END DO
      END DO
    END DO
  END IF
  IF (npar.GE.5) THEN
    ! compute a2, b2
    a2(:,:,:)=COS(th2(:,:,:)*d2r)
    b2(:,:,:)=SIN(th2(:,:,:)*d2r)
    m2(:,:,:)=ABS(1.-2*(sth2(:,:,:)*d2r)**2)
    DO itime=1,ntime
      DO is=1,np
        DO I=1,nfband
          IF (m2(I,is,itime).NE.0) THEN 
            a2(I,is,itime)=a2(I,is,itime)*m2(I,is,itime)
            b2(I,is,itime)=b2(I,is,itime)*m2(I,is,itime)
          END IF
        END DO
      END DO
    END DO
  ENDIF


!
! 4. Process time averaging
!

  ! How many output time steps?
  ! number of days in the file:
  nb_days=CEILING(moddate(ntime))-FLOOR(moddate(1))
  nb_mins=NINT(nb_days*1440)
  ntimeout=FLOOR(nb_mins/tavg)+1

  ! Allocate averaged variables
  WRITE(6,*) "allocate arrays over ", ntimeout, "time steps"
  ALLOCATE(Efa(nfband,np,ntimeout))
  ALLOCATE(a1a(nfband,np,ntimeout))
  ALLOCATE(b1a(nfband,np,ntimeout))
  ALLOCATE(m1a(nfband,np,ntimeout))
  ALLOCATE(a2a(nfband,np,ntimeout))
  ALLOCATE(b2a(nfband,np,ntimeout))

  ALLOCATE(th1a(nfband,np,ntimeout))
  ALLOCATE(sth1a(nfband,np,ntimeout))
  ALLOCATE(th2a(nfband,np,ntimeout))
  ALLOCATE(sth2a(nfband,np,ntimeout))

  ! Define outdateref for an output in "days since 1990-01-01T00:00:00Z"
  outdateref=julday(1,1,1990)
  WRITE(6,*) 'outdateref :', outdateref

  allocate(timeout(ntimeout))  
  irec=0
  itimeout=1

  ! Loop on time
  WRITE(6,*) "Loop on over ", ntime, " time steps"
  DO itime=1,ntime

    write(6,*) itimeout

    CALL caldat(INT(moddate(itime)),idc,imc,iyc)
    Jdayc=julday(idc,imc,iyc)
    CALL caldat(Jdayc,ido,imo,iyo)
    ! convert fraction of julian day into ihc, iminc
    fracday= moddate(itime) - INT(moddate(itime))
    hcur=fracday*24.
    IF (ABS(hcur-NINT(hcur)).LT.1.E-5) hcur=NINT(hcur)
    ihc=INT(hcur)
    mncur= (hcur- INT(hcur))*60.
    IF (ABS(mncur-NINT(mncur)).LT.1.E-5) mncur=NINT(mncur)
    iminc=INT(mncur)

    ! output time meeting averaging requirements
    quotient= (REAL(ihc*60+iminc))/tavg
    IF (ABS(quotient-NINT(quotient)).LT.1.E-5) quotient=NINT(quotient)
    iho=FLOOR(REAL(INT(quotient)*tavg/60))
    IF (iho.GE.24) THEN 
     CALL caldat(Jdayc+1,ido,imo,iyo)
     iho=iho-24
    END IF 
    imino=0
    irec=irec+1

!
!  Averaging ... 
! 
    IF (irec.EQ.1) THEN 
      w= 60*ihc + iminc
      IF(maxval(Ef(:,:,itime)).NE.missingE) THEN
         Efa(:,:,itimeout)=ef(:,:,itime)*w
         a1a(:,:,itimeout)=a1(:,:,itime)*ef(:,:,itime)*w
         b1a(:,:,itimeout)=b1(:,:,itime)*ef(:,:,itime)*w
         a2a(:,:,itimeout)=a2(:,:,itime)*ef(:,:,itime)*w
         b2a(:,:,itimeout)=b2(:,:,itime)*ef(:,:,itime)*w
         wE=iminc
      ELSE
         Efa(:,:,itimeout)=0.
         a1a(:,:,itimeout)=0.
         b1a(:,:,itimeout)=0.
         a2a(:,:,itimeout)=0.
         b2a(:,:,itimeout)=0.
         wE=0.
      ENDIF

!      ! write timeout in "days since 1990-01-01":
!      timeout(itimeout)= DBLE(julday(ido,imo,iyo))-DBLE(julday(1,1,1900)) &
!                        + DBLE((iho + DBLE(imino/60.)) /24.) &
!                        + outdateref
!      WRITE(6,*) 'itime, itimeout :',itime, itimeout 
!      WRITE(6,*) 'moddate(itime) :', moddate(itime)
!      WRITE(6,*) 'timeout(itimeout) :', timeout(itimeout)
!
!      itimeout=itimeout+1
    
    ELSE ! (irec.NE.1)
!
! determines weight w2 of last record (in minutes)
!
      w2=60*(24*REAL(Jdayc-Jdaylast)+(ihc-ihlast))+iminc-iminlast

      IF (w+w2.LT.tavg) THEN 
        w=w+w2
        IF(maxval(Ef(:,:,itime)).NE.missingE) THEN 
          Efa(:,:,itimeout)=Efa(:,:,itimeout)+ef(:,:,itime)*w2
          a1a(:,:,itimeout)=a1a(:,:,itimeout)+a1(:,:,itime)*ef(:,:,itime)*w2
          b1a(:,:,itimeout)=b1a(:,:,itimeout)+b1(:,:,itime)*ef(:,:,itime)*w2
          a2a(:,:,itimeout)=a2a(:,:,itimeout)+a2(:,:,itime)*ef(:,:,itime)*w2
          b2a(:,:,itimeout)=b2a(:,:,itimeout)+b2(:,:,itime)*ef(:,:,itime)*w2
          wE=wE+w2
        END IF

      ELSE ! (w+w2.GE.tavg)

        IF (w2.GT.tavg) THEN   ! this happens if big gap in data
          w=iminc
          IF(maxval(Ef(:,:,itime)).NE.missingE) THEN
            Efa(:,:,itimeout)=ef(:,:,itime)*w
            a1a(:,:,itimeout)=a1(:,:,itime)*ef(:,:,itime)*w
            b1a(:,:,itimeout)=b1(:,:,itime)*ef(:,:,itime)*w
            a2a(:,:,itimeout)=a2(:,:,itime)*ef(:,:,itime)*w
            b2a(:,:,itimeout)=b2(:,:,itime)*ef(:,:,itime)*w
            wE=iminc
          END IF
        ELSE ! (w2.LE.tavg)
          IF(maxval(Ef(:,:,itime)).NE.missingE) THEN
            wE=wE+(tavg-w)
            Efa(:,:,itimeout)=(Efa(:,:,itimeout)+ef(:,:,itime)*(tavg-w))
            a1a(:,:,itimeout)=(a1a(:,:,itimeout)+a1(:,:,itime)*ef(:,:,itime)*(tavg-w))
            b1a(:,:,itimeout)=(b1a(:,:,itimeout)+b1(:,:,itime)*ef(:,:,itime)*(tavg-w)) 
            a2a(:,:,itimeout)=(a2a(:,:,itimeout)+a2(:,:,itime)*ef(:,:,itime)*(tavg-w))
            b2a(:,:,itimeout)=(b2a(:,:,itimeout)+b2(:,:,itime)*ef(:,:,itime)*(tavg-w))
          END IF
          IF (wE.GT.0) THEN 
            Efa(:,:,itimeout)=Efa(:,:,itimeout)/wE
            a1a(:,:,itimeout)=a1a(:,:,itimeout)/wE
            b1a(:,:,itimeout)=b1a(:,:,itimeout)/wE
            a2a(:,:,itimeout)=a2a(:,:,itimeout)/wE
            b2a(:,:,itimeout)=b2a(:,:,itimeout)/wE
          ENDIF
          th1a(:,:,itimeout)=ATAN2(b1a(:,:,itimeout),a1a(:,:,itimeout))/d2r           
          th2a(:,:,itimeout)=ATAN2(b2a(:,:,itimeout),a2a(:,:,itimeout))/2./d2r 
          
          DO is=1,np
            DO J=1,nfband
              IF (th1a(J,is,itimeout).LT.0) th1a(J,is,itimeout)=th1a(J,is,itimeout)+360.
              IF (th2a(J,is,itimeout).LT.0) th2a(J,is,itimeout)=th2a(J,is,itimeout)+180.
              IF (th2a(J,is,itimeout).GE.180) th2a(J,is,itimeout)=th2a(J,is,itimeout)-180.
              IF (Efa(J,is,itimeout) /= 0) THEN
                m1a(J,is,itimeout)=SQRT(a1a(J,is,itimeout)**2+ &
                                         b1a(J,is,itimeout)**2)/Efa(J,is,itimeout)
                m2a=SQRT(a2a(J,is,itimeout)**2+b2a(J,is,itimeout)**2)/Efa(J,is,itimeout)
                sth1a(J,is,itimeout)=SQRT(ABS(2.*(1-m1a(J,is,itimeout))))/d2r
                sth2a(J,is,itimeout)=SQRT(ABS(0.5*(1-m2a)))/d2r                  
              ELSE
                sth1a(J,is,itimeout)=0.
                sth2a(J,is,itimeout)=0.
              END IF
            END DO
          END DO

          ! write iyo,imo,ido,iho,imino,Timezone_data for itime
          
    ! write timeout in "days since 1990-01-01":
    timeout(itimeout)= DBLE (DBLE(julday(ido,imo,iyo)) &
                       + DBLE((iho + DBLE(imino/60.)) /24.) &
                       - DBLE(outdateref) )


!    WRITE(6,*) 'itime, itimeout :',itime, itimeout 
!    WRITE(6,*) 'moddate(itime) :', moddate(itime)
!    WRITE(6,*) 'timeout(itimeout) :', timeout(itimeout)


!          emax=0.
!          DO J=1,nfband 
!            IF (Efa(J,itime).GT.emax) THEN 
!              Jfp=J
!              emax=Efa(J,itime)
!            ENDIF
!          ENDDO

!          IF (Jfp.NE.nfband) THEN 
!  ! Peak period adjustement, borrowed from WW3 w3iogomd.ftn 
!  ! THERE IS A BUG ... 
!            ILOW   = MAX (  1 , Jfp-1 )
!            ICEN   = MAX (  1 , Jfp   )
!            IHGH   = MIN ( nfband , Jfp+1 )
!
!            XL     = (FREQ(Jfp)-FREQ(ILOW))/FREQ(Jfp)
!            XH     =  (FREQ(IHGH)-FREQ(Jfp))/FREQ(Jfp)
!            XL2    = XL**2
!            XH2    = XH**2

!            EL     = EFa(ILOW,itime) - EFa(ICEN,itime)
!            EH     = EFa(IHGH,itime) - EFa(ICEN,itime)

  ! This gives a few strange values ... 
  !      DENOM  = XL*EH - XH*EL
  !       
  !               fp = FREQ(Jfp) * ( 1. + 0.5 * ( XL2*EH - XH2*EL )     &
  !                       / SIGN ( MAX(ABS(DENOM),1.E-15) , DENOM ) )

!            DENOM  = EH+EL
!            FAC1= 0.5 *(XH+XL) * 0.5*( EH - EL )     &
!                     / SIGN ( MAX(ABS(DENOM),1.E-15) , DENOM )
!            fp = FREQ(Jfp) *    (1 - FAC1)

  ! Before change ... 
            ! fp=freq(Jfp)


!            th1p=th1a(Jfp,itime)
!            IF (th1p.LT.0) th1p=th1p+360. 
!            th2p=th2a(Jfp,itime)
!            IF (th2p.LT.0) th2p=th2p+360. 
!            sth1p=sth1a(Jfp,itime)
!            sth2p=sth2a(Jfp,itime)
!          ELSE ! (Jfp.EQ.nfband)
!            fp=0.
!            th1p=0.
!            th2p=0.
!            sth1p=0.
!            sth2p=0.
!          ENDIF 
             
  !                  WRITE (6,*) 'TESTA:',iyo,imo,ido,iho,Hs

!          Etot=SUM(Efa(1,:,itime)*df(:))
!          a1tot=SUM(a1a(1,:,itime)*df(:))
!          b1tot=SUM(b1a(1,:,itime)*df(:))
!          IF (Etot /= 0) THEN
!            m1atot=SQRT(a1tot**2+b1tot**2)/Etot
!            sth1tot=SQRT(ABS(2.*(1-m1atot)))/d2r
!          ELSE
!            sth1tot=0.
!          ENDIF
!          th1tot=ATAN2(b1tot,a1tot)           
!          IF (th1tot.LT.0.) th1tot=th1tot+tpi      
!          th1tot=th1tot/d2r
!
!          IF (nparbo.EQ.5) THEN 
!            !WRITE (5,'(I4,X,4I3,f6.2,5E16.8)') &
!            !  iyo,imo,ido,iho,imin,Timezone_data, &
!            !  Hs,fp,f02,f0m1,Uss
!          ELSE 
!            IF (nparbo.EQ.7) THEN 
!              !WRITE (5,'(I4,X,4I3,f6.2,7E16.8)') &
!              !iyo,imo,ido,iho,imin,Timezone_data, &
!              !Hs,fp,f02,f0m1,Uss,U10,Udir
!            ELSE
!              IF (nparbo.EQ.10) THEN 
!                !WRITE (5,'(I4,X,4I3,f6.2,11E16.8)') &
!                !iyo,imo,ido,iho,imin,Timezone_data, &
!                !Hs,fp,f02,f0m1,Uss,th1p,sth1p,th1tot,sth1tot,Ussd
!              ELSE
!                IF (nparbo.EQ.11) THEN 
!                  !WRITE (5,'(I4,X,4I3,f6.2,11E16.8)') &
!                  !iyo,imo,ido,iho,imin,Timezone_data, &
!                  !Hs,fp,f02,f0m1,Uss,th1p,sth1p,th1tot,sth1tot,Ussd,U10
!                ELSE
!                  !WRITE (5,'(I4,X,4I3,f6.2,12E16.8)') &
!                  !iyo,imo,ido,iho,imino,Timezone_data, &
!                  !Hs,fp,f02,f0m1,Uss,th1p,sth1p,th1tot,sth1tot,Ussd,U10,Udir
!                END IF
!              END IF
!            ENDIF 
!          ENDIF !(nparbo.EQ.5)
!         !WRITE (9,'(I3,I5,3I3,6E16.8)') &
!             time,iyo,imo,ido,iho,Hs,Hs10,fp,fp10,th1p,sth1p

          ! increment itimeout
          itimeout=itimeout+1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          IF  (npar.EQ.5) THEN 
!
! Performs MEM estimation of directional spectrum
!
            !CALL mem_est(nfband,nabin,Efa(:,itime),a1a(:,itime), &
            !             b1a(:,itime),a2a(:,itime),b2a(:,itime),ds)
            ! this is corrected for the change of angular reference
            !CALL mem_est(nfband,nabin,Efa,-1.*b1a,-1.*a1a,-1.*a2a,b2a,ds)
            !ds2(1,:)=ds(nabin,:)
            !ds2(2:nabin,:)=ds(1:nabin-1,:)
            !WRITE(8,'(I4,2I2,A1,3I2)',iostat=icode) iyo,imo,ido,space,iho,imino,0
            !WRITE(8,'(A1,A8,A3,2F7.2,F10.1,F7.2,F6.1,F7.2,F6.1)') &
            !   "'",buoyname(1),"  '",lat,lon,depth,U10, &
            !   Udir,Curr,Currdir
            !WRITE(8,'(7G11.3)') TRANSPOSE(ds2)*nabin/(2*pi)
          ENDIF

          ! Resets the weight and values to be averaged
          ! new average starts from the last read values
          w=w+w2-tavg

          IF(maxval(Ef(:,:,itime)).NE.missingE) THEN
            Efa(:,:,itimeout)=ef(:,:,itime)*w
            a1a(:,:,itimeout)=a1(:,:,itime)*ef(:,:,itime)*w
            b1a(:,:,itimeout)=b1(:,:,itime)*ef(:,:,itime)*w
            a2a(:,:,itimeout)=a2(:,:,itime)*ef(:,:,itime)*w
            b2a(:,:,itimeout)=b2(:,:,itime)*ef(:,:,itime)*w
            wE=w
          ELSE
            Efa(:,:,itimeout)=0.
            a1a(:,:,itimeout)=0.
            b1a(:,:,itimeout)=0.
            a2a(:,:,itimeout)=0.
            b2a(:,:,itimeout)=0.
            wE=0.
          ENDIF

         
        ENDIF
      ENDIF !w+w2.LT.tavg
    ENDIF !irec.EQ.1


  ! save last day, hour, min
  Jdaylast=Jdayc
  ihlast=ihc
  iminlast=iminc

  END DO ! loop on ntime
  ! reset ntimeout
  ntimeout=itimeout-1




!------------------------------------------------------------------------------------!
!
! 5. Create NetCDF output file
!
  WRITE(6,*) 'Create output netcdf file'


  ! Create output altimeter file 
  allocate(outdimid(4))
  IF(NCTYPE.EQ.3)  iret = nf90_create(output_file, NF90_CLOBBER, ncido)
  IF(NCTYPE.EQ.4)  iret = nf90_create(output_file, NF90_NETCDF4, ncido)
  CALL CHECK_ERROR(IRET,__LINE__)  

  ! Define dimensions
  WRITE(6,*) '...dimensions'
  iret = nf90_def_dim(ncido, 'time', NF90_UNLIMITED, outdimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'station', np, outdimid(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'string16', 16, outdimid(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncido, 'frequency', nfband, outdimid(4)) 
  CALL CHECK_ERROR(IRET,__LINE__)  

  ! Define variables

!  time
  WRITE(6,*) '...variable time'
  iret=nf90_def_var(ncido, 'time', NF90_DOUBLE, outdimid(1), outvarid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(1), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(1),'long_name','julian day (UT)')
  iret=nf90_put_att(ncido,outvarid(1),'standard_name','time')
!  iret=nf90_put_att(ncido,outvarid(1),'units',modtimeunits)
  iret=nf90_put_att(ncido,outvarid(1),'units','days since 1990-01-01 00:00:00')
!  iret=nf90_put_att(ncido,outvarid(1),'_FillValue',NF90_FILL_DOUBLE)
!  iret=nf90_put_att(ncido,outvarid(1),'conventions','Relative julian days with decimal part (as parts of the day)')
!  iret=nf90_put_att(ncido,outvarid(1),'axis','T') 

!  station
  WRITE(6,*) '...variable station'
  iret=nf90_def_var(ncido, 'station', NF90_INT, (/outdimid(2)/), outvarid(2))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(2), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(2),'long_name','station id')
  iret=nf90_put_att(ncido,outvarid(2),'_FillValue',NF90_FILL_INT)
  iret=nf90_put_att(ncido,outvarid(2),'axis','X') 

!  string16
  WRITE(6,*) '...variable station'
  iret=nf90_def_var(ncido, 'string16', NF90_INT, (/outdimid(3)/), outvarid(3))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(3), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(3),'long_name','station_name number of characters')
  iret=nf90_put_att(ncido,outvarid(3),'_FillValue',NF90_FILL_INT)
  iret=nf90_put_att(ncido,outvarid(3),'axis','W') 

!  station_name
  WRITE(6,*) '...variable station_name'
  iret=nf90_def_var(ncido, 'station_name', NF90_CHAR, (/outdimid(3),outdimid(2)/), outvarid(4))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4      IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(4), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(4),'long_name','station name')
  iret=nf90_put_att(ncido,outvarid(4),'content','XW')
  iret=nf90_put_att(ncido,outvarid(4),'associates','station string16')

!  longitude
  WRITE(6,*) '...variable longitude'
  iret=nf90_def_var(ncido, 'longitude', NF90_FLOAT, (/outdimid(2),outdimid(1)/), outvarid(5))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(5), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(5),'long_name','longitude')
  iret=nf90_put_att(ncido,outvarid(5),'standard_name','longitude')
!  iret=nf90_put_att(ncido,outvarid(5),'globwave_name','longitude')
  iret=nf90_put_att(ncido,outvarid(5),'units','degree_east')
  iret=nf90_put_att(ncido,outvarid(5),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(5),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(5),'valid_min',-180.0)
  iret=nf90_put_att(ncido,outvarid(5),'valid_max',360.)
  iret=nf90_put_att(ncido,outvarid(5),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(5),'content','TX')
  iret=nf90_put_att(ncido,outvarid(5),'associates','time station')

!  latitude
  WRITE(6,*) '...variable latitude'
  iret=nf90_def_var(ncido, 'latitude', NF90_FLOAT, (/outdimid(2),outdimid(1)/), outvarid(6))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(6), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(6),'long_name','latitude')
  iret=nf90_put_att(ncido,outvarid(6),'standard_name','latitude')
!  iret=nf90_put_att(ncido,outvarid(6),'globwave_name','latitude')
  iret=nf90_put_att(ncido,outvarid(6),'units','degree_north')
  iret=nf90_put_att(ncido,outvarid(6),'scale_factor',1.)
  iret=nf90_put_att(ncido,outvarid(6),'add_offset',0.)
  iret=nf90_put_att(ncido,outvarid(6),'valid_min',-90.0)
  iret=nf90_put_att(ncido,outvarid(6),'valid_max',180.)
  iret=nf90_put_att(ncido,outvarid(6),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(6),'content','TX')
  iret=nf90_put_att(ncido,outvarid(6),'associates','time station')

!frequency
  WRITE(6,*) '...variable frequency'
  iret=nf90_def_var(ncido, 'frequency', NF90_FLOAT, (/outdimid(4)/), outvarid(7))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(7), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(7),'long_name',flongname(1))
  iret=nf90_put_att(ncido,outvarid(7),'standard_name',fstdname(1))
  iret=nf90_put_att(ncido,outvarid(7),'units',funits(1))
  iret=nf90_put_att(ncido,outvarid(7),'scale_factor',fscalefac(1))
  iret=nf90_put_att(ncido,outvarid(7),'add_offset',foffset(1))
  iret=nf90_put_att(ncido,outvarid(7),'valid_min',fvalidmin(1))
  iret=nf90_put_att(ncido,outvarid(7),'valid_max',fvalidmax(1))
  iret=nf90_put_att(ncido,outvarid(7),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(7),'axis','Y') 

!frequency1
  WRITE(6,*) '...variable frequency1'
  iret=nf90_def_var(ncido, 'frequency1', NF90_FLOAT, (/outdimid(4)/), outvarid(8))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(8), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(8),'long_name',flongname(2))
  iret=nf90_put_att(ncido,outvarid(8),'standard_name',fstdname(2))
  iret=nf90_put_att(ncido,outvarid(8),'units',funits(2))
  iret=nf90_put_att(ncido,outvarid(8),'scale_factor',fscalefac(2))
  iret=nf90_put_att(ncido,outvarid(8),'add_offset',foffset(2))
  iret=nf90_put_att(ncido,outvarid(8),'valid_min',fvalidmin(2))
  iret=nf90_put_att(ncido,outvarid(8),'valid_max',fvalidmax(2))
  iret=nf90_put_att(ncido,outvarid(8),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(8),'content','Y')
  iret=nf90_put_att(ncido,outvarid(8),'associates','frequency')

!frequency2
  WRITE(6,*) '...variable frequency2'
  iret=nf90_def_var(ncido, 'frequency2', NF90_FLOAT, (/outdimid(4)/), outvarid(9))
  CALL CHECK_ERROR(IRET,__LINE__)
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(9), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(9),'long_name',flongname(3))
  iret=nf90_put_att(ncido,outvarid(9),'standard_name',fstdname(3))
  iret=nf90_put_att(ncido,outvarid(9),'units',funits(3))
  iret=nf90_put_att(ncido,outvarid(9),'scale_factor',fscalefac(3))
  iret=nf90_put_att(ncido,outvarid(9),'add_offset',foffset(3))
  iret=nf90_put_att(ncido,outvarid(9),'valid_min',fvalidmin(3))
  iret=nf90_put_att(ncido,outvarid(9),'valid_max',fvalidmax(3))
  iret=nf90_put_att(ncido,outvarid(9),'_FillValue',NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(9),'content','Y')
  iret=nf90_put_att(ncido,outvarid(9),'associates','frequency')

!Ef
  WRITE(6,*) '...variable ef'
  iret=nf90_def_var(ncido, 'ef', NF90_FLOAT,(/outdimid(4),outdimid(2),outdimid(1)/), outvarid(10))
!/NC4       IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(10), 1, 1, deflate)
  iret=nf90_put_att(ncido,outvarid(10),'long_name',plongname(1))
  iret=nf90_put_att(ncido,outvarid(10),'standard_name',pstdname(1))
  iret=nf90_put_att(ncido,outvarid(10),'units',punits(1))
  iret=nf90_put_att(ncido,outvarid(10),'scale_factor',pscalefac(1))
  iret=nf90_put_att(ncido,outvarid(10),'add_offset',poffset(1))
  iret=nf90_put_att(ncido,outvarid(10),'valid_min',pvalidmin(1))
  iret=nf90_put_att(ncido,outvarid(10),'valid_max',pvalidmax(1))
  iret=nf90_put_att(ncido,outvarid(10),'_FillValue', NF90_FILL_FLOAT)
  iret=nf90_put_att(ncido,outvarid(10),'content','TXY')

  IF (npar.GE.3) THEN
!th1m
  WRITE(6,*) '...variable th1m'
    iret=nf90_def_var(ncido, 'th1m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(11))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(11), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(11),'long_name',plongname(2))
    iret=nf90_put_att(ncido,outvarid(11),'standard_name',pstdname(2))
    iret=nf90_put_att(ncido,outvarid(11),'units',punits(2))
    iret=nf90_put_att(ncido,outvarid(11),'scale_factor',pscalefac(2))
    iret=nf90_put_att(ncido,outvarid(11),'add_offset',poffset(2))
    iret=nf90_put_att(ncido,outvarid(11),'valid_min',pvalidmin(2))
    iret=nf90_put_att(ncido,outvarid(11),'valid_max',pvalidmax(2))
    iret=nf90_put_att(ncido,outvarid(11),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(11),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(11),'associates','time station frequency')

!sth1m
  WRITE(6,*) '...variable sth1m'
    iret=nf90_def_var(ncido, 'sth1m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(12))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(12), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(12),'long_name',plongname(3))
    iret=nf90_put_att(ncido,outvarid(12),'standard_name',pstdname(3))
    iret=nf90_put_att(ncido,outvarid(12),'units',punits(3))
    iret=nf90_put_att(ncido,outvarid(12),'scale_factor',pscalefac(3))
    iret=nf90_put_att(ncido,outvarid(12),'add_offset',poffset(3))
    iret=nf90_put_att(ncido,outvarid(12),'valid_min',pvalidmin(3))
    iret=nf90_put_att(ncido,outvarid(12),'valid_max',pvalidmax(3))
    iret=nf90_put_att(ncido,outvarid(12),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(12),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(12),'associates','time station frequency')
  END IF

  IF (npar.EQ.5) THEN
!th2m
  WRITE(6,*) '...variable th2m'
    iret=nf90_def_var(ncido, 'th2m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(13))
    !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(13), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(13),'long_name',plongname(4))
    iret=nf90_put_att(ncido,outvarid(13), 'standard_name',pstdname(4))
    iret=nf90_put_att(ncido,outvarid(13),'units',punits(4))
    iret=nf90_put_att(ncido,outvarid(13),'scale_factor',pscalefac(4))
    iret=nf90_put_att(ncido,outvarid(13),'add_offset',poffset(4))
    iret=nf90_put_att(ncido,outvarid(13),'valid_min',pvalidmin(4))
    iret=nf90_put_att(ncido,outvarid(13),'valid_max',pvalidmax(4))
    iret=nf90_put_att(ncido,outvarid(13),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(13),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(13),'associates','time station frequency')

!sth2m
  WRITE(6,*) '...variable sth2m'
    iret=nf90_def_var(ncido, 'sth2m', NF90_FLOAT, &
                      (/ outdimid(4),outdimid(2),outdimid(1) /), outvarid(14))
!/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(13), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(14),'long_name',plongname(5))
    iret=nf90_put_att(ncido,outvarid(14), 'standard_name',pstdname(5))
    iret=nf90_put_att(ncido,outvarid(14),'units',punits(5))
    iret=nf90_put_att(ncido,outvarid(14),'scale_factor',pscalefac(5))
    iret=nf90_put_att(ncido,outvarid(14),'add_offset',poffset(5))
    iret=nf90_put_att(ncido,outvarid(14),'valid_min',pvalidmin(5))
    iret=nf90_put_att(ncido,outvarid(14),'valid_max',pvalidmax(5))
    iret=nf90_put_att(ncido,outvarid(14),'_FillValue', NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(14),'content','TXY')
    iret=nf90_put_att(ncido,outvarid(14),'associates','time station frequency')
  END IF

!d
  IF (flagdpt) THEN
  WRITE(6,*) '...variable dpt'
    iret=nf90_def_var(ncido, 'dpt', NF90_FLOAT, (/ outdimid(2),dimid(1) /), outvarid(15))
  !/NC4        IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, outvarid(15), 1, 1, deflate)
    iret=nf90_put_att(ncido,outvarid(15),'long_name','depth')
    iret=nf90_put_att(ncido,outvarid(15),'standard_name','depth')
  !  iret=nf90_put_att(ncido,outvarid(15),'globwave_name','depth')
    iret=nf90_put_att(ncido,outvarid(15),'units','m')
    iret=nf90_put_att(ncido,outvarid(15),'scale_factor',1.)
    iret=nf90_put_att(ncido,outvarid(15),'add_offset',0.)
    iret=nf90_put_att(ncido,outvarid(15),'valid_min',-100.)
    iret=nf90_put_att(ncido,outvarid(15),'valid_max',10000.)
    iret=nf90_put_att(ncido,outvarid(15),'_FillValue',NF90_FILL_FLOAT)
    iret=nf90_put_att(ncido,outvarid(15),'content','TX')
    iret=nf90_put_att(ncido,outvarid(15),'associates','time station')
  END IF

  ! nf90_enddef function
  WRITE(6,*) 'end of definition'
  iret=nf90_enddef(ncido)
  CALL CHECK_ERROR(IRET,__LINE__)

!------------------------------------------------------------------------------------!
!
! 6. Put values in NetCDF output file
!
  WRITE(6,*) 'Put var in output netcdf file'
!time
  iret=nf90_put_var(ncido,outvarid(1),timeout(:),start=(/1/),count=(/ntimeout/))
  CALL CHECK_ERROR(IRET,__LINE__)
!station
  iret=nf90_put_var(ncido,outvarid(2),station(:),start=(/1/),count=(/np/))
  CALL CHECK_ERROR(IRET,__LINE__)
!string16
  iret=nf90_put_var(ncido,outvarid(3),string16,start=(/1/),count=(/16/))
  CALL CHECK_ERROR(IRET,__LINE__)
!station_name
  iret=nf90_put_var(ncido,outvarid(4),station_id(:),start=(/1,1/),count=(/LEN_TRIM(station_id(1)),np/))
  CALL CHECK_ERROR(IRET,__LINE__)
!longitude
  iret=nf90_put_var(ncido,outvarid(5),lons(:,:),start=(/1,1/),count=(/np,ntimeout/))
  CALL CHECK_ERROR(IRET,__LINE__)
!latitude
  iret=nf90_put_var(ncido,outvarid(6),lats(:,:),start=(/1,1/),count=(/np,ntimeout/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency
  iret=nf90_put_var(ncido,outvarid(7),freq(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency1
  iret=nf90_put_var(ncido,outvarid(8),freq1(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!frequency2
  iret=nf90_put_var(ncido,outvarid(9),freq2(:),start=(/1/),count=(/nfband/))
  CALL CHECK_ERROR(IRET,__LINE__)
!Ef
  iret=nf90_put_var(ncido,outvarid(10),Efa(:,:,1:ntimeout),start=(/1,1,1/),count=(/nfband,np,ntimeout/))
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (npar.GE.3) THEN
!th1m
    iret=nf90_put_var(ncido,outvarid(11),th1a(:,:,1:ntimeout),start=(/1,1,1/),count=(/nfband,np,ntimeout/))
    CALL CHECK_ERROR(IRET,__LINE__)
!sth1m
    iret=nf90_put_var(ncido,outvarid(12),sth1a(:,:,1:ntimeout),start=(/1,1,1/),count=(/nfband,np,ntimeout/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
  IF (npar.EQ.5) THEN
!th2m
    iret=nf90_put_var(ncido,outvarid(13),th2a(:,:,1:ntimeout),start=(/1,1,1/),count=(/nfband,np,ntimeout/))
    CALL CHECK_ERROR(IRET,__LINE__)
!sth2m
    iret=nf90_put_var(ncido,outvarid(14),sth2a(:,:,1:ntimeout),start=(/1,1,1/),count=(/nfband,np,ntimeout/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF
!depth
  IF (flagdpt) THEN
    iret=nf90_put_var(ncido,outvarid(15),depth(:,:),start=(/1,1/),count=(/np,ntimeout/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END IF

  iret=nf90_close(ncido) 

  WRITE(6,*) '*** END OF PROGRAM ***'

END PROGRAM buoy_average_time_nc




!==============================================================================

      SUBROUTINE CHECK_ERROR(IRET, ILINE)

      USE NETCDF
      USE W3SERVMD, ONLY: EXTCDE

      IMPLICIT NONE

      INTEGER IRET, ILINE, NDSE


      NDSE=6

      IF (IRET .NE. NF90_NOERR) THEN
        WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN INTERP :'
        WRITE(NDSE,*) ' LINE NUMBER ', ILINE
        WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
        WRITE(NDSE,*) NF90_STRERROR(IRET)
        CALL EXTCDE ( 59 )
      END IF
      RETURN

      END SUBROUTINE CHECK_ERROR

!==============================================================================
