#!/bin/sh -e

#        time = UNLIMITED ; // (5 currently)
#        s_w = 33 ;
#        eta_rho = 106 ;
#        xi_rho = 97 ;
#        s_rho = 32 ;
#        eta_v = 105 ;
#        xi_u = 96 ;
#        auxil = 4 ;

#file=roms_his_Y2009M1001.nc
file=roms_avg_Y2009M1001.nc

### procedure for lat lon ###
echo "LATLON"
# get only the vector longitude de 0 to n-2:
ncks -C -d eta_rho,0,0 -d xi_rho,0,95 -v lon_rho $file lon.nc
# get only the vector latitude de 0 to n-2:
ncks -C -d eta_rho,0,104 -d xi_rho,0,0 -v lat_rho $file lat.nc
# remove the dimension unsued for lat
ncwa -O -a xi_rho lat.nc lat.nc
# remove the dimension unsued for lon
ncwa -O -a eta_rho lon.nc lon.nc
# rename the dimension and variable names as longitude
ncrename -O -d xi_rho,longitude -v lon_rho,longitude lon.nc lon.nc
# rename the dimension and variable names as latitude
ncrename -O -d eta_rho,latitude -v lat_rho,latitude lat.nc lat.nc

### procedure for u current ###
echo "U-CURRENT"
# get only the time, u and v variables
ncks -C -v time,u,v $file roms_cur.nc
# get only the current at the sea surface n-1
ncks -d s_rho,31,31 -x -v s_rho roms_cur.nc roms_current.nc
# get only the u-component with dimension 0,nx-2 and 0,ny-2
ncks -v u -d eta_rho,0,104 -d xi_u,0,95 roms_current.nc roms_u.nc
# rename the u and v dimensions as latitude and longitude
ncrename -d xi_u,longitude -d eta_rho,latitude roms_u.nc roms_u_local.nc
# define the dimension as unlimited dimension
ncks -O --mk_rec_dmn time roms_u_local.nc roms_u_local.nc
echo "V-CURRENT"
# get only the v-component with dimension 0,nx-2 and 0,ny-2
ncks -v v -d eta_v,0,104 -d xi_rho,0,95 roms_current.nc roms_v.nc
# rename the u and v dimensions as latitude and longitude
ncrename -d xi_rho,longitude -d eta_v,latitude roms_v.nc roms_v_local.nc
# define the dimension as unlimited dimension
ncks -O --mk_rec_dmn time roms_v_local.nc roms_v_local.nc
# rename from roms_v_local.nc to roms_uv_local.nc
mv roms_v_local.nc roms_uv_local.nc
# append the u-component to v-component file
ncks -a -A roms_u_local.nc roms_uv_local.nc
# rename the variable
ncrename -v u,ucur -v v,vcur roms_uv_local.nc roms_current_local.nc
# append the latitude vector
ncks -a -A lat.nc roms_current_local.nc
# append the longitude vector
ncks -a -A lon.nc roms_current_local.nc
# remove the unused dimension s_rho
ncwa -O -a s_rho roms_current_local.nc roms_current_local.nc
# add an attibute units for the time
ncatted -a units,time,o,c,"seconds since 1900-01-01 00:00:00" roms_current_local.nc





### procedure for level ###
echo "LEVEL"
# get only the time and zeta variables
ncks -C -v time,zeta $file roms_level.nc
# get only the zeta with dimension 0,nx-2 and 0,ny-2
ncks -v zeta -d eta_rho,0,104 -d xi_rho,0,95 roms_level.nc roms_zeta.nc
# rename the zeta dimensions as latitude and longitude
ncrename -d xi_rho,longitude -d eta_rho,latitude roms_zeta.nc roms_zeta_local.nc
# define the dimension as unlimited dimension
ncks -O --mk_rec_dmn time roms_zeta_local.nc roms_zeta_local.nc
# rename the variable
ncrename -v zeta,level roms_zeta_local.nc roms_level_local.nc
# append the latitude vector
ncks -a -A lat.nc roms_level_local.nc
# append the longitude vector
ncks -a -A lon.nc roms_level_local.nc
# add an attibute units for the time
ncatted -a units,time,o,c,"seconds since 1900-01-01 00:00:00" roms_level_local.nc

rm lat.nc lon.nc roms_cur.nc roms_current.nc roms_level.nc roms_u_local.nc roms_u.nc roms_uv_local.nc roms_v.nc roms_zeta.nc roms_zeta_local.nc
echo "results are roms_current_local.nc and roms_level_local.nc"
