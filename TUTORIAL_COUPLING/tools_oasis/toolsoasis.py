#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
  Classes specifying the NetCDF properties of each type of model (WW3, MesoNH, Croco, Mars, Toy, Oasis
"""

__all__ = ['Model','WW3','MesoNH','Croco','Mars','Toy','Oasis']
__all__.sort()

import os, sys
import netCDF4
import numpy as np

class Model:
    """ Methods and properties used by all the models
    """

    def __init__(self):

        # standard and lon names and units
        self.field_attribute = {"sst" : dict( standard_name = 'sea_water_temperature',
                                    long_name = 'Temperature',
                                    units =  'degrees_celsius',),
                      "ssh" : dict( standard_name = 'sea_surface_height_above_geoid',
                                    long_name = 'Sea surface height',
                                    units =  'm',),
                      "ssu" : dict( standard_name = 'sea_surface_water_x_velocity',
                                    long_name = 'Sea surface zonal velocity',
                                    units =  'm s-1',),
                      "ssv" : dict( standard_name = 'sea_surface_water_y_velocity',
                                    long_name = 'Sea surface meridional velocity',
                                    units =  'm s-1',),
                      "u10m" : dict( standard_name = 'eastward_wind',
                                    long_name = '10-m zonal wind speed',
                                    units =  'm s-1',),
                      "v10m" : dict( standard_name = 'northward_wind',
                                    long_name = '10-m meridional wind speed',
                                    units =  'm s-1',),
                      "w10m" : dict( standard_name = 'upward_wind',
                                    long_name = '10-m upward wind speed',
                                    units =  'm s-1',),
                      "longitude" : dict( standard_name = 'longitude',
                                    long_name = 'longitude',
                                    units =  'degrees_east',),
                      "latitude" : dict( standard_name = 'latitude',
                                    long_name = 'latitude',
                                    units =  'degrees_north',),
                      "imask" : dict( standard_name = 'mask',
                                    long_name = 'imask 0 for land 1 for sea',
                                    units =  '1',),
                      "drywet" : dict( standard_name = 'wetdry_mask_at_location_t',
                                    long_name = 'wet-dry mask 0 for dry 1 for wet',
                                    units =  '1',),
                      "drywet_x" : dict( standard_name = 'wetdry_mask_at_location_u',
                                    long_name = 'wet-dry mask 0 for dry 1 for wet',
                                    units =  '1',),
                      "drywet_y" : dict( standard_name = 'wetdry_mask_at_location_v',
                                    long_name = 'wet-dry mask 0 for dry 1 for wet',
                                    units =  '1',),
                      "srf" : dict( standard_name = 'surface_of_cell',
                                    long_name = 'mesh surface',
                                    units =  'm 2',),
                      "clo" : dict( standard_name = 'longitude_of_corners',
                                    long_name = 'longitude of corners',
                                    units =  'degrees_east',),
                      "cla" : dict( standard_name = 'latitude_of_corners',
                                    long_name = 'latitude of corners',
                                    units =  'degrees_north',),
                      "hs" : dict( standard_name = 'sea_surface_wave_significant_height',
                                    long_name = 'significant height of wind and swell waves',
                                    units =  'm',),
                      "lm" : dict( standard_name = 'mean_wave_length',
                                    long_name = 'mean wave length',
                                    units =  'm',),
                      "t0m1" : dict( standard_name = 'sea_surface_wind_wave_mean_period_from_variance_spectral_density_inverse_frequency_moment',
                                    long_name = 'mean period T0m1',
                                    units =  's',),
                      "fp" : dict( standard_name = 'sea_surface_wave_peak_frequency',
                                    long_name = 'wave peak frequency',
                                    units =  's-1',),
                      "tp" : dict( standard_name = 'sea_surface_wave_peak_period',
                                    long_name = 'wave peak period',
                                    units =  's',),
                      "dir" : dict( standard_name = 'sea_surface_wave_from_direction',
                                    long_name = 'wave_from_direction',
                                    units =  'degrees',),
                      "cdir" : dict( standard_name = 'cosinus_of_sea_surface_wave_from_direction',
                                    long_name = 'cosinus of the wave direction (XXX convention)',
                                    units =  '',),
                      "sdir" : dict( standard_name = 'sinus_of_sea_surface_wave_from_direction',
                                    long_name = 'sinus of the wave direction',
                                    units =  '',),
                      "tawx" : dict( standard_name = 'eastward_wave_supported_wind_stress',
                                    long_name = 'eastward wave supported wind stress',
                                    units =  'm2 s-2',),
                      "tawy" : dict( standard_name = 'northward_wave_supported_wind_stress',
                                    long_name = 'northward wave supported wind stress',
                                    units =  'm2 s-2',),
                      "twox" : dict( standard_name = 'eastward_wave_to_ocean_stress',
                                    long_name = 'eastward wave to ocean stress',
                                    units =  'm2 s-2',),
                      "twoy" : dict( standard_name = 'northward_wave_to_ocean_stress',
                                    long_name = 'northward wave to ocean stress',
                                    units =  'm2 s-2',),
                      "tusx" : dict( standard_name = 'eastward_stokes_transport',
                                    long_name = 'eastward stokes transport',
                                    units =  'm2 s-1',),
                      "tusy" : dict( standard_name = 'northward_stokes_transport',
                                    long_name = 'northward stokes transport',
                                    units =  'm2 s-1',),
                      "ussx" : dict( standard_name = 'eastward_surface_stokes_drift',
                                    long_name = 'eastward surface_stokes drift',
                                    units =  'm s-1',),
                      "ussy" : dict( standard_name = 'northward_surface_stokes_drift',
                                    long_name = 'northward surface stokes drift',
                                    units =  'm s-1',),
                      "bhd" : dict( standard_name = 'radiation_pressure',
                                    long_name = 'radiation pressure (Bernouilli Head)',
                                    units =  'N m-1',),
                      "foc" : dict( standard_name = 'wave_to_ocean_energy_flux',
                                    long_name = 'wave to ocean energy flux',
                                    units =  'W m-2',),
                      "fbb" : dict( standard_name = 'wave_dissipation_flux_in_bbl',
                                    long_name = 'energy flux due to wave dissipation in bottom boundary layer',
                                    units =  'W m-2',),
                      "tbbx" : dict( standard_name = 'northward_wave_to_bbl_stress',
                                    long_name = 'northward wave to bottom boundary layer stress',
                                    units =  'm2 s-2',),
                      "tbby" : dict( standard_name = 'eastward_wave_to_bbl_stress',
                                    long_name = 'eastward wave to bottom boundary layer stress',
                                    units =  'm2 s-2',),
                      "ubr" : dict( standard_name = 'rms_of_bottom_velocity_amplitude',
                                    long_name = 'rms of bottom velocity amplitude',
                                    units =  'm s-1',),
                      "ubrx" : dict( standard_name = 'zonal_rms_bottom_velocity',
                                    long_name = 'zonal component of rms bottom velocity',
                                    units =  'm s-1',),
                      "ubry" : dict( standard_name = 'meridional_rms_bottom_velocity',
                                    long_name = 'meridional component of rms bottom velocity',
                                    units =  'm s-1',),
                      "cha" : dict( standard_name = 'charnock_coefficient_for_surface_roughness_length_for_momentum_in_air',
                                    long_name = 'charnock coefficient for surface roughness length for momentum in air',
                                    units =  '1',),
                      "tauu" : dict( standard_name = 'surface_downward_x_stress',
                                    long_name = 'zonal wind stress',
                                    units =  '[Pa = N m-2 = kg m-1 s-2 for MNH] [Pa s or Pa for OASIS depending on type of exchanges]',),
                      "tauv" : dict( standard_name = 'surface_downward_y_stress',
                                    long_name = 'meridional wind stress',
                                    units =  '[Pa = N m-2 = kg m-1 s-2 for MNH] [Pa s or Pa for OASIS depending on type of exchanges]',),
                      "heat" : dict( standard_name = 'surface_net_downward_non_solar_flux',
                                    long_name = 'Non solar net heat flux (LW + lat + sens). [Downward(positive when ocean is warmed)]',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "radnet" : dict( standard_name = 'surface_net_downward_radiative_flux',  # downward for the atmosphere, radiative : SW + LW, net : down - up
                                    long_name = 'Net radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "swd" : dict( standard_name = 'surface_downward_shortwave_flux',  # downward for the atmosphere
                                    long_name = 'short wave downward radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "swu" : dict( standard_name = 'surface_upward_shortwave_flux',  # upward for the atmosphere
                                    long_name = 'short wave upward radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "swnet" : dict( standard_name = 'surface_net_shortwave_flux',  # downward (atmosphere --> ocean)
                                    long_name = 'Net short wave radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "lwd" : dict( standard_name = 'surface_downward_longwave_flux',  # downward for the atmosphere
                                    long_name = 'long wave downward radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "lwu" : dict( standard_name = 'surface_upward_longwave_flux',  # upward for the atmosphere
                                    long_name = 'long wave upward radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "lwnet" : dict( standard_name = 'surface_net_longwave_flux',  # downward (atmosphere --> ocean)
                                    long_name = 'Net long wave radiation',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "hflux" : dict( standard_name = 'surface_net_downward_heat_flux',  # downward for the atmosphere, SW + LW - (LE + H)
                                    long_name = '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "evap" : dict( standard_name = 'lwe_thickness_of_upward_water_evaporation_amount',
                                    long_name = 'Evaporation [Upward (positive when the ocean evaporates)]',
                                    units =  '[kg kg-1 s-1 for MNH] [kg m-2 or kg m-2 s-1 for OASIS depending on type of exchanges]',),
                      "rain" : dict( standard_name = 'lwe_thickness_of_downward_precipitation_amount',
                                    long_name = 'Accumulated rainfall rate [Downward (positive when it is raining)]',  # up down ?
                                    units =  '[mm for MNH] [ kg m-2 or kg m-2 s-1 for OASIS depending on type of exchanges]',),
                      "pres" : dict( standard_name = 'air_pressure_at_sea_level',
                                    long_name = 'Absolute pressure',
                                    units =  '[Pa for MNH] [Pa s or Pa for OASIS depending on type of exchanges]',),
                      "hflat" : dict( standard_name = 'surface_upward_latent_heat_flux_at_t_location',  # upward for the atmosphere
                                    long_name = 'Latent heat flux',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "hfsens" : dict( standard_name = 'surface_upward_sensible_heat_flux', # upward for the atmosphere
                                    long_name = 'Sensible heat flux',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "hfir" : dict( standard_name = '',
                                    long_name = 'Infra-red net heat flux (LW flux)',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "hfrad" : dict( standard_name = '',
                                    long_name = 'Solar net heat flux (SW flux) [Downward (positive when ocean is warmed)]',
                                    units =  '[W m-2 for MNH] or [J m-2 or J m-2 s-1 for OASIS depending on type of exchanges]',),
                      "cd" : dict( standard_name = 'surface_drag_coefficient_in_air',
                                    long_name = 'surface drag coefficient',
                                    units =  'W s-2 ??',),
                      "ch" : dict( standard_name = '',
                                    long_name = 'sensible heat flux coefficient',
                                    units =  'W s-1 ??',),
                      "ce" : dict( standard_name = '',
                                    long_name = 'latent heat flux coefficient',
                                    units =  'W s-1 K-1 ??',),
                      "z0" : dict( standard_name = 'air_surface_roughness_length',
                                    long_name = 'air roughness length',
                                    units =  'm',),
                      "t2m" : dict( standard_name = 'air_potential_temperature_at_2_meters',
                                    long_name = 'temperature a 2m',
                                    units =  'K',),
                      "q2m" : dict( standard_name = '',
                                    long_name = '',
                                    units =  'kg kg-1',),
                      "hu2m" : dict( standard_name = 'relative_specific_humidity_at_2_meters',
                                    long_name = 'humidity at 2m',
                                    units =  '-',),
                     }
        self.fillvalue = 9999.
        # indexes (i,j) of the corners (at "_f" location for a mesh centered at T) to be used
        # C-tuv for MARS
        # C-uvt for CROCO
        self.arakawa = { "C-tuv" :  {"_f" : [(0,0),(-1,0),(-1,-1),(0,-1)],    # MARS for instance
                                     "_u" : [(0,1),(-1,1),(-1,0),(0,0)],
                                     "_v" : [(1,0),(0,0),(0,-1),(1,-1)]},
                         "C-uvt" :  {"_f" : [(1,1),(0,1),(0,0),(1,0)],        # CROCO
                                     "_u" : [(1,0),(0,0),(0,-1),(1,-1)],
                                     "_v" : [(0,1),(-1,1),(-1,0),(0,0)]},
                       }


    def get_topo(self,file,varname,typecode):

        """
        Read the bathymetry in a netCDF file
        :param file: contains the data
        :param varname: field names available in the netCDF file
        :return: the bathymetric field
        """

        # get the name of the variable available in the file
        name = list( set([varname.lower(),varname.upper()]) & set(file.variables.keys()) )[0]
        print "    Read the topography %s" %name

        # index of interest
        yindex = typecode.yxindex[0]
        xindex = typecode.yxindex[1]

        # read the field
        return file.variables[name][yindex[0]:yindex[1],xindex[0]:xindex[1]]

    def get_mask(self,file,varname,typecode):

        """ Get the mask : 0 for land, 1 for sea

        :param file: contains the data
        :param varname: names of the mask name (at t, u and v location)
        :param typecode: class describing the possible content of the file (according to MASR or CROCO or...)
        :return:a dictionnary with the following keys : mask_t, mask_u,mask_v
        """


        all_mask = {}
        mask_suffix = ["t","u","v"]
        # indexes of interest
        yindex = typecode.yxindex[0]
        xindex = typecode.yxindex[1]


        # get the mask variable
        if varname:
            for im,name in enumerate(varname):
                print "    get the mask %s from the netCDF file" %name
                mask = file.variables[name][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                all_mask.update( { "_".join(("mask",mask_suffix[im])) : mask})

        #estimate the mask from the bathymetry
        else:
            print "\n    get_mask: estimate the mask from the bathymetry"
            for im,name in enumerate(typecode.topo):
                try:
                    bathy = self.get_topo(file,name,typecode)
                    mask = np.ones(bathy.shape)
                    if typecode.model == "MARS":
                        mask = np.where(bathy[:] > -50., mask, 0.)
#RUSTINE wet-dry valerie        mask = np.where(bathy[:] > 5., mask, 0.)
                    elif typecode.model == "WW3":pass
                    elif typecode.model == "MNH":
                        mask = np.where(bathy[:] == 0., mask, 0.)
                    else:
                        print "ERROR in get_mask : No way to estimate it"
                        sys.exit()
                    all_mask.update( { "_".join(("mask",mask_suffix[im])) : mask})
                except Exception, message:
                    print "    The %s field is not available. None evaluation of the mask at this location" %name
                    print "    Error due to %s" %message

        return all_mask

    def get_lonlat(self,file,varname):

        # get the name of the variable available in the file
        try:
            #name = [x for x in varname if x in file.variables.keys()][0]
            name = list( set(varname) & set(file.variables.keys()) )[0]
            print "    Read the %s" %name
        except:
            print "ERROR in get_lonlat : The variable %s is not available" %varname
            sys.exit()

        # read the field
        return file.variables[name][:]

    def check_lonlat_dimension(self,lon,lat,typecode):
        if len(lon.shape) != 2:
            nlon=np.size(lon)
            nlat=np.size(lat)

#cval il y a plus joli a faire
            lon2d = np.zeros((nlat,nlon))
            lat2d = np.zeros((nlat,nlon))
            for ilat in range(nlat):
                lon2d[ilat,:] = lon[:]
            for ilon in range(nlon):
                lat2d[:,ilon] = lat[:]
        else:
            lon2d = lon
            lat2d = lat

        # keep only the indexes of interest
        yindex = typecode.yxindex[0]
        xindex = typecode.yxindex[1]

        return lon2d[yindex[0]:yindex[1],xindex[0]:xindex[1]],lat2d[yindex[0]:yindex[1],xindex[0]:xindex[1]]

    def get_var_axes(self,dims,typecode):

        """ get axes of a variable """

        axes = []
        for i,axis in enumerate(dims):
            if axis == typecode.dims["time"]['name']:
                axes.append('t')
            if axis == typecode.dims["level"]['name']:
                axes.append('z')
        return axes

    def get_var(self,file,varname,typecode):

        """ read the variable from a netcdf file """

        dims = file.variables[varname].dimensions
        axes = self.get_var_axes(dims,typecode)
        # get indexes of interest
        yindex = typecode.yxindex[0]
        xindex = typecode.yxindex[1]

        if axes == []:  # None time or level axis
            var = file.variables[varname][:][Ellipsis,yindex[0]:yindex[1],xindex[0]:xindex[1]]
        elif axes[0] == ''.join(axes) == "t":
            var = file.variables[varname][:][Ellipsis,yindex[0]:yindex[1],xindex[0]:xindex[1]]
        elif axes[0] == "z":
            var = file.variables[varname][:][typecode.surfindex,yindex[0]:yindex[1],xindex[0]:xindex[1]]
        elif axes[0] == "t":
            var = file.variables[varname][:][Ellipsis,typecode.surfindex,yindex[0]:yindex[1],xindex[0]:xindex[1]]

        return var

    def get_surf(self,file,typecode,dx=None,dy=None):

        print "    Get the mesh surface from dx and dy read from the netCDF file"

        if dx is None or dy is None:
            # get indexes of interest
            yindex = typecode.yxindex[0]
            xindex = typecode.yxindex[1]
            # Read or evaluate the size mesh
            if typecode.dx.startswith("1/"): name=typecode.dx[2:]
            else: name = typecode.dx
            dx = file.variables[name][yindex[0]:yindex[1],xindex[0]:xindex[1]]
            if typecode.dy.startswith("1/"): name=typecode.dy[2:]
            else: name = typecode.dy
            dy = file.variables[name][yindex[0]:yindex[1],xindex[0]:xindex[1]]
            if "/" in typecode.dx: dx[:] = 1/dx[:]
            if "/" in typecode.dy: dy[:] = 1/dy[:]

        surf = dx*dy
        return surf

    def get_lonlat_corner(self,lon,lat,indexes=False):

        """ save the longitude and latitude according to the type of the Arakawa C-grid"""

        ndims = (4, lon.shape[0], lon.shape[1])
        clo = np.zeros(ndims)
        cla = np.zeros(ndims)
        iend = lon.shape[1]
        jend = lon.shape[0]

        if indexes:
            for ic,ij in enumerate(indexes):
                idec = ij[0]
                jdec=ij[1]
                # upper-right : 0
                # upper-left : 1
                # lower-left : 2
                # lower-right : 3
                clo[ic,0-min(0,jdec):jend-max(0,jdec),0-min(0,idec):iend-max(0,idec)] = \
                    lon[0+max(0,jdec):jend+min(0,jdec),0+max(0,idec):iend+min(0,idec)]
                clo[ic,0,0-min(0,idec):iend-max(0,idec)] = lon[0+max(0,jdec),0+max(0,idec):iend+min(0,idec)] - \
                            (lon[0-min(0,jdec),0+max(0,idec):iend+min(0,idec)] - lon[0,0+max(0,idec):iend+min(0,idec)])
                clo[ic,jend-1,0-min(0,idec):iend-max(0,idec)] = lon[jend+min(0,jdec)-1,0+max(0,idec):iend+min(0,idec)] + \
                            (lon[jend-1,0+max(0,idec):iend+min(0,idec)] - lon[jend-max(0,jdec)-1,0+max(0,idec):iend+min(0,idec)])
                clo[ic,0-min(0,jdec):jend-max(0,jdec),0] = lon[0+max(0,jdec):jend+min(0,jdec),0+max(0,idec)] - \
                            (lon[0+max(0,jdec):jend+min(0,jdec),0-min(0,idec)] - lon[0+max(0,jdec):jend+min(0,jdec),0])
                clo[ic,0-min(0,jdec):jend-max(0,jdec),iend-1] = lon[0+max(0,jdec):jend+min(0,jdec),iend+min(0,idec)-1] + \
                            (lon[0+max(0,jdec):jend+min(0,jdec),iend-1] - lon[0+max(0,jdec):jend+min(0,jdec),iend-max(0,idec)-1])
                clo[ic,0,0] = lon[0+max(0,jdec),0+max(0,idec)] - (lon[0-min(0,jdec),0-min(0,idec)] - lon[0,0])
                clo[ic,jend-1,iend-1] = lon[jend+min(0,jdec)-1,iend+min(0,idec)-1] + \
                                 (lon[jend-1,iend-1] - lon[jend-max(0,jdec)-1,iend-max(0,idec)-1])

                clo[ic,0,iend-1] = lon[0+max(0,jdec),iend+min(0,idec)-1] + \
                                 (lon[0-min(0,jdec),iend-1] - lon[0,iend-max(0,idec)-1])
                clo[ic,jend-1,0] = lon[jend+min(0,jdec)-1,0+max(0,idec)] - \
                                 (lon[jend-1,0-min(0,idec)] - lon[jend-max(0,jdec)-1,0])
                cla[ic,0-min(0,jdec):jend-max(0,jdec),0-min(0,idec):iend-max(0,idec)] = \
                    lat[0+max(0,jdec):jend+min(0,jdec),0+max(0,idec):iend+min(0,idec)]
                cla[ic,0,0-min(0,idec):iend-max(0,idec)] = lat[0+max(0,jdec),0+max(0,idec):iend+min(0,idec)] - \
                            (lat[0-min(0,jdec),0+max(0,idec):iend+min(0,idec)] - lat[0,0+max(0,idec):iend+min(0,idec)])
                cla[ic,jend-1,0-min(0,idec):iend-max(0,idec)] = lat[jend+min(0,jdec)-1,0+max(0,idec):iend+min(0,idec)] + \
                            (lat[jend-1,0+max(0,idec):iend+min(0,idec)] - lat[jend-max(0,jdec)-1,0+max(0,idec):iend+min(0,idec)])
                cla[ic,0-min(0,jdec):jend-max(0,jdec),0] = lat[0+max(0,jdec):jend+min(0,jdec),0+max(0,idec)] - \
                            (lat[0+max(0,jdec):jend+min(0,jdec),0-min(0,idec)] - lat[0+max(0,jdec):jend+min(0,jdec),0])
                cla[ic,0-min(0,jdec):jend-max(0,jdec),iend-1] = lat[0+max(0,jdec):jend+min(0,jdec),iend+min(0,idec)-1] + \
                            (lat[0+max(0,jdec):jend+min(0,jdec),iend-1] - lat[0+max(0,jdec):jend+min(0,jdec),iend-max(0,idec)-1])
                cla[ic,0,0] = lat[0+max(0,jdec),0+max(0,idec)] - (lat[0-min(0,jdec),0-min(0,idec)] - lat[0,0])
                cla[ic,jend-1,iend-1] = lat[jend+min(0,jdec)-1,iend+min(0,idec)-1] + \
                                 (lat[jend-1,iend-1] - lat[jend-max(0,jdec)-1,iend-max(0,idec)-1])
                cla[ic,0,iend-1] = lat[0+max(0,jdec),iend+min(0,idec)-1] - \
                                 (lat[0-min(0,jdec),iend-1] - lat[0,iend-max(0,idec)-1])
                cla[ic,jend-1,0] = lat[jend+min(0,jdec)-1,0+max(0,idec)] + \
                                 (lat[jend-1,0-min(0,idec)] - lat[jend-max(0,jdec)-1,0])


        elif not indexes:
            # rough estimate
            dlon = np.zeros(lon.shape)
            dlat = np.zeros(lon.shape)
            dlon[:,:-1] = np.diff(lon,n=1,axis=-1)/2.
            dlat[:-1,:] = np.diff(lat,n=1,axis=-2)/2.
            dlon[:,-1] = dlon[:,-2]
            dlat[-1,:] = dlat[-2,:]
            # upper-right
            clo[0,:,:] = lon[:,:]+dlon
            cla[0,:,:] = lat[:,:]+dlat
            # upper-left
            clo[1,:,:] = lon[:,:]-dlon
            cla[1,:,:] = lat[:,:]+dlat
            # lower-left
            clo[2,:,:] = lon[:,:]-dlon
            cla[2,:,:] = lat[:,:]-dlat
            # lower-right
            clo[3,:,:] = lon[:,:]+dlon
            cla[3,:,:] = lat[:,:]-dlat

        return clo,cla

    def get_oasis_corner(self,file,typecode,lon,lat):
        """ Get the longitude and latitude of the 4 corners of each cell
                    0 : upper-right
                    1 : upper-left
                    2 : lower-left
                    3 : lower-right
            Check the lon lat available and :
              - save the correct longitude and latitude of the corner (according to the location of the variable)
              - estimate the lon lat et the corner from the longitude and latitude at t-location
            Pour l instant je n ai gere que la mesh centree sur t
        """

        print "    check for OASIS corner (longitude and latitude of each corners of the cells)"

        ndims = (4, lon.shape[-2], lon.shape[-1])
        # get indexes of interest
        yindex = typecode.yxindex[0]
        xindex = typecode.yxindex[1]

        corner = {}

        # Read all the available longitude and latitude and get corresponding clon_, clat_
        for i,vname in enumerate(typecode.longitude):
            if typecode.loc_f in vname:
                try:
                    # make sure the variable is in the file
                    print "    get_oasis_corner: get the corners of a cell centered around the t-location if available"
                    lon_name = list( set([vname]) & set(file.variables.keys()) )[0]
                    # read the variable
                    lon_f = file.variables[typecode.longitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    # one assume the location are in the same order in the definition of self.longitude and self.latitude
                    lat_f = file.variables[typecode.latitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    [clon_t,clat_t] = self.get_lonlat_corner(lon_f,lat_f,indexes=self.arakawa[typecode.grid]["_f"])
                    corner.update( {"cell_t_from_f":[clon_t,clat_t]} )
                except Exception,message:
                    print "    Actually the longitude and latitude of the corner will be estimated from the longitude and latitude at the mesh centre !"
                    print "    Error due to %s" %message
                    [clon_t,clat_t] = self.get_lonlat_corner(lon,lat)
                    corner.update( {"cell_t_from_f":[clon_t,clat_t]} )
            elif typecode.loc_u in vname:
                try:
                    # make sure the variable is in the file
                    print "    get_oasis_corner: get the corners of a cell centered around the v-location if available"
                    lon_name = list( set([vname]) & set(file.variables.keys()) )[0]
                    # read the variable
                    lon_u = file.variables[typecode.longitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    # one assume the location are in the same order in the definition of self.longitude and self.latitude
                    lat_u = file.variables[typecode.latitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    [clon_v,clat_v] = self.get_lonlat_corner(lon_u,lat_u,indexes=self.arakawa[typecode.grid]["_u"])
                    corner.update( {"cell_v_from_u":[clon_v,clat_v]} )
                except Exception,message:
                    print "    cell centered around the v-location if available not processed"
                    print "    Error due to %s" %message
            elif typecode.loc_v in vname:
                try:
                    # make sure the variable is in the file
                    print "    get_oasis_corner: get the corners of a cell centered around the u-location if available"
                    lon_name = list( set([vname]) & set(file.variables.keys()) )[0]
                    # read the variable
                    lon_v = file.variables[typecode.longitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    # one assume the location are in the same order in the definition of self.longitude and self.latitude
                    lat_v = file.variables[typecode.latitude[i]][yindex[0]:yindex[1],xindex[0]:xindex[1]]
                    [clon_u,clat_u] = self.get_lonlat_corner(lon_v,lat_v,indexes=self.arakawa[typecode.grid]["_v"])
                    corner.update( {"cell_u_from_v":[clon_u,clat_u]} )
                except Exception,message:
                    print "    cell centered around the v-location if available not processed"
                    print "    Error due to %s" %message

            else:
                    print "    get_oasis_corner: get the corners of a cell centered around the t-location"
                    [clon_t,clat_t] = self.get_lonlat_corner(lon,lat)
                    if len( [vv for vv in typecode.longitude if typecode.loc_f in vv] ) == 1:
                        corner.update( {"cell_f_from_t":[clon_t,clat_t]} )
                    else:
                        print "    Actually the longitude and latitude of the corner is estimated from the longitude and latitude at the mesh centre !"
                        corner.update( {"cell_t_from_t":[clon_t,clat_t]} )
        return corner

    def create_oasis_grid(self,file,typecode,lon,lat,outdir):

        print "\n"
        print "Create the OASIS file grid"

        # Read or evaluate the size mesh
        try:
            surf = self.get_surf(file,typecode)

        except Exception,message:
            print "    dx and dy not available. They must be evaluated from the longitude and latitude fields."
            print "    Error due to %s" %message
            try:
                print "    estimate of dx dy from lon lat at T-location (not accurate but...)"
                dlon = np.diff(lon,n=1,axis=-1)
                dlat = np.diff(lat,n=1,axis=-1)
                dx = np.zeros(lon.shape)
                dx[:,:-1] = self.get_dist_circle(dlon,dlat,lat[:,:-1],lat[:,:-1],typecode.earth_radius)
                dx[:,-1] = dx[:,-2]
                dlon = np.diff(lon,n=1,axis=-2)
                dlat = np.diff(lat,n=1,axis=-2)
                dy = np.zeros(lon.shape)
                dy[:-1,:] = self.get_dist_circle(dlon,dlat,lat[1:,:],lat[:-1,:],typecode.earth_radius)
                dy[-1,:] = dy[-2,:]
                surf = self.get_surf(file,typecode,dx=dx,dy=dy)
            except Exception,message:
                print "ERROR create_oasis_grid : DX and DY cannot be evaluated !"
                print "    Error due to %s" %message
                sys.exit()

        # Get the lon,lat et the corners of the mesh for the OASIS grid
        corner = self.get_oasis_corner(file,typecode,lon,lat)

        # Get the land mask
        maskland = self.get_mask(file,typecode.mask,typecode)

        # Save the grid
        self.save_grid(typecode,lon,lat,surf,maskland,corner,outdir)

    def get_dist_circle(self,dlon,dlat,latp,latm,radius):
        """ estimate the circle distance from 2 longitudes & latitudes (Haversine formula) """
        dist_lat = np.sin( 0.5*np.deg2rad(dlat) )**2
        dist_lon = np.sin( 0.5*np.deg2rad(dlon) )**2
        dist_lon *= np.cos( np.deg2rad(latp) )*np.cos( np.deg2rad(latm) )
        dist = np.arcsin( np.sqrt( dist_lat + dist_lon) )
        dist *= 2.0*radius
        return dist

    def save_grid(self,typecode,lon,lat,surf,maskland,clonlat,outdir):

        # Create the output file
        gridname = ".".join(("_".join(("grid_from",typecode.model)),"nc"))
        gridname = os.path.join(outdir,gridname)
        print "    Save the grid file named %s" %gridname
        gridout = netCDF4.Dataset(gridname,'w',format='NETCDF3_64BIT')
        gridout.Description='Grid file for (spatial interpolation with) OASIS coupling'

        # Create the dimensions of the file
        gridout.createDimension('nlon',lon.shape[-1])
        gridout.createDimension('nlat',lon.shape[-2])
        gridout.createDimension('ncorner',4)

        # Save the variables

        #longitude
        self.save_var(gridout,"longitude",lon)

        #latitude
        self.save_var(gridout,"latitude",lat)

        #mask
        for key_mask in maskland.keys():
            dims = self.check_dimensions(gridout,maskland[key_mask])
            self.save_var(gridout,"imask",maskland[key_mask],dims=dims,loc=key_mask[5:6])

        #corners longitude and latitude
        for key_corner in clonlat.keys():
            dims = self.check_dimensions(gridout,clonlat[key_corner][0])
            self.save_var(gridout,"clo",clonlat[key_corner][0],dims=("ncorner",dims[0],dims[1]),loc=key_corner[5:6])
            self.save_var(gridout,"cla",clonlat[key_corner][1],dims=("ncorner",dims[0],dims[1]),loc=key_corner[5:6])

        #surface
        self.save_var(gridout,"srf",surf)

        # Close the file
        gridout.close()

    def save_var(self,file,varname,var,dims=("nlat","nlon"),loc=None,keyname=None):

        """ Save the variable in the NetCDF file
        :param file: NetCDF opened file
        :param varname: Name of the variable
        :param var: the data to saved (np.array)
        :param dims: [optional if dimension differ from yx]
        :return: nothing but the variable is saved in the NetCDF file
        """

        # Create the variable
        name = varname
        if loc: name = "_".join((name,loc))
        varout = file.createVariable(name,'d',dimensions=dims,fill_value=self.fillvalue)

        # Add attributes
        try:
            longname = self.field_attribute[varname]["long_name"]
        except:
            longname = self.field_attribute[keyname]["long_name"]
        varout.long_name = longname
        try:
            stdname = self.field_attribute[varname]["standard_name"]
        except:
            stdname = self.field_attribute[keyname]["standard_name"]
        varout.standard_name = stdname
        try:
            unit = self.field_attribute[varname]["units"]
        except:
            unit = self.field_attribute[keyname]["units"]
        varout.units = unit

        # Save the variable
        file.variables[name][:] = var

    def check_dimensions(self,fileout,var):
        """
        Make sure the dimensions alonx x and y are available in the netCDF file
        before saving the variable

        if required, add the dimensions nlon_u or nlat_v in the netCDF file

        :param file: netCDF file that contains the available dimensions
        :param var: variable to get the usefull dimensions
        :return: the dimensions to be used to save the variable
        """

        dims = ("nlat","nlon")

        if var.shape[-2:] != ( len(fileout.dimensions["nlat"]),len(fileout.dimensions["nlon"]) ):

            # check if the required dimension in latitude is in the netCDF file
            if var.shape[-2] != len(fileout.dimensions["nlat"]):
                if "nlat_v" in fileout.dimensions.keys():
                    if var.shape[-2] == len(fileout.dimensions["nlat_v"]):
                        dim_lat = "nlat_v"
                    else:
                        print "ERROR in check_dimensions. Aie aie aie !"
                        sys.exit()
                else:
                    print "    add the nlat_v = %s dimension in the netCDF file" %var.shape[-2]
                    fileout.createDimension('nlat_v',var.shape[-2])
                    dim_lat = "nlat_v"
            else:
                dim_lat = "nlat"

            # check if the required dimension in longitude is in the netCDF file
            if var.shape[-1] != len(fileout.dimensions["nlon"]):
                if "nlon_u" in fileout.dimensions.keys():
                    if var.shape[-1] == len(fileout.dimensions["nlon_u"]):
                        dim_lon = "nlon_u"
                    else:
                        print "ERROR in check_dimensions. Aie aie aie !"
                        sys.exit()
                else:
                    print "    add the nlon_u = %s dimension in the netCDF file" %var.shape[-1]
                    fileout.createDimension('nlon_u',var.shape[-1])
                    dim_lon = "nlon_u"
            else:
                dim_lon = "nlon"

            dims = (dim_lat,dim_lon)

        return dims

    def save_mask(self,mask,fileout,typemask):

        for keyname in mask.keys():

            # check if the dimensions are available in the netCDF file, 
            # and get the yx dimensions to be used
            dim = self.check_dimensions(fileout,mask[keyname])

            # Create the 2D mask and its attributes
            dims = (dim[0],dim[1])
            varout = fileout.createVariable(typemask[keyname]["name"],'d',dimensions=dims,fill_value=self.fillvalue)
            varout.long_name = self.field_attribute[keyname]["long_name"]
            varout.standard_name = self.field_attribute[keyname]["standard_name"]

            # Save the field
            fileout.variables[typemask[keyname]["name"]][:] = mask[keyname]
        
    def save_mask_inputfile(self,mask,fileout,typemask):

        for keyname in mask.keys():

            # check if the dimensions are available in the netCDF file, 
            # and get the yx dimensions to be used
            dim = self.check_dimensions(fileout,mask[keyname])

            # Create the 2D mask and its attributes
            dims = ('time',dim[0],dim[1])
            varout = fileout.createVariable(typemask[keyname]["name"],'d',dimensions=dims,fill_value=self.fillvalue)
            varout.long_name = self.field_attribute[keyname]["long_name"]
            varout.standard_name = self.field_attribute[keyname]["standard_name"]

            # Save the field
            vartmp = np.zeros( ( 1, mask[keyname].shape[0] , mask[keyname].shape[1] ) )
            vartmp[0,:,:] = mask[keyname]
            fileout.variables[typemask[keyname]["name"]][:] = vartmp
        
    def save_rstrt(self,lon,lat,data,tzyx,mask,filename,incode,outcode):

        # Create the output file
        print "\n"
        print "Create the rstart file named %s" %filename
        fileout = netCDF4.Dataset(filename,'w',format='NETCDF3_64BIT')
        fileout.Description='Restart file for OASIS coupling'

        # Create the dimensions of the file
        fileout.createDimension('nlon',lon.shape[-1])
        fileout.createDimension('nlat',lon.shape[-2])

        # Save the (prognostics) fields but the wet-dry masks
        for iv in range(len(data)):

            # get the variable name
            varname = data[iv].keys()[0]
            print "    Save the variable %s" %varname
            # get the key variable name that link the variable of the input file and the one in the output file
            keyname = None
            for k,v in incode.field.iteritems():
                if varname in v["name"]:
                    keyname = k
                    break
            if keyname is None:
                print "ERROR save_rstrt None key name !!!! How can it be possible ?"
                sys.exit()

            # check if the dimensions are available in the netCDF file, introduce them if required
            #      and get the yx dimensions to be used
            dim = self.check_dimensions(fileout,data[iv][varname])

            # Create the variable and its attributes
            if len(data[iv][varname].shape) > 2: # get the fisrt record and remove time axis
                tmp = data[iv][varname][incode.tindex,...]
            else: tmp = data[iv][varname]
            dims = (dim[0],dim[1])
            for ik,kname in enumerate( outcode.outfield[keyname]["name"] ):
                varout = fileout.createVariable(outcode.outfield[keyname]["name"][ik],'d',dimensions=dims,fill_value=self.fillvalue)
                varout.long_name = self.field_attribute[keyname]["long_name"]
                varout.standard_name = self.field_attribute[keyname]["standard_name"]
                varout.units = self.field_attribute[keyname]["units"]
                min_value = np.floor(np.min(data[iv][varname]))
                max_value = np.ceil(np.max(data[iv][varname]))
                varout.valid_min = min_value
                varout.valid_max = max_value

                # Save the field
                np.ma.masked_where(data[iv][varname] < min_value, data[iv][varname])
                np.ma.masked_where(data[iv][varname] > max_value, data[iv][varname])
                fileout.variables[outcode.outfield[keyname]["name"][ik]][:] = tmp

        # Save wet-dry masks sent to the wave model
        try:
            if incode.mask2wave:
                self.save_mask(mask,fileout,incode.mask2wave)
        except Exception, message:
            print "    Error when saving mask2wave due to %s" %message
            print "    But think that WW3 instance has no attribute 'mask2wave' before to conclude there an error"

        # Save wet-dry sent to the atmospheric model
        try:
            if incode.mask2atm:
                print "VAL mask key",mask.keys(),incode.mask2atm
                self.save_mask(mask,fileout,incode.mask2atm)
        except Exception, message:
            print "    Error when saving mask2atm due to %s" %message
            print "    But think that MNH instance has no attribute 'mask2atm' before to conclude there an error"

        # Save wet-dry masks sent to the ocean model
        try:
            if incode.mask2oce:
                self.save_mask(mask,fileout,incode.mask2oce)
        except Exception, message:
            print "    Error when saving mask2oce due to %s" %message
            print "    But think that MARS or CROCO instance has no attribute 'mask2oce' before to conclude there an error"

        # Close the file
        fileout.close()

    def save_inputfile(self,lon,lat,data,tzyx,mask,filename,incode,outcode,set_varaxes=False):

        # Create the output file
        print "\n"
        print "Create the rstart file named %s" %filename
        fileout = netCDF4.Dataset(filename,'w',format='NETCDF3_64BIT')
        fileout.Description='Restart file for OASIS coupling'

        # Create the dimensions of the file
        fileout.createDimension('nlon',lon.shape[-1])
        fileout.createDimension('nlat',lon.shape[-2])
        fileout.createDimension('time',None)

        if set_varaxes is not False:

            # Longitude
            varout = fileout.createVariable('nlon','d',dimensions=('nlon'),fill_value=self.fillvalue)
            varout.long_name = 'longitude'
            varout.standard_name = 'longitude'
            varout.units = 'degrees_east'
            varout.valid_min = -180.
            varout.valid_maw =  180.
            fileout.variables['nlon'][:] = lon[1,:]

            # Latitude
            varout = fileout.createVariable('nlat','d',dimensions=('nlat'),fill_value=self.fillvalue)
            varout.long_name = 'latitude'
            varout.standard_name = 'latitude'
            varout.units = 'degrees_north'
            varout.valid_min = -180.
            varout.valid_maw =  180.
            fileout.variables['nlat'][:] = lat[:,1]

            # Time
            varout=fileout.createVariable('time','d',('time') )
            varout.long_name = "time"
            varout.calendar = "proleptic_gregorian"
            varout.units = "seconds since 1970-01-01 00:00:00"
            fileout.variables['time'][:] = set_varaxes

        # Save the (prognostics) fields but the wet-dry masks
        for iv in range(len(data)):

            # get the variable name
            varname = data[iv].keys()[0]
            # get the key variable name that link the variable of the input file and the one in the output file
            keyname = None
            for k,v in incode.field.iteritems():
                if varname in v["name"]:
                    keyname = k
                    break
            if keyname is None:
                print "ERROR save_inputfile None key name !!!! How can it be possible ?"
                sys.exit()

            # check if the dimensions are available in the netCDF file, introduce them if required
            #      and get the yx dimensions to be used
            dim = self.check_dimensions(fileout,data[iv][varname])

            # Create the variable and its attributes
            dims = ('time',dim[0],dim[1])
            for ik,kname in enumerate( outcode.outfield[keyname]["name"] ):
                varout = fileout.createVariable(outcode.outfield[keyname]["name"][ik],'d',dimensions=dims,fill_value=self.fillvalue)
                varout.long_name = self.field_attribute[keyname]["long_name"]
                varout.standard_name = self.field_attribute[keyname]["standard_name"]
                varout.units = self.field_attribute[keyname]["units"]
                min_value = np.floor(np.min(data[iv][varname]))
                max_value = np.ceil(np.max(data[iv][varname]))
                varout.valid_min = min_value
                varout.valid_max = max_value

                # Save the field
                np.ma.masked_where(data[iv][varname] < min_value, data[iv][varname])
                np.ma.masked_where(data[iv][varname] > max_value, data[iv][varname])
                if len(data[iv][varname].shape)==2:
                   vartmp = np.zeros( ( 1, data[iv][varname].shape[0] , data[iv][varname].shape[1] ) )
                   vartmp[0,:,:] = data[iv][varname][:,:]
                else:
                   vartmp = data[iv][varname]
                fileout.variables[outcode.outfield[keyname]["name"][ik]][:] = vartmp

        # Save wet-dry masks sent to the wave model
#cval en fait j aurai pu ajouter plusieurs noms dans mas2model et ne pas faire tous ce stests. (meme methode que dans save_rstrt)
        try:
            if incode.mask2wave:
                self.save_mask_inputfile(mask,fileout,incode.mask2wave)
        except Exception, message:
            print "    Error when saving mask2wave due to %s" %message
            print "    But think that WW3 instance has no attribute 'mask2wave' before to conclude there an error"

        # Save wet-dry sent to the atmospheric model
        try:
            if incode.mask2atm:
                self.save_mask_inputfile(mask,fileout,incode.mask2atm)
        except Exception, message:
            print "    Error when saving mask2atm due to %s" %message
            print "    But think that MNH instance has no attribute 'mask2atm' before to conclude there an error"

        # Save wet-dry masks sent to the ocean model
        try:
            if incode.mask2oce:
                self.save_mask_inputfile(mask,fileout,incode.mask2oce)
        except Exception, message:
            print "    Error when saving mask2oce due to %s" %message
            print "    But think that MARS or CROCO instance has no attribute 'mask2oce' before to conclude there an error"

        # Close the file
        fileout.close()


class WW3(Model):
    """ Set parameters relative to the WW3 code """

    def __init__(self):
        Model.__init__(self)
        self.model="WW3"
        self.earth_radius = 6369426.75 # 4*10^7 / 2 pi
        self.latitude=["latitude","y"]
        self.longitude=["longitude","x"]
        self.dx="y en a pas"
        self.dy="y en a pas"
        self.loc_u="_u"  # does not exist
        self.loc_v="_v"  # does not exist
        self.loc_f="_f"  # does not exist
        self.grid= "hum"
        self.mask=["MAPSTA"]
        self.topo=["dpt"]
        self.dims = { "time" : { 'name':"time"},
                      "level" : {'name':"level"},
                      "nj" : { 'name':"longitude"},
                      "ni" : { 'name':"latitude"}, }
        self.field = { "imask" : {'name':["mapsta"]},
                       "t0m1" : {'name':["t0m1"]},
                       "dir" : {'name':["dir"]},
                       "cdir" : {'name':["cdir"]},
                       "sdir" : {'name':["sdir"]},
                       "bhd" : {'name':["bhd"]},
                       "twox" : {'name':["twox","utwo"]},
                       "twoy" : {'name':["twoy","vtwo"]},
                       "tusx" : {'name':["utus"]},
                       "tusy" : {'name':["vtus"]},
                       "ussx" : {'name':["uuss"]},
                       "ussy" : {'name':["vuss"]},
                       "ubr" : {'name':["ubr"]},
                       "ubrx" : {'name':["uubr"]},
                       "ubry" : {'name':["vubr"]},
                       "foc" : {'name':["foc"]},
                       "fbb" : {'name':["fbb"]},
                       "tbbx" : {'name':["tbbx","utbb"]},
                       "tbby" : {'name':["tbby","vtbb"]},
                       "tawx" : {'name':["tawx","utaw"]},
                       "tawy" : {'name':["tawy","vtaw"]},
                       "lm" : {'name':["lm"]},
                       "ssu" : {'name':["ucur"]},
                       "ssv" : {'name':["vcur"]},
                       "cha" : {'name':["cha"]},
                       "hs" : {'name':["hs"]},
                       "fp" : {'name':["fp"]},
                       "tp" : {'name':["tp"]}, }
        self.outfield = { "DRY" : {'name':["WW3_ODRY"]},
                       "t0m1" : {'name':["WW3_T0M1"]},
                       "hs" : {'name':["WW3__AHS","WW3__OHS"]},
                       "dir" : {'name':["WW3__DIR"]},      # necessaire car ne sort que cela dans ses outputs
                       "cdir" : {'name':["WW3_CDIR"]},
                       "sdir" : {'name':["WW3_SDIR"]},
                       "bhd" : {'name':["WW3__BHD"]},
                       "twox" : {'name':["WW3_TWOX"]},
                       "twoy" : {'name':["WW3_TWOY"]},
                       "tusx" : {'name':["WW3_TUSX"]},
                       "tusy" : {'name':["WW3_TUSY"]},
                       "ussx" : {'name':["WW3_USSX"]},
                       "ussy" : {'name':["WW3_USSY"]},
                       "ubr" : {'name':["WW3__UBR"]},
                       "ubrx" : {'name':["WW3_UBRX"]},
                       "ubry" : {'name':["WW3_UBRY"]},
                       "foc" : {'name':["WW3__FOC"]},
                       "fbb" : {'name':["WW3__FBB"]},
                       "tbbx" : {'name':["WW3_TBBX"]},
                       "tbby" : {'name':["WW3_TBBY"]},
                       "tawx" : {'name':["WW3_TAWX"]},
                       "tawy" : {'name':["WW3_TAWY"]},
                       "lm" : {'name':["WW3___LM"]},
                       "ssu" : {'name':["WW3_WSSU"]},
                       "ssv" : {'name':["WW3_WSSV"]},
                       "cha" : {'name':["WW3__CHA"]},
                       "fp" : {'name':["WW3___FP"]},
                       "tp" : {'name':["WW3___TP"]}, }
        self.mask2oce  = {"drywet"  : {'name':"WW3_ODRY"} }
        self.mask2atm = {"drywet"  : {'name':"WW3_ADRY"} }
        self.surfindex = -1
        #self.yxindex = [(1,-1),(1,-1)]
        self.yxindex = [(None,None),(None,None)]
        self.tindex = -1
        #self.tindex = 0

class fromMesoNH(Model):
    """ Set parameters relative to the MesoNH code
        Goal : copy paste variables
                - without changes in names
                - adding standard, long names and units
               in a unique file """

    def __init__(self):

        Model.__init__(self)
        self.model="MNH"
        self.earth_radius = 6371229.
        self.latitude=["LAT"]
        self.longitude=["LON"]
        self.dx="y'en a pas"
        self.dy="y'en a pas"
        self.loc_u="_u"  # does not exist
        self.loc_v="_v"  # does not exist
        self.loc_f="_f"  # does not exist
        self.grid= "hum"
        self.mask=None
        self.topo=["ZS"]
        self.dims = { "time" : { 'name':"time"},
                      "level" : {'name':"Z"},
                      "ni" : {'name':"X"},
                      "nj" : { 'name':"Y"}, }
        self.field = { "u10m" : {'name':["UT"]}, #,"ZON10M_SEA"]}, nul ?
                       "v10m" : {'name':["VT"]}, #,"MER10M_SEA"]}, nul dans sf2.nc ?
                       "w10m" : {'name':["WT"]}, #,"W10M_SEA"]},
                       "wind" : {'name':["sqrt(u10*u10+v10*v10)"]},
                       "tauu" : {'name':["FMU_SEA"]},
                       "tauv" : {'name':["FMV_SEA"]},
                       "tau" : {'name':["sqrt(taux*taux+tauy*tauy)"]},
                       "heat" : {'name':["heat"]},
                       "swnet" : {'name':["swnet"]},
                       "radnet" : {'name':["RN_SEA"]},
                       "hflux" : {'name':["GFLUX_SEA"]},
                       "swd" : {'name':["SWD_SEA"]},
                       "swu" : {'name':["SWU_SEA"]},
                       "lwd" : {'name':["LWD_SEA"]},
                       "lwu" : {'name':["LWU_SEA"]},
                       "hflat" : {'name':["LE_SEA"]}, # flux de chaleur sensible
                       "hfsens" : {'name':["H_SEA"]}, # flux de chaleur latent
                       "evap" : {'name':["evap"]},
                       "rain" : {'name':["ACPRT"]},  #ACPRR autrement pour pluie seulement ?
                       "swow" : {'name':["ACPRSnon"]}, # cval on s en fout ? un peu mon n'veu !
                       "watf" : {'name':[""]},  # eau des lacs rivieres
                       "pres" : {'name':["PABST"]},
                       "cd" : {'name':["CD_SEA"]},
                       "ch" : {'name':["CH_SEA"]},
                       "ce" : {'name':["CE_SEA"]},
                       "z0" : {'name':["Z0_SEA"]},
                       "t2m" : {'name':["T2M_SEA"]},
                       "q2m" : {'name':["Q2M_SEA"]},
                       "hu2m" : {'name':["HU2M_SEA"]},
                       "sst" : {'name':["SST"]},
                       "hs" : {'name':["HS"]},
                       "tp" : {'name':["TP"]},
                      }
        self.outfield = self.field
        self.mask2wave = {"drywet"  : {'name':"MNH_WWDT"},
                          "drywet_x" : {'name':"MNH_WWDU"},
                          "drywet_y" : {'name':"MNH_WWDV"} }
        self.mask2oce = {"drywet"  : {'name':"MNH_OWDT"},
                         "drywet_x" : {'name':"MNH_OWDU"},
                         "drywet_y" : {'name':"MNH_OWDV"} }
        self.surfindex = 2
        self.yxindex = [(1,-1),(1,-1)]
        self.tindex = 0
        self.fillvalue = 999.

class MesoNH(Model):
    """ Set parameters relative to the MesoNH code """

    def __init__(self):

        Model.__init__(self)
        self.model="MNH"
        self.earth_radius = 6371229.
        self.latitude=["LAT"]
        self.longitude=["LON"]
        self.dx="y'en a pas"
        self.dy="y'en a pas"
        self.loc_u="_u"  # does not exist
        self.loc_v="_v"  # does not exist
        self.loc_f="_f"  # does not exist
        self.grid= "hum"
        self.mask=None
        self.topo=["ZS"]
        self.dims = { "time" : { 'name':"time"},
                      "level" : {'name':"Z"},
                      "ni" : {'name':"X"},
                      "nj" : { 'name':"Y"}, }
        #self.field = { "u10m" : {'name':["UT","ZON10M_SEA"]},  #cval attention quand dispo il ne faut garder que ZON10M_SEA. En tout cas c est redondant !!!!!!
        #               "v10m" : {'name':["VT","MER10M_SEA"]},  # idem
        self.field = { "u10m" : {'name':["UT"]},  #cval attention quand dispo il ne faut garder que ZON10M_SEA. En tout cas c est redondant !!!!!!
                       "v10m" : {'name':["VT"]},  # idem
                       "wind" : {'name':["sqrt(u10*u10+v10*v10)"]}, # si besoin à estimer si u10 et v10 lu à partir de son expression en utilisant numexpr
                       "tauu" : {'name':["FMU_SEA"]},
                       "tauv" : {'name':["FMV_SEA"]},
                       "tau" : {'name':["sqrt(taux*taux+tauy*tauy)"]},  # si besoin à estimer si u10 et v10 lu à partir de son expression en utilisant numexpr
                       "heat" : {'name':["heat"]},
                       "swnet" : {'name':["swnet"]},
                       "radnet" : {'name':["RN_SEA"]},
                       "hflux" : {'name':["GFLUX_SEA"]},
                       "swd" : {'name':["SWD_SEA"]},
                       "swu" : {'name':["SWU_SEA"]},
                       "lwd" : {'name':["LWD_SEA"]},
                       "lwu" : {'name':["LWU_SEA"]},
                       "hflat" : {'name':["LE_SEA"]}, # flux de chaleur sensible
                       "hfsens" : {'name':["H_SEA"]}, # flux de chaleur latent
                       "evap" : {'name':["evap"]},
                       "rain" : {'name':["ACPRT"]},  #ACPRR autrement pour pluie seulement ?
                       "swow" : {'name':["ACPRSnon"]}, # cval on s en fout ? un peu mon n'veu !
                       "watf" : {'name':[""]},  # eau des lacs rivieres
                       "pres" : {'name':["PABST"]}, }
        self.outfield = { "u10m" : {'name':["MNH__U10"]},
                          "v10m" : {'name':["MNH__V10"]},
                          "wind" : {'name':["MNH_WIND"]},
                          "tauu" : {'name':["MNH_FMSU"]},
                          "tauv" : {'name':["MNH_FMSV"]},
                          "tau" : {'name':["MNH_FMSM"]},
                          "heat" : {'name':["MNH_HEAT"]},  # non solar heat flux, positive when downward (atmos --> ocean)
                          "radnet" : {'name':["MNH_RADN"]}, # net radiative flux (SW + LW), positive when downward (atmos --> ocean)
                          "hflux" : {'name':["MNH_HFLX"]}, # total heat fluw at air-sea interface, positive when downward (atmos --> ocean)
                          "swd" : {'name':["MNH_HSWD"]},  # short wave downward 
                          "swu" : {'name':["MNH_HSWU"]},  # short wave upward
                          "lwd" : {'name':["MNH_HLWD"]},  # long wave downward
                          "lwu" : {'name':["MNH_HLWU"]},  # long wave upward
                          "swnet" : {'name':["MNH_SNET"]}, # net short wave positive when downward (atmos --> ocean)
                          "lwnet" : {'name':["MNH_LNET"]}, # net long wave positive when downward (atmos --> ocean)
                          "hfsens" : {'name':["MNH_FSEN"]}, # positive when upward (ocean --> atmos)
                          "hflat" : {'name':["MNH_FLAT"]}, # positive when upward (ocean --> atmos)
                          "evap" : {'name':["MNH_EVAP"]}, # positive when upward (ocean --> atmos)
                          "rain" : {'name':["MNH_RAIN"]}, # positive when downward (atmos --> ocean)
                          "snow" : {'name':["MNH_SNOW"]},
                          "watf" : {'name':["MNH_WATF"]},
                          "pres" : {'name':["MNH_PRES"]}, }
        self.mask2wave = {"drywet"  : {'name':"MNH_WWDT"},
                          "drywet_x" : {'name':"MNH_WWDU"},
                          "drywet_y" : {'name':"MNH_WWDV"} }
        self.mask2oce = {"drywet"  : {'name':"MNH_OWDT"},
                         "drywet_x" : {'name':"MNH_OWDU"},
                         "drywet_y" : {'name':"MNH_OWDV"} }
        self.surfindex = 2
        self.yxindex = [(1,-1),(1,-1)]
        self.tindex = 0
        self.fillvalue = 999.

class Croco(Model):
    """ Set parameters relative to the CROCO code """

    def __init__(self):

        Model.__init__(self)
        self.model="CROCO"
        self.earth_radius = 6.371315e+6
        self.latitude=["lat_rho","lat_u","lat_v","lat_psi"]
        self.longitude=["lon_rho","lon_u","lon_v","lon_psi"]
        self.dx="1/pm"
        self.dy="1/pn"
        self.loc_u="_u"
        self.loc_v="_v"
        self.loc_f="_psi"
        self.grid = "C-uvt"  # u/v at the right/top of t
        self.mask=["mask_rho","mask_u","mask_v"]
        self.topo=["h"]
        self.dims = { "time" : {'name':"time"},
                      "level" : { 'name':"s_rho"},
                      "nj" : { 'name' : "eta_rho"},
                      "ni" : { 'name' : "xi_rho"}, }
        self.field = { "ssh" : {'name':["zeta"]},
                       "sst" : {'name':["temp"]},
                       "ssu" : {'name':["u"]},
                       "ssv" : {'name':["v"]} }
        self.outfield = { "ssh" : {'name':["SRMSSHV0"]},
                          "sst" : {'name':["SRMSSTV0"]},
                          "ssu" : {'name':["SRM_UZV0"]},
                          "ssv" : {'name':["SRM_VZV0"]} }
        self.mask2wave = {"drywet"  : {'name':"SMR_WWDT"},
                          "drywet_x" : {'name':"SMR_WWDU"},
                          "drywet_y" : {'name':"SMR_WWDV"} }
        self.mask2atm = {"drywet"  : {'name':"SMR_AWDT"},
                         "drywet_x" : {'name':"SMR_AWDU"},
                         "drywet_y" : {'name':"SMR_AWDV"} }
        self.surfindex = -1
        self.yxindex = [(None,None),(None,None)]
        self.tindex = 0


class Mars(Model):
    """ Set parameters relative to the MARS code """

    def __init__(self):

        Model.__init__(self)
        self.model="MARS"
        self.earth_radius = 6.367517e+6
        self.latitude=["latitude","latitude_u","latitude_v","latitude_f"]
        self.longitude=["longitude","longitude_u","longitude_v","longitude_f"]
        self.dx="dx"
        self.dy="dy"
        self.loc_u="_u"
        self.loc_v="_v"
        self.loc_f="_f"
        self.grid = "C-tuv"  # u/v at the right/top of t
        self.mask=None
        self.topo=["h0","hx","hy"]
        self.dims = { "time" : {'name':"time"},
                      "level" : { 'name':"level"},
                      "nj" : { 'name' : "nj"},
                      "ni" : { 'name' : "ni"}, }
        self.field = { "ssh" : {'name':["SSH","XE","ssh","xe"]},
                       "sst" : {'name':["TEMP"]},
                       "ssu" : {'name':["UZ","uz"]},
                       "ssv" : {'name':["VZ","vz"]} }
        self.outfield = { "ssh" : {'name':["MARS_SSH"]},
                          "sst" : {'name':["MARS_SST"]},
                          "ssu" : {'name':["MARSASSU","MARSWSSU"]},
                          "ssv" : {'name':["MARSASSV","MARSWSSV"]} }
        self.mask2wave = {"drywet"  : {'name':"MARSWWDH"},
                          "drywet_x" : {'name':"MARSWWDU"},
                          "drywet_y" : {'name':"MARSWWDV"} }
        self.mask2atm = {"drywet"  : {'name':"MARSAWDT"},
                         "drywet_x" : {'name':"MARSAWDU"},
                         "drywet_y" : {'name':"MARSAWDV"} }
        self.surfindex = -1
        self.yxindex = [(1,None),(1,None)]
        self.tindex = 0
        self.fillvalue = 999.

class Toy(Model):
    """ Set parameters relative to the TOY code """

    def __init__(self):

        Model.__init__(self)
        self.outfield = { "ssh" : {'name':["TOY__SSH"]},
                          "sst" : {'name':["TOY__SST"]},
                          "ssu" : {'name':["TOY_ASSU","TOY_WSSU"]},
                          "ssv" : {'name':["TOY_ASSV","TOY_WSSV"]},
                          "u10m": {'name':["TOY_U10M"]},
                          "v10m": {'name':["TOY_V10M"]},
                          "wind" : {'name':["TOY_WIND"]},
                          "tauu" : {'name':["TOY_FMSU"]},
                          "tauv" : {'name':["TOY_FMSV"]},
                          "tau" : {'name':["TOY_FMSM"]},
                          "heat" : {'name':["TOY_HEAT"]},  # non solar heat flux, positive when downward (atmos --> ocean)
                          "radnet" : {'name':["TOY_RADN"]}, # net radiative flux (SW + LW), positive when downward (atmos --> ocean)
                          "hflux" : {'name':["TOY_HFLX"]}, # total heat fluw at air-sea interface, positive when downward (atmos --> ocean)
                          "swd" : {'name':["TOY_HSWD"]},  # short wave downward 
                          "swu" : {'name':["TOY_HSWU"]},  # short wave upward
                          "lwd" : {'name':["TOY_HLWD"]},  # long wave downward
                          "lwu" : {'name':["TOY_HLWU"]},  # long wave upward
                          "swnet" : {'name':["TOY_SNET"]}, # net short wave positive when downward (atmos --> ocean)
                          "lwnet" : {'name':["TOY_LNET"]}, # net long wave positive when downward (atmos --> ocean)
                          "hfsens" : {'name':["TOY_FSEN"]},
                          "hflat" : {'name':["TOY_FLAT"]},
                          "evap" : {'name':["TOY_EVAP"]},
                          "rain" : {'name':["TOY_RAIN"]},
                          "swow" : {'name':["TOY_SNOW"]},
                          "watf" : {'name':["TOY_WATF"]},
                          "pres" : {'name':["TOY_PRES"]},
                          "t0m1" : {'name':["TOY_T0M1"]},
                          "hs" : {'name':["TOY__AHS","TOY__OHS"]},
                          "dir" : {'name':["TOY__DIR"]},      # necessaire car ne sort que cela dans ses outputs
                          "cdir" : {'name':["TOY_CDIR"]},
                          "sdir" : {'name':["TOY_SDIR"]},
                          "bhd" : {'name':["TOY__BHD"]},
                          "twox" : {'name':["TOY_TWOX"]},
                          "twoy" : {'name':["TOY_TWOY"]},
                          "tusx" : {'name':["TOY_TUSX"]},
                          "tusy" : {'name':["TOY_TUSY"]},
                          "ussx" : {'name':["TOY_USSX"]},
                          "ussy" : {'name':["TOY_USSY"]},
                          "ubr" : {'name':["TOY__UBR"]},
                          "ubrx" : {'name':["TOY_UBRX"]},
                          "ubry" : {'name':["TOY_UBRY"]},
                          "foc" : {'name':["TOY__FOC"]},
                          "fbb" : {'name':["TOY__FBB"]},
                          "tbbx" : {'name':["TOY_TBBX"]},
                          "tbby" : {'name':["TOY_TBBY"]},
                          "tawx" : {'name':["TOY_TAWX"]},
                          "tawy" : {'name':["TOY_TAWY"]},
                          "lm" : {'name':["TOY___LM"]},
                          "cha" : {'name':["TOY__CHA"]},
                          "oHS" : {'name':["TOY__oHS"]},
                          "fp" : {'name':["TOY___FP"]},
                          "tp" : {'name':["TOY___TP"]},
                        }
        self.mask2wave = {"drywet"  : {'name':"TOY_WWDH"},
                          "drywet_x" : {'name':"TOY_WWDU"},
                          "drywet_y" : {'name':"TOY_WWDV"} }
        self.mask2atm = {"drywet"  : {'name':"TOY_AWDT"},
                         "drywet_x" : {'name':"TOY_AWDU"},
                         "drywet_y" : {'name':"TOY_AWDV"} }
        self.mask2oce = {"drywet"  : {'name':"TOY_OWDT"},
                         "drywet_x" : {'name':"TOY_OWDU"},
                         "drywet_y" : {'name':"TOY_OWDV"} }
        self.dims = { "time" : {'name':"time"},
                      "level" : { 'name':"level"},
                      "nj" : { 'name' : "nlat"},
                      "ni" : { 'name' : "nlon"}, }


class Oasis(Model):
    """ Set variable relative to the outputs from OASIS
        One assume the grid is done when fields are extracted from OASIS export files
        WARNING : self.field must be defined by the user because depends on each coupling
    """

    def __init__(self):

        Model.__init__(self)
        self.model="OASIS"
        self.earth_radius = None  # user definition
        self.latitude=["lat","LAT","latitude","latitude_u","latitude_v","latitude_f","lat_rho","lat_u","lat_v","lat_psi"]
        self.longitude=["lon","LON","longitude","longitude_u","longitude_v","longitude_f","lon_rho","lon_u","lon_v","lon_psi"]
        self.dx="not available"  # user definition
        self.dy="not available"  # user definition
        self.loc_u="_u"
        self.loc_v="_v"
        self.loc_f="_f"
        self.grid = None  # user definition
        self.mask= None  # user definition
        self.topo=["h0","hx","hy"]
        self.dims = { "time" : {'name':"time"},
                      "level" : { 'name':"level"},
                      "nj" : { 'name' : "nj"},
                      "ni" : { 'name' : "ni"}, }
        self.field = { "u10" : {'name':["MNH__U10"]},  # to be completed
                       "v10" : {'name':["MNH__V10"]},
                       "tauu" : {'name':["MNH_FMSU"]},
                       "tauv" : {'name':["MNH_FMSV"]},
                       "swnet" : {'name':["MNH_SNET"]},
                       "heat" : {'name':["MNH_HEAT"]},  # non solar heat flux, positive when downward (atmos --> ocean)
                       "evap" : {'name':["MNH_EVAP"]},
                       "rain" : {'name':["MNH_RAIN"]},
                       "pres" : {'name':["MNH_PRES"]},}
        self.outfield = { "u10" : {'name':["MNH__U10"]},
                          "v10" : {'name':["MNH__V10"]},
                          "tauu" : {'name':["MNH_FMSU"]},
                          "tauv" : {'name':["MNH_FMSV"]},
                          "heat" : {'name':["MNH_HEAT"]},
                          "swnet" : {'name':["MNH_SNET"]},
                          "evap" : {'name':["MNH_EVAP"]},
                          "rain" : {'name':["MNH_RAIN"]},
                          "pres" : {'name':["MNH_PRES"]}, }
        self.mask2wave = {"drywet"  : {'name':"MNH_WWDT"},
                          "drywet_x" : {'name':"MNH_WWDU"},
                          "drywet_y" : {'name':"MNH_WWDV"} }
        self.mask2oce = {"drywet"  : {'name':"MNH_OWDT"},
                         "drywet_x" : {'name':"MNH_OWDU"},
                         "drywet_y" : {'name':"MNH_OWDV"} }
        self.surfindex = None
        self.yxindex = [(None,None),(None,None)]


