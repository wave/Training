#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Valerie Garnier'
__email__ = 'vgarnier@ifremer.fr'
__date__ = '2015-03-21'
__doc__ = """  DESCRIPTION \n   =========== \n

   Creation of grid and restart files for oasis use

   REQUIRED :
   ----------

   a list of input files
   --outfile : name of the output file where are gathjered all the variables
   --process or --var
   

   OPTIONAL :
   ----------

   --indir : to specify the directory where are the data to be processes
   --outdir : to specify the directory where you want to save the figures
   --process : for special processing ["OASIS" : get the name of the data from the files names]
   --var : varname available in each input file

   EXAMPLE :
             ! plusieurs fichiers sans specifier les variables (OASIS)
             connect_files.py MNH_EVAP_mesonh_05.nc,MNH_FMSU_mesonh_01.nc,MNH_FMSV_mesonh_02.nc,MNH_HEAT_mesonh_03.nc,MNH_PRES_mesonh_07.nc,MNH_RAIN_mesonh_06.nc,MNH_SNET_mesonh_04.nc --indir /home/caparmor-work/marsdev/NONREGRESSION/COUPLING/INPUT_FILES/OASIS --outfile oasis.nc --process OASIS

             ! une variable de moins que le nb total de fichiers
             connect_files.py ref.nc,sf2.nc,sf2.nc,sf2.nc,sf2.nc,sf2.nc --var ZON10M_SEA,GFLUX_SEA,swnet,heat,evap --outfile rstrt_MNH_001.nc

             ! 1 fichier a ajouter contenant plusieurs variables
             connect_files.py ref.nc,sf2.nc --outfile rstrt_MNH_001.nc


   END DESCRIPTION \n   ===============
   """


import os, time
import argparse
import shutil
import socket
import subprocess
import itertools
import netCDF4


def setmodenv( hostname ):

    ''' set the correct environment path '''
    if hostname in ["caparmor","service7","service8","service13"]:
        cmd = os.popen('/usr/bin/modulecmd python use /export/home13/previmer/op/bin/modules' )
        exec(cmd)
        cmd = os.popen('/usr/bin/modulecmd python load netcdf4' )
        exec(cmd)
        path_nco = "/export/home13/previmer/op/bin/nco-netcdf4/bin/"
    elif "".join([v for v in list(itertools.chain.from_iterable( hostname )) if not v.isdigit()]) == "rin":
        cmd = os.popen('/usr/bin/modulecmd python use /export/home13/previmer/op/bin/modules' )
        exec(cmd)
        cmd = os.popen('/usr/bin/modulecmd python load netcdf4' )
        exec(cmd)
        path_nco = "/export/home13/previmer/op/bin/nco-netcdf4/bin/"
    else:
        path_nco = "/usr/bin"

    return path_nco

if __name__ == '__main__' :

    # Parser to specify the choice of the user through arguments given to the script
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        prog = 'connect_files.py',
        description=__doc__,
        usage="%(prog)s --indir /home/caparmor-work/marsdev/NONREGRESSION/COUPLING/INPUT_FILES/OASIS --outfile PATH/file.nc --tidegauge 'BREST','SAINT-MALO' --print --waves 'Q1,O1,P1,K1,N2,M2,S2,K2,M4' --fes2004 --fes2012 --cstfrance",
        version=__date__
        )

    parser.add_argument("infile", help="NetCDF files from where you will extract the field",default=None)
    parser.add_argument("--outfile", help="Name of the output NetCDF osen by the user",default=None)
    parser.add_argument("--var", help="Name of the variables to be added in the first file of the list \n (must be in the same order as the list of input files)",default=None)

    parser.add_argument("--process", help="For special processing [i.e. OASIS]",default=None)

    parser.add_argument('--indir', help='Directory where are the data', default="./")
    parser.add_argument('--outdir', help='Directory where are saved the figures', default="./")

    parser.add_argument("-p","--print", dest='debug', action='store_true',help="print intermediate variables")
    parser.add_argument(     "--chainvalid", dest='chainvalid', action='store_true',help="do not append history of netcdf files (chain of validation purpose only)")

    options = parser.parse_args()
    if options.debug: print "\n options summary %s \n" %options


    # checking of the options
    # !!!!!!!!!!!!!!!!!!!!!!!

    if not options.outfile: parser.error('Specify --outfile, the name of the output file')
    outfile = os.path.join(options.outdir,options.outfile)

    files = options.infile.split(',')

    if not options.var:
        if options.process == "OASIS":
            list_var = [name[0:8] for name in files[1:]]
        else:
            file0 = netCDF4.Dataset( os.path.join(options.indir,files[0]) )
            list_var_ref = [str(v) for v in file0.variables.keys()]
            list_var = []
            for ifi in range(1,len(files)):
                file1 = netCDF4.Dataset( os.path.join(options.indir,files[ifi]) )
                list_var.extend( [str(v) for v in file1.variables.keys()] )
                var_redondant = list( set(list_var_ref) & set(list_var) )
                for v in var_redondant: list_var.remove(v)
            del file0,list_var_ref,file1,var_redondant
    else:
        list_var = options.var.split(',')
    print "Add the following variables %s" %(list_var)
    print "into the first file %s" %files[0]

    # Save the command line
    # !!!!!!!!!!!!!!!!!!!!!

    f = open(".".join((".".join(('cmdline',__file__.split('/')[-1].replace('.py',''))),'log')) , "a" )
    f.write( time.asctime() +'\n' )
    f.write( __file__ +' with the following options:\n' )
    f.write(str(options)+'\n')
    f.write('\n'*2)
    f.close()

    # Miscellaneous
    # !!!!!!!!!!!!!

    # Get the correct path of nco according to tne machine
    path_nco = setmodenv( socket.gethostname() )


    # Processing
    # !!!!!!!!!!

    # Create the output file
    print "\n"
    print "Create the output file %s \n" %outfile
    shutil.copy(os.path.join(options.indir,files[0]),outfile)

    for i,f in enumerate(files[1:]):

        file = os.path.join(options.indir,f)
        vars = netCDF4.Dataset( file ).variables.keys()

        list_remove = []

        for varname in list_var:

            if varname in vars:
              list_remove.append(varname)

        if list_remove:
            varname = ','.join(list_remove)
            if options.chainvalid:
                cmd = " ".join(( os.path.join(path_nco,"ncks"), "-h -A -v",varname,file,outfile ))
            else:
                cmd = " ".join(( os.path.join(path_nco,"ncks"), "-A -v",varname,file,outfile ))
            print "Add the %s variable" %varname
            s = subprocess.call(cmd.split(" "))

        # remove saved variables from the list
        for v in list_remove:
            list_var.remove(v)
            list_remove = []


    print "That's it, guy"
