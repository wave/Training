#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Valerie Garnier, Joris Pianezze '
__email__ = 'vgarnier@ifremer.fr'
__date__ = '2015-03-12'
__doc__ = """  DESCRIPTION \n   =========== \n

    Add empty field to a netCDF file with model NetCDF properties


    REQUIRED :
    ----------

        an [list of] input file
        --model : model from which data come from [MARS, CROCO, MNH, WW, OASIS or TOY ]
        --varname : name of the variable (key from toolsoasis.py point of view) to be added
        --dimx name  (make sure it is the correct name in the file)
        or/and
        --dimy name  (make sure it is the correct name in the file)
        or/and
        --dimz name  (make sure it is the correct name in the file)
        or/and
        --dimt name  (make sure it is the correct name in the file)

    OPTIONAL :
    ----------

        --indir : to specify the directory where are the data to be processes
        --outdir : to specify the directory where you want to save the figures
        -p : to print intermediate logs

    EXAMPLE :

        add_emptyfield_toncfile.py file.nc --varname evap --model TOY --dimx nlon --dimy nlat


   END DESCRIPTION \n   ===============
   """


import os, time, sys
import shutil
import argparse
import netCDF4
import numpy as np
import toolsoasis


def checkDims(infile,dimname,dimvar,model):

    """

    :param infile:opened NetCDF file (objet)
    :param dimname:name of the dimension type ['time','level','longitude' or 'latitude']
    :param dimvar:real name of the dimension in the netCDF file
    :return:
    """
    print dimname,dimvar
    try:
        if infile.dimensions[ dimvar ]:
            value = int( "".join([el for el in str(infile.dimensions[ dimvar ])[-10:] if hasNumbers(el) ]) )
    except:
        print "    None '%s' axis in the input file. " %dimvar #%(dimvar,infile.model_name)
        print "    Have you properly specified the dimension name ? It must be among '%s'" %model.dims[dimname]["name"]
        sys.exit()

    return dimvar,value

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


if __name__ == '__main__' :

    # Parser to specify the choice of the user through arguments given to the script
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        prog = 'add_emptyfield_toncfile.py',
        description=__doc__,
        usage="%(prog)s infile.nc --model MNH --varname evap --dimx X --dimy Y",
        version=__date__
        )

    parser.add_argument("infile", help="NetCDF files from where you will extract the field",default=None)
    parser.add_argument("--model", help="model whose data come from [MARS, CROCO, MNH, WW, OASIS or TOY]",default=None)
    parser.add_argument("--varname", help="name of the variable to be added among \n "
                                          "'ssh,sst,ssu,ssv,u10m,v10m,wind,tau,tauu,tauv,snet,heat,hfsens,hflat,rain,snow,watf,pres, \n"
                                          " t0m1,oHS,cdir,sdir,bhd,twox,twoy,ubr,ubrx,ubry,foc,tawx,tawy,lm,CHA,aHS,TP,fp'",default=None)
    parser.add_argument("--dimt", help="name of the time dimension [default: %(default)s]",default=None)
    parser.add_argument("--dimz", help="name of the level dimension [default: %(default)s]",default=None)
    parser.add_argument("--dimy", help="name of the latitude dimension [default: %(default)s]",default=None)
    parser.add_argument("--dimx", help="name of the longitude dimension [default: %(default)s]",default=None)

    parser.add_argument('--indir', help='Directory where are the data', default="./")
    parser.add_argument('--outdir', help='Directory where are saved the figures', default="./")

    parser.add_argument("-p","--print", dest='debug', action='store_true',help="print intermediate variables")


    options = parser.parse_args()
    if options.debug: print "\n options summary %s \n" %options


    # checking of the options
    # !!!!!!!!!!!!!!!!!!!!!!!

    model = options.model
    if options.model not in ["MARS","CROCO","MNH","WW","OASIS","TOY"]: parser.error('Specify --model [MARS, CROCO, MNH, WW, OASIS or TOY]')

    # Save the command line
    # !!!!!!!!!!!!!!!!!!!!!

    f = open(".".join((".".join(('cmdline',__file__.split('/')[-1].replace('.py',''))),'log')) , "a" )
    f.write( time.asctime() +'\n' )
    f.write( __file__ +' with the following options:\n' )
    f.write(str(options)+'\n')
    f.write('\n'*2)
    f.close()

    # Processing
    # !!!!!!!!!!

    # Initialization
    if options.model == "MARS": input = toolsoasis.Mars()
    elif options.model == "CROCO": input = toolsoasis.Croco()
    elif options.model == "MNH": input = toolsoasis.MesoNH()
    elif options.model == "WW": input = toolsoasis.WW3()
    elif options.model == "OASIS": input = toolsoasis.Oasis()
    elif options.model == "TOY": input = toolsoasis.Toy()


    # Variables
    # ---------

    for ifile,file in enumerate(options.infile.split(',')):

        print "\n"
        print "Save a copy of the file (in case a problem occurs during the processing) %s" %os.path.join(options.indir,'.'.join((file,'backup')))
        print os.path.join(options.indir,file)
        print os.path.join(options.outdir,'.'.join((file,'backup')))
        shutil.copy( os.path.join(options.indir,file), os.path.join(options.outdir,'.'.join((file,'backup'))) )

        print "\n"
        print "Read the file %s" %os.path.join(options.indir,file)

        # Read the fields of interest
        infile = netCDF4.Dataset(os.path.join(options.indir,file),'r+')

        try:
            for vname in input.outfield[options.varname]['name']:
                if infile.variables[ vname ]:
                    print "The %s variable already exists in the ncfile"
                    print "Nothing to do !!!!"

        except:
            print "The %s variable does not already exist and an empty field is added to the ncfile"

            # get the available dimensions in the file
            dimnames = []
            dims = []
            if options.dimt:
                [name,value] =  checkDims(infile,"time",options.dimt,input)
                dims.append(value)
                dimnames.append(name)
            if options.dimz:
                [name,value] = checkDims(infile,"level",options.dimz,input)
                dims.append(value)
                dimnames.append(name)
            if options.dimy:
                [name,value] = checkDims(infile,"latitude",options.dimy,input)
                dims.append(value)
                dimnames.append(name)
            if options.dimx:
                [name,value] = checkDims(infile,"longitude",options.dimx,input)
                dims.append(value)
                dimnames.append(name)
            dimnames = tuple(dimnames)
            dims = tuple(dims)

            # create an empty variable
            var=np.zeros(dims)

            # add the empty variable in the netCDF file
            input.save_var(infile,vname,var,dims=dimnames,keyname=options.varname)


        # Close the input file
        infile.close()


    print "\n"
    print "That's it, guy"
