#!/bin/bash -e

obs_dir="/home/datawork-cersat-public/provider/cci_seastate/products/v3/data/satellite/altimeter/l2p/jason-3/2018/"
file_regexp='*-201801*.nc'

rm -f obs.list
for file in $(find $obs_dir -name "$file_regexp" | sort -u)
do
  if [ ! -z "$(ncdump -h $file | grep "relative_pass_number = 61")" ]
  then
    echo $file >> obs.list
  fi
done
