#!/bin/bash -e

obs_dir="/home/datawork-cersat-public/provider/cci_seastate/products/v3/data/satellite/altimeter/l2p/jason-3/"
file_regexp='*.nc'

rm -f obs.list
for year in 2018 2019 2020
do
  for file in $(find $obs_dir/$year/ -name "$file_regexp" | sort -u)
  do
    if [ ! -z "$(ncdump -h $file | grep "relative_pass_number = 61")" ]
    then
      echo $file >> obs.list
    fi
  done
done
