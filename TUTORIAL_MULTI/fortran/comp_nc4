#!/bin/bash -e
#
# Compiles a fortran90 code
#

if [ $# -lt 1 ]
then
  echo '  need at least one argument :'
  echo '  $1 : prog name without extension'
  echo '  $2 (optional) : [debug | mpi | mpi_debug ]'
  exit
fi


debug=0
mpi=0
if [ $# -eq 2 ] ; then
  if [ "$2" == "debug" ] ; then
    debug=1
  elif [ "$2" == "mpi" ] ; then
    mpi=1
  elif [ "$2" == "mpi_debug" ] ; then
    mpi=1
    debug=1
  fi
fi

# comp, inlcude, lib
comp=$($NETCDF_CONFIG --fc)
incs=$($NETCDF_CONFIG --fflags)
libs=$($NETCDF_CONFIG --flibs)
echo $comp


# pre-process if necessary
prep='no'
if [ -e ${1}.F90 ] ; then
  prep='yes'
fi 
prog=${1}
if [ "$prep" = "yes" ] ; then
  echo 'preprocessing...'
  if [ $mpi = 1 ] ; then
    $comp -Ddist -E ${prog}.F90 > ${prog}.f90
  else
    $comp -E ${prog}.F90 > ${prog}.f90
  fi
fi

# options
if [ "$comp" == "ifort" ] || [ "$comp" == "mpiifort" ] || [ "$comp" == "ifort -lmpi" ]
then
  # common options
  optc="-c -no-fma -ip -i4 -real-size 32 -fp-model precise -assume byterecl -convert big_endian -fno-alias -fno-fnalias -fpp"
  optl="-o $prog"

  if [ $debug = 1 ]
  then
    #debugging
    optc="$optc -O0 -g -debug all -warn all -check all -check noarg_temp_created -fp-stack-check -heap-arrays -traceback"
    optl="$optl -O0 -g -traceback"
  else
    #optimized
    optc="$optc -O3"
    optl="$optl -O3"
  fi
  # mpi enabled
  #if [ $mpi = 1 ] ; then
  #  echo 'mpi linking'
  #  optl="-O3 -o $prog $MPI_LIBS"
  #fi
elif [ $comp = "gfortran" ] || [ "$comp" == "mpif90" ]
then
  # common options
  optc="-c -fno-second-underscore -ffree-line-length-none -fconvert=big-endian -fpp"
  optl="-o $prog"

  if [ $debug = 1 ]
  then
    #debugging
    optc="$optc -O0 -g -Wall -fcheck=all -ffpe-trap=invalid,zero,overflow -frecursive -fbacktrace"
    optl="$optl -O0 -g -fbacktrace"
  else
    #optimized
    optc="$optc -O3"
    optl="$optl -O3"
  fi
else
  echo "PROBLEM: compilator not recognized : $comp"
fi



#compilation#
echo "$comp $optc $incs $prog.f90"
$comp $optc $incs $prog.f90

#linkage#
echo "$comp $optl $prog.o $libs"
$comp $optl $prog.o $libs

#remove prepro mpi
if  [ "$prep" = "yes" ] ; then
  rm ${prog}.f90
fi
