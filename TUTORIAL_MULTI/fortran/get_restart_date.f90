!
! 19981120  add sections in the main program to read ECMWF verion 0 GRIB
!
! New main program to solve 'GRIB' in BDS problem   19971105
!
! This GRIB decoder has been tested on SGI/iris4, HP/ux, SUN/sunOS4.1.3 and
!*********************************************************************
      FUNCTION julday(id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!
!*********************************************************************
      IMPLICIT NONE
	   INTEGER id,mm,iyyy
      INTEGER IGREG 
      INTEGER*4 ja,jm,jy
      INTEGER*4 julday
      IGREG=15+31*(10+12*1582)
      jy=iyyy
      IF (jy.EQ.0) WRITE(6,*) 'There is no zero year !!'
      IF (jy.LT.0) jy=jy+1
      IF (mm.GT.2) THEN
         jm=mm+1
      ELSE
         jy=jy-1
         jm=mm+13
      ENDIF
      julday=INT(365.25*jy)+int(30.6001*jm)+id+1720995
      IF (id+31*(mm+12*iyyy).GE.IGREG) THEN
         ja=INT(0.01*jy)
         julday=julday+2-ja+INT(0.25*ja)
      ENDIF
      RETURN
      END 

!*********************************************************************
      SUBROUTINE caldat(julian,id,mm,iyyy)
! See numerical recipes 2nd ed. The order of month and day have been swapped!
!*********************************************************************
      IMPLICIT NONE
	   INTEGER*4 julian
      INTEGER id,mm,iyyy
      INTEGER IGREG
      INTEGER*4 ja,jalpha,jb,jc,jd,je

      IGREG=2299161
      if (julian.GE.IGREG) THEN
         jalpha=INT(((julian-1867216)-0.25)/36524.25)
         ja=julian+1+jalpha-INT(0.25*jalpha)
      ELSE
         ja=julian
      ENDIF
      jb=ja+1524
      jc=INT(6680.+((jb-2439870)-122.1)/365.25)
      jd=365*jc+INT(0.25*jc)
      je=INT((jb-jd)/30.6001)
      id=jb-jd-INT(30.6001*je)
      mm=je-1
      IF (mm.GT.12) mm=mm-12
      iyyy=jc-4715
      IF (mm.GT.2) iyyy=iyyy-1
         IF (iyyy.LE.0) iyyy=iyyy-1
      RETURN
      END 


! 
!----------------------------------------------------------------------------
! compile:  f90 -o get_restart_date -convert big_endian -assume byterecl get_restart_date.f90
! en argument le nom du domaine 

program get_restart_date
IMPLICIT NONE

   INTEGER ntime
   
   INTEGER icode,icode2,icode3,icode4,icode5
      
   INTEGER I,J,K,k1,ka,jk,ii,isel,isum,iprnt,infunit,indxt,ibeg,imore,Ifile
   INTEGER iy,im,id,ih,ih2,im1,im2,id1,id2,ih1,ih12,imi1,imi2,is1,is2
   INTEGER iyt,imt,idt,iy1
   
   INTEGER(KIND=4) julday,jday1,jday2,jdayt
   INTEGER imi,isi,is
   INTEGER mgrib,ngrib,ist,lenpds,igridtype
   REAL d2r,latmin,latmax,lonmin,lonmax,templat,templat2
   REAL, ALLOCATABLE    :: U(:,:),V(:,:),ICE(:,:)
   CHARACTER*17 strN1
   CHARACTER*80 fwindsnap,str1
   CHARACTER*20 datenow,restartfile,moddeffile,grid, test
   CHARACTER               :: IDTST*31, VERTST*10
      CHARACTER            :: TYPE*4, FNAME*12, IDTST2*26, TNAME*30
   
   INTEGER                 :: NX, NY, NSEA, NSEAL, FLAGTR, NSEAT
   INTEGER, PARAMETER      :: LRB = 4         
   INTEGER                 :: TIME(2),TODAY,LRECL,MSPEC,IERR
   LRECL=100

! Reads NX from mod_def.ww3
   call getarg(1,grid)
   restartfile='restart.'//grid
   moddeffile='mod_def.'//grid
   !write(*,*)restartfile , moddeffile

   OPEN (1,FILE=moddeffile,FORM='UNFORMATTED',           &
                STATUS='OLD',IOSTAT=IERR)
   READ (1,IOSTAT=IERR)                     &
                IDTST, VERTST, NX, NY, NSEA !attention 
                ! ce n'est pas lordre réel
  ! WRITE(6,*) 'NX,NY,NSEA:' ,NX, NY, NSEA
   CLOSE(1)
! Open the restart file to define the record length LRECL

!   READ(*,'(A)') restartfile
   OPEN (1,FILE=restartfile,FORM='UNFORMATTED',ACCESS='DIRECT',   &
                RECL=LRECL,IOSTAT=IERR,STATUS='OLD')
          READ (1,REC=1,IOSTAT=IERR)                       &
            IDTST2, VERTST, TNAME, TYPE, NSEAT, MSPEC
         NX=NY!car b dans le READ inversion NX, NY, NSEA
         LRECL  = MAX ( LRB*MSPEC ,                          &
                    LRB*(6+(25/LRB)+(9/LRB)+(29/LRB)+(3/LRB)) )
   CLOSE(1)
 !  write(*,*)  'MSPEC',MSPEC,'LRECL', LRECL
! Reads the restart file with the proper record length

  ! OPEN(1,FILE=restartfile,FORM='UNFORMATTED',ACCESS='DIRECT',   &
   !             RECL=LRECL,IOSTAT=IERR,STATUS='OLD')
   

   OPEN(1,FILE=restartfile,FORM='UNFORMATTED',ACCESS='DIRECT',   &
                RECL=LRECL,IOSTAT=IERR,STATUS='OLD')
   READ (1,REC=2,IOSTAT=IERR)                       &
            TIME
   !WRITE(6,*) 'LRECL:',LRECL,IDTST2, VERTST, TNAME, TYPE, NSEAT, MSPEC
   CLOSE(1)
   !WRITE(*,*) 'time1:',TIME(1),'time2:',TIME(2)

   WRITE(*,'(I8,X,I6)') TIME

   end
!
