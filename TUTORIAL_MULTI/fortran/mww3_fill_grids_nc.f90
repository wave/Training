!==============================================================================
!==============================================================================

MODULE W3SERVMD

      PUBLIC
!
      INTEGER, PRIVATE        :: NDSTRC = 6, NTRACE = 0, PRFTB
      LOGICAL, PRIVATE        :: FLPROF = .FALSE.
!
      CONTAINS

!==============================================================================

      SUBROUTINE NEXTLN ( CHCKC , NDSI , NDSE )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         18-Nov-1999 |
!/                  +-----------------------------------+
!/
!/    15-Jan-1999 : Final FORTRAN 77                    ( version 1.18 )
!/    18-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Sets file pointer to next active line of input file, by skipping
!     lines starting with the character CHCKC.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       CHCKC   C*1   I  Check character for defining comment line.
!       NDSI    Int.  I  Input dataset number.
!       NDSE    Int.  I  Error output dataset number.
!                        (No output if NDSE < 0).
!     ----------------------------------------------------------------
!
!/ ------------------------------------------------------------------- /
      IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: NDSI, NDSE
      CHARACTER, INTENT(IN)   :: CHCKC*1
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
!/S      INTEGER, SAVE           :: IENT = 0
      INTEGER                 :: IERR
      CHARACTER               :: TEST*1
!/
!/ ------------------------------------------------------------------- /
!/
!/S      CALL STRACE (IENT, 'NEXTLN')
!
  100 CONTINUE
      READ (NDSI,900,END=800,ERR=801,IOSTAT=IERR) TEST
      IF (TEST.EQ.CHCKC) THEN
          GOTO 100
        ELSE
          BACKSPACE (NDSI,ERR=802,IOSTAT=IERR)
        END IF
      RETURN
!
  800 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,910)
      CALL EXTCDE ( 1 )
!
  801 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 2 )
!
  802 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR
      CALL EXTCDE ( 3 )
!
! Formats
!
  900 FORMAT (A)
  910 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     PREMATURE END OF INPUT FILE'/)
  911 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR IN READING FROM FILE'/               &
               '     IOSTAT =',I5/)
  912 FORMAT (/' *** WAVEWATCH-III ERROR IN NEXTLN : '/         &
               '     ERROR ON BACKSPACE'/                       &
               '     IOSTAT =',I5/)

END SUBROUTINE NEXTLN

!==============================================================================

SUBROUTINE EXTCDE ( IEXIT )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         06-Jan-1999 |
!/                  +-----------------------------------+
!/
!/    06-Jan-1998 : Final FORTRAN 77                    ( version 1.18 )
!/    23-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Perfor a program stop with an exit code.
!
!  2. Method :
!
!     Machine dependent.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IEXIT   Int.   I   Exit code to be used.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!  5. Called by :
!
!     Any.
!
!  9. Switches :
!
!     !/MPI  MPI finalize interface if active
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
      IMPLICIT NONE
!
!/MPI      INCLUDE "mpif.h"
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: IEXIT
!/
!/ ------------------------------------------------------------------- /
!/
!/MPI      INTEGER                 :: IERR_MPI
!/MPI      LOGICAL                 :: RUN
!/
!/ Test if MPI needs to be closed
!/
!/MPI      CALL MPI_INITIALIZED ( RUN, IERR_MPI )
!/MPI      IF ( RUN ) THEN
!/MPI          CALL MPI_BARRIER ( MPI_COMM_WORLD, IERR_MPI )
!/MPI          CALL MPI_FINALIZE (IERR_MPI )
!/MPI        END IF
!
!/F90      CALL EXIT ( IEXIT )
!/DUM      STOP
!/
!/ End of EXTCDE ----------------------------------------------------- /
!/
END SUBROUTINE EXTCDE

!==============================================================================

END MODULE W3SERVMD



!==============================================================================
!==============================================================================
!
PROGRAM MWW3_FILL_GRIDS_NC
!                  +---------------------------------------+
!                  | MWW3_FILL_GRIDS_NC                    |
!                  | WAVE TOOLS      SHOM & Ifremer        |
!                  |          F. Ardhuin                   |
!                  |          R. Magne                     |
!                  |          M. Accensi                   |
!                  |                            FORTRAN 90 |
!                  | Last update :             21-Nov-2010 |
!                  +---------------------------------------+
!
!    08-Jan-2008 : Created for ASCII files
!    16-Apr-2010 : Adapted for NC files
!    21-Nov-2010 : adapted for version 4.04
!    21-Nov-2011 : adapted for netCDF4 with multivariables
!
!  1. Purpose :
!
!     Fills fine grids with coarse grids in multigrid runs 
!
!  2. Method :
!
!
!  3. Parameters :
!
!
!input: file1 file2
!
  USE W3SERVMD
  USE NETCDF
  IMPLICIT NONE


  INTEGER                                 :: NDS, NDSE, ICODE, NFILL, NFIELD, GTYPE
  CHARACTER                               :: COMSTR*1
  REAL                                    :: MFILL1, MFILL2, SCALE1, SCALE2
  INTEGER                                 :: VARTYPE1, VARTYPE2
  INTEGER                                 :: IT, JF, iret
  CHARACTER*80                            :: name, nametmp
  INTEGER                                 :: xtype, ndims, dimids(3), natts
  INTEGER                                 :: nvars1, ncid1, dimid1(3), varid1(121)
  INTEGER                                 :: start1(3),count1(3),dimln1(3)
  INTEGER                                 :: nvars2, ncid2, dimid2(3), varid2(121)
  INTEGER                                 :: start2(3),count2(3),dimln2(3)
  CHARACTER(len=160)                      :: file,file1, file2, mod_def1, &
                                             mod_def2,rep1,rep2
  INTEGER                                 :: time1(2), time2(2)
  CHARACTER(len=100)                      :: ENAME, UNITS, FORMF 
  CHARACTER(5)                            :: linear(88), near(33)
  INTEGER                                 :: IFIELD1, IFIELD2
  INTEGER                                 :: nx1,ny1,nt1
  INTEGER                                 :: nx2,ny2,nt2
  REAL                                    :: lon_min1, lon_max1,lat_min1,lat_max1, FSC1
  REAL                                    :: lon_min2, lon_max2,lat_min2,lat_max2, FSC2
  REAL,DIMENSION(:,:),ALLOCATABLE         :: ARRAY1,ARRAY2
  REAL,DIMENSION(:),  ALLOCATABLE         :: LON1,LAT1, LON2, LAT2, TIMEARRAY1, TIMEARRAY2
  CHARACTER(LEN=30)                       :: FNAME0, FNAME1, FNAME2, FNAME3,      &
                                             FNAME4, FNAME5, FNAME6, FNAME7,      &
                                             FNAMEP, FNAMEG, FNAMEF, FNAMEI, GNAME
  CHARACTER(LEN=35)                       :: IDTST
  INTEGER                                 :: I, J, MTH, MK, NSEA, NX, NY, NBI, NFBPO, &
                                             NBO(0:9),NBO2(0:9),TRFLAG
  INTEGER, ALLOCATABLE                    :: MAPTMP(:,:),MAPTMP2(:,:),MAPSTA2(:,:)
  INTEGER, ALLOCATABLE                    :: I1FILL(:,:,:,:), I2FILL(:,:)
  REAL,    ALLOCATABLE                    :: RD(:,:)
  CHARACTER(LEN=10)                       :: VERTST
  LOGICAL                                 :: FLAGLL, GLOBAL, READMODDEF
  REAL                                    :: SX,SY,X0,Y0, &
                                             XN_1,XN_2,YN_1,YN_2, SX1,SY1,SX2,SY2, &
                                             XPT, YPT
  REAL                                    :: RX,RDX,RY,RDY, DIFRES
  INTEGER                                 :: IX2,IY2,IX1,IY1
  REAL, ALLOCATABLE                       :: ZB(:) 
  CHARACTER(LEN=10), PARAMETER            :: VERGRD = 'III  4.00 '


  !=================================
  ! 1. Reading files ...
  !=================================

  NDS=11


  OPEN(NDS,file='mww3_fill_grids_nc.inp',STATUS='OLD') 
  READ (NDS,'(A)') COMSTR
  IF (COMSTR.EQ.' ') COMSTR = '$'
  CALL NEXTLN(COMSTR, NDS, NDSE)
  READ(NDS,'(A100)') rep1
  CALL NEXTLN(COMSTR, NDS, NDSE)
  READ(NDS,'(A100)') rep2
  CALL NEXTLN(COMSTR, NDS, NDSE)
  READ(NDS,'(A100)') mod_def1
  CALL NEXTLN(COMSTR, NDS, NDSE)
  READ(NDS,'(A100)') mod_def2
  CLOSE(NDS)

  CALL GETARG(1,file)
  file1=trim(rep1)//'/'//trim(file)
  CALL GETARG(2,file)
  file2=trim(rep2)//'/'//trim(file)

  mod_def1=trim(mod_def1)
  mod_def2=trim(mod_def2)

!
!1.1 file1: coarse resolution file
!
  write(6,*) 'coarse res file : ', trim(file1)
  iret=NF90_OPEN (trim(file1),nf90_nowrite, ncid1)
  CALL CHECK_ERROR(IRET,__LINE__)


  !Reading of the grid parameters
  iret=NF90_INQ_DIMID  (ncid1, 'longitude', dimid1(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid1, 'longitude', varid1(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid1, dimid1(1), len=NX1)
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQ_DIMID  (ncid1, 'latitude', dimid1(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid1, 'latitude', varid1(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid1, dimid1(2),  len=NY1)
  CALL CHECK_ERROR(IRET,__LINE__)
       
  iret=NF90_INQ_DIMID  (ncid1, 'time', dimid1(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid1, dimid1(3),  len=NT1)
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQ_VARID  (ncid1, 'time', varid1(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid1, 'latitude', varid1(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid1, 'longitude', varid1(1))
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQUIRE  (ncid1, nVariables=nvars1)
  CALL CHECK_ERROR(IRET,__LINE__)

  ALLOCATE(LON1(NX1),LAT1(NY1),TIMEARRAY1(NT1))
  iret=NF90_GET_VAR (ncid1,varid1(1),lON1)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_GET_VAR (ncid1,varid1(2),lAT1)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_GET_VAR (ncid1,varid1(3),timearray1)
  CALL CHECK_ERROR(IRET,__LINE__)

  NFIELD=nvars1-3  ! This is not good for unstructured grids 

  ! test if there is a MAPSTA variable
  IFIELD1=4
  iret=NF90_INQUIRE_VARIABLE (ncid1,IFIELD1, name, xtype, ndims, dimids, natts)
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (name.EQ.'MAPSTA') THEN
    WRITE(6,*) name," is present in source file"
    IFIELD1=IFIELD1+1
    NFIELD=NFIELD-1
  END IF

  SX1=(LON1(NX1)-LON1(1))/(NX1-1)
  SY1=(LAT1(NY1)-LAT1(1))/(NY1-1)

  ALLOCATE(ARRAY1(NX1,NY1)) 
!  DO I=IFIELD1,nvars1
!    iret=NF90_INQUIRE_VARIABLE (ncid1,I, name, xtype, ndims, dimids, natts)
!    CALL CHECK_ERROR(IRET,__LINE__)
!    varid1(I)=I
!  END DO

!
!1.1 file2: high resolution file
!
  write(6,*) 'high res file : ', trim(file2)
  iret=NF90_OPEN (trim(file2),nf90_write, ncid2)
  CALL CHECK_ERROR(IRET,__LINE__)

  !Reading of the grid parameters
  iret=NF90_INQ_DIMID  (ncid2, 'longitude', dimid2(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid2, 'longitude', varid2(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid2, dimid2(1),  len=NX2)
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQ_DIMID  (ncid2, 'latitude', dimid2(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid2, 'latitude', varid2(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid2, dimid2(2),  len=NY2)
  CALL CHECK_ERROR(IRET,__LINE__)
       
  iret=NF90_INQ_DIMID  (ncid2, 'time', dimid2(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQUIRE_DIMENSION (ncid2, dimid2(3),  len=NT2)
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQ_VARID  (ncid2, 'time', varid2(3))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid2, 'latitude', varid2(2))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_INQ_VARID  (ncid2, 'longitude', varid2(1))
  CALL CHECK_ERROR(IRET,__LINE__)

  iret=NF90_INQUIRE  (ncid2, nVariables=nvars2)
  CALL CHECK_ERROR(IRET,__LINE__)

  ALLOCATE(LON2(NX2),LAT2(NY2),TIMEARRAY2(NT2))
  iret=NF90_GET_VAR(ncid2,varid2(1),LON2)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_GET_VAR(ncid2,varid2(2),LAT2)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_GET_VAR(ncid2,varid2(3),timearray2)
  CALL CHECK_ERROR(IRET,__LINE__)

  NFIELD=nvars1-3  ! This is not good for unstructured grids 
  READMODDEF=.TRUE.

  ! test if there is a MAPSTA variable
  IFIELD2=4
  iret=NF90_INQUIRE_VARIABLE (ncid2,IFIELD2, name, xtype, ndims, dimids, natts)
  CALL CHECK_ERROR(IRET,__LINE__)
  IF (name.EQ.'MAPSTA') THEN 
    WRITE(6,*) name," is present in dest file"
    IFIELD2=IFIELD2+1
    NFIELD=NFIELD-1
    READMODDEF=.FALSE.
    ALLOCATE ( MAPTMP(NY2,NX2) )
    ALLOCATE ( MAPTMP2(NX2,NY2) )
    iret=NF90_GET_VAR(ncid2,4,MAPTMP2)
    CALL CHECK_ERROR(IRET,__LINE__)
    MAPTMP=TRANSPOSE(MAPTMP2)
  END IF

  SX2=(LON2(NX2)-LON2(1))/(NX2-1)
  SY2=(LAT2(NY2)-LAT2(1))/(NY2-1)

  DIFRES=SX1*SY1/(SX2*SY2)
  ALLOCATE(ARRAY2(NX2,NY2))

!  DO I=IFIELD1,nvars2
!    iret=NF90_INQUIRE_VARIABLE (ncid2,I, name, xtype, ndims, dimids, natts)
!    CALL CHECK_ERROR(IRET,__LINE__)
!    varid2(I)=I
!  END DO
!
!1.3 Reading the mod_def to get MAPSTA 
!    Find ignored nodes (MAPSTA=0)
!

 IF (READMODDEF) THEN 

    OPEN (14,FILE=trim(mod_def2), FORM='UNFORMATTED',STATUS='OLD', IOSTAT=ICODE)
    IF (ICODE.EQ.0) THEN 
      READ (14)  IDTST , VERTST, NX, NY, NSEA, MTH, MK,                &
                 NBI, NFBPO, GNAME, FNAME0, FNAME1, FNAME2, FNAME3,    &
                 FNAME4, FNAME5, FNAME6, FNAME7, FNAMEP, FNAMEG,       &
                 FNAMEF, FNAMEI
      READ (14)  (NBO(I),I=0,NFBPO), (NBO2(I),I=0,NFBPO)
      ALLOCATE ( ZB(NSEA))
      ALLOCATE ( MAPTMP(NY,NX) )
      ALLOCATE ( MAPSTA2(NY,NX) )

      READ (14)  GTYPE, FLAGLL, GLOBAL
      IF (GTYPE.NE.1) THEN
        WRITE(6,*) 'THIS PROGRAM ONLY WORKS FOR RECT GRIDS, GTYPE.EQ.1' 
        STOP
      END IF
      READ (14)  SX, SY, X0, Y0
      READ (14)  ZB, MAPTMP
      CLOSE(14)

      MAPSTA2 = MOD(MAPTMP+2,8) - 2
    END IF
  END IF
!
! 2. Find excluded point value=0
!
    NFILL=0
    DO IX2=1,NX2
      DO IY2=1,NY2
        ! from mod_def
        IF (ALLOCATED(MAPSTA2)) THEN
          IF  ( MAPSTA2(IY2,IX2) .EQ. 0 .OR. (MAPTMP(IY2,IX2) .EQ. 0 .AND. DIFRES < 1.2)) NFILL=NFILL+1
        ! from netcdf
        ELSE
          IF  ( MAPTMP(IY2,IX2) .GE. 2 .OR. (MAPTMP(IY2,IX2) .EQ. 0 .AND. DIFRES < 1.2)) NFILL=NFILL+1
        END IF
      END DO 
    END DO
    ALLOCATE(I1FILL(2,NFILL,2,4),I2FILL(NFILL,2))
    ALLOCATE(RD(NFILL,4))
    I1FILL(:,:,:,:)=0
    I2FILL(:,:)=0
    RD(:,:)=0

  !Position of the point PT to interpolate   
  I=0
  DO IX2=1,NX2
    DO IY2=1,NY2
      IF  ( MAPTMP(IY2,IX2) .GE. 2 .OR. (MAPTMP(IY2,IX2) .EQ. 0 .AND. DIFRES < 1.2)) THEN 
        XPT=LON2(1)+(IX2-1)*SX2
        YPT=LAT2(1)+(IY2-1)*SY2  

        ! Calculate interpolation data
        RX     = ( XPT - LON1(1) ) / SX1
        RY     = ( YPT - LAT1(1) ) / SY1
!
! Added check on NX1 and NY1 bounds to allow filling by a grid 1 
! that may not contain all grid 2 (e.g. Black Sea and Med)
!
        IF (RX.GE.0.AND.RX.LE.REAL(NX1).AND.RY.GE.0.AND.RY.LT.REAL(NY1)) THEN
          I=I+1
          I2FILL(I,1)=IX2
          I2FILL(I,2)=IY2

!
! bilinear interpolation   I1FILL(1,*,*,*)
!                  
          RDX    =  MOD ( RX , 1. )
          RDY    =  MOD ( RY , 1. )
          IF ( RDX.LT.0. ) RDX = RDX + 1.
          IF ( RDY.LT.0. ) RDY = RDY + 1.
     
          IX1    = 1 + INT(RX)
          IY1    = 1 + INT(RY)
            
          I1FILL(1,I,1,1)  = IX1
          I1FILL(1,I,1,2)  = IX1 + 1
          I1FILL(1,I,1,3)  = IX1
          I1FILL(1,I,1,4)  = I1FILL(1,I,1,2)

          I1FILL(1,I,2,1)  = IY1
          I1FILL(1,I,2,2)  = IY1
          I1FILL(1,I,2,3)  = IY1 + 1
          I1FILL(1,I,2,4)  = I1FILL(1,I,2,3)
     
          RD(I,1)  = (1.-RDX) * (1.-RDY)
          RD(I,2)  =   RDX    * (1.-RDY)
          RD(I,3)  = (1.-RDX) *   RDY
          RD(I,4)  =   RDX    *   RDY 

!
! nearest point   I1FILL(2,*,*,*)
!
          IX1    = 1 + NINT(RX)
          IY1    = 1 + NINT(RY)
          I1FILL(2,I,1,1:4)  = IX1
          I1FILL(2,I,2,1:4)  = IY1

        END IF
      END IF
    END DO
  END DO

  start1(:)=1
  count1(1)=NX1
  count1(2)=NY1
  count1(3)=1
  start2(:)=1
  count2(1)=NX2
  count2(2)=NY2
  count2(3)=1

  ! List of variables for interpolation types
!  linear=(/'hs','U10','V10','wsf','uuss','vuss','mssx','mssy','foc', &
!           'spr','hs0','hs1','hs2','hs3','t','CgE','l','wl','t02','t0m1','cha','utus','vtus','utwo','vtwo', &
!           'faw','hsw','taw','mscx','mscy','sxy'/)

!  near=(/'dir','dp','fp','uabr','vabr','uubr','vubr','th0','th1','th2','th3','tp0','tp1','tp2','tp3'/)


        !list of variables for each interpolation method
        ! general variables // 82
        linear= (/'spr  ','dpt  ','wlv  ','hs   ','lm   ','t0m1 ','phs0 ','phs1 ', &
                  'phs2 ','phs3 ','phs4 ','phs5 ','pws0 ','pws1 ','pws2 ',         &
                  'pws3 ','pws4 ','pws5 ','t01  ','cge  ','t02  ','uabr ',         &
                  'vabr ','uubr ','vubr ','sxy  ','utwo ','vtwo ','bhd  ',         &
                  'utus ','vtus ','uuss ','vuss ','usf  ','uust ','vust ',         &
                  'faw  ','foc  ','utwo ','vtwo ','utaw ','vtaw ','utwa ',         &
                  'vtwa ','cha  ','wcc  ','tws  ','mssx ','mssy ','msc  ',         &
                  'p2s  ','p2l  ','ucur ','vcur ','uwnd ','vwnd ','ice  ',         &
        ! old var names 
                  'Depth','t    ','tm0m1','hs0  ','hs1  ','hs2  ','hs3  ','hs4  ', &
                  'hs5  ','wl   ','ws0  ','ws1  ','ws2  ','ws3  ','ws4  ','ws5  ', &
                  'CgE  ','Sxy  ','bh   ','wsf  ','U    ','V    ','U10  ','V10  ','CI   ', &
        ! new var names
                  'Ua   ','Ug   ','Va   ','Vg   ','Ha   ','Hg   '/)


        ! peak variables // 33
        near= (/'dp   ','dir  ','fp   ','ptp0 ','ptp1 ','ptp2 ','ptp3 ','ptp4 ', &
                'ptp5 ','pdir0','pdir1','pdir2','pdir3','pdir4',     &
                'pdir5','th0  ','th1  ','th2  ','th3  ','th4  ','th5  ','si0  ',  &
                'si1  ','si2  ','si3  ','si4  ','si5  ',                      &
        ! old var names
                'tp0  ','tp1  ','tp2  ','tp3  ','tp4  ','tp5  '/)      



  ! loop on time step
  DO IT=1,NT1
    start1(3)=IT
    start2(3)=IT
    DO JF=IFIELD1,nvars2
      iret=NF90_INQUIRE_VARIABLE (ncid1,JF, nametmp,xtype=VARTYPE1)
      CALL CHECK_ERROR(IRET,__LINE__)
      write(*,*) trim(nametmp)
      IF(VARTYPE1.EQ.NF90_CHAR) CYCLE
      iret=NF90_GET_ATT (ncid1,JF,'_FillValue', MFILL1)
      if (iret.NE.0) THEN
        if(VARTYPE1.EQ.NF90_SHORT) MFILL1=NF90_FILL_SHORT
        if(VARTYPE1.EQ.NF90_INT) MFILL1=NF90_FILL_INT
        if(VARTYPE1.EQ.NF90_FLOAT) MFILL1=NF90_FILL_FLOAT
        if(VARTYPE1.EQ.NF90_DOUBLE) MFILL1=NF90_FILL_DOUBLE
      END IF
      iret=NF90_GET_ATT (ncid1,JF,'scale_factor', SCALE1)
      if (iret.NE.0) SCALE1=1.0
      iret=NF90_GET_VAR (ncid1,JF,ARRAY1,start1,count1)
      CALL CHECK_ERROR(IRET,__LINE__)
      iret=NF90_INQUIRE_VARIABLE (ncid2,JF, name,xtype=VARTYPE2)
      CALL CHECK_ERROR(IRET,__LINE__)
      IF(VARTYPE2.EQ.NF90_CHAR) CYCLE
      iret=NF90_GET_ATT (ncid2,JF,'_FillValue', MFILL2)
      if (iret.NE.0) THEN
        if(VARTYPE1.EQ.NF90_SHORT) MFILL2=NF90_FILL_SHORT
        if(VARTYPE1.EQ.NF90_INT) MFILL2=NF90_FILL_INT
        if(VARTYPE1.EQ.NF90_FLOAT) MFILL2=NF90_FILL_FLOAT
        if(VARTYPE1.EQ.NF90_DOUBLE) MFILL2=NF90_FILL_DOUBLE
      END IF
      iret=NF90_GET_ATT (ncid2,JF,'scale_factor', SCALE2)
      if (iret.NE.0) SCALE2=1.0
      iret=NF90_GET_VAR (ncid2,JF,ARRAY2,start2,count2)
      CALL CHECK_ERROR(IRET,__LINE__)

      ! test if variable filled are the same
      IF (name .NE. nametmp) THEN
        WRITE(*,*) "FILES ARE NOT IDENTICALS IN THE VARIABLE ORDER"
        STOP
      END IF

      DO I=1,NFILL
!
! Test on MFILL2: only fills grid which has not been filled before!
!
        IF ((I2FILL(I,1).NE.0) .AND. (I2FILL(I,2).NE.0)) THEN
          IF (ARRAY2(I2FILL(I,1),I2FILL(I,2)).EQ.MFILL2) THEN
!
! Bilinear interpolation if no land point nearby   I1FILL(1,*,*,*)
!         
            IF (sum((/index(linear(:),trim(name))/)).NE.0)  THEN
!write(*,*) "linear"
              IF ( ( ARRAY1(I1FILL(1,I,1,1),I1FILL(1,I,2,1)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(1,I,1,2),I1FILL(1,I,2,2)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(1,I,1,3),I1FILL(1,I,2,3)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(1,I,1,4),I1FILL(1,I,2,4)).EQ. MFILL1 ) ) THEN
            
                ARRAY2(I2FILL(I,1),I2FILL(I,2)) =   MFILL2
             
              ELSE         
                ARRAY2(I2FILL(I,1),I2FILL(I,2)) =                 & 
                RD(I,1)*ARRAY1(I1FILL(1,I,1,1),I1FILL(1,I,2,1)) +  &
                RD(I,2)*ARRAY1(I1FILL(1,I,1,2),I1FILL(1,I,2,2)) +  &
                RD(I,3)*ARRAY1(I1FILL(1,I,1,3),I1FILL(1,I,2,3)) +  &
                RD(I,4)*ARRAY1(I1FILL(1,I,1,4),I1FILL(1,I,2,4))
              END IF

!
! Nearest point   I1FILL(2,*,*,*)
!         
            ELSE IF (sum((/index(near(:),trim(name))/)).NE.0)  THEN
!write(*,*) "near"
              IF ( ( ARRAY1(I1FILL(2,I,1,1),I1FILL(2,I,2,1)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(2,I,1,2),I1FILL(2,I,2,2)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(2,I,1,3),I1FILL(2,I,2,3)).EQ. MFILL1 ) .OR. &
                 ( ARRAY1(I1FILL(2,I,1,4),I1FILL(2,I,2,4)).EQ. MFILL1 ) ) THEN

                ARRAY2(I2FILL(I,1),I2FILL(I,2)) =   MFILL2
             
              ELSE   
                ARRAY2(I2FILL(I,1),I2FILL(I,2))= ARRAY1(I1FILL(2,I,1,1),I1FILL(2,I,2,1))
              END IF

            END IF  
          END IF                 
        END IF ! END OF TEST ON MFILL2
      END DO ! END OF LOOP ON POINTS TO BE FILLED
      iret=NF90_PUT_VAR(ncid2,JF,ARRAY2,start2,count2)
      CALL CHECK_ERROR(IRET,__LINE__)

    END DO    ! END OF LOOP OVER FIELDS
  END DO      ! END OF LOOP OVER TIME STEPS
  !		  
  iret=NF90_CLOSE(ncid1)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=NF90_CLOSE(ncid2)
  CALL CHECK_ERROR(IRET,__LINE__)
  !
  !
  !
  END PROGRAM

  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------

      SUBROUTINE CHECK_ERROR(IRET, ILINE)

      USE NETCDF

      IMPLICIT NONE

      INTEGER IRET, ILINE, NDSE


      NDSE=6

      IF (IRET .NE. NF90_NOERR) THEN
        WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN INTERP :'
        WRITE(NDSE,*) ' LINE NUMBER ', ILINE
        WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
        WRITE(NDSE,*) NF90_STRERROR(IRET)
        STOP
        !CALL EXTCDE ( 59 )
      END IF
      RETURN

      END SUBROUTINE CHECK_ERROR

  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------

   function lnblnk(s)
   character*(*) s
   integer lnblnk

   lnblnk = index(s, ' ') - 1
   if (lnblnk.lt.0) lnblnk = 0

   return
   end

